<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}
                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}

    {#     <!--[if lt IE 9]> #}
    {#     {{ assets.outputJs('header-ie9') }} #}
    {# <![endif]--> #}
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">
<div class="js-svg-spinner hide">
<div class="sp-block-group text-center">
    {% include 'partials/newsfeed/spinner.volt' %}
</div>
</div>
{% include 'partials/newsfeed/header.volt' %}


<div class="sp-block-group community-content-container bg-f5f5f6 js-community-content-container">
    <div class="sp-block four bg-f5f5f6 community-sidebar-container js-community-sidebar-container">
        <div class="sp-block seven-xxl five-offset-xxl color-777777 h5">
            {% include 'partials/newsfeed/sidebar.volt' %}
        </div>
    </div>
    <div class="sp-block eight four-offset bg-white height-all item-container">
        {% if type === 'argumentsbox' %}
            <div class="responsive-iframe speakol-arguments-box margin-top-sm" data-lang="{{ lang }}" data-newsfeed="1" data-app="{{ appId }}" data-width="650" data-href="{{ argumentsboxUrl }}"  {{ uuid ? 'data-uuid="' ~ uuid ~ '"' : '' }}>

            </div>
        {% elseif type === 'comparison' %}
        <div class="responsive-iframe speakol-comparison margin-top-lg margin-left-5" data-app="{{ appId }}" data-newsfeed="1" data-lang="{{ lang }}" data-width="600" data-href="{{ config.application.webhost }}/comparisons/render?app=2&comparison={{ slug }}">
        </div>
        {% elseif type === 'debate' %}
            <div class="responsive-iframe speakol-debate margin-top-lg margin-left-5" data-app="{{ appId }}" data-newsfeed="1"  data-lang="{{ lang }}" data-width="600" data-href="{{ config.application.webhost }}/debates/render?app=2&debate={{ slug }}">
            </div>
        {% endif %}
        <script>
            (function(doc, sc, id) {
                var js, sjs = document.getElementsByTagName(sc)[0];
                if(doc.getElementById(id)) return;
                js = document.createElement(sc);
                js.src = "{{ config.application.webhost }}js/sdk.js";js.id=id;
                sjs.parentNode.insertBefore(js, sjs);
            })(document, 'script', 'speakol-sdk');
        </script>
    </div>
</div>

{{ assets.outputJs() }}


<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-56411923-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
// voting circle canvas
    if (($(".naqeshny-canvas")).length) {

        $(".naqeshny-canvas").each(function (index, obj) {
            $(obj).debateVersus();
        });
    }
</script>
</body>
</html>
