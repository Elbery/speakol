/*
---
MooTools: the javascript framework

web build:
 - http://mootools.net/core/76bf47062d6c1983d66ce47ad66aa0e0

packager build:
 - packager build Core/Core Core/Array Core/String Core/Number Core/Function Core/Object Core/Event Core/Browser Core/Class Core/Class.Extras Core/Slick.Parser Core/Slick.Finder Core/Element Core/Element.Style Core/Element.Event Core/Element.Delegation Core/Element.Dimensions Core/Fx Core/Fx.CSS Core/Fx.Tween Core/Fx.Morph Core/Fx.Transitions Core/Request Core/Request.HTML Core/Request.JSON Core/Cookie Core/JSON Core/DOMReady Core/Swiff

copyrights:
  - [MooTools](http://mootools.net)

licenses:
  - [MIT License](http://mootools.net/license.txt)
...
*/
                                                                                                                                                                                                                        

/*
---
description: A class that provides cross-domain communication between an iframe element and its parent window.

license: MIT-style

authors:
- Lorenzo Stanco

requires:
- core/1.2.5:Class
- core/1.2.5:Class.Extras
- core/1.2.5:JSON

provides: 
- CrossDomainFragment

credits: http://www.ibm.com/developerworks/web/library/wa-crossdomaincomm/

...
*/


