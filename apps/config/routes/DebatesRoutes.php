<?php
namespace Speakol\Routes;
class DebatesRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'debates',));
        $this->setPrefix('/debates');
        $this->add('/code', array('action' => 'code'))->setName("debate-code");
        $this->add('/render', array('action' => 'render'))->setName("render-debates-box");
    }
}
