var loginbox;

(function ($) {

    $.fn.loginbox = function (options) {

        var _this = this;

        var s = $.extend({
            formId: '.login-box-form',
            afterSuccess: function () {
                return true;
            }
        }, options);

        _this.init = function () {
            _this.appendEvents();
        };

        _this.appendEvents = function () {
            $('body').on('click', '.ajaxfiy-me', _this.giveMeFlag);
            $('body').on('submit', s.formId, _this.ajaxfiy);
        };

        _this.giveMeFlag = function (e) {

            $('.ajaxfiy-me').removeAttr('data-flg-me');
            $(this).attr('data-flg-me', '1');
            outside.me = this;
        };

        _this.ajaxfiy = function (e) {

            e.preventDefault();

            var element = this;

            $('.error-box').removeClass('alert-danger');
            $('.error-box').removeClass('alert-success');
            $('.error-box').addClass('hide');

            var url = $(element).attr('action'),
                    params = {
                        email: $(element).find('#email').val(),
                        password: $(element).find('#password').val(),
                    },
                    btnText = $(element).find('input[type=submit]').val()
                    ;
            $(element).find('input[type=submit]').val('Loading ..');
            $(element).find('input[type=submit]').attr('disabled', 'disabled');
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                success: function (data) {

                    $(element).find('input[type=submit]').val(btnText);
                    $(element).find('input[type=submit]').removeAttr('disabled');
                    data = JSON.parse(data);

                    if (data.status == 'OK') {

                        s.afterSuccess($('.ajaxfiy-me[data-flg-me=1]'));

                        $(element).parents('.login_container').find('.error-box').html(data.data);
                        $(element).parents('.login_container').find('.error-box').addClass('alert-success');
                        $(element).parents('.login_container').find('.error-box').removeClass('hide');

                    } else {
                        $(element).parents('.login_container').find('.error-box').html(data.messages);
                        $(element).parents('.login_container').find('.error-box').addClass('alert-danger');
                        $(element).parents('.login_container').find('.error-box').removeClass('hide');
                    }
                }
            });
        };

        _this.init();

        var outside = {
            me: _this,
            setParams: function (key, value) {
                s[key] = value;
            },
            getParams: function (key) {
                return s && s[key] ? s[key] : false;
            },
            close: function () {
                $('.modal-header .close').click();
            },
            call: function (methodName, params) {
                s[methodName](params);
            }
        };

        return outside;

    };

    $(document).ready(function () {

        if ($('.ajaxfiy-me').length) {

            loginbox = $('.ajaxfiy-me').loginbox({
                afterSuccess: function (me) {

                    var type = $(me).data('type');
                    type = type ? type : 'click';
                    switch (type) {
                        case 'submit':
                            $(me).removeClass('ajaxfiy-me');
                            $(me).removeAttr('data-target');

                            var frm = $(me).data('form-id');
                            loginbox.close();
                            $(frm).submit();
                            break;
                        case 'click':
                            $(me).removeClass('ajaxfiy-me');
                            $(me).removeAttr('data-target');
                            loginbox.close();
                            $(me).click();
                            window.location.reload();
                            break;
                        default :
                            window.location.reload();
                            break;
                    }
                }
            });
        }

    });

}(jQuery));