<div class="row ads-container">
{% if ads is defined %}
    {% for ad in ads %}
                <div class="col-xs-6 no_padding pos_statc ads-container">
                    <ul class="{{ loop.first ? 'left_pros_comments' : 'right_pros_comments'  }} clearfix appndbl_comnts_cont">
                        <li>
                            {% include "partials/argumentsbox/ad" with ['ad' : ad]%}
                        </li>
                    </ul>
                </div>
    {% endfor %}
{% endif %}
</div>
