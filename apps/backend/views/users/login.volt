<div id="wrapper">

    <!-- Sidebar -->

    <!-- Page content -->

        <!-- Keep all page content within the page-content inset div! -->
        <div class="row bg-green white login-header margin-bottom">
            <div class="col-xs-10 {{ lang === 'ar' ? "rtl" : ""  }}">
                <h3>{{ t._('sign-in-to-speakol')  }}</h3>
            </div>
            <div class="col-xs-2">
                <button class="no-border bg-transparent white login-close js-login-close inline-block width-all"><i class="fa fa-close fa-2x"></i></button>
            </div>
        </div>
<div class="row negative-margin-top">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
        <div class="row margin-bottom {{ lang === 'ar' ? "rtl" : ""  }}">
            {{ form('users/login', 'class': 'login-form') }}
                <div class="form_row">
                  {{ textField( {
                        "email", 
                        'placeholder': t._('email-addr'), 
                        'class': 'form-control input-lg width-all', 
                        "data-validation-engine" :"validate[required, custom[email]]",
                        "data-errormessage-value-missing": t._('email-required'),
                        "data-errormessage-custom-error": t._('email-not-valid'),
                        "data-prompt-position": "topRight:-130"
                    } ) }}
                    <div class="remember-me">
                        <input type="checkbox" id="rememberMe"/>
                        <label for="rememberMe"> {{ t._('remember-me') }}</label>
                    </div>
                </div>
                <div class="form_row">
                 {{ passwordField( {
                    "password",
                    'size': 45, 
                    'id': 'password',
                    'placeholder': t._('passwd'), 
                    'class': 'form-control input-lg width-all', 
                    "autocomplete": "off", 
                    "data-validation-engine" :"validate[required]",
                    "data-errormessage-value-missing": t._('password-required'),
                    "data-prompt-position": "topRight:-130"
                    }) }}

                    <div class="forgot">
                        <a href="{{ url('users/forgetpassword') }}"> {{ t._('forgot-passwd') }} </a>
                    </div>
                </div>
                <div class="form_row">
                    <input type="submit" value="{{ t._('sign-in')  }}" class="btn-lg {{  lang === "ar" ? "pull-left" : "pull-right"  }} bg-dark-blue">
                </div>
            {{ endForm() }}
        </div>
        <div class="row margin-bottom">
            <p class="text-center h3 line-behind">
                {{ t._('OR')  }}
            </p>
        </div>
        <div class="row text-center margin-bottom">
            <a href="javascript:" data-href="/users/connect/facebook" data-name="Facebook window" class="btn big-button bg-facebook-blue js-social-button">
                <i class="fa fa-facebook fa-lg pull-left"></i>{{ t._('connect-to-facebook')  }}            </a>
        </div>
        <div class="row text-center margin-bottom">
            <a href="javascript:" data-href="/users/connect/google" data-name="Google Window" class="btn big-button bg-google-blue js-social-button">
                <i class="fa fa-google-plus fa-lg pull-left"></i>{{ t._('connect-to-google')  }}
            </a>
        </div>
        <div class="row margin-bottom text-center">
            <h4>{{ t._('have-account')  }}</h4>
        </div>
        <div class="row text-center">
            <a class="btn big-button text-center" href="/users/register{{ (request.get('redirect_url')) ? "?redirect_url=" ~ request.get('redirect_url') : ""  }}">{{ t._('create-new-account') }}</a>
        </div>
</div>
