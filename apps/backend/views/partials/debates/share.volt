<div class="shr_dsc">{{ t._('share-discussion') }}
    <div class="social_shares">
        <a class="social-change facebook_icon" href="https://www.facebook.com/sharer/sharer.php?u=[url]" title="{{ t._('share-on-facebook') }}" target="_blank">{{ t._('share-on-facebook') }}</a>
        <a class="social-change twitter_icon" href="https://twitter.com/home?status=[url]" title="{{ t._('tweet') }}" target="_blank">{{ t._('tweet') }}</a>
        <a class="social-change gPlus_icon" href="https://plus.google.com/share?url=[url]" title="{{ t._('share-on-google-plus') }}" target="_blank">{{ t._('share-on-google-plus') }}</a>
        <a class="social-change linkedIn_icon" href="https://www.linkedin.com/shareArticle?mini=true&url=[url]" title="{{ t._('share-on-linkedin') }}" target="_blank">{{ t._('share-on-linkedin') }}</a>
    </div>
</div>
