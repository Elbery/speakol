{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main padding-left-22  {{ reachedQuota ? 'relative' : ''}}">
        {% if reachedQuota %}
                <input type="hidden" name="" id="reachedQuota">
                <div class="row overlay bg-f6f6f6"></div>
                <div class="row">
                    <div class="col-sm-11  margin-top-sm padding-top-xs padding-bottom-xs padding-left-10 reached-quota bg-f9c6c6 color-9b3535">
                        <div class="pull-left">
                            <i class="speakol-icon speakol-icon-md speakol-times-circle"></i>
                        </div>
                        <div class="media-body font-15 margin-top-xs">
                            <p class="padding-left-10 no-margin">{{ t._('reached-quota') }} <a class="color-9b3535 decoration-underline weight-bold" href="/pricing">{{ t._('upgrade-limit') }}</a></p>
                        </div>
                    </div>
                </div>
        {% endif %}
        {% include "partials/main/page-title" with [ 'title': debate ? t._('edit-debate') : t._('crt_debt') ]  %}
        <div class="row">
            {{ form(debate ? 'debates/edit?slug=' ~ debate_slug : 'debates/code', 'id': 'debate_form', 'class': 'form-horizontal col-sm-11','enctype': 'multipart/form-data') }}
                <div class="form-group">
                    {{ textField( {"debate_title", 'placeholder': t._('debt-qst'), "class": "arial-font form-control font-22 weight-bold input-lg no-radius no-padding", "data-validation-engine" :"validate[required]", "value" : debate ? debate.title : ''} )}}
                        <a href="#" class="info-tooltip absolute top-0 right-0 bg-yellow black circle text-center" data-toggle="tooltip" title="{{ t._('exmp-debt') }}" data-animation="true" data-placement="top" data-trigger="hover" >?</a>
                </div>
                <div class="form-group debate-img-form-group">
                {% set side1 = debate.sides[0] %}
                {% set side2 = debate.sides[1] %}
                {% set meta1 = side1.meta[0] %}
                {% set meta2 = side2.meta[0] %}
                <div class="form-group relative">
                    <div class="debate-user-img debate-user-img-left bg-white circle col-xs-1 text-center">
                        <div class="fileUpload btn-default btn-xs bg-white absolute debate-upload-btn-left width-150 font-11">
                            <span class="width-all"><i class="fa fa-upload"></i>&nbsp;&nbsp;{{ debate ?  t._('change') : t._('upload') }}</span>
                            {{ fileField( {"debater_1_pic", "class": "upload", 'id': 'uploadBtnA', "data-validation-engine" : debate ? "" : "validate[required]"} )}}
                        </div>
                        <span class="uploaded-img blue" id="debaterA-img">
                            {% if debate %}
                                <img class="img-circle" src="{{ meta1.image }}">
                            {% else %}
                                A
                            {% endif %}
                        </span>
                    </div>
                    <div class="h5  col-xs-5 debate-bar debate-bar-left bg-blue text-right white"><span>(50%)  </span>{{ debate ? meta1.title : t._('debater-a') }}</div>
                    <div class="h5  col-xs-5 debate-bar debate-bar-right bg-red text-left white">{{ debate ? meta2.title : t._('debater-b') }}<span>  (50%)</span></div>
                    <div class="debate-user-img debate-user-img-right bg-white circle col-xs-1 text-center">
                        <div class="fileUpload btn-default btn-xs bg-white absolute debate-upload-btn-right width-150 font-11">
                            <span class="width-all">{{ debate ? t._('change') : t._('upload') }}&nbsp;&nbsp;<i class="fa fa-upload"></i></span>
                            {{ fileField( {"debater_2_pic", "class": "upload", 'id': 'uploadBtnB', "data-validation-engine" : debate ? "" : "validate[required]"} )}}
                        </div>
                        <span class="uploaded-img red" id="debaterB-img">
                            {% if debate %}
                                <img class="img-circle" src="{{ meta2.image }}">
                            {% else %}
                                B
                            {% endif %}
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4 col-lg-3 margin-bottom-sm margin-top-xs">
                                {{ textField( {"side_1_title", 'placeholder': t._('debater-a'), "class": "form-control no-radius h4 debate-title no-padding" ~ (lang === 'ar' ? 'text-right' : ''), "data-validation-engine" :"validate[required]", "value" : debate ? meta1.title : ''} )}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 margin-bottom-sm">
                                {{ textArea( {"side_1_job_title", 'placeholder': t._('debater-a-biography'), "class": "form-control no-radius no-resize no-padding font-15 padding-top-xs no-indent debate-textarea-sm " ~ (lang ==='ar' ? 'rtl' : ''), "value" : debate ? meta1.job_title : ''} )}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 margin-bottom-sm">
                                {{ textArea({"debater_1_opinion", "placeholder": t._('debtr-a-opnion'), "class": "form-control no-radius no-resize no-padding font-15 padding-top-xs no-indent debate-textarea " ~ (lang ==='ar' ? 'rtl' : '') , "data-validation-engine" :"validate[required]", "value": debate ? meta1.opinion : ''}) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8 col-lg-3 col-lg-offset-9 margin-top-xs margin-bottom-sm">
                                {{ textField( {"side_2_title", 'placeholder': t._('debater-b'), "class": "form-control no-radius h4 debate-title   no-padding no-indent " ~ (lang === 'ar' ? 'text-right' : ''), "data-validation-engine" :"validate[required]", "value" : debate ? meta2.title : ''} )}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 margin-bottom-sm">
                                {{ textArea( {"side_2_job_title", 'placeholder': t._('debater-b-biography'), "class": "form-control no-radius no-resize no-padding font-15 padding-top-xs no-indent debate-textarea-sm " ~ (lang === 'ar' ? 'rtl' : ''), "value" : debate ? meta2.job_title : ''} )}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 margin-bottom-sm">
                                {{ textArea({"debater_2_opinion", "placeholder": t._('debtr-b-opnion'), "class": "form-control no-radius no-resize no-paddingfont-15 padding-top-xs no-indent debate-textarea " ~ (lang === 'ar' ? 'rtl' :''), "data-validation-engine" :"validate[required]", "value" : debate ? meta2.opinion : ''}) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-3 {{ lang === 'ar'? 'pull-right text-left' : 'text-right'}}">
                        <label class="{{ (session.get('lang') === 'ar') ? "rtl" : "" }} margin-top-xs" for="">{{ t._('debt_last') }}:</label>
                    </div>
                    <div class="col-lg-3 {{ lang === 'ar'? 'pull-right' : ''}}">
                        <select id="endDate" class="form-control no-radius no-padding-height {{ lang === 'ar' ? 'rtl' : ''  }}">
                            <option value="7"{{ debate and debate.max_days == 7 ?  'selected' : '' }}>7 {{ t._('days') }}</option>
                            <option value="14" {{ debate and debate.max_days == 14 ?  'selected' : '' }}>14 {{ t._('days') }}</option>
                            <option value="21" {{ debate and debate.max_days == 21 ?  'selected' : '' }}>21 {{ t._('days') }}</option>
                            <option value="other"{{ debate and debate.max_days > 21 ?  'selected' : '' }}>{{ t._('Other')  }}</option>
                        </select>
                    </div>
                    <div class="col-lg-3 {{ debate and debate.max_days > 21 ? '' : 'hide' }} {{ lang === 'ar'? 'pull-right' : ''}}" id="otherDate">
+                        {% if debate and debate.max_days > 21 %}
+                            {% set otherDate = utility.getDebateDuration(debate.max_days) %}
+                        {% endif %}
                        <div class="col-lg-3 {{ lang === 'ar' ? 'pull-right' : ''}}">
                            <input id="" type="text" name="" class="form-control no-radius" value="{{ otherDate ? otherDate['number'] : '' }}">
                        </div>
                        <div class="col-lg-9 {{ lang === 'ar'? 'pull-right' : ''}}">
                            <select class="form-control no-radius no-padding-height {{ lang === 'ar' ? 'rtl' : ''  }}">
                                <option value="days" {{ otherDate and otherDate['format'] === 'days' ? 'selected' : '' }}>{{ t._('days') }}</option>
                                <option value="months" {{ otherDate and otherDate['format'] === 'months' ? 'selected' : '' }}>{{ t._('months') }}</option>
                                <option value="years" {{ otherDate and otherDate['format'] === 'years' ? 'selected' : '' }}>{{ t._('years') }}</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="max_days" id="max_days" value="{{ debate ? debate.max_days : 7 }}" data-validation-engine="validate[required, custom[integer], min[1], max[5475]]" data-errormessage-range-overflow="Maximum is 15 Years" >
                </div>
                <div class="form-group">
                    <div class="col-sm-3 {{ lang === 'ar'? 'pull-right text-left' : 'text-right'}}">
                        <label for="debtCategory" class="{{ (session.get('lang') === 'ar') ? "rtl" : "" }} margin-top-xs">{{ t._('dbat-category') }}:</label>
                    </div>
                    <div class="col-sm-3 {{ lang === 'ar'? 'pull-right' : ''}}">
                        {{ select("category", categories, 'using': ['id', 'name'], "useEmpty" : true, "id": "debtCategory", 'class': 'form-control no-radius no-padding-height ' ~ (lang === 'ar' ? 'rtl' : ''), "data-validation-engine" :"validate[required]") }}
                    </div>
                </div>
                <div class="form-group">
                        {{ submitButton( { debate ? t._('update') : t._("get-code"), 'class': 'btn btn-primary col-sm-3 no-radius bg-light-blue ' ~ (lang === 'ar' ? 'col-sm-offset-6' : 'col-sm-offset-3')} )}}
                </div>
                {{ endform() }}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
{% if not debate %}
<div class="modal fade" id="get_code" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="raw_code_container">
                                            <div><span>{{ t._('choz-cntnt-lang') }}:</span> 
                                                <select id="arg-box-lang" name="arg-box-lang">
                                                    <option value="en">English</option>
                                                    <option value="ar">اللغة العربية</option>
                                                </select>
                                            </div>
                                            <div class="speakol-debate-box">
<pre id="gnrtd_code"><code>&lt;div class="speakol-debate" data-lang="en" data-width="600" data-href="{{ debate_slug }}"  &gt;&lt;/div&gt; &lt;script type="text/javascript" &gt; (function(doc, sc, id) { var js, sjs = document.getElementsByTagName(sc)[0]; if(doc.getElementById(id)) return; js = document.createElement(sc); js.src = "{{config.application.sdk_url}}js/sdk.js"; js.id=id; sjs.parentNode.insertBefore(js, sjs); })(document, 'script', 'speakol-sdk'); &lt;/script&gt;</code></pre>
                                            </div><p>{{ t._('argbox-code') }}</p></div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="form_row">
                                            <!--<span class="alert-success"> Copied </span>-->
                                            <a href="javascript:" class="btn bg-grey preview-btn">{{ t._('preview') }}</a>
                                            <a href="javascript:" class="btn blue_btn cp-generated-code">{{ t._('copy-code') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{% endif %}
{% if not debate %}
{% if successfullyCreated %}
    <input type="hidden" id="d_code_btn" value="Get Code" data-target="#get_code" data-toggle="modal">
    <script type="text/javascript" src="{{ this.config.application.static_js ~ '/js/vendor/jquery-1.10.1.min.js?' ~ this.config.application.cachestring }}"  ></script>
    <script>
        $(document).ready(function (e) {
            $('#d_code_btn').click();
        });
    </script>
{% endif %}
{% endif %}
