(function($) {
    $('#pricingForm').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this),
            $pricingModal = $('#pricingModal'),
            $loadingSpinner = $pricingModal.find('.js-loading-spinner'),
            $successMessage = $pricingModal.find('.js-success-message'),
            $amount = $('.js-amount'),
            $alerts = $('.js-alerts'),
            data = $this.serialize();
        
        $pricingModal.modal('show');
        $alerts.html('');
        $.ajax({
            url: '/pricing/addCard',
            type: 'POST',
            data: data,
            success: function(res) {
                if(res.status === 'OK' && res.modal === 1) {
                    $loadingSpinner.addClass('hide');
                    $successMessage.removeClass('hide');
                    $amount.html('$' + res.amount);
                    return;
                }
                if(res.status === 'OK') {
                    $pricingModal.modal('hide');
                    var html = renderSuccessTemplate(res.message);
                    $alerts.append(html);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $pricingModal.modal('hide');
                console.log('cool');
                var res = JSON.parse(jqXHR.responseText); 
                var errorMsg = renderErrorsTemplate(res.message);
                $alerts.append(errorMsg);
            },
            dataType: 'json'
        });
    });


    function renderErrorsTemplate(message) {
        var html = '';
        html += '<div class="row margin-top-sm padding-top-xs padding-bottom-xs padding-left-10 bg-f9c6c6 color-9b3535">';
        html += '<div class="pull-left">';
        html += '<i class="speakol-icon speakol-icon-md speakol-times-circle color-dc5959"></i>';
        html += '</div>';
        html += '<div class="media-body font-15 margin-top-xs">';
        html += '<p class="padding-left-10 no-margin">' +  message + '</p>';
        html += '</div>';
        html += '</div>';
        return html;
    }

    function renderSuccessTemplate(message) {
        var html = '';
        html += '<div class="row margin-top-sm padding-top-xs padding-bottom-xs padding-left-10 bg-dff0d8 color-468847">';
        html += '<div class="pull-left">';
        html += '<i class="fa fa-check-circle margin-top-xs fa-2x"></i>';
        html += '</div>';
        html += '<div class="media-body font-15 margin-top-xs">';
        html += '<p class="padding-left-10 no-margin">' +  message + '</p>';
        html += '</div>';
        html += '</div>';
        return html;
    }
})(jQuery);
