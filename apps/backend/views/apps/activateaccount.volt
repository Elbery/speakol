<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">

        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container login">
                    <div class="top_bar">
                        <span>{{ t._('frst-time-spkl') }} ?</span>
                        {{ linkTo({'apps/create', t._('create-accnt-now'), "class": "btn blue_btn"}) }}
                    </div>
                    <div class="login_container">
                        <h2>Enter Your Password:</h2>
                        {{ form() }}
                        <div class="form_cont">

                            <div class="login_form">
                                <div class="input-group">
                                    <span class="input-group-addon password_ico"></span>
                                    {{ passwordField( {
                                        "password", 
                                        'placeholder': t._('passwd'), 
                                        'class': 'form-control', 
                                        "autocomplete": "off", 
                                        "data-validation-engine" :"validate[required]",
                                        "data-errormessage-value-missing": t._('password-required')
                                    } ) }}
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon password_ico"></span>
                                    {{ passwordField( {
                                        "confirm_password", 
                                        'placeholder': t._('confirm-passwd'), 
                                        'class': 'form-control', 
                                        "autocomplete": "off", 
                                        "data-validation-engine" :"validate[equals[password]]",
                                        "data-errormessage-pattern-mismatch": t._('password-not-match')
                                    } ) }}
                                </div>
                                <div class="form_row">
                                    {{ submitButton('Enter Password') }}
                                </div>
                            </div>
                        </div>
                        {{ endForm() }}

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
