<div class="row">
    <ul class="nav nav-pills js-yourcontent" role="tablist">
        <li class="active"><a class="showAll paginate-me" href="#" data-type="all">{{ t._('all')  }}</a></li>
        <li role="presentation"><a class="paginate-me font-15" aria-controls="debates" role="tab" data-toggle="pill" data-type="debate" href="#debates">{{ t._('debates')  }}</a></li>
        <li role="presentation"><a class="paginate-me font-15" aria-controls="comparisons" role="tab" data-toggle="pill" data-type="comparison" href="#comparisons">{{ t._('comparisons')  }}</a></li>
        <li role="presentation"><a class="paginate-me font-15" aria-controls="argumentbox" role="tab" data-toggle="pill" data-type="argumentbox" href="#argumentbox">{{ t._('argumentboxes')  }}</a></li>
    </ul>
</div>
<div class="row">
    <div class="tab-content js-types">
        {% include "partials/moderates/yourcontent_tabs.volt" %}
    </div>
</div>
<div class="row">
    {% if hasMore['all'] %}
        <a  
            data-all="{{ hasMore['all'] }}" 
            data-type="all" 
            data-debate="{{ hasMore['debate'] }}" 
            data-comparison="{{ hasMore['comparison'] }}" 
            data-argumentbox="{{ hasMore['argumentsbox'] }}" 
            data-url="/moderates/index" 
            data-ajax-url="/moderates/index?type=all" 
            class="pag-types more_cont no-radius light-blue padding-top-lg padding-bottom-lg col-sm-6 col-sm-offset-3 bg-e8e8e8 weight-bold h5" href="#" data-page="1">{{ t._('shw-nxt') }}</a>
    {% endif %}
</div>
