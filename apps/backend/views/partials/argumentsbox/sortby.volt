<div class="sortby-dropdown btn-group">
<span class="hide-xs">{{ t._('sort-comments') }}</span>
  <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <span class="sort-order" data-value="most_voted">{{ t._('most-voted')  }}</span> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="javascript:" data-value="most_voted">{{ t._('most-voted') }}</a></li>
    <li><a href="javascript:" data-value="desc">{{ t._('desc') }}</a></li>
    <li><a href="javascript:" data-value="asc">{{ t._('asc') }}</a></li>
  </ul>
</div>
