<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Confirmation;
use \Phalcon\Validation\Validator\InclusionIn;
class StaticValidation extends SpeakolValidation {
    public function __construct($rules = false, $exclude = false) {
        $this->loadCustomTrans("static");
        $this->validations = array("contact_name" => array(new PresenceOf(array('message' => $this->t->_('name-missing')))), "contact_email" => array(new Email(array('message' => $this->t->_('email-not-valid'))), new PresenceOf(array('message' => $this->t->_('email-missing'))),), "contact_subject" => array(new PresenceOf(array('message' => $this->t->_('message-missing')))), "contact_message" => array(new PresenceOf(array('message' => $this->t->_('subject-missing')))),);
        parent::__construct($rules, $exclude);
    }
    public function initialize() {
        parent::initialize();
    }
}
