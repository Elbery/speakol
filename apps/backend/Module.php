<?php
namespace Speakol\Backend;
include 'Utility.php';
use Phalcon\Loader, Phalcon\Mvc\Dispatcher, Phalcon\Mvc\View, Phalcon\Mvc\ModuleDefinitionInterface, Speakol\Library\Utility, Phalcon\Mvc\View\Engine\Volt;
class Module implements ModuleDefinitionInterface {
    public function registerAutoloaders(\Phalcon\DiInterface $di=null) {
        $loader = new Loader();
        $loader->registerClasses(array("Google_Client" => '../library/Google/src/Google/Client.php', 
                                       "Google_Service_Oauth2" => '../apps/library/Google/src/Google/Service/Oauth2.php', 
                                       "Facebook" => '../apps/library/Facebook/sdk/facebook.php', 
                                       "TCPDF" => '../apps/library/tcpdf/tcpdf.php', 
                                       "Readability" => '../apps/library/Readability/Readability.inc.php',))->registerNamespaces(array('Speakol\Backend\Controllers' => '../apps/backend/controllers/', 
                                                                                                                                       'Speakol\Backend\Validations' => '../apps/backend/validations/', 
                                                                                                                                       'Speakol\Library' => '../apps/library/Speakol/', 
                                                                                                                                       'Speakol\Social' => '../apps/library/Speakol/Social/', 
                                                                                                                                       'Speakol\Recaptcha' => '../apps/library/Recaptcha/',));
        $loader->register();
    }
    public function registerServices(\Phalcon\DiInterface $di = null) { 
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Speakol\Backend\Controllers");
            return $dispatcher;
        });
        $di->set('voltService', function ($view, $di) {
            $volt = new Volt($view, $di);
            $volt->setOptions(array("compiledPath" => "", "compiledExtension" => ".compiled", "stat" => true, "compileAlways" => true,));
            $compiler = $volt->getCompiler();
            $compiler->addFunction('sessionName', 'session_name');
            $compiler->addFunction('substr', 'substr');
            $compiler->addFunction('strtotime', 'strtotime');
            $compiler->addFunction('urldecode', 'urldecode');
            return $volt;
        });
        $di->set('view', function () {
            $view = new View();
            $view->setViewsDir('../apps/backend/views/');
            $view->registerEngines(array(".volt" => 'voltService'));
            return $view;
        }); 
        $di->set('flash', function () {
            $flash = new \Phalcon\Flash\Session(array('error' => 'alert alert-danger no-radius no-margin', 'success' => 'alert alert-success no-radius no-margin', 'notice' => 'alert alert-info no-radius no-margin',));
            return $flash;
        });
        $di->setShared('session', function () {
            session_set_cookie_params(1209600, '/', '.speakol.dev');
            $session = new \Phalcon\Session\Adapter\Memcache(array('host' => '127.0.0.1', 'port' => 11211, 'lifetime' => 864000, 'prefix' => '', 'persistent' => true));
            if (!$session->isStarted() && !$session->getId()) {
                
                $session->start(); 
            }
            return $session;
        });
        $di->set('utility', function () {
            return new Utility($this);
        });
        $di->set('cookies', function () {
            $cookies = new \Phalcon\Http\Response\Cookies();
            return $cookies;
        });
        $di->set('facebook', function () {
            $facebook = new \Facebook(array('appId' => '1405353299758190', 'secret' => '3b59d237d528d97fcc287f4e34fcd06e', 'allowSignedRequest' => false));
            return $facebook;
        });
        $di->set('google', function () {
            $google = new \Google_Client();
            $google->setClientId('5420708974-g2geolk9m1m5dkuk3nspe863i9rslnke.apps.googleusercontent.com');
            $google->setClientSecret('zXCQ-OILrno7gvuFlL2Bao5J');
            $google->setApprovalPrompt('auto');
            $google->setScopes(array('https://www.googleapis.com/auth/plus.me', 'email', 'profile',));
            return $google;
        });
    }
}
