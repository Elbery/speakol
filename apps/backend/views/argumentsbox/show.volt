<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}
                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}

        {{ assets.outputJs('header') }}
        <!--[if lt IE 9]>
        {{ assets.outputJs('header-ie9') }}
    <![endif]-->
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">

        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> bsp-block-groupser. Please <a href="http://bsp-block-groupsehappy.com/">upgrade your bsp-block-groupser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
{% if authorized %}
    <input id="option" type="hidden" name="" value="{{ optionID }}">
    <input id="pluginOwner" type="hidden" name="" value="{{ LoggedInUserData.app.id === jsVars['app_id'] }}">
    <input id="paidPlan" type="hidden" name="" value="{{ LoggedInUserData.app.id === jsVars['app_id']  and not freePlan }}">
    {% if argumentsbox.audio %}
        <input type="hidden" id="audioEnabled">
    {% endif %}
{% endif %}
{% if authorized %}
<div class="speakol-container container-fluid no-padding {{ newsfeed ? 'newsfeed' : '' }}">
    <div class="sp-block-group menu-bar padding-top-xs padding-bottom-sm {{ newsfeed ? 'width-92' : 'dropdown-underline width-90' }} margin-center">
        <div class="five-xs sp-block seven">
            {% if isLoggedIn and not newsfeed %}
            <div class="dropdown plugin-dropdown">
                <button class="padding-left-0 btn btn-default dropdown-toggle no-radius" type="button" id="userDropdown" data-toggle="dropdown" aria-expanded="true">
                    {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                        {{ image( LoggedInUserData.profile_picture, 'class': 'radius-5 profile-picture-sm') }}
                    {% else %}
                        {{ image("img/user_default.gif", 'class': 'radius-5 profile-picture') }}
                    {% endif %}
                    <span class="{{ lang === 'ar' ? 'rtl inline-block' : '' }}">
                        <span class="hide-xxs">{{ t._('hello') }}, </span>{{ LoggedInUserData.name }}
                    </span>
                    <i class="fa fa-caret-down color-c1cacf"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius js-user-dropdown" role="menu" aria-labelledby="userDropdown">
                    {% if argumentsbox and argumentsbox.app_id === LoggedInUserData.app.id and not freePlan %}
                        <li role="presentation" id="editPluginLink"><a role="menuitem" tabindex="-1" href="/argumentsbox/edit?url={{ argumentsbox.url | url_encode }}" target="_blank">{{ t._('edit') }}</a></li>
                    {% endif %}
                     <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ logout }}">{{ t._('logout') }}</a></li>
                 </ul>
            </div>
            {% endif %}
            {% if newsfeed %}
                <div class="media">
                    <div class="pull-left">
                        <img src="{{ argumentsbox.app_image }}" class="publisher-image radius-5" alt="...">
                    </div>
                    <div class="media-body">
                        <h3 class="font-15 no-margin margin-bottom-xs">
                        <a class="anchor-reset light-blue" href="/apps/profile/{{ argumentsbox.app_id }}" target="_blank"><span class="weight-bold">{{ argumentsbox.app_name }}</span></a> {{ t._('posted-argumentsbox') }}</h3>
                        <p class="h6 color-777777">{{ utility.formatDate(argumentsbox.created_at, 'F jS, Y \a\t h:i A') }}</p>
                    </div>
                </div>
            {% endif %}
        </div>
        <div class="seven-xs sp-block five">
            {% if not newsfeed %}
            <div class="dropdown plugin-dropdown pull-right padding-top-sm">
                <button class="btn-reset margin-top-xs dropdown-toggle no-radius js-notifications-button" type="button" id="notificationDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-bell sp-icon-lg color-9ca7ad"></i>
                </button>
                {% if isLoggedIn %}
                <ul class="tip notification-plugin-tip border-none dropdown-menu right-0 left-auto no-shadow no-radius color-9ca7ad js-notifications notifications-dropdown bg-f0f0f0" role="menu" aria-labelledby="notificationDropdown">
                </ul>
                {% endif %}
            </div>
            {% if isLoggedIn %}
                <div class="hide js-notification-indicator notification-indicator pull-right radius-5 bg-bb1111 white padding-left-5 padding-right-5 weight-bold">0</div>
            {% endif %}
            {% endif %}
            <div class="dropdown plugin-dropdown pull-right {{  newsfeed ? '' : 'padding-top-sm'}}">
                <button class="btn btn-default dropdown-toggle no-radius" type="button" id="shareDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-icon-lg sp-share"></i>
                </button>
                <ul class="dropdown-menu no-shadow right-0 left-auto no-radius color-9ca7ad" role="menu" aria-labelledby="shareDropdown">
                                <li role="prensentation"><a class="" href="https://www.facebook.com/sharer/sharer.php?u={{ url  }}" title="{{ t._('share-on-facebook') }}" target="_blank"><i class="fa fa-facebook"></i>&nbsp;{{ t._('share-on-facebook') }}</a></li>
                                <li role="prensentation"><a class="" href="https://twitter.com/home?status={{ url }}" title="{{ t._('tweet') }}" target="_blank"><i class="fa fa-twitter"></i>&nbsp;{{ t._('tweet') }}</a></li>
                                <li role="prensentation"><a class="" href="https://plus.google.com/share?url={{ url }}" title="{{ t._('share-on-google-plus') }}" target="_blank"><i class="fa fa-google-plus"></i>&nbsp;{{ t._('share-on-google-plus') }}</a></li>
                                <li role="prensentation"><a class="" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url }}" title="{{ t._('share-on-linkedin') }}" target="_blank"><i class="fa fa-linkedin"></i>&nbsp;{{ t._('share-on-linkedin') }}</a></li>
                </ul>
            </div>            
            <div class="dropdown plugin-dropdown pull-right {{ newsfeed ? '' : 'padding-top-sm' }}">
                <button class="btn btn-default dropdown-toggle no-radius js-current-sort" type="button" id="sortDropdown" data-toggle="dropdown" aria-expanded="true" data-value="most_voted">
                    <i class="sp-icon sp-sort sp-icon-lg color-9ca7ad"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius right-0 left-auto" role="menu" aria-labelledby="sortDropdown">
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="most_voted"><i class="fa fa-2x fa-area-chart"></i>&nbsp;{{ t._('most-voted') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="desc"><i class="fa fa-2x fa-sort-amount-desc"></i>&nbsp;{{ t._('desc') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="asc"><i class="fa fa-2x fa-sort-amount-asc"></i>&nbsp;{{ t._('asc') }}</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
{% if isFreePlan %}
    {% set title = '' %}
    <input type="hidden" id="noTitle" value="true">
{% elseif argumentsbox.id %}
    {% set title = argumentsbox.title | trim%}
    {% if title === '' or request.get('no-title') === 'true' %}
        {% set title = '' %} 
        <input type="hidden" id="noTitle" value="true">
    {% elseif not title %}
        {% set title = t._('what-do-you-think') %} 
    {% endif %}
{% else %}
    {% set title = request.get('no-title') === "true" ? "" : request.get('title')  %}
    {% if request.get('no-title')  %}
        <input type="hidden" id="noTitle" value="true">
    {% endif %}
{% endif  %}
    {%if newsfeed %}
        <div class="sp-block-group text-left width-92 margin-center">
            <h3 class="js-title-text font-20 margin-top-xs color-636363">
                <i class="speakol-icon speakol-argumentsbox-green margin-top-xs newsfeed-margin"></i>&nbsp;{{ title }}
            </h3>
        </div>
    {% else %}
    <div class="sp-block-group text-center margin-height-30">
        <h3 charset="utf-8" class="js-title-text no-margin">{{ title }}</h3>
        {% if edited %}
            <p class="js-edited-indicator cursor-pointer bg-f5f5f5 inline-block no-margin-height margin-center font-13 color-999999 margin-top-sm radius-5 padding-top-xs padding-bottom-xs padding-left-10 padding-right-10">{{ t._('edited') }}</p>
            <div class="js-edited-date bg-f5f5f5 margin-center color-999999 hide four relative radius-5 updated-date padding-top-sm padding-bottom-sm h6">{{ argumentsbox.updated_date }}</div>
        {% endif %}
    </div>
    {% endif %}
    <div class="sp-block-group text-center margin-bottom-lg  width-95 margin-center padding-top-xs">
        <div class="six sp-block {{ lang === 'ar' ? 'six-push' : '' }}">
            {% set side = argumentsbox.sides[0] %}
            {% if LoggedInUserData %}
            <div class="sp-block-group">
                <div class="{{  lang === 'ar' ? 'rtl text-right' : '' }} js-unvote-container sp-block-group width-90 bg-green white radius-5 margin-center margin-bottom-sm text-left {{ side.voted ? '' : 'hide' }}" data-side-id="{{ side.id }}">
                   <button class="js-unvote btn-reset white {{ lang === 'ar' ? 'green-vertical-separator-left' : 'green-vertical-separator-right'}} inline-block no-padding-height padding-bottom-xs margin-top-xs margin-bottom-xs" data-argumentsbox-id="{{ argumentsbox.id }}" data-side-id="{{ side.id }}">
                       <i class="fa fa-times"></i>
                   </button>
                    &nbsp;{{ t._('voted!') }}
                </div>
                <div class="sp-block-group js-argument-form transition-opacity {{ side.voted ? '' : 'hide opacity-0' }}" data-side-id="{{ side.id }}">
                    <div class="sp-block-group margin-bottom-sm">
                        {{ text_area("comment", "placeholder": t._('commentbox-placeholder'), "class":"no-resize width-90 h5 no-indent padding-5 " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "4") }}
                    </div>
                    <div class="sp-block-group text-right width-90 margin-center js-normal-submit">
                        {% if argumentsbox.audio %}
                        <button class="js-record-button {{ lang ==='ar' ? 'rtl' : '' }} no-padding padding-top-xs padding-bottom-xs shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm seven font-13 sp-block">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                <i class="fa fa-microphone fa-stack-1x white"></i>
                            </span>
                            <span class="hide-xxs hide-xs">{{ t._('audio-comment') }}</span>
                        </button>
                        {% else %}
                        <div class="{{ lang ==='ar' ? 'rtl' : '' }} four-xs four-sm seven font-13 sp-block">
                        </div>
                        {% endif %}
                        <div class="js-image-form">
                            <div class="sp-block  one-offset-xs one-offset two-xs three-sm two js-image-upload-container text-center">
                                <span class="fileUpload inline-block btn-reset margin-top-xs">
                                    <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                    {% if LoggedInUserData %}
                                        {% set classes = 'js-image-upload' %}
                                    {% else %}
                                        {% set classes = 'js-image-loggedout' %}
                                    {% endif %}
                                    {{ file_field(
                                        "attached",
                                        "class": classes,
                                        "accept":".png,.jpeg,.jpg,.gif"
                                    ) }}
                                </span>
                            </div>
                            <div class="sp-block two one-offset-xs two-xs three-sm js-image-container hide">
                                <button class="btn-reset js-delete-image delete-image absolute"><i class="fa fa-times"></i></button>
                                <img src="" alt="" class="js-image profile-picture-sm">
                            </div>
                        </div>
                        
                        <div class="sp-block two four-xs four-sm  {{ lang === 'ar' ? 'font-13' : '' }}">
                            {{ submit_button("post", "value": t._('sbmt-arg'), "class":"js-submit-arg btn-reset font-13 btn-default bg-4e4e4e width-all post-button") }}
                        </div>
                    </div>
                    {% if argumentsbox.audio %}
                    <div class="js-record-bar hide record-bar sp-block-group width-90 margin-center h5 padding-bottom-xs padding-top-xs">
                            <div class="sp-block seven twelve-xs timer white margin-top-xs text-left padding-left-5">
                                <i class="fa fa-microphone weight-bold"></i>
                                <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
                                <span class="hide-xxs hide-xs js-recording-text"> - {{ t._('recording') }} </span>
                            </div>
                            <div class="sp-block five twelve-xs record-buttons text-right padding-right-5">
                                <button class="js-send-record  six-xs send-record btn-reset radius-5 padding-top-xs padding-bottom-xs">{{ t._('send') }}</button>
                                <button class="js-cancel-record float-right-xs two-xs cancel-record btn-reset radius-5 padding-top-xs padding-bottom-xs">
                                        &#10005;
                                </button>
                            </div>
                    </div>
                    {% endif %}
                    {% if argumentsbox.id is defined %}
                        {{ hidden_field("argumentbox_id", "value": argumentsbox.id) }}
                        {{ hidden_field("link", "value": argumentsbox.url) }}
                    {% else %}
                        {{ hidden_field("argumentbox_id", "value": '') }}
                        {{ hidden_field("link", "value": url) }}
                    {% endif %}
                        {{ hidden_field("argument_id", "value": argumentsbox.id) }}
                </div>
            </div>
            {% endif %}
            <button class="btn btn-default width-90 vote-button bg-fafafa js-vote {{ side.voted ? 'hide' : '' }}" data-argumentsbox-id="{{ argumentsbox.id }}" data-side-id="{{ side.id }}">
                <span class="{{ lang === 'ar' ? 'text-right' : 'text-left' }} sp-block margin-top-xs margin-bottom-xs color-4dac54 ">
                    {% if lang === 'ar' %}
                    <span class="h4 sp-block nine-xs eight-sm eight one-offset-right-lg margin-top-sm margin-bottom-sm">
                        {{ t._(options[0]) }}
                    </span>
                    {% endif %}
                    <i class="speakol-icon speakol-agree speakol-icon-sm sp-block text-center three-xs sp-inline-xs margin-top-sm"></i>
                    <span class="sp-block four-sm two sp-hide-xs">
                        <span class="fa-1-half-x fa-stack circle border-4dac54 bg-white">
                            <i class="fa fa-circle white fa-stack-2x"></i>
                            <i class="speakol-icon speakol-agree fa-stack-1x"></i>
                        </span>
                    </span>
                    {% if lang !== 'ar' %}
                    <span class="h4 sp-block nine-xs eight-sm eight one-offset-lg margin-top-sm margin-bottom-sm">
                        {{ t._(options[0]) }}
                    </span>
                    {% endif %}
                </span>
            </button>
        </div>
        <div class="six sp-block {{ lang === 'ar' ? 'six-pull' : '' }}">
            {% set side = argumentsbox.sides[1] %}
            {% if LoggedInUserData %}
            <div class="sp-block-group">
                <div class="{{ lang === 'ar' ? 'rtl text-right' : '' }} js-unvote-container sp-block-group width-90 bg-d11b1f white radius-5 margin-center margin-bottom-sm text-left {{ side.voted ? '' : 'hide'}}" data-side-id="{{ side.id }}">
                   <button class="js-unvote btn-reset white {{ lang === 'ar' ? 'red-vertical-separator-left' : 'red-vertical-separator-right'}} inline-block no-padding-height padding-bottom-xs margin-top-xs margin-bottom-xs" data-argumentsbox-id="{{ argumentsbox.id }}" data-side-id="{{ side.id }}">
                       <i class="fa fa-times"></i>
                   </button>
                    &nbsp;{{ t._('voted!') }}
                </div>
                <div class="js-argument-form sp-block-group transition-opacity {{ side.voted ? '' : 'hide opacity-0' }}" data-side-id="{{ side.id }}">
                    <div class="sp-block-group margin-bottom-sm">
                        {{ text_area("comment", "placeholder": t._('commentbox-placeholder'), "class":"no-resize width-90 h5 no-indent padding-5 " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "4") }}
                    </div>
                    <div class="js-normal-submit sp-block-group text-right width-90 margin-center">
                        {% if argumentsbox.audio %}
                        <button class="js-record-button {{ lang ==='ar' ? 'rtl' : '' }} no-padding padding-top-xs padding-bottom-xs shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm seven font-13 sp-block">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                <i class="fa fa-microphone fa-stack-1x white"></i>
                            </span>
                            <span class="hide-xxs hide-xs">{{ t._('audio-comment') }}</span>
                        </button>
                        {% else %}
                        <div class="{{ lang ==='ar' ? 'rtl' : '' }}  four-xs four-sm seven font-13 sp-block">
                        </div>
                        {% endif %}
                        <div class="js-image-form">
                            <div class="sp-block  one-offset-xs one-offset two-xs three-sm two js-image-upload-container text-center">
                                <span class="fileUpload inline-block btn-reset margin-top-xs">
                                    <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                    {% if LoggedInUserData %}
                                        {% set classes = 'js-image-upload' %}
                                    {% else %}
                                        {% set classes = 'js-image-loggedout' %}
                                    {% endif %}
                                    {{ file_field(
                                        "attached",
                                        "class": classes,
                                        "accept":".png,.jpeg,.jpg,.gif"
                                    ) }}
                                </span>
                            </div>
                            <div class="sp-block two one-offset-xs two-xs three-sm js-image-container hide">
                                <button class="btn-reset js-delete-image delete-image absolute"><i class="fa fa-times"></i></button>
                                <img src="" alt="" class="js-image profile-picture-sm">
                            </div>
                        </div>
                        {{ hidden_field("argument_id", "value": argumentsbox.id) }}
                    {% if argumentsbox.id is defined %}
                        {{ hidden_field("argumentbox_id", "value": argumentsbox.id) }}
                        {{ hidden_field("link", "value": argumentsbox.url) }}
                    {% else %}
                        {{ hidden_field("argumentbox_id", "value": '') }}
                        {{ hidden_field("link", "value": url) }}
                    {% endif %}
                        <div class="sp-block two four-xs four-sm  {{ lang === 'ar' ? 'font-13' : '' }}">
                            {{ submit_button("post", "value": t._('sbmt-arg'), "class":"js-submit-arg btn-reset font-13 btn-default bg-4e4e4e width-all post-button") }}
                        </div>
                    </div>
                    {% if argumentsbox.audio %}
                    <div class="js-record-bar hide record-bar sp-block-group width-90 margin-center h5 padding-bottom-xs padding-top-xs">
                            <div class="sp-block seven twelve-xs timer white margin-top-xs text-left padding-left-5">
                                <i class="fa fa-microphone weight-bold"></i>
                                <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
                                <span class="hide-xxs hide-xs js-recording-text"> - {{ t._('recording') }} </span>
                            </div>
                            <div class="sp-block five twelve-xs record-buttons text-right padding-right-5">
                                <button class="js-send-record  six-xs send-record btn-reset radius-5 padding-top-xs padding-bottom-xs">{{ t._('send') }}</button>
                                <button class="js-cancel-record float-right-xs two-xs cancel-record btn-reset radius-5 padding-top-xs padding-bottom-xs">
                                        &#10005;
                                </button>
                            </div>
                    </div>
                    {% endif %}
                </div>
            </div>
            {% endif %}
            <button class="btn btn-default width-90 vote-button bg-fafafa js-vote {{ side.voted ? 'hide' : '' }}" data-argumentsbox-id="{{ argumentsbox.id }}" data-side-id="{{ side.id }}">
                <span class="{{ lang === 'ar' ? 'text-right' : 'text-left' }} sp-block margin-top-xs margin-bottom-xs color-d11b1f">
                    {% if lang === 'ar' %}
                    <span class="h4 sp-block nine-xs eight-sm eight one-offset-right-lg margin-top-sm margin-bottom-sm">
                        {{ t._(options[1]) }}
                    </span>
                    {% endif %}
                    <i class="speakol-icon speakol-disagree speakol-icon-sm sp-block text-center three-xs sp-inline-xs margin-top-sm"></i>
                    <span class="sp-block four-sm two sp-hide-xs">
                        <span class="fa-1-half-x fa-stack circle border-d11b1f bg-white">
                            <i class="fa fa-circle white fa-stack-2x"></i>
                            <i class="speakol-icon speakol-disagree fa-stack-1x"></i>
                        </span>
                    </span>
                    {% if lang !== 'ar' %}
                    <span class="h4 sp-block nine-xs eight-sm eight one-offset-lg margin-top-sm margin-bottom-sm">
                        &nbsp;{{ t._(options[1]) }}
                    </span>
                    {% endif %}
                </span>
            </button>
        </div>
    </div>
    
    <div class="sp-block-group margin-top-lg margin-bottom-lg progress-container relative width-90 margin-center">
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? argumentsbox.votes.sides[1] : argumentsbox.votes.sides[0] %}
                {% set color = (lang === 'ar') ?  'color-d11b1f' : 'color-4dac54' %}
                {% set bg = (lang === 'ar') ?  'bg-d11b1f' : 'bg-4dac54' %}
                <div class="sp-block six text-left color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : ''}}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes ? side.votes : 0 }}</span> {{ t._('votes') }}
                </div>
                <div class="sp-block six text-right js-side-percentage h4 {{ color }} padding-bottom-sm side-percentage-left" data-side-id="{{ side.id }}">{{ side.percentage ? side.percentage : '0%' }}</div>
            </div>
            <div class="progress circular-radius flip vote-progress no-shadow no-border">
                <div class="progress-bar circular-radius {{ bg }} js-progress-bar no-shadow no-border arg-progress-bar-left" style="width: {{ side.percentage ? side.percentage : '0%' }}" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.percentage ? side.percentage : '0%' }}</span>
                </div>
            </div>
        </div>
        <div class="sp-block progress-circle text-center">
            <canvas class="naqeshny-canvas js-voting-circle" data-lang="{{ lang  }}" width="80" height="80">
                <div class="js-data data hide">
                    {% if argumentsbox.votes is defined %}
                    {
                        "pro-votes": "{{ argumentsbox.votes.proVotes }}",
                        "pro-color": "#4dac54",
                        "con-votes": "{{ argumentsbox.votes.conVotes }}",
                        "con-color": "#d11b1f"
                    }
                    {% else %}
                    {
                        "pro-votes": "0",
                        "pro-color": "#4dac54",
                        "con-votes": "0",
                        "con-color": "#d11b1f"
                    }
                    {% endif %}
                </div>
            </canvas>
        </div>
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? argumentsbox.votes.sides[0] : argumentsbox.votes.sides[1] %}
                {% set color = (lang === 'ar') ?  'color-4dac54' : 'color-d11b1f' %}
                {% set bg = (lang === 'ar') ?  'bg-4dac54' : 'bg-d11b1f' %}
                <div class="sp-block six text-left js-side-percentage h4 {{ color }} padding-bottom-sm side-percentage-right" data-side-id="{{ side.id }}">{{ side.percentage ? side.percentage : '0%' }}</div>
                <div class="sp-block six text-right color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes ? side.votes : 0 }}</span> {{ t._('votes') }}
                </div>
            </div>
            <div class="progress circular-radius vote-progress no-shadow no-border">
                <div class="progress-bar circular-radius {{ bg }} no-shadow no-border js-progress-bar arg-progress-bar-right" style="width: {{side.percentage ? side.percentage : '0%'}}" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.percentage ? side.percentage : '0%' }}</span>
                </div>
            </div>
        </div>
    </div>

    {% include "partials/argumentsbox/sides.volt" %}
    {% include "partials/argumentsbox/byspeakol.volt" %}
</div>
{% include "partials/main/errorbox.volt" %}

{% else %}
{% include "partials/argumentsbox/unauthorized.volt" %}
{% endif %}


{{ assets.outputJs('footer') }}

<script>window.jQuery || document.write('<script src="vendor/js/jquery-1.10.1.min.js"><\/script>')</script>

<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-56411923-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
// voting circle canvas
    if (($(".naqeshny-canvas")).length) {

        $(".naqeshny-canvas").each(function (index, obj) {
            $(obj).debateVersus();
        });
    }
</script>
</body>
</html>
