<?php
namespace Speakol\Backend\Controllers;
class IndexController extends BaseController {
    public function indexAction() {
        $this->tag->appendTitle(' Home');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/home.js?' . $this->config->application->cache_string);
        $userSession = $this->getAppData();
        $userSession = !empty($userSession) ? (array)$userSession : false;
        $this->view->setVar('userSession', $userSession);
        $this->view->setVar('lang', $this->getLang());
        $introParagraph = $this->utility->iCurl('/speakol/intro/show', 'GET', $hdrs = null , array('lang' => $this->getLang()));
        if ($introParagraph->status === 'OK') {
            $this->view->introParagraph = $introParagraph->data->dashboard;
        } else {
            $this->view->introParagraph = '';
        }
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/home.css?' . $this->config->application->cache_string);
        if ($this->getLang() === 'ar') {
            $this->view->pick('index/ar/index');
        }
    }
}
