(function($) {

    $('.js-card-form .cc-number').payment('formatCardNumber');
    $('.js-card-form .cc-cvc').payment('formatCardCVC');

    $(".js-card-form").validationEngine();


    function validCardNumber(field, rules, i, options) {
        if(!$.payment.validateCardNumber(field.val())) {
            return "* Invalid card number";
        }
    }

    function validCardType(field, rules, i , options) {

        var cardTypes = {
            "Visa":"visa",
            "MasterCard":"mastercard",
            "American Express":"amex",
            "Discover":"discover",
            "Diners Club":"dinersclub",
            "JCB": "jcb"
        };

        var $methodSelect = field.parents('form').find('select[name="method"]')

        var cardType = $.payment.cardType(field.val()),
            selectedCardType = cardTypes[$methodSelect.val()];
        if(cardType !== selectedCardType) {
            
            return "* Card type doesn't match card number";
        }
        
    }

    window.validCardNumber = validCardNumber;
    window.validCardType = validCardType;

    $(document).on('click', '.js-update-card', function() {
        var cardId = $(this).attr('data-card-id');
        $('.js-card-form-container[data-card-id="' + cardId + '"]').toggleClass('hide');
    });

    $(document).on('click', '.js-cancel-btn', function(e) {
        e.preventDefault();
        $(this).parents('.js-card-form-container').addClass('hide');
    });

    $(document).on('click', '.js-delete-card', function() {
        var $this = $(this);
        var cardId = $(this).attr('data-card-id');
        var confirmation = confirm('Are you sure you want to delete this card?');
        if(confirmation) {
            $.ajax({
                url: '/billing/delete/' + cardId,
                type: 'delete',
            }).done(function(data) {
                if(data.status === 'OK') {
                    $('.js-card[data-card-id="'+ cardId +'"]').fadeOut(500, function () {
                        $(this).remove();
                        if(!$('.js-card').length) {
                            $('.js-add-form-container').removeClass('hide');
                            $('.js-payment-text').text('You do not have a payment method specified yet.');
                            $('.js-show-payments').text('You can add your payment method here');
                        } 
                    });

                    $('.js-card-form-container[data-card-id="' + cardId + '"]').remove();

                }
            }).fail(errorHandler); 
        }
    });


    $('.js-show-payments, .js-main-cancel-btn').on('click', function(e) {
        e.preventDefault();
        $('.js-update-notice').toggleClass('hide'); 
    });


    $(document).on('submit', '.js-update-card-form', function(e) {
        var $this = $(this);
        var cardId = $this.attr('data-card-id'); 
        e.preventDefault();
        var formData = $this.serialize(); 
        $.ajax({
            url: $this.attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json'
        }).then(function(data) {
            if(data.status === 'OK') {
                return $.ajax({
                   url: '/billing/show/' + cardId,
                   type: 'get'
                });
            }
        }).done(function(data) {
            if(data.status === 'OK') {
                if(data.card.default_card) {
                    $('.js-payment-text').text('Your current payment method is ' + data.card.payment_method + ' ending with ' + data.card.number.substr(data.card.number.length - 4) +  ',' );
                }
                var $html = $("<div></div>").html(data.html);
                var $card = $html.find('.js-card');
                var $cardForm = $html.find('.js-card-form-container');
                $('.js-card[data-card-id="' + cardId + '"]').html($card.html());
               $('.js-card-form-container[data-card-id="' + cardId + '"]').html($cardForm.html());
            }
        }).fail(errorHandler);
    });


    $(document).on('submit', '.js-add-card-form', function(e) {
       var $this = $(this);
       e.preventDefault();
       var formData = $this.serialize();
       $.ajax({
         url: $this.attr('action'),
         data: formData,
         type: 'post',
         dataType: 'json'
       }).then(function(data) {
           if(data.status === 'OK') {
               return $.ajax({
                   url: 'billing/show/' + data.data.card_id,
                   type: 'get'
               });
           }
       }).done(function(data) {
           if(data.status === 'OK') {
                if(!$('.js-card').length) {
                    $('.js-payment-text').text('Your current payment method is ' + data.card.payment_method + ' ending with ' + data.card.number.substr(data.card.number.length - 4) +  ',' );
                    $('.js-show-payments').text('You can update your payment method here');
                }
               var $html = $('<div></div>').html(data.html);
               $('.js-add-form-container').addClass('hide').before($html.html());
               resetForm($this);
           }
       }).fail(errorHandler);
    });


    $('.js-sort-select').on('change', function() {
        var $this = $(this);
        var value = $this.val();
        $.ajax({
            url: '/billing/sortinvoices/' + value,
            type: 'get',
            dataType: 'json'
        }).done(function(data) {
            if(data.status === 'OK') {
                var template = renderinvoices(data.data.invoices);
                $('.js-invoice').remove();
                $('.js-invoices-table').append(template);
            }
        });
    });

    $(document).on( 'change', '.js-card-form input, .js-card-form textarea, .js-card-form select', enableSubmitButton);
    $(document).on('keyup',  '.js-card-form input, .js-card-form textarea', enableSubmitButton);

    function enableSubmitButton(e) {
        var $el = $(this),
            $cardForm = $el.parents('.js-card-form').first(),
            $cardFormSubmit = $cardForm.find('input[type=submit]').first();
        $cardFormSubmit.prop('disabled', false);
    }

    function errorHandler(jqXHR) {
        var response = JSON.parse(jqXHR.responseText);
        var errorMsg = '<div class="alert alert-danger col-sm-12">'+ response.message +'</div>';
        $('.js-update-notice').prepend(errorMsg);
        setTimeout(function() {
            $('.alert').remove();
        }, 3000);
    }

    function resetForm($form) {
        $form.find('input:text, input:password, input:file, select, textarea').val('');
        $form.find('input:radio, input:checkbox')
        .removeAttr('checked').removeAttr('selected');
    }


    function renderinvoices(invoices) {
        var template = '';
        invoices.forEach(function(invoice) {
            var date = new Date(invoice.date) , dateFormat = '';
            var month = date.getMonth() + 1;
            var day = date.getDate();
            dateFormat += day < 10 ? "0" + day : day;
            dateFormat += "/";
            dateFormat += month < 10 ? "0" + month : month; 
            dateFormat += "/";
            dateFormat += date.getFullYear();
            template += '<tr class="js-invoice">';
            template += '<td class="text-center weight-normal">'+ invoice.id +'</td>';
            template += '<td class="text-center weight-normal">'+ invoice.payment_method + invoice.card_no +'</td>';
            template += '<td class="text-center weight-normal">'+ invoice.status +'</td>';
            template += '<td class="text-center weight-normal">'+ dateFormat +'</td>';
            template += '<td class="text-center weight-normal"><a href="/billing/viewreceipt/' + invoice.id + '">View receipt</a></td>';
            template += '</tr>';
        });

        return template;
    }

})(jQuery);
