<div id="wrapper">

    <!-- Sidebar -->


        <div class="row bg-green white login-header margin-bottom">
            <div class="col-xs-10 {{ lang === 'ar' ? "rtl" : ""  }}">
                <h3>{{ t._('create-new-account')  }}</h3>
            </div>
            <div class="col-xs-2">
                <button class="no-border bg-transparent white login-close js-login-close inline-block width-all"><i class="fa fa-close fa-2x"></i></button>
            </div>
        </div>
<div class="row negative-margin-top">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
        <div class="row margin-bottom {{ lang === 'ar' ? "rtl" : ""  }}">
            {% set postUrl = (request.get('redirect_url')) ? 'users/register?redirect_url=' ~ request.get('redirect_url') : 'users/register'  %}
            {{ form(postUrl, 'class': 'login-form') }}
                <div class="form_row full">
                    {{ textField({"first_name", 
                        "placeholder": t._('first-name'), 
                        "class": "form-control input-lg width-all", 
                        "data-validation-engine" :"validate[required]", 
                        "data-errormessage-value-missing": t._('fname-required'),
                        "data-prompt-position": "topRight:-130"
                    }) }}
                </div>
                <div class="form_row full">
                    {{ textField({"last_name", 
                        "placeholder": t._('last-name'), 
                        "class": "form-control input-lg width-all", 
                        "data-validation-engine" :"validate[required]", 
                        "data-errormessage-value-missing": t._('lname-required'),
                        "data-prompt-position": "topRight:-130"
                    }) }}
                </div>
                <div class="form_row">
                  {{ textField( {
                        "email", 
                        'placeholder': t._('email-addr'), 
                        'class': 'form-control input-lg width-all', 
                        "data-validation-engine" :"validate[required, custom[email]]",
                        "data-errormessage-value-missing": t._('email-required'),
                        "data-errormessage-custom-error": t._('email-not-valid'),
                        "data-prompt-position": "topRight:-130"
                    } ) }}
                </div>
                <div class="form_row">
                 {{ passwordField( {
                    "password",
                    'id': 'password',
                    'placeholder': t._('passwd'), 
                    'class': 'form-control input-lg width-all', 
                    "autocomplete": "off", 
                    "data-validation-engine" :"validate[required]",
                    "data-errormessage-value-missing": t._('password-required'),
                    "data-prompt-position": "topRight:-130"
                    }) }}
                </div>
                <div class="form_row">
                    {{ passwordField( {
                        "confirm_password", 
                        'placeholder': t._('confirm-password'), 
                        'class': 'form-control input-lg width-all', 
                        "autocomplete": "off", 
                        "data-validation-engine" :"validate[equals[password]]",
                        "data-errormessage-pattern-mismatch": t._('password-not-match'),
                        "data-prompt-position": "topRight:-130"
                    } ) }}
                </div>
               <div class="form_row">
                   <div class="input-group">
                     <div class="radio-inline">
                         {{radio_field('gender', 'value': '1', 'id': 'male')}}
                         <label for="male">{{ t._('male')  }}</label>
                     </div>
                     <div class="radio-inline">
                         {{radio_field('gender', 'value': '0', 'id': 'female')}}
                         <label for="female">{{ t._('female')  }}</label>
                     </div>
                   </div>
               </div>
               <div class="form_row">
                   <div class="g-recaptcha" data-sitekey="{{ publicKey  }}"></div>
               </div>
               <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                <div class="form_row">
                    <input type="submit" value="{{ t._('register')  }}" class="btn-large {{  (lang === 'ar') ? "pull-left" : "pull-right" }} bg-dark-blue">
                </div>
            {{ endForm() }}
        </div>
</div>
