<footer>
    <ul class="footer_links clearfix">
        <li><?php echo $this->tag->linkto('static/about', 'About Us'); ?></li>
        <li><?php echo $this->tag->linkto('pdf/Speakol-Apps.pdf', 'Help'); ?></li>
        <li><?php echo $this->tag->linkto('static/terms', 'Terms'); ?></li>
        <li><?php echo $this->tag->linkto('static/privacy', 'Privacy'); ?></li>
        <li><?php echo $this->tag->linkto('static/apps', 'Apps'); ?></li>
        <li><?php echo $this->tag->linkto('static/careers', 'Careers'); ?></li>
    </ul>
    <span class="copyRights">Copyright © 2013 - All rights reserved, Speakol.</span>
</footer>