<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Confirmation;
use \Phalcon\Validation\Validator\InclusionIn;
class CardsValidation extends SpeakolValidation {
    public function __construct($rules = false, $exclude = false) {
        $this->loadCustomTrans("apps");
        $this->validations = array('name' => array(new PresenceOf(array('message' => $this->t->_('card-name-required'))),), 'method' => array(new PresenceOf(array('message' => $this->t->_('card-method-required'))),), 'number' => array(new PresenceOf(array('message' => $this->t->_('card-number-required'))),), 'code' => array(new PresenceOf(array('message' => $this->t->_('card-code-required'))),), 'year' => array(new PresenceOf(array('message' => $this->t->_('card-year-required'))),), 'month' => array(new PresenceOf(array('message' => $this->t->_('card-month-required'))),), 'plan_id' => array(new PresenceOf(array('message' => $this->t->_('card-plan-required'))),),);
        parent::__construct($rules, $exclude);
    }
    public function initialize() {
        parent::initialize();
    }
}
