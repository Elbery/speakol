<table class="body-wrap" style="width: 100%; margin: 0; padding: 0;"><tr style="margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"></td>
        <td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">
            <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table style="width: 100%; margin: 0; padding: 0;"><tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;"><td class="va_t" style="vertical-align: top; margin: 0; padding: 0;" valign="top">
                            <span class="img_circl" style="border-radius: 50%; display: inline-block; width: 57px; height: 57px; overflow: hidden; text-align: center; vertical-align: top; margin: auto 10px auto auto; padding: 0; border: 2px solid #ededed;"> <img src="{{ config.application.webservice ~ challeger_profile_picture}}" alt="" style="max-width: 100%; margin: 0; padding: 0;" /></span>
                        </td>
                        <td style="margin: 0; padding: 0;">
                            <p style="font-weight: normal; font-size: 14px; line-height: 1.6; color: #4c4c4c; margin: 0 0 10px; padding: 0;">Hi <span style="margin: 0; padding: 0;">{{ debate_owner }}</span>, </p>
                            <h5 style="line-height: 1.1; color: #4c4c4c; font-weight: 700; font-size: 20px; margin: 0 0 15px; padding: 0;">{{ challeger_name }}, {{ t._('dbt-chlng-invit') }}  <a href="{{ config.application.webhost }}/debates/challengers/{{ debate_slug }}">{{ debate_slug }}</a></h5>
                        </td>
                    </tr></table></div><!-- /content -->

        </td>
        <td style="margin: 0; padding: 0;"></td>
    </tr>
</table>
<!-- /BODY -->