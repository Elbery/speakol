<section class="comparison_container">
    <div class="story_title_info clearfix">
        <h1 class="item_title">
            {{comparison.title}}
        </h1>
    </div>
    <ul class="comparison_items elems_{{ comparison.sides|length }} clearfix">
        {% for side in comparison.sides %}
            {% set total_pages = side.max %}
            <li data-side-id="{{side.id}}" data-comments-page="1" data-total-pages="{{side.max}}">
                {% include "partials/comparison/side" with ['comparison':comparison, 'side':side, 'ad': comparison.ads[loop.index0] ] %}
            </li>
        {% endfor %}
    </ul>

</section>
