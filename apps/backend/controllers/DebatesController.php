<?php
namespace Speakol\Backend\Controllers;
use \Phalcon\Tag as Tag;
class DebatesController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('debates');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function codeAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->view->successfullyCreated = false;
        $this->tag->appendTitle(' Create Debate');
        $this->view->debate_slug = "";
        $userSession = $this->getAppData();
        if (is_null($userSession)) {
            $this->flash->error("You should login first");
            return $this->response->redirect('apps/login');
        }
        if (empty($userSession->id)) {
            $this->flash->error('You should create application first.');
            return $this->response->redirect('apps/create');
        }
        $pluginsLimit = $this->checkPluginLimit();
        if ($pluginsLimit['errors']) {
            $this->flash->error($pluginsLimit['errors']);
            return $this->response->redirect('dashboard');
        }
        $this->view->reachedQuota = $pluginsLimit['remainingPlugins'] === 0;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/popups.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/debate.js?' . $this->config->application->cache_string);
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:' . $this->getAppToken()), array());
        $categories = $result->messages;
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $this->view->setVar('categories', $categoryList);
        $this->view->user = $userSession;
        if ($this->request->isPost()) {
            $postFields = $this->getPostData();
            if ($postFields) {
                $apiResponse = $this->utility->iCurl('/debates/create', 'post', array('Authorization:' . $this->getAppToken()), $postFields, false);
                if (strtoupper($apiResponse->status) == "ERROR") {
                    $this->flash->error($apiResponse->message);
                } else if (strtoupper($apiResponse->status) == "SUCCESS") {
                    $this->view->setVar('app_id', $userSession->id);
                    $this->view->setVar('debate_slug', $this->config->application->webhost . "debates/render?app=" . $userSession->id . "&debate=" . $apiResponse->data);
                    $this->view->successfullyCreated = true;
                }
            }
        }
    }
    protected function getPostData($update = false) {
        $debater_1_pic = '';
        $debater_2_pic = '';
        if ($this->request->hasFiles()) {
            foreach ($this->request->getUploadedFiles() as $file) {
                switch ($file->getKey()) {
                    case 'debater_1_pic':
                        $debater_1_pic = $file->getTempName() . ';filename=' . $file->getName();
                    break;
                    case 'debater_2_pic':
                        $debater_2_pic = $file->getTempName() . ';filename=' . $file->getName();
                    break;
                }
            }
        }
        $userData = $this->getAppOwnerData();
        $userSession = $this->getAppData();
        $postFields = array('title' => $this->request->getPost('debate_title'), 'category_id' => $this->request->getPost('category'), 'app_id' => $userSession->id, 'max_days' => $this->request->getPost('max_days'), 'user_id' => $userData->id, 'type' => 0, 'debater_1_pic' => $debater_1_pic ? "@$debater_1_pic" : '', 'debater_2_pic' => $debater_2_pic ? "@$debater_2_pic" : '', 'sides' => json_encode(array(array('id' => $userData->id, 'title' => $this->request->getPost('side_1_title'), 'job_title' => $this->request->getPost('side_1_job_title'), 'opinion' => $this->request->getPost('debater_1_opinion')), array('id' => $userData->id, 'title' => $this->request->getPost('side_2_title'), 'job_title' => $this->request->getPost('side_2_job_title'), 'opinion' => $this->request->getPost('debater_2_opinion')))),);
        if ((empty($debater_1_pic) || empty($debater_2_pic)) && !$update) {
            $this->flash->error('Debater photo is required');
            return false;
        }
        return $postFields;
    }
    public function editAction() {
        $slug = $this->request->get('slug');
        if (!$slug) {
            $this->flash->error('No slug has been given');
            return $this->response->redirect('dashboard');
        }
        $app = $this->getAppData();
        if (!$app) {
            $this->flash->error('You should login first');
            return $this->response->redirect('apps/login');
        }
        if ($app->plan_id && $app->plan_id === 1) {
            return $this->response->redirect('dashboard');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        if ($this->request->isPost()) {
            $postFields = $this->getPostData(true);
            if ($postFields) {
                $postFields['slug'] = $slug;
                $response = $this->utility->iCurl('/debates/update', 'POST', $hdrs, $postFields, true);
                if ($response->status !== 'OK') {
                    $this->flash->error($response->message);
                } else {
                    $this->flash->success($this->t->_('update-succeeded'));
                }
            }
        }
        $params = array('slug' => $slug,);
        $this->view->debate_slug = $slug;
        $response = $this->utility->iCurl('/debates/show/info', 'GET', $hdrs, $params);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('dashboard');
        }
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/popups.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/debate.js?' . $this->config->application->cache_string);
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:' . $this->getAppToken()), array());
        $categories = $result->messages;
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $this->view->setVar('categories', $categoryList);
        $this->view->debate = $response->data;
        Tag::setDefault('category', $this->view->debate->category);
        $this->view->pick('debates/code');
    }
    public function renderAction() {
        $url = $this->request->get('url');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $newsfeed = $this->request->get('newsfeed');
        $newsfeed = $newsfeed === '1' ? true : false;
        $authorized = true;
        $this->view->newsfeed = $newsfeed;
        if ($this->cookies->has(session_name())) {
            $error = $this->session->get('error_socail');
            if ($error) {
                $this->flash->error($error);
                $this->session->remove('error_socail');
            }
        }
        $token = $this->getToken();
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        } else {
            $hdrs = array('Authorization:');
        }
        $this->view->isAppLoggedIn = false;
        if ($this->getAppData()) {
            $this->view->isAppLoggedIn = true;
            $this->view->appData = $this->getAppData();
        }
        $params = array('app_id' => $this->request->get('app'), 'debate_slug' => $this->request->get('debate'), 'url' => $this->request->get('url'), 'token' => $token, 'order_by' => 'most_voted',);
        $objDebateJSON = $this->utility->iCurl('/debates/' . $params['debate_slug'] . '/show', 'get', $hdrs, $params, true);
        if ($objDebateJSON->status === 'ERROR' && $objDebateJSON->unauthorized_domain) {
            $authorized = false;
        }
        if ((is_null($objDebateJSON) || $objDebateJSON->status == 'ERROR') && !$objDebateJSON->unauthorized_domain) {
            return $this->view->pick('errors/show404');
            $this->utility->throwCustomException($objDebateJSON->messages);
        }
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->setLang($objUserProfile->data->user->locale);
            }
        } else {
            $objUserProfile = false;
        }
        $this->view->title = 'Speakol | ' . $objDebateJSON->data->title;
        $this->view->authorized = true;
        if (!$authorized && !$newsfeed) {
            $this->view->authorized = false;
        } else {
            $this->view->debate = $objDebateJSON->data;
            if (!$this->isUrlHttps($url) && !$newsfeed) {
                $this->view->debate->audio = false;
            }
            $created_at = $this->view->debate->created_at;
            $updated_at = $this->view->debate->updated_at;
            $this->view->edited = strtotime($updated_at) > strtotime($created_at);
            $this->view->fburl = $this->utility->getAbsoluteURL('users', 'facebook?r_url=1');
            $this->view->googleurl = $this->utility->getAbsoluteURL('users', 'google?r_url=1');
            $this->view->isLoggedIn = $objUserProfile ? true : false;
            $this->view->logout = $objUserProfile ? $this->url->get("users/logout") : '';
            $this->view->LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
            $this->view->lang = $this->request->get('lang') ? $this->request->get('lang') : $this->getLang();
            $this->view->url = urldecode($this->view->debate->url);
            if ($newsfeed) {
                $this->view->url = urlencode($this->config->application->main_website . 'newsfeed/debate?slug=' . $this->view->debate->slug);
            }
            $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(), 'url' => $this->request->get('url') ? $this->request->get('url') : '',);
        }
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->collection('header')->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->collection('header-ie9')->addJs('http://html5shim.googlecode.com/svn/trunk/html5.js?' . $this->config->application->cache_string);
        $this->assets->collection('footer')->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/mediaelement-and-player.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/ramjet.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string);
        if ($authorized || $newsfeed) {
            $this->assets->collection('footer')->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('vendor/js/audiorecorder.js?' . $this->config->application->cache_string)->addJs('vendor/js/speexextensions.js?' . $this->config->application->cache_string)->addJs('js/recorder.js?' . $this->config->application->cache_string)->addJs('js/audioplayer.js?' . $this->config->application->cache_string)->addJs('js/debateplugin.js?' . $this->config->application->cache_string)->addJs('js/header.js?' . $this->config->application->cache_string)->addJs('js/arguments.js?' . $this->config->application->cache_string)->addJs('js/replies.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/loginbox.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string);
        }
    }
    function exceptionHandler($exception) {
        $this->view->errorMessage = $exception->getMessage();
        var_dump(__FUNCTION__);
        exit(0);
        $this->view->pick("argumentsbox/error");
    }
    public function voteAction() {
        $token = $this->getToken();
        if (empty($token)) {
            return false;
        }
        $userSession = $this->getUserData();
        $postData = array('user_id' => $userSession->id, 'side_id' => 1,);
        $result = $this->utility->iCurl('/debates/vote', 'POST', array('Authorization:' . $this->getToken()), $postData);
        $categories = $result->messages;
    }
    public function deleteAction($slug) {
        $result = $this->utility->iCurl('/debates/' . $slug . '/delete', 'delete', array('Authorization:' . $this->getAppToken()), array());
        if ($this->request->isAjax()) {
            if (strtoupper($result->status) == "SUCCESS") {
                $this->response->setJsonContent(array('status' => 'SUCCESS'));
            } else if (strtoupper($result->status) == "ERROR") {
                $this->response->setJsonContent(array('status' => 'ERROR'));
            }
            return $this->response;
        } else {
            if (strtoupper($result->status) == "SUCCESS") {
                $this->flash->success('Content deleted Successfully!');
            } else if (strtoupper($result->status) == "ERROR") {
                $this->flash->error('Some thing went wrong please try again later!');
            }
            return $this->response->redirect('admin/reports');
        }
    }
}
