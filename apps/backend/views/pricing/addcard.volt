{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': 'Sign up for ' ~ upgradePlanDetails['name']  ]  %}
        <div class="row margin-top-lg">
            
            <div class="media upgrade-plan-info padding-bottom-sm">
                <div class="pull-right margin-top-xs">
                    <h3 class="font-25 no-margin">
                        {{ upgradePlanDetails['price'] }}
                    </h3>
                </div>
                <div class="media-body">
                    <h3 class="weight-bold font-25 color-499856 no-margin">{{ upgradePlanDetails['name'] }}</h3>
                    <p class="color-585858 no-margin h5 padding-bottom-xs margin-top-xs">{{ t._('monthly-subscription') }}</p>
                </div>
            </div>
            <div class="js-alerts col-sm-11 margin-bottom-sm">
                
                {% set colors = ['error': 'color-9b3535', 'notice': 'color-966f1d', 'success': 'color-468847'] %}
                {% set backgrounds = [ 'notice': 'bg-fbe2a7', 'error': 'bg-f9c6c6', 'success': 'bg-dff0d8' ] %}
                {% set icons = [ 'error': 'speakol-icon speakol-icon-md speakol-times-circle color-dc5959', 'notice': 'speakol-icon speakol-icon-md speakol-exclamation-triangle color-e6b729', 'success' : 'fa fa-check-circle margin-top-xs fa-2x' ] %}
                {% for flashType, messages in flash.getMessages() %}
                    {% for message in messages %}
                    <div class="row padding-top-xs padding-bottom-xs padding-left-10 {{ backgrounds[flashType] }} {{ colors[flashType] }}">
                        <div class="pull-left">
                            <i class="{{ icons[flashType] }}"></i>
                        </div>
                        <div class="media-body font-15 margin-top-xs">
                            <p class="padding-left-10 no-margin"> {{ message }}</p>
                        </div>
                    </div>
                    {% endfor %}
                {% endfor %}
            </div>
            {{ form( 'pricing/addcard', 'id': 'pricingForm', 'class': 'form-horizontal padding-top-sm col-sm-11 js-card-form add-card-form','enctype': 'multipart/form-data') }}
                {% if upgradePlan %}
                    <input type="hidden" name="upgrade_plan" value="{{ upgradePlan  }}">
                {% endif %}
                <div class="form-group">
                    <label for="name" class="col-sm-2 margin-top-xs font-15 text-right">{{ t._('payment-method') }}</label>
                    <div class="col-sm-3 add-card-input">
                        <select id="method"
                                name="method"
                                class="form-control no-radius no-padding-height arial-font"
                                data-validation-engine="validate[required]"
                        >
                            <option value="">{{ t._('credit-card') }}</option>
                            <option value="Visa">Visa</option>
                            <option value="MasterCard">MasterCard</option>
                            <option value="American Express">American Express</option>
                            <option value="Discover">Discover</option>
                            <option value="Diners Club">Diners Club</option>
                            <option value="JCB">JCB</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc-number" class="col-sm-2 margin-top-xs font-15 text-right">{{ t._('card-number') }}</label>
                    <div class="col-sm-3 add-card-input">
                        <input id="cc-number"
                            name="number"
                            class="form-control no-radius font-15 cc-number arial-font"
                            type="tel"
                            placeholder="•••• •••• •••• ••••"
                            data-validation-engine="validate[required, funcCall[validCardNumber], funcCall[validCardType]]"
                        >
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 margin-top-xs font-15 text-right">{{ t._('name-card') }}</label>
                    <div class="col-sm-3 add-card-input">
                        {{ textField({
                            "name",
                            "class": "form-control no-radius font-15",
                            "data-validation-engine" :"validate[required]"
                        }) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="month" class="font-15 text-right col-sm-2">{{ t._('expire-date') }}</label>
                    <div class="col-sm-3 add-card-input">
                        <div class="row">
                                <select id="month"
                                    name="month"
                                    class="inline-block color-555555 five bg-white no-padding-height padding-5 no-radius margin-left-5 border-d1d1d1"
                                >
                                    <option value="">{{ t._('month') }}</option>
                                    <option value="1">{{ t._('january')}}</option>
                                    <option value="2">{{ t._('february')}}</option>
                                    <option value="3">{{ t._('march')}}</option>
                                    <option value="4">{{ t._('april')}}</option>
                                    <option value="5">{{ t._('may')}}</option>
                                    <option value="6">{{ t._('june')}}</option>
                                    <option value="7">{{ t._('july')}}</option>
                                    <option value="8">{{ t._('august')}}</option>
                                    <option value="9">{{ t._('september')}}</option>
                                    <option value="10">{{ t._('october')}}</option>
                                    <option value="11">{{ t._('november')}}</option>
                                    <option value="12">{{ t._('december')}}</option>
                                </select>
                                <select id="year"
                                    name="year"
                                    class="inline-block color-555555 four bg-white no-padding-height padding-5 no-radius border-d1d1d1 add-card-year"
                                >
                                    <option value="">{{ t._('year') }}</option>
                                    {% for year in currentYear..currentYear+10 %}
                                        <option value="{{ year }}">{{ year }}</option>
                                    {% endfor %}
                                </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc-cvc" class="col-sm-2 font-15 text-right">{{ t._('security-code') }}</label>
                    <div class="col-sm-1 add-card-input">
                        <input id="cc-cvc"
                            name="code"
                            class="form-control no-radius font-15 cc-cvc arial-font"
                            type="tel"
                            placeholder="••••"
                            autocomplete="off"
                            data-validation-engine="validate[required]"
                        >
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-2 text-right">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-info fa-stack-1x white"></i>
                        </span>
                    </div>
                    <div class="col-sm-8">
                        <p class="h5 add-card-info padding-top-xs padding-left-5">
{{ t._('info-payment') }}
                        </p>
                    </div>
                </div>
                <div class="form-group margin-top-lg">
                    {{ submitButton({
                        t._('purchase'), 
                        'class': 'add-card-submit btn btn-primary padding-top-sm padding-bottom-sm font-15 col-sm-3 col-sm-offset-2 no-radius bg-light-blue',
                        'disabled': true
                    })}}
                </div>
            {{ endform }}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
<div class="pricing-modal modal" id="pricingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div class="js-success-message text-center  hide">
                    <i class="fa fa-check-circle color-499856 fa-8x display-block margin-center "></i>
                    <h3 class="color-499856 font-28 margin-top-xs weight-light">{{ t._('payment-successful') }}</h3>
                    <p class="h4 margin-top-lg padding-top-lg {{ lang === 'ar' ? 'rtl' : '' }}">
                         {{ t._('thanks-speakol') }} <span class="weight-bold">{{ upgradePlanDetails['name'] }}</span>{{ t._('payment-was') }} <span class="font-weight js-amount"></span> {{ t._('deducted') }} 
                    </p>
                    <p class="payment-small-paragraph font-13 color-a6a6a6 margin-bottom-lg padding-bottom-lg margin-center">
                        {{ t._('automatic-renew') }} <a href="/billing" class="anchor-reset color-499856">{{ t._('billing-history') }}</a>.
                    </p>
                </div>
                <div class="js-loading-spinner text-center  padding-bottom-lg">
<!--
     Taken from https://github.com/jxnblk/loading
-->
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="200" height="200" fill="white">
  <circle cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(45 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.125s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(90 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.25s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(135 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.375s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(180 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.5s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(225 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.625s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(270 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.75s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(315 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.875s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
  <circle transform="rotate(180 16 16)" cx="16" cy="3" r="0" fill="#57b25f" stroke-width="0.5">
    <animate attributeName="r" values="0;3;0;0" dur="1s" repeatCount="indefinite" begin="0.5s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" calcMode="spline" />
  </circle>
</svg>
                </div>
            </div>
        </div>
    </div>
</div>
