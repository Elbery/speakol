<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Controllers\BaseController;
class NotificationsController extends BaseController {
    public function initialize() {
        parent::initialize();
    }
    private function _adaptNotification($notifications) {
        $allNotification = array();
        $website = $this->config->application->main_website;
        if (!$notifications) {
            return $allNotification;
        }
        array_walk($notifications, function (&$notification, $index) {
            $notification['extra_info'] = json_decode($notification['extra_info'], true);
            $notification['url'] = rawurldecode($notification['url']);
            $notification['url'] = rawurldecode($notification['url']);
            $notification['url'] = rawurldecode($notification['url']);
        });
        $notifiables = array('arg-reply', 'arg-thumbup', 'reply-thumbup', 'arg-thumbdown', 'reply-thumbdown', 'approve', 'challenge');
        $notifications = array_filter($notifications, function ($notification) use ($notifiables) {
            return in_array($notification['notifiable_type'], $notifiables);
        });
        return $notifications;
    }
    public function indexAction() {
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('debates');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
        $this->tag->appendTitle(' User notifications');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            return $this->response->redirect('newsfeed');
        }
        $page = $this->request->get('page');
        $page = $page ? $page : 1;
        $this->view->fullPage = true;
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $params = array('page' => $page, 'limit' => 10,);
        $result = $this->utility->iCurl("/notifications/listbyrecipient", 'get', $hdrs, $params, false, true);
        if ($result['status'] !== 'OK' && $result['status'] !== 'SUCCESS') {
            $notifications = array();
        } else {
            $notifications = $this->_adaptNotification($result['data']);
        }
        $this->view->setVar('notifications', $notifications);
        $this->view->nextPage = $page + 1;
        if ($this->request->isAjax()) {
            $this->view->pick('partials/argumentsbox/notifications');
            return;
        }
        $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, true, false);
        $this->view->isLoggedIn = true;
        $this->view->LoggedInUserData = $objUserProfile->data->user;
        $this->setLang($objUserProfile->data->user->locale);
        $favorites = $objUserProfile->data->favorites ? $objUserProfile->data->favorites : array();
        $categoriesData = $this->formatCategories($favorites, $categoryId);
        foreach ($categoriesData as $key => $data) {
            $this->view->{$key} = $data;
        }
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        $this->view->pick('notifications/index');
    }
    public function viewAction() {
        $postFields = array('page' => 1, 'limit' => 5,);
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $result = $this->utility->iCurl("/notifications/listbyrecipient", 'get', $hdrs, $postFields, false, true);
        $notifications = $this->_adaptNotification($result['data']);
        $html = $this->utility->renderTemplate("partials", "argumentsbox/notifications", array('notifications' => $notifications));
        if ($html != false) {
            $this->response->setJsonContent(array('status' => 'success', 'data' => array('html' => $html, 'new_items' => $result['data']['new_items'])));
        } else if (strtoupper($result['status']) == "ERROR") {
            $this->response->setJsonContent(array('status' => 'error'));
        }
        return $this->response;
    }
    public function updatebyrecipientAction() {
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $result = $this->utility->iCurl("/notifications/updatebyrecipient", 'put', $hdrs, array(), false, true);
        if ($result) {
            $this->response->setJsonContent(array('status' => 'success', 'data' => $result));
        } else if (strtoupper($result['status']) == "ERROR") {
            $this->response->setJsonContent(array('status' => 'error'));
        }
        return $this->response;
    }
    public function debatechallengeAction() {
        $debateData = array_shift(array_values($this->dispatcher->getParams()));
        $challegerName = $this->getUserData()->name;
        $options = array('challeger_name' => $challegerName, 'challeger_profile_picture' => $this->getUserData()->profile_picture, 'debate_owner' => $debateData->name, 'debate_slug' => $debateData->slug, 'template_name' => 'challenge');
        $content = $this->utility->renderEmailContent($this, array('emails', 'challenge'), $options);
        $emailPostFields = array('html_content' => $content, 'text' => '', 'subject' => 'Speakol debates challenge', 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to_email' => $debateData->email);
        $notifyPostFields = array('notification_type' => 'debate', 'notifiable_id' => $this->request->getPost('debate_id'), 'notifiable_type' => 'challenge', 'user_id' => $this->getUserData()->id, 'recipient_id' => $debateData->id, 'url' => $debateData->slug);
        $result = $this->utility->iCurl('/notifications/create', 'POST', array('Authorization:' . $this->getToken()), $notifyPostFields, false, true);
        $result = $this->utility->iCurl('/notifications/email/send', 'POST', array('Authorization:' . $this->getToken()), $emailPostFields, false, true);
    }
    public function debateapproveAction() {
        $data = array_values($this->dispatcher->getParams());
        $data = $data['0'];
        $challegerName = $data->challenger->name;
        $debateOwnerName = $this->getUserData()->name;
        $options = array('challeger_name' => $challegerName, 'challeger_profile_picture' => $data->challenger->profile_picture, 'debate_owner' => $debateOwnerName, 'debate_slug' => $data->debate->slug, 'template_name' => 'challenge');
        $content = $this->utility->renderEmailContent($this, array('emails', 'challenge-approve'), $options);
        $emailPostFields = array('html_content' => $content, 'text' => '', 'subject' => 'Speakol debates challenge approveal ', 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to_email' => $data->challenger->email);
        $notifyPostFields = array('notification_type' => 'debate', 'notifiable_id' => $data->debate->id, 'notifiable_type' => 'approve', 'user_id' => $this->getUserData()->id, 'recipient_id' => $data->challenger->id, 'url' => $data->debate->slug);
        $this->utility->iCurl('/notifications/create', 'POST', array('Authorization:' . $this->getToken()), $notifyPostFields, false, true);
        $this->utility->iCurl('/notifications/email/send', 'POST', array('Authorization:' . $this->getToken()), $emailPostFields, false, true);
        return true;
    }
}
