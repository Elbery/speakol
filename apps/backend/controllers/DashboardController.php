<?php
namespace Speakol\Backend\Controllers;
use \Phalcon\Tag as Tag;
class DashboardController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->currentUserIs(array('2', '3'), 'apps/login');
    }
    public function indexAction() {
        $this->tag->appendTitle(' Dashboard');
        $app = $this->getAppData();
        $user = $this->session->get('app_data')->user;
        $this->view->admin = ($email === $this->config->application->admin_mail);
        if (empty($app->id)) {
            return $this->response->redirect('apps/create');
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $objJSON = $this->utility->iCurl('/apps/listStats/' . $app->id, 'get', $hdrs, array(), false, true);
        $this->view->data = $objJSON['data'];
        $this->view->appData = $app;
        $this->view->planID = $app->plan_id;
        $this->view->user = $user;
        $this->view->downgradeStatus = $user->app->downgrade;
        $pluginsLimit = $this->view->data['plugins_creation']['data'];
        $remainingPlugins = $pluginsLimit['remaining_plugins'];
        $this->view->maxPlugins = $pluginsLimit['max_no'] === 'Unlimited' ? 'Unlimited' : intval($pluginsLimit['max_no']);
        $lang = $this->getLang();
        $text = '';
        if ($remainingPlugins <= 0 && $this->view->maxPlugins !== 'Unlimited') {
            $text = $this->t->_('reached-quota');
        } else if ($remainingPlugins > 5) {
            $text = $this->t->_('you-have') . ' ' . $remainingPlugins . ' ' . $this->t->_('remaining-month');
        } else if ($remainingPlugins <= 5 && $lang === 'ar') {
            $text = $this->t->_('you-have') . ' ' . $this->t->_('only') . ' ' . $remainingPlugins;
            $text.= ' ' . $this->t->_('remaining-month');
        } else {
            $text = $this->t->_('you-have') . ' ' . $remainingPlugins . ' ' . $this->t->_('only');
            $text.= ' ' . $this->t->_('remaining-month');
        }
        $this->view->remainingPlugins = $remainingPlugins;
        $this->view->quotaText = $text;
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:xxxxxxxxxxx'), array());
        $categories = $result->messages;
        $categoryList = array('' => $this->t->_('choose-category'));
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $langs = array("en" => "English", "ar" => "العربية");
        $this->view->setVar('langs', $langs);
        $this->view->setVar('categories', $categoryList);
        $this->view->jsVars = array('took_tour' => $this->getTookTour(), 'lang' => $this->getLang(),);
        Tag::setDefault("app_name", $user->name);
        Tag::setDefault("app_url", $app->website[0]);
        Tag::setDefault("category", $app->category_id);
        Tag::setDefault("lang", $this->session->get('lang'));
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/shepherd.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/dashboard.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/mediaelement-and-player.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.cookie.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/shepherd.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/audiorecorder.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/speexextensions.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/recorder.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/audioplayer.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/tour.js?' . $this->config->application->cache_string);
    }
    public function infoAction() {
        $this->tag->appendTitle(' Dashboard');
        $app = $this->getAppData();
        $user = $this->session->get('app_data')->user;
        $this->view->admin = ($email === $this->config->application->admin_mail);
        if (empty($app->id)) {
            return $this->response->redirect('apps/create');
        }
        $this->view->app = $app;
        $this->view->user = $user;
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:xxxxxxxxxxx'), array());
        $categories = $result->messages;
        $categories = array_filter($categories, function ($v, $k) use ($app) {
            return $v->id === $app->category_id;
        });
        $categories = array_values($categories);
        $this->view->category = $categories[0]->name;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/shepherd.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/dashboard.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.cookie.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/shepherd.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/tour.js?' . $this->config->application->cache_string);
    }
    public function updateStatsAction() {
        $app = $this->getAppData();
        if (empty($app->id)) {
            $this->response->setStatusCode(401, "UNauthorized");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'Unauthorized', 'code' => 404));
            return $this->response;
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $time = $this->request->get('time');
        if (!isset($time) && empty($time)) {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'time must be a number', 'code' => 404));
            return $this->response;
        }
        $response = $this->utility->iCurl('/apps/interactions/stats', 'GET', $hdrs, array('time' => $time));
        if ($response->status !== 'OK') {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $response->message, 'code' => 404));
            return $this->response;
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => $response->data, 'code' => 200));
        return $this->response;
    }
}
