<li class="js-argumentsbox">
    <div class="row thin-underline margin-top-sm">
        <div class="col-xs-1">
            {% if appImage %}
                {{ image(appImage, "class": "media-object img-circle") }}
            {% else %}
                {{ image("img/user_default.gif", "class": "media-object img-circle") }}
            {% endif %}
        </div>
        <div class="col-xs-11">
            <div class="row">
                <div class="col-xs-11">
                    {% set title = item['data']['recomended']['title'] | trim %}
                    {% if title === '' %}
                        {% set title = t._('no-title') %}
                    {% elseif not title %}
                        {% set title = t._('what-do-you-think') %}
                    {% endif %}
                    {% if planID and planID <= 1 %}
                    <h4 class="font-20 black weight-bold" target="_blank" ><i class="speakol-icon speakol-argumentsbox"></i> {{ title }}</h4>
                    {% else %}
                    {% set url =  '/argumentsbox/edit?url=' ~ item['data']['url'] | url_encode %}
                    {% if item['data']['code'] is defined and item['data']['code'] %}
                        {% set url = url ~ '&uuid=' ~ item['data']['code'] %}
                    {% endif %}
                    <a 
                        href="{{ url }}"
                        class="font-20 black weight-bold"
                        target="_blank"
                    >
                        <i class="speakol-icon speakol-argumentsbox"></i> {{ title }}
                    </a>
                    {% endif %}
                </div>
                <div class="col-xs-1 text-right">
                    <button
                        class="btn btn-default bg-white no-radius border-none js-remove-argumentsbox"
                        data-argumentsbox-url="{{ item['data']['url'] | url_encode }}"
                        {% if item['data']['code'] is defined and item['data']['code'] %}
                        data-uuid="{{ item['data']['code'] }}"
                        {% endif %}
                    >
                        <i class="fa fa-times red"></i>
                    </button>
                </div>
            </div>
            <div class="row margin-bottom-sm">
            {% if item['data']['description'] and item['data']['description'] != 'undefined' and item['data']['description'] != '' %}
               <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-quote-left fa-2x"></i>
                    </div>
                    <div class="media-body">
                     <p class="h5">
                        {{ item['data']['description'] }}
                     </p>
                    </div>
               </div>
           {% endif  %}
            </div>
            <div class="row color-7f8080 font-13">
                <div class="col-xs-10 text-left">
                    <time>{{ item['data']['created_at'] }}</time> 
                </div>
                <div class="col-xs-2 text-right">
                    <p>
                        <i class="fa fa-comment"></i>&nbsp;{{ item['counts']['total_comments']}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</li>
