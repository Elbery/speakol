$(document).ready(function ($) {
    var tour;

    tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-arrows',
            scrollTo: false
        }
    });

    tour.addStep('debate', {
        text: [
            '<span class="title">Create Debate</span>',
            '<span class="additional-info">Purpose: Host a two-sided debate topic.</span>',
            '<span class="step"><span class="number">1</span><span class="description">Click on "Create Debate" located in the left toolbar or right side of the page.</span></span>',
            '<span class="step"><span class="number">2</span><span class="description">After being redirected, enter your debate topic in the space provided and upload participating debaters\' photo, name, job title and opinion. Then select debate period and category.</span></span>',
            '<span class="step"><span class="number">3</span><span class="description">Choose preferred language and click on "Get Code". Copy and Paste the code in the desired location and start the experience.</span></span>'
        ],
        attachTo: '.shepherd-debate right',
        classes: 'example-step-extra-class',
        buttons: [
            {
                text: 'Next',
                action: tour.next
            }
        ]
    });
    tour.addStep('comparison', {
        text: [
            '<span class="title">Create Comparison</span>',
            '<span class="additional-info">Purpose: Compare up to 4 different items and give your opinion.</span>',
            '<span class="step"><span class="number">1</span><span class="description">Click on "Create Comparison" located in the left toolbar or right side of the page.</span></span>',
            '<span class="step"><span class="number">2</span><span class="description">After being redirected, enter your comparison topic in the space provided. Upload option images with their respective titles and choose the comparison category.</span></span>',
            '<span class="step"><span class="number">3</span><span class="description">Choose preferred language and click on "Get Code". Copy and Paste the code in the desired location and start the experience.</span></span>'
        ],
        attachTo: '.shepherd-comparison right',
        classes: 'example-step-extra-class',
        buttons: [
            {
                text: 'Back',
                classes: 'pull-left',
                action: tour.back
            }, {
                text: 'Next',
                action: tour.next
            }
        ]
    });
    tour.addStep('argument', {
        text: [
            '<span class="title">Create Argument Box</span>',
            '<span class="additional-info">Purpose: Host user discussion.</span>',
            '<span class="step"><span class="number">1</span><span class="description">Click on "Create Argument Box" located in the toolbars on the right or left side of the page.</span></span>',
            '<span class="step"><span class="number">2</span><span class="description">After being redirected, enter the width of your iframe in the space provided.</span></span>',
            '<span class="step"><span class="number">3</span><span class="description">Choose preferred language and click on "Get Code". Copy and Paste the code in the desired location and start the experience.</span></span>',
        ],
        attachTo: '.shepherd-argument right',
        classes: 'example-step-extra-class',
        buttons: [
            {
                text: 'Back',
                classes: 'pull-left',
                action: tour.back
            }, {
                text: 'Exit',
                action: tour.next
            }
        ]
    });
    if(parseInt(phpVars.took_tour) !== 1) {
       tour.start();
       tour.on('complete', function() {
           setTookTour();
       });
    }


    function setTookTour () {
        $.ajax({
            url: '/users/tooktour',
            type: 'PUT',
            dataType: 'json'
        });
    }

});
