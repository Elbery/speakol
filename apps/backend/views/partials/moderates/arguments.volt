{% if not arguments %}
    <div class="row">
        <h3>{{ t._('no-args-crtd') }}!</h3>
    </div>
{% endif %}
<ul class="js-arg-items col-lg-11">
    {% include "partials/moderates/arguments_items.volt"  %}
</ul>
