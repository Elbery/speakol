<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Confirmation;
use \Phalcon\Validation\Validator\InclusionIn;
class AppsValidation extends SpeakolValidation {
    public function __construct($rules = false, $exclude = false) {
        $this->loadCustomTrans("apps");
        $this->validations = array("app_logo" => array(new PresenceOf(array('message' => $this->t->_('logo-required'))),), "name" => array(new PresenceOf(array('message' => $this->t->_('name-required'))), new StringLength(array('messageMaximum' => $this->t->_('name-max-length'), 'messageMinimum' => $this->t->_('name-min-length'), 'min' => 2, 'max' => 64))), "website" => array(new PresenceOf(array('message' => $this->t->_('website-required'))), new StringLength(array('messageMaximum' => $this->t->_('website-max-length'), 'messageMinimum' => $this->t->_('website-min-length'), 'min' => 8, 'max' => 255))), "country_id" => array(new PresenceOf(array('message' => $this->t->_('country-required')))), "category_id" => array(new PresenceOf(array('message' => $this->t->_('category-required')))), "email" => array(new Email(array('message' => $this->t->_('email-not-valid'))), new PresenceOf(array('message' => $this->t->_('email-required'))),), "password" => array(new PresenceOf(array('message' => $this->t->_('password-required'))), new StringLength(array('messageMinimum' => $this->t->_('password-min-length'), 'min' => 8)),), "old_password" => array(new PresenceOf(array('message' => $this->t->_('old-password-required'))),), "confirm_email" => array(new Confirmation(array('message' => $this->t->_('email-not-match'), 'with' => 'email'))), "confirm_password" => array(new Confirmation(array('message' => $this->t->_('password-not-match'), 'with' => 'password'))), 'newsletter' => array(new InclusionIn(array('message' => $this->t->_('newsletter-invalid'), 'domain' => array('0', '1', '2'))),),);
        parent::__construct($rules, $exclude);
    }
    public function initialize() {
        parent::initialize();
    }
}
