<?php
namespace Speakol\Recaptcha;
class Recaptcha extends \Phalcon\DI\Injectable {
    const RECAPTCHA_API_SERVER = 'http://www.google.com/recaptcha/api';
    const RECAPTCHA_API_SECURE_SERVER = 'https://www.google.com/recaptcha/api';
    const RECAPTCHA_VERIFY_SERVER = 'www.google.com';
    const RECAPTCHA_ERROR_KEY = 'To use reCAPTCHA you must get an API key from <a href="https://www.google.com/recaptcha/admin/create">https://www.google.com/recaptcha/admin/create</a>';
    const RECAPTCHA_ERROR_REMOTE_IP = 'For security reasons, you must pass the remote IP address to reCAPTCHA';
    public static $error = 'incorrect-captcha-sol';
    public static $is_valid = false;
    public static function get($publicKey, $error = '', $useSSL = false) {
        $publicKey = $publicKey or die(self::RECAPTCHA_ERROR_KEY);
        $server = $useSSL ? self::RECAPTCHA_API_SECURE_SERVER : self::RECAPTCHA_API_SERVER;
        if ($error) $error = "&amp;error=" . $error;
        $content = '
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : "custom",
    custom_theme_widget: "recaptcha_widget"
 };
</script>
         <div id="recaptcha_widget" style="display:none">

   <div id="recaptcha_image"></div>
   <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>

   <span class="recaptcha_only_if_image">Enter the words above:</span>
   <span class="recaptcha_only_if_audio">Enter the numbers you hear:</span>

   <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" data-validation-engine="validate[required]"/>

   <div><a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a></div>
   <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type(\'audio\')">Get an audio CAPTCHA</a></div>
   <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type(\'image\')">Get an image CAPTCHA</a></div>

   <div><a href="javascript:Recaptcha.showhelp()">Help</a></div>

 </div>

 <script type="text/javascript"
    src="http://www.google.com/recaptcha/api/challenge?k=' . $publicKey . '">
 </script>
 <noscript>
   <iframe src="http://www.google.com/recaptcha/api/noscript?k=' . $publicKey . '"
        height="300" width="500" frameborder="0"></iframe><br>
   <textarea name="recaptcha_challenge_field" rows="3" cols="40">
   </textarea>
   <input type="hidden" name="recaptcha_response_field"
        value="manual_challenge">
 </noscript>

        ';
        return $content;
    }
    public static function check($privateKey, $remoteIP, $challenge, $response, $extra_params = array()) {
        $privateKey = $privateKey or die(self::RECAPTCHA_ERROR_KEY);
        $remoteIP = $remoteIP or die(self::RECAPTCHA_ERROR_REMOTE_IP);
        if (!$response) return self::$is_valid;
        $url = "/siteverify?secret=$privateKey&response=$response";
        $response = self::httpPost(self::RECAPTCHA_API_SECURE_SERVER, $url, array() + $extra_params);
        if ($response->success) self::$is_valid = true;
        else self::$error = $response->error_codes;
        return self::$is_valid;
    }
    private static function httpPost($host, $path, $data, $port = 80) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host . $path);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, false);
    }
    private static function qsEncode($data) {
        $req = '';
        foreach ($data as $key => $value) $req.= $key . '=' . urlencode(stripslashes($value)) . '&';
        $req = substr($req, 0, strlen($req) - 1);
        return $req;
    }
}
