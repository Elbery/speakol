{% set action = [
'arg-thumbup': t._('arg-thumbup'),
'reply-thumbup': t._('reply-thumbup'),
'arg-thumbdown': t._('arg-thumbdown'),
'reply-thumbdown': t._('reply-thumbdown'),
'arg-reply': t._('arg-reply'),
'approve': t._('approve'),
'challenge': t._('challenge')] %}
{% if notifications | length %}
    {% for notification in notifications %}
    {%set notifiables = [] %}
    <li>
        <div class="media {{ fullPage ? 'community-notification-item' : 'width-90 margin-center notification-item' }} margin-top-sm padding-bottom-sm  cursor-pointer js-notification-item">
            <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
                {% if notification['image'] %}
                <img src="{{ notification['image'] }}" alt="" class="media-object {{ fullPage ? 'community-notification-image' : 'notification-image' }}">
                {% else %}
                <img src="/img/user_default.gif" alt="" class="media-object {{ fullPage ? 'community-notification-image' : 'notification-image' }}">
                {% endif %}
            </div>
            <div class="media-body font-13 {{ lang === 'ar' ? 'rtl text-right' : '' }}">
                <p class="media-heading color-555555 {{ fullPage ? 'no-margin' : '' }}">
                    <strong>{{ notification['name'] }}</strong> {{ action[notification['notifiable_type']] }} 
                </p>
                <p class="no-margin color-9e9a9a">
                    <a href="{{ notification['url'] }}" target="_blank" class="anchor-reset js-notification-url">
                        {{ notification['created_at'] }}
                    </a>
                </p>
            </div>
        </div>
    </li>
    {% endfor %}
    {% if fullPage %}
        <li class="hide"><a class="js-feed-paginate" href="/notifications?page={{ nextPage }}">next</a></li>
    {% endif %}
    {% if not fullPage %}
    <li>
        <div class="text-center weight-bold padding-top-lg padding-bottom-lg light-blue">
            <a class="anchor-reset" href="/notifications" {{ communityPage ? '' : 'target="_blank"' }}>
                {{ t._('all-notifications') }}
            </a>
        </div>
    </li>
    {% endif %}
{% endif %}
