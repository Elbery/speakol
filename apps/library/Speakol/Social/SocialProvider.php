<?php
namespace Speakol\Social;
interface SocialProvider {
    public function getLoginURL($redirectURI);
    public function getAccessToken($code, $state_code_a, $redirectURI);
    public function getSocialId();
}
