(function($) {

    $('.more_cont').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            parentPane = $this.parents('.tab-pane'),
            page = parentPane.attr('data-page'),
            sort = parentPane.attr('data-sort'),
            sortKey = parentPane.attr('data-sort-key');
        if(sort && sortKey) {
            var $sortItem = parentPane.find('.js-sort-item[data-sort-key="' + sortKey + '"]');
            sortHandler($sortItem, true);
            return;
        }
        $.ajax({
            url: '/stats',
            dataType: 'json',
            type: 'GET',
            data: {
                page: ++page 
            },
            success: function (response, textStatus, jqXHR) {
                if(response.status === "OK") {
                    if(parentPane.attr('data-pane-type') === 'users') {
                        var users = response.data.users;
                        var html = renderUsersTemplate(users.users_data);
                        if(!users.has_more) {
                            $this.remove();
                        }
                    } else {
                        var apps = response.data.apps;
                        var html = renderAppsTemplate(apps.apps_data);
                        if(!apps.has_more) {
                            $this.remove();
                        }
                    }
                    parentPane.attr('data-page', page);
                    parentPane.children('table').find('tbody').append(html);
                }
            },
        });
    });


    $('.js-sort-item').on('click', function() {
        var $el = $(this);
        sortHandler($el);
    });


    function sortHandler($el, paging) {
        var $tableContainer = $el.parents('.js-table-container'),
            page = +$tableContainer.attr('data-page'),
            sortKey = $el.attr('data-sort-key'),
            sort = $el.attr('data-sort'),
            url = '',
            paging = (paging === undefined) ? false : true,
            limit = 10;
        
        var nextSort = {
            'none': 'asc',
            'asc' : 'desc',
            'desc': 'asc'
        };

        var sortIcons = {
            'none': 'fa-sort',
            'asc': 'fa-sort-asc',
            'desc': 'fa-sort-desc'
        };

        if($tableContainer.attr('data-pane-type') === 'apps') {
            url = '/stats/sort/apps';
        } else {
            url = '/stats/sort/users';
        }


        if(!paging) {
            sort = nextSort[sort];
        }
        if(sort === $tableContainer.attr('data-sort') && sortKey === $tableContainer.attr('data-sort-key')) {
            page++;
            $tableContainer.attr('data-page', page);
        } else {
            limit = 10 * page;
            page = 1;
        }
        $tableContainer.find('.js-sort-item').attr('data-sort', 'none');
        $tableContainer.find('.js-sort-item .fa').removeClass('fa-sort-asc fa-sort-desc').addClass('fa-sort');
        $el.find('.fa').removeClass('fa-sort').addClass(sortIcons[sort]);
        $el.attr('data-sort', sort);
        $tableContainer.attr('data-sort-key', sortKey);
        $tableContainer.attr('data-sort', sort);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'GET',
            data: {
                page: page,
                sort: sort,
                sort_key: sortKey,
                limit: limit
            },
            success: function (response, textStatus, jqXHR) {
                if(response.status === "OK") {
                    var html = '';
                    if($tableContainer.attr('data-pane-type') === 'users') {
                        var users = response.data.users.users_data;
                        html = renderUsersTemplate(users);
                    } else {
                        var apps = response.data.apps.apps_data;
                        html = renderAppsTemplate(apps);
                    }
                    if(paging) {
                        $tableContainer.children('table').find('tbody').append(html);
                    } else {
                        $tableContainer.children('table').find('tbody').html(html);
                    }
               }
            }
        });
    }


    function renderUsersTemplate(users) {
        var html = "";
        users.forEach(function(user) {
            html +="<tr>";
            html +="<td> " + user.name + " </td>";
            html +="<td> " + user.email + " </td>";
            html +="<td> " + user.country + " </td>";
            html +="<td> " + user.created_at + " </td>";
            html +="<td> " + user.job_title + " </td>";
            html +="<td> " + user.dob + " </td>";
            html +="<td> " + user.votes + " </td>";
            html +="<td> " + user.arguments + " </td>";
            html +="</tr>";
        });
        return html;
        
    }

    function renderAppsTemplate(apps) {
        var html = "";
        apps.forEach(function(app) {
            html +="<tr>";
            html +="<td> " + app.name + " </td>";
            html +="<td> " + app.website + " </td>";
            html +="<td> " + app.email + " </td>";
            html +="<td> " + app.arguments + " </td>";
            html +="<td> " + app.country + " </td>";
            html +="<td> " + app.created_at + " </td>";
            html +="<td> " + app.plugins_count + " </td>";
            html +="</tr>";
        });
        return html;
        
    }

})(jQuery);
