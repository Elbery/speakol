{% set url = '#' %}
<div class="sp-block-group">
    {% if plugin.action === 'create' %}
        {% set postTypes = ['argumentboxes': t._('posted-argumentsbox'), 'debates': t._('posted-debate'), 'comparisons': t._('posted-comparison')] %}
    {% elseif plugin.action === 'vote' %}
        {% set postTypes = ['argumentboxes': t._('voted-argumentsbox'), 'debates': t._('voted-debate'), 'comparisons': t._('voted-comparison')] %}
    {% elseif plugin.action === 'comment'%}
        {% set postTypes = ['argumentboxes': t._('commented-argumentsbox'), 'debates': t._('commented-debate'), 'comparisons': t._('commented-comparison')] %}
    {% endif %}
    <div class="sp-block eight media seven-xs">
        <div class="pull-left">
            {% if userProfile %}
            <img src="{{userImage }}" class="publisher-image radius-5" alt="...">
            {% elseif appProfile %}
            <img src="{{ appImage }}" class="publisher-image radius-5" alt="...">
            {% else %}
            <img src="{{ plugin.app.image }}" class="publisher-image radius-5" alt="...">
            {% endif %}
        </div>
        <div class="media-body">
            
            {% if userProfile %}
            <h3 class="font-15 no-margin margin-bottom-xs header-action"><a href="/users/profile/{{ userId }}?lang={{ lang }}" class="light-blue weight-bold anchor-reset">{{ userName }}</a> {{ postTypes[plugin.type] }}</h3>
            {% elseif appProfile %}
            <h3 class="font-15 no-margin margin-bottom-xs header-action"><a href="/apps/profile/{{ appId }}?lang={{lang}}" class="light-blue weight-bold anchor-reset">{{ appName }}</a> {{ postTypes[plugin.type] }}</h3>
            {% else %}
            <h3 class="font-15 no-margin margin-bottom-xs header-action"><a href="/apps/profile/{{ plugin.app.id }}?lang={{ lang }}" class="light-blue weight-bold anchor-reset">{{ plugin.app.name }}</a> {{ postTypes[plugin.type] }}</h3>
            {% endif %}
            <p class="h6 color-777777">{{ utility.formatDate(plugin.created_at, 'F jS, Y \a\t h:i A') }}</p>
        </div>
    </div>
    <div class="sp-block four relative five-xs">
        <div class="dropdown plugin-dropdown pull-right header-share-dropdown {{ plugin.highlighted or superAdmin ? 'header-share-highlight' : '' }}">
            <button class="btn-reset dropdown-toggle no-radius" type="button" id="shareDropdown" data-toggle="dropdown" aria-expanded="true">
                <i class="sp-icon sp-icon-lg sp-share"></i>
            </button>
            <ul class="dropdown-menu no-shadow right-0 left-auto no-radius color-9ca7ad" role="menu" aria-labelledby="shareDropdown">
                            {% if plugin.type === 'debates' %}
                                {% set shareUrl  = config.application.main_website ~ 'newsfeed/debate?slug=' ~ (plugin.data.slug | url_encode) %}
                            {% elseif plugin.type === 'argumentboxes' %}
                                {% set shareUrl  = config.application.main_website ~ 'newsfeed/argumentsbox?url=' ~ (plugin.data.url | url_encode)  %}
                            {% elseif plugin.type === 'comparisons' %}
                                {% set shareUrl  = config.application.main_website ~ 'newsfeed/comparison?slug=' ~ (plugin.data.slug | url_encode) %}
                            {% endif %}
                            <li role="prensentation"><a class="" href="https://www.facebook.com/sharer/sharer.php?u={{ shareUrl | url_encode  }}" title="{{ t._('share-on-facebook') }}" target="_blank"><i class="fa fa-facebook"></i>&nbsp;{{ t._('share-on-facebook') }}</a></li>
                            <li role="prensentation"><a class="" href="https://twitter.com/home?status={{ shareUrl | url_encode }}" title="{{ t._('tweet') }}" target="_blank"><i class="fa fa-twitter"></i>&nbsp;{{ t._('tweet') }}</a></li>
                            <li role="prensentation"><a class="" href="https://plus.google.com/share?url={{ shareUrl | url_encode }}" title="{{ t._('share-on-google-plus') }}" target="_blank"><i class="fa fa-google-plus"></i>&nbsp;{{ t._('share-on-google-plus') }}</a></li>
                            <li role="prensentation"><a class="" href="https://www.linkedin.com/shareArticle?mini=true&url={{ shareUrl | url_encode }}" title="{{ t._('share-on-linkedin') }}" target="_blank"><i class="fa fa-linkedin"></i>&nbsp;{{ t._('share-on-linkedin') }}</a></li>
            </ul>
        </div>            
        {% if plugin.highlighted and not superAdmin %}
        <div class="relative radius-3 white bg-666c77 h5 pull-right margin-right-10 highlighted-indicator">{{ t._('highlighted') }}</div>
        {% endif %}
        {% if (plugin.highlight) and superAdmin %}
            <button class="relative btn-reset js-highlight js-highlighted radius-3 white bg-666c77 h5 highlighting-button unhighlight-action pull-right" data-type="{{ plugin.type }}" data-plugin-id="{{ plugin.data.id }}">
                {{ t._('highlighted') }}
            </button>
        {% endif %}
        {% if (not plugin.highlight) and superAdmin %}
            <button class="relative btn-reset js-highlight radius-3 bg-white color-666c77 h5 highlighting-button highlight-action pull-right border-666c77" data-type="{{ plugin.type }}" data-plugin-id="{{ plugin.data.id }}">
                {{ t._('highlight') }}
            </button>
        {% endif %}
    </div>
</div>
