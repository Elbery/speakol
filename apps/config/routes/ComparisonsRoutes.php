<?php
namespace Speakol\Routes;
class ComparisonsRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'comparisons',));
        $this->setPrefix('/comparisons');
        $this->add('', array('action' => 'index'))->setName("comparisons-list");
        $this->add('/code', array('action' => 'code'))->setName("comparison-code");
        $this->add('/render', array('action' => 'render'))->setName("render-comparrison-box");
    }
}
