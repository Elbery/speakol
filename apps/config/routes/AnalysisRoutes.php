<?php
namespace Speakol\Routes;
class AnalysisRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'analysis',));
        $this->setPrefix('/analysis');
        $this->add('', array('action' => 'index'))->setName("analysis-list");
    }
}
