<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Validations as Validations;
use Speakol\Recaptcha\Recaptcha;
use \Phalcon\Tag as Tag;
use Tartan\Mandrill\Exception;
class AppsController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('apps');
        $this->loadCustomTrans('user');
        if ($this->dispatcher->getActionName() !== 'profile') {
            $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
            $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
            $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
            $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
            $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
            $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        }
    }
    public function apiAction() {
        try {
            if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
                $this->response->setStatusCode(401, "Unauthorized");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => array('Unauthorized'), 'code' => 401));
                return $this->response;
            }
            $method = $this->request->get('method') ? $this->request->get('method') : $this->request->getMethod();
            $action = $this->request->get('action');
            $data = $this->request->get();
            if (empty($method) || empty($action)) {
                throw new \Exception('Action is missing!');
            }
            unset($data['_url']);
            unset($data['action']);
            if ($this->request->hasFiles()) {
                $files = $this->request->getUploadedFiles();
                foreach ($files as $file) {
                    $filename = $file->getName();
                    $audio = $this->request->getPost('audio');
                    $audio = $audio ? true : false;
                    $data[$filename . '[]'] = $this->utility->getCurlValue($file, $audio);
                }
            }
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $return = $this->utility->iCurl($action, $method, $hdrs, $data, true);
            if ($return->status === "OK") {
                $this->response->setStatusCode(200, $return->status);
            } else {
                $this->response->setStatusCode($return->code, $return->status);
            }
            $this->response->setJsonContent($return);
        }
        catch(\Exception $ex) {
            $this->response->setStatusCode($ex->getCode(), 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $ex->getMessage()));
        }
        $data = $this->request->getPost();
        return $this->response;
    }
    public function createAction() {
        $publicKey = $this->config->application->recaptcha->public_key;
        $state = $this->session->get('state');
        if (!$this->request->isPost()) {
            $this->session->remove('state');
        }
        if ($this->request->isPost() && $state) {
            $yourPrivateKey = $this->config->application->recaptcha->private_key;
            $recaptcha = Recaptcha::check($yourPrivateKey, $_SERVER['REMOTE_ADDR'], $this->request->getPost('recaptcha_challenge_field'), $this->request->getPost('g-recaptcha-response'));
            if (!$recaptcha) {
                $this->session->remove("state");
                $this->flash->error('Recaptcha code is invalid!');
            }
            if ($this->request->isPost() && $recaptcha) {
                $postData = array('name' => $this->request->getPost('name'), 'email' => $this->request->getPost('email'), 'website' => $this->request->getPost('website'), 'category_id' => $this->request->getPost('category'), 'country_id' => $this->request->getPost('country'), 'redirect_url' => $this->config->application->webhost . 'apps/activateaccount/', 'card' => array('name' => $this->request->getPost('card_name'), 'method' => $this->request->getPost('card_method'), 'number' => $this->request->getPost('card_number'), 'code' => $this->request->getPost('card_code'), 'year' => $this->request->getPost('card_year'), 'month' => $this->request->getPost('card_month'), 'plan_id' => $this->request->getPost('plan'),), 'logo' => $this->request->getPost('logo'),);
                if ($state === 4 || $state === 3) {
                    $postData['password'] = $this->request->getPost('password');
                    $postData['confirm_password'] = $this->request->getPost('confirm_password');
                    $appValidator = new Validations\AppsValidation(array('name', 'email', 'website', 'category_id', 'country_id', 'password', 'confirm_password'));
                } else {
                    $appValidator = new Validations\AppsValidation(array('name', 'email', 'website', 'category_id', 'country_id'));
                }
                $appMsgs = $appValidator->validate($postData);
                if (count($appMsgs)) {
                    $this->session->remove('state');
                    $this->showErrorMsgs($appMsgs);
                } else {
                    if ($state === 4 || $state === 3) {
                        unset($postData['confirm_password']);
                    }
                    $result = $this->utility->iCurl('/apps/create', 'POST', array('Authorization:xxxxxxxxxxx'), $postData, false, false, true);
                    if (strtoupper($result->status) == "OK") {
                        $this->flash->success('App has been created successfully but you still need to verify it. please check your mail for verification link');
                        return $this->response->redirect('');
                    } else {
                        $this->session->remove('state');
                        $this->flash->error($result->messages);
                    }
                }
            }
        }
        $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.payment.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/create.js?' . $this->config->application->cache_string);
    }
    public function checkemailAction() {
        $this->response->setHeader("Content-Type", "application/json");
        $state = $this->session->get('state');
        if ($this->request->isPost() && $this->request->isAjax() && !$state) {
            $params = array('email' => $this->request->getPost('email'),);
            $appValidator = new Validations\AppsValidation(array('email'));
            $appMsgs = $appValidator->validate($params);
            if (count($appMsgs)) {
                $this->response->setStatusCode(500, "Internal Server Error");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $appMsgs[0]->__toString()));
                return $this->response;
            }
            $newUser = $this->request->getPost('new_user');
            if ($newUser !== "0" && $newUser !== "1") {
                $this->response->setStatusCode(500, "Internal Server Error");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('specify-user')));
                return $this->response;
            }
            $response = $this->utility->iCurl('/user/emailstatus', 'POST', NULL, $params, false, false, true);
            if ($response->status !== "OK") {
                $this->response->setStatusCode(500, "Internal Server Error");
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
                return $this->response;
            }
            $state = intval($response->data);
            if ($state === 1 || ($newUser && $state !== 4)) {
                $this->response->setStatusCode(301, "Found");
                $message = ($state === 1) ? $this->t->_('publisher-exists') : $this->t->_('normal-user-exists');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $message));
                return $this->response;
            }
            if (!$newUser && $state === 4) {
                $this->response->setStatusCode(404, 'Not Found');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $this->t->_('user-not-exist')));
                return $this->response;
            }
            $categories = $this->utility->iCurl('/categories', 'GET', array('Authorization:xxxxxxxxxxx'), array());
            $categoryList = array('' => $this->t->_('choose-category'));
            foreach ($categories->messages as $category) {
                $categoryList[$category->id] = $category->name;
            }
            $countries = $this->utility->iCurl('/countries', 'get', array('Authorization:xxxxxxxxxxx'), array());
            $countryList = array('' => $this->t->_('choose-country'));
            foreach ($countries->messages as $country) {
                $countryList[$country->id] = $country->name;
            }
            $publicKey = $this->config->application->recaptcha->public_key;
            $html = $this->utility->renderTemplate('partials', 'apps/createForm', array('state' => $state, 'countries' => $countryList, 'categories' => $categoryList, 'publicKey' => $publicKey, 'currentYear' => date('Y'),));
            $this->session->set('state', $state);
            $this->response->setStatusCode(200, 'OK');
            $this->response->setJsonContent(array('status' => 'OK', 'data' => array('html' => $html)));
            return $this->response;
        }
    }
    public function updateAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->tag->appendTitle(' Create New App');
        $app = $this->getAppData();
        if (empty($app->id)) {
            $this->flash->error('You must be logged in.');
            return $this->response->redirect('apps/login');
        }
        if ($this->request->isPost()) {
            $postData = array('name' => trim($this->request->getPost('app_name')), 'old_password' => $this->request->getPost('old_password'), 'password' => $this->request->getPost('password'), 'confirm_password' => $this->request->getPost('confirm_password'), 'category_id' => $this->request->getPost('category'), 'locale' => $this->request->get('lang'), 'newsletter' => $this->request->get('newsletter'), 'logo' => $this->request->get('app_logo'), 'app_id' => $app->id, 'website' => array(),);
            if ($postData['password']) {
                $appValidator = new Validations\AppsValidation(array('name', 'category_id', 'password', 'old_password', 'confirm_password', 'newsletter'));
            } else {
                $appValidator = new Validations\AppsValidation(array('name', 'category_id', 'newsletter'));
            }
            $appMsgs = $appValidator->validate($postData);
            $websites = $this->request->getPost('website');
            $websitesValidated = $this->validateWebsites($websites);
            if (count($appMsgs)) {
                $this->showErrorMsgs($appMsgs);
            } else if ($websitesValidated) {
                $postData['website'] = $websites;
                unset($postData['confirm_password']);
                $result = $this->utility->iCurl('/apps/update', 'POST', $this->utility->getAuthorizationHeadersForApp(), $postData, false, false, true);
                if ($result->status == 'OK') {
                    $this->setLang($result->data->locale);
                    $this->loadMainTrans();
                    $this->loadCustomTrans('apps');
                    $this->loadCustomTrans('user');
                    $appData = $this->session->get('app_data');
                    $appData->user->name = $result->data->name;
                    $appData->user->profile_picture = $result->data->profile_picture;
                    $appData->user->app->category_id = $result->data->category_id;
                    $appData->user->slug = $result->data->slug;
                    $appData->user->app->website = $result->data->website;
                    $this->session->set('app_data', $appData);
                    $this->flash->success($this->t->_('success-update-app'));
                } else {
                    $this->flash->error($result->messages);
                }
            }
            return $this->response->redirect('apps/settings');
        }
    }
    public function activateaccountAction($token) {
        if (!$token) {
            $this->repsonse->redirect('');
        }
        $postData = array('token' => $token,);
        $response = $this->utility->iCurl('/user/activatepublisher', 'POST', NULL, $postData, false, false, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('');
        }
        $this->flash->success('Your account has been activated successfully. Please login below');
        return $this->response->redirect('apps/login');
    }
    public function forgetpasswordAction() {
        $this->tag->appendTitle(' | Forget Password');
        if ($this->request->isPost()) {
            $data = array("email" => $this->request->getPost('email'),);
            try {
                $appValidator = new Validations\AppsValidation(array("email"));
                $appMsgs = $appValidator->validate($data);
                if (count($appMsgs)) {
                    $this->showErrorMsgs($appMsgs);
                } else {
                    $postData = array('redirect_url' => $this->config->application->webhost . 'apps/resetpassword/');
                    $forgetPassword = $this->utility->iCurl('/user/forgot_password/' . $data['email'], 'get', NULL, $postData, true, false);
                    if ($checkVerificationCode['status'] = 'OK') {
                        $this->flash->success($this->t->_('user-forget-password-success'));
                        return $this->response->redirect('');
                    } else {
                        throw new Exception($checkVerificationCode['messages']);
                    }
                }
            }
            catch(\Exception $ex) {
                $message = $ex->getMessage();
                $this->flash->error($message);
            }
        }
        $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
    }
    public function activecodeAction() {
        $this->assets->addCss('css/en/login.css?' . $this->config->application->cache_string);
        $this->tag->appendTitle(' | Resend Vefication code');
        if ($this->request->isPost()) {
            try {
                $email = filter_var($this->request->getPost('email', "email"), FILTER_VALIDATE_EMAIL);
                if (empty($email)) {
                    throw new \Exception('Please enter a valid email', 400);
                }
                $postData = array('redirect_url' => $this->config->application->webhost . $this->url->get('apps/activateaccount/'), 'email' => $email);
                $resendVerifcation = $this->utility->iCurl('/user/resend_confirmation', 'post', NULL, $postData, true, false);
                if ($checkVerificationCode['status'] = 'OK') {
                    $this->flash->success($this->t->_('user-forget-password-success'));
                    return $this->response->redirect('apps/activecode');
                } else {
                    throw new Exception($checkVerificationCode['messages']);
                }
            }
            catch(\Exception $ex) {
                $message = $ex->getMessage();
                $this->flash->error($message);
            }
        }
    }
    public function resetpasswordAction($code) {
        $this->assets->addCss('css/en/login.css?' . $this->config->application->cache_string);
        $this->tag->appendTitle(' | Forget Password');
        try {
            if ($this->request->isPost()) {
                $postData = array('password' => $this->request->get('password', 'string', false), 'confirm_password' => $this->request->get('confirm_password', 'string', false),);
                $appValidator = new Validations\AppsValidation(array('password', 'confirm_password'));
                $appMsgs = $appValidator->validate($postData);
                if (count($appMsgs)) {
                    $this->showErrorMsgs($appMsgs);
                } else {
                    unset($postData['confirm_password']);
                    $checkVerificationCode = $this->utility->iCurl('/user/reset_password/' . $code, 'post', NULL, $postData, true, true);
                    if ($checkVerificationCode['status'] = 'OK') {
                        $this->flash->success('Your password updated successfully!');
                        return $this->response->redirect('apps/login');
                    } else {
                        throw new Exception($checkVerificationCode['messages']);
                    }
                }
            }
            $postData = array('step' => '1');
            $checkVerificationCode = $this->utility->iCurl('/user/reset_password/' . $code, 'post', NULL, $postData, true, true);
            if ($checkVerificationCode['status'] == 'OK') {
                $this->view->userData = $checkVerificationCode['data'];
            } else {
                throw new \Exception($checkVerificationCode['messages'], 400);
            }
            $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
        }
        catch(\Exception $ex) {
            $message = $ex->getMessage();
            $code = $ex->getCode();
            $this->flash->error($message);
            if ($code == 400) {
                $this->response->redirect('apps/forgetpassword');
            }
        }
    }
    public function loginAction() {
        $this->tag->appendTitle(' publisher login');
        $userSession = $this->getAppData();
        $redirectURL = $this->request->get('redirect_url');
        if (!empty($userSession)) {
            return $this->response->redirect('https://plugins.speakol.com/dashboard');
        }
        if ($this->request->isPost()) {
            $params = array('email' => $this->request->getPost('email'), 'password' => $this->request->getPost('password'), 'role' => 3);
            $appValidator = new Validations\AppsValidation(array('email', 'password'));
            $appMsgs = $appValidator->validate($params);
            if (count($appMsgs)) {
                $this->showErrorMsgs($appMsgs);
            } else {
                $response = $this->utility->iCurl('/user/signin', 'post', array('Authorization:xxxxxxxxxxx'), $params, false, false, true);
                if (strtoupper($response->status) == 'OK') {
                    $this->session->remove('user_data');
                    $this->session->set('app_data', $response->data);
                    $this->setLang($response->data->user->locale);
                    if (!$redirectURL) {
                        return $this->response->redirect('https://plugins.speakol.com/dashboard');
                    } else {
                        return $this->response->redirect($redirectURL);
                    }
                } else {
                    $this->flash->error($response->message);
                    return $this->response->redirect('apps/login');
                }
            }
        }
        $this->view->redirectURL = $redirectURL;
        $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
    }
    public function logoutAction() {
        $token = $this->getAppToken();
        $appToken = $this->getAppToken();
        if ($appToken) {
            $this->utility->iCurl('/user/signout', 'delete', array('Authorization:' . $appToken), NULL);
        }
        if ($token) {
            $this->utility->iCurl('/user/signout', 'delete', array('Authorization:' . $token), NULL);
        }
        $this->session->remove('user_data');
        $this->session->remove('app_data');
        $this->setLang('en');
        $this->session->destroy();
        $params = session_get_cookie_params();
        setcookie(session_name(), '', 0, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
        if ($this->request->isAjax()) {
            $this->response->setJsonContent(array('status' => 'OK', 'data' => true));
            return $this->response;
        } else {
            return $this->response->redirect($this->request->getHTTPReferer(), false, 301);
        }
    }
    public function settingsAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $app = $this->getAppData();
        $user = $this->session->get('app_data')->user;
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:xxxxxxxxxxx'), array());
        $categories = $result->messages;
        $categoryList = array('' => $this->t->_('choose-category'));
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $langs = array("en" => "English", "ar" => "العربية");
        $websites = $user->app->website;
        $websiteLimits = array(1 => 1, 2 => 2, 3 => 3, 4 => 4);
        unset($websites[1]);
        $this->view->planID = $user->app->plan_id;
        $this->view->websiteLimit = $websiteLimits[$this->view->planID];
        $this->view->websites = $websites;
        $this->view->websitesLength = count($websites);
        $this->view->setVar('langs', $langs);
        $this->view->setVar('categories', $categoryList);
        $this->view->app_id = $app->id;
        $this->view->newsletter = $app->newsletter;
        Tag::setDefault("app_name", $user->name);
        Tag::setDefault("category", $app->category_id);
        Tag::setDefault("lang", $this->getLang());
    }
    public function profileAction($appId) {
        if (!$appId || !ctype_digit($appId)) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong profile', 'code' => 400), 'newsfeed');
        }
        $this->loadCustomTrans('newsfeed');
        $this->loadCustomTrans('arguments');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->communityPage = true;
        $type = $this->request->get('type');
        $page = $this->request->get('page');
        $lastCreatedDate = $this->request->get('last_date');
        $page = $page ? $page : 1;
        $type = $type ? $type : 'all';
        $limit = 10;
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $hdrs = array('Authorization:xxxxxxxxxxxxxx');
            $this->view->isLoggedIn = false;
        } else {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $this->view->isLoggedIn = true;
        }
        $params = array('page' => $page, 'limit' => $limit, 'type' => $type,);
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        $response = $this->utility->iCurl('/feeds/profile/' . $appId, 'GET', $hdrs, $params);
        if ($response && $response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => '', 'code' => 400), 'newsfeed');
        }
        $appInfo = $response->data->info;
        $plugins = $response->data->info->plugins;
        $plugins = $this->formatPlugins($plugins);
        $this->view->plugins = $plugins;
        $this->view->appProfile = true;
        $this->view->appImage = $response->data->image;
        $this->view->appName = $response->data->name;
        $this->view->appId = $appId;
        $lastCreatedDate = $plugins[count($plugins) - 1]->created_at;
        $lastCreatedDate = str_replace(' ', '+', $lastCreatedDate);
        $url = $this->request->get('_url');
        $nextUrl = $url;
        $url.= '?page=' . ($page);
        $nextUrl.= '?page=' . ($page + 1);
        if ($type !== 'all') {
            $url.= '&type=' . $type;
            $nextUrl.= '&type=' . $type;
        }
        $url.= '&last_date=' . $lastCreatedDate;
        $nextUrl.= '&last_date=' . $lastCreatedDate;
        $url.= '&lang=' . $this->view->lang;
        $nextUrl.= '&lang=' . $this->view->lang;
        $this->view->url = $url;
        $this->view->nextUrl = $nextUrl;
        $this->view->type = $type;
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->view->superAdmin = $this->view->LoggedInUserData->role_id === 2;
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites;
            }
        } else {
            $objUserProfile = false;
        }
        $this->view->paginate = false;
        if ($this->request->isAjax()) {
            $this->view->highlightedPlugins = array();
            $this->view->paginate = true;
            $this->view->pick('partials/newsfeed/feed');
            return;
        }
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        $favorties = array();
        $categoriesData = $this->formatCategories($favorites);
        $this->view->appInteractions = $appInfo;
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->view->highlightedPlugins = array();
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        if ($this->view->superAdmin) {
            $this->assets->addJs('js/highlight.js?' . $this->config->application->cache_string);
        }
        $this->view->pick('apps/profile');
    }
    private function validateWebsites($websites = array()) {
        if (!is_array($websites) || !$websites) {
            $this->flash->error($this->t->_('websites-required'));
            return false;
        }
        $validWebsiteIncluded = false;
        foreach ($websites as $website) {
            if ($website && is_string($website)) {
                $validWebsiteIncluded = true;
            }
        }
        if (!$validWebsiteIncluded) {
            $this->flash->error($this->t->_('one-valid-website'));
            return false;
        }
        return true;
    }
}
