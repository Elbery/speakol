(function() {
    var Replies = function(lang) {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = lang;
        this.audio = $('#audioEnabled').length;
        this.recorder = speakolRecorder;
        this.timer = new Timer(45, undefined, function() {
            this.recorder.stop();
            var finishedText = this.timer.$display.attr('data-finished');
            this.timer.$display.html(finishedText);
        }.bind(this));
        this.bindEvents();
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };

    Replies.Events = {
        '.js-reply-thumbs-down': 'thumbsDown',
        '.js-reply-thumbs-up': 'thumbsUp',
        '.js-more-replies': 'showMore',
        '.js-show-nested-replies': 'showNestedReplies',
        '.js-more-nested-replies': 'showMoreNestedReplies',
        '.js-submit-reply': 'submitReply',
        '.js-reply-record-button': 'startRecording',
        '.js-reply-cancel-record': 'cancelRecording',
        '.js-reply-send-record': 'sendRecording'
    };

    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }
    Replies.prototype.bindEvents = function() {
        var _this = this;
        $.each(Replies.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };


    Replies.prototype.thumbsUp = function($el) {

        var action = "",
            replyId = $el.attr('data-reply-id'),
            lang = this.lang,
            url = this.url;
        $el.prop('disabled', true);
        if($el.attr('data-ad') === 'true') {
            action = '/ads/replies/' + replyId + '/thumb_up';
        } else {
            action = '/replies/' + replyId + '/thumb_up';
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: action,
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        })
        .done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                updateThumbs($el);
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 302) {
                $el.prop('disabled', false);
                removeThumb($el);
            } else {
                this.errorHandler(jqXHR, textStatus, errorThrown);
            }
        }.bind(this));
    };


    Replies.prototype.thumbsDown = function($el) {
        var action = "",
            replyId = $el.attr('data-reply-id'),
            lang = this.lang,
            url = this.url;
        $el.prop('disabled', true);
        if($el.attr('data-ad') === 'true') {
            action = '/ads/replies/' + replyId + '/thumb_down';
        } else {
            action = '/replies/' + replyId + '/thumb_down';
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: action,
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        })
        .done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                updateThumbs($el);
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 302) {
                $el.prop('disabled', false);
                removeThumb($el);
            } else {
                this.errorHandler(jqXHR, textStatus, errorThrown);
            }
        }.bind(this));
    };

    Replies.prototype.startRecording = function($el) {

      var $replyForm = $el.parents('.js-reply-form').first(),
          $recordBar = $replyForm.find('.js-record-bar').first(),
          $normalSubmit = $replyForm.find('.js-normal-submit').first(),
          $timer = $recordBar.find('.js-timer');
      $normalSubmit
      .addClass('hide')
      .find('input, button')
      .prop('disabled', true);
      resetForms();
      $recordBar
      .removeClass('hide')
      .find('button')
      .prop('disabled', false);
      $recordBar.find('.js-recording-text').removeClass('hide');
      this.timer.setDisplay($timer);
      var start = function() {
          this.timer.set();
          this.recorder.record();
      }.bind(this);

      if(this.recorder.initialized) {
          console.log(this.recorder.recording, "inside replies");
          if(this.recorder.recording) {
              this.recorder.stop();
          }
          start();
      } else {
          this.recorder.init(start);
      }
    };

    Replies.prototype.cancelRecording = function($el) {
        removeRecordBar($el);
        this.recorder.stop();
        this.timer.reset();
    };

    function resetForms() {
      $('.js-record-bar').each(function() {
          var $this = $(this);
          if(!$this.hasClass('hide')) {
              $this.find('.js-reply-cancel-record').trigger('click');
              $this.find('.js-cancel-record').trigger('click');
          }
      });
    }
    function removeRecordBar($el) {
        var $replyForm = $el.parents('.js-reply-form'),
            $recordBar = $replyForm.find('.js-record-bar'),
            $normalSubmit = $replyForm.find('.js-normal-submit');
      $normalSubmit
      .removeClass('hide')
      .find('input, button')
      .prop('disabled', false);
      $recordBar
      .addClass('hide')
      .find('button')
      .prop('disabled', true);
    }

    Replies.prototype.sendRecording = function($el, e) {
        $el.text('Sending....');
        this.recorder.stop();
        this.timer.reset();
        setTimeout(function() {
            $el.text('Send');
            removeRecordBar($el);
            if(this.recorder.error) {
                var errorMsg = this.lang === 'ar' ? 'لم تقم بالسماح للميكروفون بالعمل' : 'You haven\'t allowed the microphone';
                this.showErrorMsg(errorMsg); 
            } else {
                this.submitReply($el, e, true);
            }
        }.bind(this), 200);
    };


    Replies.prototype.showMore = function($el, e) {
        var params = {},
            $repliesContainer = $el.parents('.js-replies-container');
        params.argument_id = $repliesContainer.attr('data-argument-id');
        params.ad_id = ($el.attr('data-ad') === 'true') ? $repliesContainer.attr('data-ad-id') : "";
        params.page = +$repliesContainer.attr('data-comments-page') + 1;
        params.lang = this.lang;
        params.limit = 10;
        params.ajax =true;
        params.right = ($repliesContainer.parents('.js-side').attr('data-align') === 'right');
        params.comparison = ($repliesContainer.parents('.js-side').hasClass('js-comparison-side'));
        params.audio = this.audio;
        $el
        .attr('data-old', $.trim($el.text()))
        .text($el.attr('data-loading'));

        $.ajax({
            url: '/arguments/render/replies/',
            type: 'GET',
            dataType: 'json',
            data: params
        }).done(function(data) {
            data = data.data;
            if(data) {
                var $html = $('<div>' + data.html + '</div>');
                $repliesContainer.find('.js-replies').append($html.html());
                $repliesContainer.attr('data-comments-page', params.page);
                $el.text($el.attr('data-old'));
                if(!data.hasmore) {
                    $el
                    .addClass('hide')
                    .prop('disabled', true);
                }
                addReadMore();
            }
        }).fail(this.errorHandler.bind(this));
    };


    Replies.prototype.showNestedReplies = function($el) {
        var $parentReply = $el.parents('.js-reply').first(),
            $nestedRepliesContainer = $parentReply.find('.js-nested-replies-container').first(),
            $nestedRepliesShowMore = $nestedRepliesContainer.find('.js-more-nested-replies'),
            parentReplyId = $parentReply.attr('data-reply-id'),
            argumentId = $parentReply.attr('data-argument-id'),
            adId = $parentReply.attr('data-ad-id'),
            lang = this.lang,
            right = $nestedRepliesContainer.parents('.js-side').attr('data-align') === 'right', 
            comparison = $nestedRepliesContainer.parents('.js-side').hasClass('js-comparison-side'); 

        if(!$nestedRepliesContainer.hasClass('hide')) {
            $nestedRepliesContainer.addClass('opacity-0');
            setTimeout(function() {
                $nestedRepliesContainer.addClass('hide');
            },200);
            $nestedRepliesShowMore
            .removeClass('hide')
            .prop('disabled', false);
            $nestedRepliesContainer.attr('data-comments-page', 1);
            addReadMore();
            return;
        }

        $nestedRepliesContainer.removeClass('hide');
        setTimeout(function() {
            $nestedRepliesContainer.removeClass('opacity-0');
        }, 10);

        $.ajax({
            url: '/arguments/render/nested-replies/',
            type: 'GET',
            data: {
                'parent_reply': parentReplyId,
                'argument_id': argumentId,
                'ad_id': adId,
                'lang': lang,
                'ajax': false,
                'right': right,
                'comparison': comparison
            }
        }).done(function(data) {
            data = data.trim();
            if(data) {
                $nestedRepliesContainer
                .find('.js-nested-replies')
                .first()
                .append(data);
            }
        }).fail(this.errorHandler.bind(this));
    };


    Replies.prototype.showMoreNestedReplies = function($el) {
        var params = {},
            $nestedRepliesContainer = $el.parents('.js-nested-replies-container').first(),
            $parentReply = $nestedRepliesContainer.parents('.js-reply').first();
        params.argument_id = $parentReply.attr('data-argument-id'); 
        params.limit = 5;
        params.page = +$nestedRepliesContainer.attr('data-comments-page') + 1;
        params.ad_id = ($el.attr('data-ad') === 'true') ? $parentReply.attr('data-ad-id') : "";
        params.parent_reply = $parentReply.attr('data-reply-id');
        params.lang = this.lang;
        params.ajax = true;
        params.right = ($nestedrepliesContainer.parents('.js-side').attr('data-align') === 'right');
        params.comparison = ($nestedrepliesContainer.parents('.js-side').hasClass('js-comparison-side'));
        params.audio = this.audio;

        $el
        .attr('data-old', $.trim($el.text()))
        .text('Loading..');

        $.ajax({
            url: '/arguments/render/nested-replies/',
            type: 'GET',
            dataType: 'json',
            data: params
        }).done(function(data) {
            if(data) {
                var $html = $('<div>' + data.html + '</div>');
                $nestedRepliesContainer
                .find('.js-nested-replies')
                .first()
                .append($html.html()); 
                $el.text($el.attr('data-old'));
                if(!data.hasmore) {
                    $el.addClass('hide');
                    $el.prop('disabled', true);
                }
                $nestedRepliesContainer.attr('data-comments-page', params.page);
                addReadMore();
            }
        }).fail(this.errorHandler.bind(this));
    };

    Replies.prototype.submitReply = function($el, e, audio) {
        var $replyForm = $el.parents('form').first(),
            $replyFormContainer = $replyForm.parents('.js-reply-form').first(),
            $replyFormInputs = $replyForm.find('input, textarea'),
            $replyFileInput = $replyForm.find('input[type=file]'),
            argumentId = "",
            adId = "",
            parentReplyId = "",
            formData = new FormData(),
            postDataObject = {}, //this is for saving the last action in localstorage
            url = this.url,
            lang = this.lang,
            audioEnabled = this.audio;

        $replyForm.find('input, textarea, button').prop('disabled', true);
        audio = audio || false;
        if(audio && this.recorder.checkIfNoAudio()) {
            var message = lang === 'ar' ? 'لا يوجد صوت' : 'No audio detected';
            this.showErrorMsg(message);
            return;
        }
        if(!audio) {
            var file = $replyFileInput.prop('files')[0];
            if(!!file && file.size > 512 * 1024) {
                var message = lang === 'ar' ? 'يجب أن يكون حجم الصورة أقل من 512 كيلوبايت' : 'Image size must be smaller than 512 Kilobytes';
                this.showErrorMsg(message);
                return;
            }
            formData.append('file', file);
            postDataObject['file'] = file;
        } else {
            formData.append('audio', 1);
            postDataObject['audio'] = 1;
            var file = this.recorder.exportOgg();
            formData.append('file', file);
            postDataObject['file'] = file;
        }
        $replyFormInputs.each(function(index, element) {
            var $input = $(element);
            if($input.attr('type') !== 'file') {
                formData.append($input.attr('name'), $input.val());
                postDataObject[$input.attr('name')] = $input.val();
            }

            if($input.attr('name') === 'argument_id') {
                argumentId = $input.val();
            }

            if($input.attr('name') === 'ad_id') {
                adId = $input.val();
            }

            if($input.attr('name') === 'parent_reply') {
                parentReplyId = $input.val();
            }

        }.bind(this));


        if($replyForm.attr('data-ad') === 'true') {
            formData.append('action', '/ads/replies/create');
            postDataObject['action'] = '/ads/replies/create';
        } else {
            formData.append('action', '/replies/create');
            postDataObject['action'] = '/replies/create';
        }

        formData.append('link', phpVars.url ? phpVars.url : '');
        formData.append('lang', lang);
        postDataObject['lang'] = lang;

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            postDataObject: postDataObject,
        }).then(function(data) {
            if(data.status === 'OK') {
                $replyForm.find('textarea[name="context"]').val('');
                var replyId = data.data.id;
                var right = $replyForm.parents('.js-side').attr('data-align') === 'right';
                var comparison = $replyForm.parents('.js-side').hasClass('js-comparison-side');

                return $.ajax({
                    url: '/arguments/render/reply',
                    type: 'GET',
                    data: {
                        'ad_id': adId,
                        'argument_id': argumentId,
                        'reply_id': replyId,
                        'parent_reply': parentReplyId,
                        'nested': parentReplyId ? 1 : 0,
                        'lang': lang,
                        'right': right,
                        'audio': audioEnabled
                    }
                });
            }
        }).done(function(data) {
            var $repliesList = "";
            if(parentReplyId) {
                $repliesList = $replyFormContainer.parents('.js-nested-replies-container').first().find('.js-nested-replies').first();
            } else {
                $repliesList = $replyFormContainer.parents('.js-replies-container').first().find('.js-replies').first();
            }

            $repliesList.prepend(data);
            addReadMore();
        }).fail(this.errorHandler.bind(this)).always(function() {
            $replyForm.find('input, textarea, button').prop('disabled', false);
        });
    };



    Replies.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler(jqXHR.settings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.messages,
            403: response.messages,
            404: t.rnf,
            409: response.messages,
            500: response.messages
        };
        this.showErrorMsg(errorMsgs[status]);
    };


    Replies.prototype.showErrorMsg = function(message) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-reply-thumbs-up, .js-reply-thumbs-down').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(message);
        $('#errorbox').modal('show');
    };

    Replies.prototype.unauthorizedHandler = function(ajaxSettings) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-reply-thumbs-up, .js-reply-thumbs-down').prop('disabled', false);
        if(ajaxSettings) {
            var lastAction = {
                url: ajaxSettings.url,
                type: ajaxSettings.type,
                dataType: ajaxSettings.dataType,
                data: ajaxSettings.data,
                processData: ajaxSettings.processData,
                contentType: ajaxSettings.contentType,
            };

            if(ajaxSettings.postDataObject) {
                var postDataObject = ajaxSettings.postDataObject;
                if(postDataObject['file']) {
                    var fileReader = new FileReader();
                    fileReader.onload = function(e) {
                        postDataObject['file'] = e.target.result;
                        lastAction.postDataObject = postDataObject;
                        localStorage.setItem('lastAction', JSON.stringify(lastAction));
                    }
                    fileReader.readAsDataURL(postDataObject['file']);
                } else {
                    lastAction.postDataObject = ajaxSettings.postDataObject;
                    localStorage.setItem('lastAction', JSON.stringify(lastAction));
                }
            } else {
                localStorage.setItem('lastAction', JSON.stringify(lastAction));
            }

        }
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    function removeThumb($el) {
        var numThumbs = +$el.attr('data-thumbs'),
            $elText = $el.find('span'),
            isThumbsUp = $el.hasClass('js-reply-thumbs-up'),
            colorClass = isThumbsUp ? 'color-5bb767' : 'color-d11b1f';
        numThumbs = numThumbs - 1;
        $el.removeClass('js-voted')
            .removeClass(colorClass)
            .addClass('color-c1cacf')
            .attr('data-thumbs', numThumbs);
        $elText.text(numThumbs);
    }


    function updateThumbs($el) {
        var $replyActions = $el.parents('.js-reply-actions'),
            numThumbs = +$el.attr('data-thumbs'),
            $elText = $el.find('span'),
            isThumbsUp = $el.hasClass('js-reply-thumbs-up'),
            oppositeClass = isThumbsUp ? 'js-reply-thumbs-down' : 'js-reply-thumbs-up',
            $oppositeEl = $replyActions.find('.' + oppositeClass);
            colorClass = isThumbsUp ? 'color-5bb767' : 'color-d11b1f',

        numThumbs = numThumbs + 1;
        $elText.text(numThumbs);
        $el.addClass('js-voted')
            .addClass(colorClass)
            .removeClass('color-c1cacf')
            .attr('data-thumbs', numThumbs);
       if($oppositeEl.hasClass('js-voted')) {
           var oppositeThumbs = +$oppositeEl.attr('data-thumbs');
           var oppositeColorClass = isThumbsUp ?  'color-d11b1f':'color-5bb767' ;
           var $oppositeElText = $oppositeEl.find('span');
           oppositeThumbs--;
           $oppositeEl.removeClass('js-voted')
                        .removeClass(oppositeColorClass)
                        .addClass('color-c1cacf')
                        .attr('data-thumbs', oppositeThumbs);
          $oppositeElText.text(oppositeThumbs);
       }
    }

    var replies = new Replies(phpVars.lang);
})();
