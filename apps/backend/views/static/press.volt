<style>

    li {
        list-style-type: decimal;
    }
</style>
<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">


        <div class="story_container static_content">


            <h2>Speakol Press Release - 5 November 2014, Web Summit '14, (Dublin):</h2>   
            {{ linkTo({'downloads/speakol_logo.zip', 'Download Speakol Logo','class':'grnbtn', 'title': 'Download Speakol Logo'}) }}

            <p class="large">Speakol has launched a new plugin that provides media publications, content providers, popular brands and start-ups an enhanced user interaction with topical media content and new product launches.</p>

            <p class="large">Speakol - Speak Out Loud - was originally launched in 2012 as 'Naqeshny' (meaning debate with me in Arabic), in the midst of the Egyptian Revolution. It was designed as a free, democratic and open platform which facilitated healthy and critical political debates and discussions around socio-political conditions within the country.</p>

            <p class="large">With a 400k+ strong user base, the name change and relaunch corresponds to Speakol’s vision of going global.</p>

            <p class="large">Their priorities in the immediate future will be to provide a unique service to media content publishers and brands in the form of three plugins: Debates, Argument Boxes and Comparisons (where users can debate between two presented sides to a story, voting up or down on an issue, or choose between up to four given options on any topic - respectively).</p>

            <p class="large">They already have 80+ publishers using the service in Egypt.</p>

            <p class="large">
            <h2>10 Benefits of Using Speakol</h2>

            <ol>
                <li>Seamlessly create a user profile via an easy to use registration page</li> 
                <li>Straight forward integration into any CMS via embeddable HTML codes</li>
                <li>Create real-time debates and comparisons related to your published content via their well structured and user friendly interface</li>
                <li>Enhance, change and encourage user interaction with your content and gain invaluable and measurable sentiment around any topic that you publish</li>
                <li>Easily share debates to traditional social networks such as Facebook, Twitter and Google+</li>
                <li>Access analytical data instantly via their simple to use dashboard</li>
                <li>Retain users of your website via convenient user notification systems</li>
                <li>Use alongside your existing commenting systems (not to replace them)</li>
                <li>Easily remove with no impact on your website functionality</li>
                <li>Free to use (premium subscriptions with enhanced services to launch within 6-12 months)</li>
            </ol>
            </p>
            <p class="large">
                Speakol has recently launched an arm in the United Kingdom aiming to provide local content providers and brands their service, and are aiming to create similar worldwide partnerships and collaborations with interested parties. 
            </p>
            <p class="large">  They will also be relaunching their social network where any user can login, build a profile and proceed to create debates and discussions with the wider public on an unlimited number of topics of their interest.
            </p>
            <p class="large"> Bootstrapped to date with USD300K and with a current 15 strong staff, Speakol are building on their success in Egypt and aim to change how users interact with online content in a unique, structured, civilized and different manner. 
            </p>
        </div>

    </div>
</div>