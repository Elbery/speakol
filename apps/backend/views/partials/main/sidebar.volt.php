
<?php $controller = $this->dispatcher->getControllerName(); ?>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar_brand"><?php $speakolLogo = $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?><?php echo $this->tag->linkto('index', $speakolLogo); ?></li>
        <?php if ($this->session->app_data) { ?>
        <li class="sidebar_dshbrd <?php echo ($controller == 'dashboard' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('dashboard', 'Dashboard'); ?></li>
        <li class="sidebar_dbt <?php echo ($controller == 'debates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('debates/code', 'Create Debate'); ?></li>
        <li class="sidebar_cmprsn <?php echo ($controller == 'comparisons' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('comparisons/code', 'Create Comparison'); ?></li>
        <li class="sidebar_argmnt <?php echo ($controller == 'arguments' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('arguments/code', 'Create Argument'); ?></li>
        <li class="sidebar_mdrt <?php echo ($controller == 'moderates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('moderates/index', 'Moderate'); ?></li>
        <li class="sidebar_anlsys <?php echo ($controller == 'analysis' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('analysis', 'Analysis'); ?></li>
        <li class="sidebar_aignout"><?php echo $this->tag->linkto('apps/logout', 'Sign Out'); ?></li>
        <?php
} ?>
    </ul>
</nav>