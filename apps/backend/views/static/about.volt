<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">


        <div class="story_container static_content">
            <h1>What is Speakol?</h1>
            <p class="large">With Speakol you’re a few clicks away from engaging a special kind of community on your website.</p>
            <p class="large">Speakol - <i>Speak Out Loud</i> - was originally launched in 2012 as Naqeshny (meaning <i>debate with me</i> in Arabic), in the midst of the Egyptian Revolution. It was designed as a free, democratic and open platform which facilitated healthy and critical political debates and discussions around socio-political conditions within the country.</p>
            <p>Speakol helps you - the media publisher or brand - build a social community of debaters around your content, driving real-time engagement and interaction on any topic on your site.</p>
            <h2>10 Benefits of Using Speakol</h2>
            <ol>
                <li><p class="normal">1. Seamlessly create a user profile via our easy to use registration page.</p></li>
                <li><p class="normal">2. Straight forward integration into any CMS via embeddable HTML codes.</p></li>
                <li><p class="normal">3. Create debates and comparisons related to your published content via our well structured and user friendly interface.</p></li>
                <li><p class="normal">4. Enhance, change and encourage user interaction with your content and gain invaluable and measurable sentiment around any topic that you publish.</p></li>
                <li><p class="normal">5. Easily share debates to traditional social networks such as Facebook, Twitter and Google+.</p></li>
                <li><p class="normal">6. Access analytical data instantly via our simple to use dashboard.</p></li>
                <li><p class="normal">7. Retain users of your website via convenient user notification systems.</p></li>
                <li><p class="normal">8. Use alongside your existing commenting systems (not to replace them).</p></li>
                <li><p class="normal">9. Easily remove with no impact on your website functionality.</p></li>
                <li><p class="normal">10. Free to use (premium subscriptions with enhanced services to launch within 6-12 months).</p></li>
            </ol>
            <h1>Speakol Social Network...COMING SOON!</h1>
            <p class="normal">Speakol will soon be launching a social network where users can create, join and spread topical debates and discussions with your friends and networks. </p>
            <p class="normal">Inside Speakol, you can create, join and share:</p>
            <ol>
                <li>1. Debates <br /><p class="normal">Create and join discussions around topics of your choice, pick a side and support your views.</p></li>
                <li>2. Comparisons <br /><p class="normal">Create comparisons on anything from political parties to tech gadgets, vote among up to four items and explain your opinions.</p></li>
            </ol>

        </div>

    </div>
</div>
