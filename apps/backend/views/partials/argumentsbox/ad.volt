<div class="sp-block-group js-argument argument" data-ad-id="{{ ad.id }}" data-owner-id="{{ ad.owner_id  }}">
    <div class="sp-block-group no-margin">
        <div class="media sp-block eleven {{ right ? 'one-push' : '' }}">
          <div class="{{ right ? "pull-right" : "pull-left" }}">
                {% if ad.owner_photo is defined and ad.owner_photo != "" %}
                    <a href="javascript:"><img class="media-object profile-picture-sm radius-5" src="{{ad.owner_photo}}" alt="" title=""></a>
                {% else %}
                    <a href="javascript:">{{ image("img/user_default.gif" , 'class': 'media-object profile-picture-sm radius-5') }}</a>
                {% endif %}
          </div>
          <div class="media-body">
                {% if right %}
                    {% set headingContent = '<i class="fa fa-check-circle font-11 color-5bb767"></i>&nbsp;' ~ ad.owner_name %} 
                {% else %}
                    {% set headingContent = ad.owner_name ~  '&nbsp;<i class="fa fa-check-circle font-11 color-5bb767"></i>' %} 
                {% endif %}
                {% if ad.url %}
                <a href="{{ utility.isFullURL(ad.url) ?   ad.url : 'http://' ~ ad.url }}" target="_blank" class="media-heading h5 font-13-xs light-blue display-block {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                </a>
                {% else %}
                <h4 class="media-heading h5 font-13-xs light-blue {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                </h4>
                {% endif %}
                <p class="font-11 color-aebac1 {{ right ? "text-right" : "" }} {{ lang === 'ar' ? 'rtl' : '' }}">{{ t._('on') }} {{ ad.created_at }}</p>
          </div>
        </div>
    </div>
    <div class="sp-block-group no-margin">
        <p class="readmore font-13 break-word no-margin">{{ utility.insertHashtagHTML(ad.context) }}</p>
        {% if ad.attached_photos %}
        {% for file in ad.attached_photos %}
            {% set extension = substr(file.url, -3)  %}
            {% if extension === "ogg"  %}
                <div class="js-player player">
                    <input type="hidden" value="{{ file.url  }}">
                    <button class="js-play-button play-button clearfix">
                        <span class="play-icon fa fa-play pull-left"></span>
                        <span class="play-text font-13 pull-left" data-text="{{ t._('click-play')  }}" data-text-small="{{ t._('click-play-small') }}"></span>
                    </button>
                </div>
            {% else %}
             {{ image(file.url, "class":"context_img margin-top-sm") }}
            {% endif %}
        {% endfor %}
        {% endif %}
    </div>

    <div class="sp-block-group text-center margin-bottom-sm h6 js-argument-actions margin-top-sm">
        <div class="sp-block four-xs three-sm  vertical-separator-right two {{comparison ? 'comparison-thumbs-button' : ''}} text-left text-center-xs text-center-sm">
            <button data-ad-id="{{ ad.id }}" class="js-thumbs-up btn-reset no-padding {{ (ad.currentUserhasVoted and ad.currentUserVote) ? "js-voted color-5bb767" : "color-c1cacf"  }}" title="{{ t._('vote_up') }}" data-thumbs="{{ ad.thumbs_up }}" data-ad="true">
                <i class="sp-icon sp-thumbs-up"></i>&nbsp;
                <span>{{ ad.thumbs_up}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm two {{ comparison ? 'comparison-thumbs-button' : ''  }} text-right text-center-xs text-center-sm">
            <button data-ad-id="{{ ad.id }}" class="js-thumbs-down btn-reset no-padding {{ (ad.currentUserhasVoted and !ad.currentUserVote) ? "js-voted color-d11b1f" : "color-c1cacf"  }}" title="{{ t._('vote_down') }}" data-thumbs="{{ ad.thumbs_down }}" data-ad="true">
                <i class="sp-icon sp-thumbs-down"></i>&nbsp;
                <span>{{ ad.thumbs_down}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm three-offset-sm {{ comparison ? 'two comparison-reply-button' : 'two six-offset-lg' }} text-right">
            <a class="color-c1cacf js-show-replies show-replies hover-c1cacf" href="javascript:" data-ad="true"><span>{{ad.replies_count}}</span>&nbsp;<i class="sp-icon sp-comment"></i></a>
        </div>
    </div>
    {% set argumentsBoxURL = "" %}
    {% if ad.replies is defined %}
        {% include "partials/argumentsbox/replies-container" with ['adID': ad.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies': ad.replies, 'replies_count' : ad.replies_count, 'total_replies_pages': ad.total_replies_pages is defined?ad.total_replies_pages:0 ] %}
    {% else %}
        {% include "partials/argumentsbox/replies-container" with ['adID': ad.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies_count' : ad.replies_count, 'total_replies_pages': ad.total_replies_pages is defined?ad.total_replies_pages:0] %}
    {% endif %}
</div>
