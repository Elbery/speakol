<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}

                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}
        {{ assets.outputJs('header') }}
        <!--[if lt IE 9]>
        {{ assets.outputJs('header-ie9') }}
    <![endif]-->
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">
        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

{% if authorized %}
{% if comparison.audio %}
    <input type="hidden" id="audioEnabled">
{% endif %}
        <input id="url" type="hidden" name="" value="{{ comparison.url  }}">
        {% if comparison.hide_comment %}
            <input id="hide_comments" type="hidden" name="hide_comments">
        {% endif %}
        <div class="speakol-container container-fluid no-padding">
            <div class="sp-block-group menu-bar padding-top-xs padding-bottom-sm {{ newsfeed ? '' : 'dropdown-underline' }} width-90 margin-center">
        <div class="five-xs sp-block seven">
            {% if isLoggedIn and not newsfeed %}
            <div class="dropdown plugin-dropdown">
                <button class="padding-left-0 btn btn-default dropdown-toggle no-radius" type="button" id="userDropdown" data-toggle="dropdown" aria-expanded="true">
                    {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                        {{ image( LoggedInUserData.profile_picture, 'class': 'radius-5 profile-picture-sm') }}
                    {% else %}
                        {{ image("img/user_default.gif", 'class': 'radius-5 profile-picture') }}
                    {% endif %}
                    <span class="{{ lang === 'ar' ? 'rtl inline-block' : '' }}">
                        <span class="hide-xxs">{{ t._('hello') }}, </span>{{ LoggedInUserData.name }}
                    </span>
                    <i class="fa fa-caret-down color-c1cacf"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius" role="menu" aria-labelledby="userDropdown">
                    {% if comparison.app_id === LoggedInUserData.app.id and LoggedInUserData.app.plan_id > 1 %}
                        <li role="presentation" id=""><a role="menuitem" tabindex="-1" href="/comparisons/edit?slug={{ comparison.slug }}" target="_blank">{{ t._('edit') }}</a></li>
                    {% endif %}
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ logout }}">{{ t._('logout') }}</a></li>
                </ul>
            </div>
            {% endif %}
            {% if newsfeed %}
                <div class="media">
                    <div class="pull-left">
                        <img src="{{ comparison.app_image }}" class="publisher-image radius-5" alt="...">
                    </div>
                    <div class="media-body">
                        <h3 class="font-15 no-margin margin-bottom-xs">
                            <a class="anchor-reset light-blue" href="/apps/profile/{{ comparison.app_id }}" target="_blank"><span class="weight-bold">{{ comparison.app_name }}</span></a> {{ t._('posted-comparison') }}</h3>
                        <p class="h6 color-777777">{{ utility.formatDate(comparison.created_at, 'F jS, Y \a\t h:i A') }}</p>
                    </div>
                </div>
            {% endif %}
        </div>
        <div class="seven-xs sp-block five">
            {% if not newsfeed %}
            <div class="dropdown plugin-dropdown pull-right padding-top-sm">
                <button class="btn-reset margin-top-xs dropdown-toggle no-radius js-notifications-button" type="button" id="notificationDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-bell sp-icon-lg color-9ca7ad"></i>
                </button>
                {% if isLoggedIn %}
                <ul class="tip notification-plugin-tip border-none dropdown-menu right-0 left-auto no-shadow no-radius color-9ca7ad js-notifications notifications-dropdown bg-f0f0f0" role="menu" aria-labelledby="notificationDropdown">
                </ul>
                {% endif %}
            </div>
            {% if isLoggedIn %}
                <div class="hide js-notification-indicator notification-indicator pull-right radius-5 bg-bb1111 white padding-left-5 padding-right-5 weight-bold">0</div>
            {% endif %}
            {% endif %}
            <div class="dropdown plugin-dropdown pull-right {{ newsfeed ? '' : 'padding-top-sm' }}">
                <button class="btn btn-default dropdown-toggle no-radius" type="button" id="shareDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-icon-lg sp-share"></i>
                </button>
                <ul class="dropdown-menu no-shadow right-0 left-auto no-radius color-9ca7ad" role="menu" aria-labelledby="shareDropdown">
                                <li role="prensentation"><a class="" href="https://www.facebook.com/sharer/sharer.php?u={{ url  }}" title="{{ t._('share-on-facebook') }}" target="_blank"><i class="fa fa-facebook"></i>&nbsp;{{ t._('share-on-facebook') }}</a></li>
                                <li role="prensentation"><a class="" href="https://twitter.com/home?status={{ url }}" title="{{ t._('tweet') }}" target="_blank"><i class="fa fa-twitter"></i>&nbsp;{{ t._('tweet') }}</a></li>
                                <li role="prensentation"><a class="" href="https://plus.google.com/share?url={{ url }}" title="{{ t._('share-on-google-plus') }}" target="_blank"><i class="fa fa-google-plus"></i>&nbsp;{{ t._('share-on-google-plus') }}</a></li>
                                <li role="prensentation"><a class="" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url }}" title="{{ t._('share-on-linkedin') }}" target="_blank"><i class="fa fa-linkedin"></i>&nbsp;{{ t._('share-on-linkedin') }}</a></li>
                </ul>
            </div>            
            <div class="dropdown plugin-dropdown pull-right {{ newsfeed ? '' : 'padding-top-sm' }}">
                <button class="btn btn-default dropdown-toggle no-radius js-current-sort" type="button" id="sortDropdown" data-toggle="dropdown" aria-expanded="true" data-value="most_voted">
                    <i class="sp-icon sp-sort sp-icon-lg color-9ca7ad"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius right-0 left-auto" role="menu" aria-labelledby="sortDropdown">
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="most_voted"><i class="fa fa-2x fa-area-chart"></i>&nbsp;{{ t._('most-voted') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="desc"><i class="fa fa-2x fa-sort-amount-desc"></i>&nbsp;{{ t._('desc') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="asc"><i class="fa fa-2x fa-sort-amount-asc"></i>&nbsp;{{ t._('asc') }}</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    {%if newsfeed %}
        <div class="sp-block-group text-left width-90 margin-center">
            <h3 class="font-20 margin-top-xs color-636363">
                <i class="speakol-icon speakol-comparison-green newsfeed-margin"></i>&nbsp;{{ comparison.title }}
            </h3>
        </div>
    {% else %}
            <div class="sp-block-group width-90 margin-center text-center margin-height-30">
                <h3 class="font-20">{{ comparison.title }}</h3>
                {% if edited %}
                    <p class="js-edited-indicator cursor-pointer bg-f5f5f5 inline-block no-margin-height margin-center font-13 color-999999 margin-top-sm radius-5 padding-top-xs padding-bottom-xs padding-left-10 padding-right-10">{{ t._('edited') }}</p>
        <div class="js-edited-date bg-f5f5f5 margin-center color-999999 hide four relative radius-5 updated-date padding-top-sm padding-bottom-sm h6">{{ comparison.updated_date }}</div>
                {% endif %}
            </div>
    {% endif %}
            <div class="sp-block-group margin-center width-92">
                {% set classes = [ '2' : 'six-xl six-lg', '3' : 'four-xl four-lg', '4': 'three-xl three-lg']%}
                {% for side in comparison.sides %}
                    {% set meta = side.meta[0] %}
                    <div class=" sp-block  {{ classes[loop.length] }} hide-xxs hide-xs {{ loop.length > 4 or (not comparison.align) ? 'hide' : '' }}">
                        <div class="js-comparison-tab comparison-tab bg-f5f5f5 width-90 margin-center padding-bottom-sm padding-top-sm radius-5-top" data-side-id="{{ side.id }}">
                            {% if meta.image %}
                            <div class="sp-block-group text-center padding-top-sm">
                                <img src="{{meta.image}}" alt="" class="profile-picture-xl radius-5"/>
                            </div>
                            {% endif %}
                            <div class="sp-block-group text-center">
                                <h4 class="font-16 light-blue comparison-title">{{meta.title}}</h4> 
                                <div class=" margin-center ten margin-top-lg {{ lang === 'ar' ? 'flip' : '' }}">
                                    <div class="comparison-progress-container">
                                        <div class="comparison-progress bg-green relative">
                                            <div 
                                                class="comparison-progress-indicator bg-ececec js-progress-bar"
                                                style="left: {{ side.perc }}%;"
                                                data-side-id="{{ side.id }}"
                                                aria-valuenow="{{ side.perc }}"
                                                aria-valuemin="0"
                                                aria-valuemx="100"
                                            >
                                                <div 
                                                    class="
                                                    js-progress-indicator
                                                    comparison-progress-text 
                                                    bg-green white 
                                                    radius-5 
                                                    text-center 
                                                    {{ lang === 'ar' ? 'flip' : '' }}
                                                    "
                                                    data-side-id="{{ side.id }}"
                                                >
                                                    {{ side.perc }}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="{{ side.voted ? 'js-unvote bg-b5b5b5 white' : 'js-vote bg-white color-5bb767' }} btn-reset border-e0e5e0 radius-5 {{ lang === 'ar' ? 'font-11 line-height-inherit eight rtl' : 'font-13 seven' }} padding-top-xs padding-bottom-xs margin-top-sm" data-side-id="{{ side.id }}" data-comparison-id="{{ comparison.id }}">
                                    <i class="fa {{ side.voted ? 'fa-times' : 'fa-thumbs-up' }}"></i>&nbsp;{{ side.voted ? t._('voted') : t._('vote') }}
                                </button>
                                <p class="font-13 margin-top-xs {{ lang === 'ar' ? 'rtl' : '' }}">
                                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span>&nbsp;{{ t._('votes') }}
                                </p>
                            </div>
                            
                        </div>
                    </div>
                {% endfor %}
            </div>
                <div class="sp-block-group margin-center {{ comparison.align ? 'width-92' : '' }}">
                    {% for side in comparison.sides %}
                    {% set meta = side.meta[0] %}                
                            {% set classes = [ '2' : 'six-xl six-lg', '3' : 'four-xl four-lg', '4': 'three-xl three-lg']%}
                        <div class="js-side js-comparison-side sp-block margin-bottom-xs margin-top-xs {{ loop.length < 5 and comparison.align ? classes[loop.length] ~ " js-vertical" : "" }}" data-side-id="{{ side.id }}" data-comments-page="1" {{ lang === 'ar' ? 'data-align="right"' : '' }}>
                            <div class="js-comparison-accordion comparison-accordion radius-5 media bg-f5f5f5 width-90 {{ comparison.align ? 'width-all-xs width-all-sm': '' }} margin-center  {{ loop.length < 5 and comparison.align ? 'hide-xl hide-lg' : '' }}" data-side-id="{{ side.id }}">
                                {% if meta.image %}
                                <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
                                    <img src="{{meta.image}}" alt="" class="profile-picture-xl radius-5"/>
                                </div>
                                {% endif %}
                                <div class="media-body">
                                    <div class="sp-block-group margin-bottom-xs {{ lang === 'ar' ? 'text-right' : 'text-left' }}">
                                            <h4 class="font-20 light-blue no-margin margin-bottom-sm">{{meta.title}}</h4> 
                                    </div>
                                <div class="{{ lang === 'ar' ? 'flip' : '' }}">
                                    <div class="comparison-progress-container">
                                        <div class="comparison-progress bg-green relative">
                                            <div 
                                                class="comparison-progress-indicator bg-ececec js-progress-bar"
                                                style="left: {{ side.perc }}%;"
                                                data-side-id="{{ side.id }}"
                                                aria-valuenow="{{ side.perc }}"
                                                aria-valuemin="0"
                                                aria-valuemx="100"
                                            >
                                                <div 
                                                    class="
                                                    js-progress-indicator
                                                    comparison-progress-text 
                                                    bg-green white 
                                                    radius-5 
                                                    text-center 
                                                    {{ lang === 'ar' ? 'flip' : '' }}
                                                    "
                                                    data-side-id="{{ side.id }}"
                                                >
                                                    {{ side.perc }}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="sp-block-group margin-top-sm margin-bottom-sm">
                                        <div class="sp-block six-xs {{ lang === 'ar' ? 'four-sm rtl nine-push eight-push-sm push-six-xs three text-right' : 'four-sm width-20' }}">
                                            <button class="{{ side.voted ? 'js-unvote bg-b5b5b5 white' : 'js-vote bg-white color-5bb767' }} btn-reset border-e0e5e0 radius-5 font-13 width-all padding-top-xs padding-bottom-xs {{ lang === 'ar' ? 'text-right' : '' }}" data-side-id="{{ side.id }}" data-comparison-id="{{ comparison.id }}">
                                                <i class="fa {{ side.voted ? 'fa-times' : 'fa-thumbs-up' }}"></i>&nbsp;{{ side.voted ? t._('voted') : t._('vote') }}
                                            </button>
                                        </div>
                                        <div class="sp-block six-xs two-sm two text-center h6 padding-top-sm {{ lang === 'ar' ? 'four-push rtl two-push-sm reset-left-xs pull-six-xs' : '' }}">
                                            <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span>&nbsp;{{  t._('votes') }}
                                        </div>
                                        <div class="sp-block seven five-sm js-small-argument-form hide-xxs hide-xs {{ lang === 'ar' ? 'five-pull six-pull-sm' : '' }}">
                                            <div class="media">
                                                <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
                                                {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                                                    {{ image(LoggedInUserData.profile_picture, 'class': 'radius-5 textarea-image') }}
                                                {% else %}
                                                    {{ image("img/user_default.gif", 'class': 'radius-5 textarea-image') }}
                                                {% endif %}
                                                </div>
                                                <div class="media-body">
                                                        {{ text_area("comment", "placeholder": t._('comparison-comment-placeholder'), "class":"no-resize no-padding width-all small-textarea " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "1") }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="js-comparison-content sp-block-group radius-5-bottom border-ededed {{ comparison.align ? 'comparison-content-vertical' : 'comparison-content-horizontal'}} {{ comparison.hide_comment ? 'hide' : '' }} {{ lang === 'ar' ? 'lang-ar' : '' }}" data-side-id="{{ side.id }}">
                            <div class="js-comparison-description sp-block-group hide font-13 color-cacccd margin-bottom-lg argument {{ lang === 'ar' ? 'rtl' : '' }}" data-side-id="{{ side.id }}">
                                    {%set meta = side.meta[0]%}
                                    {{ meta.description }}
                            </div>
                                    <div class="sp-block-group comparison-arguments">
                                        <div class="js-argument-form-container hide">
                                            <div class="{{ lang === 'ar' ? 'rtl' : '' }} sp-block-group thin-underline thin-overline padding-top-xs padding-bottom-xs argument margin-top-xs">
                                                {{  side.arguments_count ~ ' ' ~ t._('Comments')}} 
                                            </div>
                                            <div class="media argument">
                                                <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
                                                {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                                                    {{ image(LoggedInUserData.profile_picture, 'class': 'radius-5 profile-picture-sm') }}
                                                {% else %}
                                                    {{ image("img/user_default.gif", 'class': 'radius-5 profile-picutre-sm') }}
                                                {% endif %}
                                                </div>
                                                <div class="media-body js-argument-form">
                                                    <div class="sp-block-group">
                                                        {{ text_area("comment", "placeholder": t._('comparison-comment-placeholder'), "class":"no-resize no-padding width-all " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "4") }}

                                                    </div>
                                                    <div class="sp-block-group margin-top-sm">
                                                        <div class="sp-block-group text-right js-normal-submit">
                                                            {% if comparison.audio %}
                                                            <button class="js-record-button padding-top-xs padding-bottom-xs shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm five font-13 sp-block {{ lang === 'ar' ? 'rtl' : '' }}">
                                                                <span class="fa-stack">
                                                                    <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                                                    <i class="fa fa-microphone fa-stack-1x white"></i>
                                                                </span>
                                                                <span class="hide-xxs hide-xs">{{ t._('audio-comment') }}</span>
                                                            </button>
                                                            {% else %}
                                                            <div class="four-xs four-sm five font-13 sp-block {{ lang === 'ar' ? 'rtl' : '' }}">
                                                            </div>
                                                            {% endif %}
                                                            <div class="js-image-form">
                                                                <div class="sp-block  two-offset-xs two-xs one-offset-sm three-sm two three-offset-lg two-offset-xl js-image-upload-container text-center">
                                                                    <span class="fileUpload inline-block btn-reset margin-top-xs">
                                                                        <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                                                        {% if LoggedInUserData %}
                                                                            {% set classes = 'js-image-upload' %}
                                                                        {% else %}
                                                                            {% set classes = 'js-image-loggedout' %}
                                                                        {% endif %}
                                                                        {{ file_field(
                                                                            "attached",
                                                                            "class": classes,
                                                                            "accept":".png,.jpeg,.jpg,.gif"
                                                                        ) }}
                                                                    </span>
                                                                </div>
                                                                <div class="sp-block two two-offset-xs three-offset-lg one-offset-sm two-offset-xl one-offset-xs two-xs three-sm js-image-container hide text-center">
                                                                    <button class="btn-reset js-delete-image delete-image-comparison absolute"><i class="fa fa-times"></i></button>
                                                                    <img src="" alt="" class="js-image profile-picture-sm">
                                                                </div>
                                                            </div>

                                                            <div class="sp-block two four-xs four-sm">
                                                                {{ submit_button("post", "value": t._('sbmt-arg'), "class":"js-submit-arg btn-reset font-13 btn-default bg-4e4e4e width-all") }}
                                                            </div>
                                                        </div>
{% if comparison.audio %}
<div class="js-record-bar hide record-bar sp-block-group h5 padding-bottom-xs padding-top-xs">
        <div class="sp-block seven twelve-xs timer white margin-top-xs text-left padding-left-5">
            <i class="fa fa-microphone weight-bold"></i>
            <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
            <span class="hide-xxs hide-xs js-recording-text"> - {{ t._('recording') }} </span>
        </div>
        <div class="sp-block five twelve-xs record-buttons text-right padding-right-5">
            <button class="js-send-record  six-xs send-record btn-reset radius-5 padding-top-xs padding-bottom-xs">{{ t._('send') }}</button>
            <button class="js-cancel-record float-right-xs two-xs cancel-record btn-reset radius-5 padding-top-xs padding-bottom-xs">
                    &#10005;
            </button>
        </div>
</div>
{% endif %}

                                                    </div>
                                                    {{ hidden_field("container_id", "value": comparison.id) }}
                                                    {{ hidden_field("container_type", "value": comparison.type) }}
                                                </div>
                                            </div>
                                        </div>
                                        <ul>
                                            {% if comparison.ads is defined %}
                                                {% set right = lang === 'ar' %}
                                                {%set ad = comparison.ads[loop.index0] %}
                                                {% if ad is defined %}
                                                <li>
                                                    {% include "partials/argumentsbox/ad" with ['ad': ad, 'argumentsBoxURL':argumentsbox.url, 'right': right] %}
                                                </li>
                                                {% endif %}
                                            {% endif %}
                                        </ul>
                                        <ul class="js-arguments">
                                            {% set right = lang === 'ar' %}
                                            {% for argument in side.args.args %}
                                                {% include "partials/argumentsbox/argument" with ['comparison':comparison, 'argument':argument, 'right': right] %}
                                            {% endfor %}
                                        </ul>
                                    </div>
                                    {%  if side.args.hasmore is defined and side.args.hasmore %}

                                    <div class="sp-block-group text-center {{ lang === 'ar' ? 'rtl' : '' }} ">
                                        <button class="js-show-more  bg-f5f5f5 font-11 radius-5 inline-block width-95 padding-top-xs padding-bottom-xs  margin-bottom-xs color-6c9ecb border-none" data-loading="{{ t._('loading') }}"> {{ t._('show') ~ ' ' ~  side.arguments_count ~ ' ' ~ t._('comments')  }}</button>
                                    </div>
                                    {% endif %}

                            </div>
                        </div>
                    {% endfor %}
                </div>
    <div class="{{ comparison.align ? '' : 'margin-center'}}">
        {% include "partials/argumentsbox/byspeakol.volt" %}
    </div>
        </div>
{% include "partials/main/errorbox.volt" %}
{% else %}
{% include "partials/argumentsbox/unauthorized.volt" %}
{% endif %}
                {{ assets.outputJs('footer') }}
        <script>window.jQuery || document.write('<script src="vendor/js/jquery-1.10.1.min.js"><\/script>')</script>
        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-56411923-1', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>
