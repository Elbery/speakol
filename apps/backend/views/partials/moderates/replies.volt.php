<script>
    viewMore = '<?php echo $hasMore['reply']; ?>';
</script>

<?php if (!$replies) { ?>
    <div class="blc_content clearfix">
        <ul class="ul_vert item_typs">
            <div class="cont_num">
                <small>No replies created!</small>
            </div>
        </ul>
    </div>
<?php
} ?>

<?php foreach ($replies as $element) { ?>
    <li>
        <div class="chkbx_cont">
            <input type="checkbox" name="selected[args][]" value="<?php echo $element['id']; ?>"/>
        </div>
        <div class="comment_unit clearfix">
            <ul class="actions_buttons">
                <a class="delete_button txt_ind reply_delete"  title="Delete" data-reply_id="<?php echo $element['id']; ?>" href="#">Delete</a>
            </ul>
            <div class="img_circl_container">
                <span class="img_circl">
                    <a href="">
                        <?php if (isset($element['user']['profile_picture']) && $element['user']['profile_picture']) { ?>
                            <?php echo $this->tag->image(array($this->config->application->webservice . $element['user']['profile_picture'])); ?>
                        <?php
    } else { ?>
                            <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                        <?php
    } ?>
                    </a>
                </span>
                <span class="nameAndDate">
                    <a href="#"> <?php echo $element['user']['first_name']; ?> <?php echo $element['user']['last_name']; ?></a>
                    <time><span><?php echo $element['date']; ?></span></time>
                </span>
            </div>
            <div class="comment_area">
                <p><?php echo $element['content']; ?></p>
                <!--<img class="context_img" src="img/context_img.jpg" alt=""/>-->
            </div>
            <div class="replies_actions">
                <div class="vote_action">
                    <div class="votes_up_num">
                        <a href="#" class="votes_up" title="vote up"> <?php echo $element['thumbs_up']; ?></a>
                    </div>
                    <div class="votes_down_num">
                        <a href="#" class="votes_down" title="vote down"> <?php echo $element['thumbs_down']; ?></a>
                    </div>
                </div>
                <div class="reports">
                    <span> <?php echo $element['reports']; ?> Reports </span>
                </div>
            </div>
        </div>
    </li>
<?php
} ?>

