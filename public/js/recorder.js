(function() {
    function Recorder() {
        var self = this;
        this.config = {
            'worker_path': '/vendor/js/worker.js',
            'error': function(err) {
                this.error = true;
            }.bind(this)
        };
        this.clip = [];
        this.initialized = false;
        this.recording = false;
        this.error = true;

    }


    Recorder.prototype.init = function(callback) {
        this.config.callback = function() {
            this.error = false;
            callback();
        }.bind(this);
        AudioRecorder.init(this.config);
        this.initialized = true;
        this.noAudio = false;
    };

    Recorder.prototype.record = function() {
        this.recording = true;
        AudioRecorder.record();
    };

    Recorder.prototype.stop = function(callback) {
        this.recording = false;
        AudioRecorder.stopRecording(function(clip) {
            this.clip = clip.speex;
            var sum = clip.samples.reduce(function(previous, current) {
                var sample = current < 0 ? current * -1 : current;
                return previous + sample;
            });

            this.noAudio = !sum;
            AudioRecorder.clear();
            if(callback) {
                callback();
            }
        }.bind(this));
    };

    Recorder.prototype.resetClip = function() {
        this.clip = [];
    };

    Recorder.prototype.checkIfNoAudio = function() {
        console.log(this.noAudio);
        return this.noAudio;
    }

    Recorder.prototype.exportOgg = function() {
        if(!this.clip) {
            return false;
        }

        var spxdata = new Uint8Array(this.clip.length);
        for(var i = 0 ; i < spxdata.length ; i++) {
            spxdata[i] = this.clip[i];
        }
        var segments = [];
        for( i = 0 ; i < spxdata.length / 70 ; i++) {
            segments.push(70);
        }
        var sampleRate = 44100;
        var isNarrowband = sampleRate < 16000;
        var oggdata = new Ogg(null, {file: true});


        var spxhdr = new SpeexHeader({
            bitrate: -1,
            extra_headers: 0,
            frame_size: isNarrowband ? 160 : 320,
            frames_per_packet: 1,
            header_size: 80,
            mode: isNarrowband ? 0 : 1,
            mode_bitstream_version: 4,
            nb_channels: 1,
            rate: sampleRate,
            reserved1: 0,
            reserved2: 0,
            speex_string: "Speex   ",
            speex_version_id: 1,
            speex_version_string: "1.2rc1\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
            vbr: 0
        });

        var comment = "Encoded with speex.js";
        var spxcmt = new SpeexComment({
            vendor_string: comment,
            vendor_length: comment.length
        });

        var result = oggdata.mux([spxhdr, spxcmt, [spxdata, segments]]);
        var byteArray = new Uint8Array(result.length);
        for(i = 0; i < result.length ; i++) {
            byteArray[i] = result.charCodeAt(i);
        }
        var fileblob = new Blob([byteArray], {type: 'audio/ogg'});
        fileblob.name = Date.now() + '.ogg';
        fileblob.lastModifiedDate = new Date();
        return fileblob;
    };

    Recorder.prototype.convertToWav = function(url, callback) {
        function encodeWAV(samples){
            var buffer = new ArrayBuffer(44 + samples.length * 2);
            var view = new DataView(buffer);
            var numChannels = 1;
            var sampleRate = 44100;

            /* RIFF identifier */
            writeString(view, 0, 'RIFF');
            /* RIFF chunk length */
            view.setUint32(4, 36 + samples.length * 2, true);
            /* RIFF type */
            writeString(view, 8, 'WAVE');
            /* format chunk identifier */
            writeString(view, 12, 'fmt ');
            /* format chunk length */
            view.setUint32(16, 16, true);
            /* sample format (raw) */
            view.setUint16(20, 1, true);
            /* channel count */
            view.setUint16(22, numChannels, true);
            /* sample rate */
            view.setUint32(24, sampleRate, true);
            /* byte rate (sample rate * block align) */
            view.setUint32(28, sampleRate * 4, true);
            /* block align (channel count * bytes per sample) */
            view.setUint16(32, numChannels * 2, true);
            /* bits per sample */
            view.setUint16(34, 16, true);
            /* data chunk identifier */
            writeString(view, 36, 'data');
            /* data chunk length */
            view.setUint32(40, samples.length * 2, true);

            floatTo16BitPCM(view, 44, samples);

            return view;
        }

        function floatTo16BitPCM(output, offset, input){
            for (var i = 0; i < input.length; i++, offset+=2){
                var s = Math.max(-1, Math.min(1, input[i]));
                output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
            }
        }

        function writeString(view, offset, string){
            for (var i = 0; i < string.length; i++){
                view.setUint8(offset + i, string.charCodeAt(i));
            }
        }

        Speex.readArrayBuffer(url , function(res) {
            res = new Uint8Array(res);
            var data = '';
            var len = res.length;
            for(var i = 0; i < len ; i = i + 1024) {
                to = Math.min(len,i+1024);
                data += String.fromCharCode.apply(null, res.subarray(i,to));
            }
            var file = Speex.decodeFile(data);
            var samples = file[0];
            samples = encodeWAV(samples);
            var blob = new Blob([samples], { type: 'audio/wav' });
            callback(blob);
        });
    };




function Timer(duration, $display, finish){
    this.duration = duration;
    this.time = duration;
    this.$display = $display;
    this.timer =  "";
    this.updateUI = function($display, timer) {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        $display.html(minutes + ":" + seconds);
    };
    this.finish = finish;
}
Timer.prototype.set = function() {
    console.log('setting up timer');
    this.timer = setInterval(function(){
        this.updateUI(this.$display, this.time);
        if (--this.time < 0) {
            clearInterval(this.timer);
            this.$display.html("");
            this.finish();
        }
    }.bind(this),1000);
};
Timer.prototype.reset = function(){
    console.log('clearing timer');
    clearInterval(this.timer);
    this.time = this.duration;
    this.$display.text("00:00");
};

Timer.prototype.setDisplay = function($display) {
    this.$display = $display;
};


window.speakolRecorder = new Recorder();
window.Timer = Timer;
})();
