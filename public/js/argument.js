$('document').ready(function () {


    $(document).on("click", "#agr_code_btn", function () {

        var appId, appUrl, appWidth, appDesc, code;
        appId = $('#app_id').attr('data-app_id');
        appUrl = $('#app_url').val();
        appWidth = jQuery('#app_width').val() ? parseInt(jQuery('#app_width').val()) : 650;
        if(!appWidth){
            alert('Width must be a number');
            return false;
        }
        appDesc = $('#app_title').val();
code ="<div class=\"speakol-arguments-box\" data-app=\"" + appId + "\" data-width=\"" + appWidth + "\"></div>\n";
code += "<script>\n";
code += "(function(doc, sc, id) {\n";
code += " var js, sjs = document.getElementsByTagName(sc)[0];\n";
code += " if(doc.getElementById(id)) return;\n";
code += " js = document.createElement(sc);\n";
code += " js.src = 'http://plugins.speakol.com/js/sdk.js'; js.id=id;\n";
code += " sjs.parentNode.insertBefore(js, sjs);\n";
code += "})(document, 'script', 'speakol-sdk');\n";
code += "</script>\n";
        $('#gnrtd_code').text(code);
    });

    $(document).on('change', '#arg-box-lang', function () {
        var lang = $(this).val();
        var codeText = $($('#gnrtd_code').text()); // convert code text into html object
        $(codeText).attr('data-lang', lang);
        var jsTag = codeText.next();
        $(jsTag).removeAttr('data-lang');
        jsTag = $(jsTag).prop('outerHTML');

        codeText = $(codeText).prop('outerHTML');
        $('#gnrtd_code').text(codeText + "\n" + jsTag);
    });

});
