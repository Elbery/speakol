<div class="sp-block-group feed-item radius-5">
    {% include 'partials/newsfeed/plugin-header.volt' %}
    <div class="sp-block-group margin-top-xs">
        <h2 class="font-20 light-blue no-margin margin-bottom-xs margin-top-lg"><i class="speakol-icon speakol-argumentsbox-green feed-item-icon margin-top-xs"></i>&nbsp;{{ plugin.articleContent['title'] }}</h2>
    </div>
    {% if plugin.articleContent['content'] %}
    <div class="sp-block-group color-3e3e3e readmore h5 debate-readmore-lg">
        {% for paragraph in plugin.articleContent['content'] %}
            <p class="wide-line-height">{{ paragraph }}</p>
        {% endfor %}
    </div>
    {% endif %}
    <div class="sp-block-group margin-top-lg margin-bottom-lg padding-bottom-lg progress-container relative margin-center">
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? plugin.sides[1] : plugin.sides[0] %}
                {% set color = (lang === 'ar') ?  'color-d11b1f' : 'color-4dac54' %}
                {% set bg = (lang === 'ar') ?  'bg-d11b1f' : 'bg-4dac54' %}
                <div class="sp-block six text-left color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : ''}}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes ? side.votes : 0 }}</span> {{ t._('votes') }}
                </div>
                <div
                    class="sp-block six text-right js-side-percentage h4 {{ color }} padding-bottom-sm side-percentage-left"
                    data-side-id="{{ side.id }}">
                    {{ side.perc ? side.perc ~ '%' : '0%' }}
                </div>
            </div>
            <div class="progress circular-radius flip vote-progress no-shadow no-border">
                <div
                    class="progress-bar circular-radius {{ bg }} js-progress-bar no-shadow no-border arg-progress-bar-left"
                    style="width: {{ side.perc ? side.perc ~ '%' : '0%' }}"
                    data-side-id="{{ side.id }}"
                >
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : '0%' }}</span>
                </div>
            </div>
        </div>
        <div class="sp-block progress-circle text-center">
            <canvas class="naqeshny-canvas js-voting-circle" data-lang="{{ lang  }}" width="80" height="80">
                <div class="js-data data hide">
                    {% set proSide = plugin.sides[0] %}
                    {% set conSide = plugin.sides[1] %}
                    {
                        "pro-votes": "{{ proSide.votes }}",
                        "pro-color": "#4dac54",
                        "con-votes": "{{ conSide.votes }}",
                        "con-color": "#d11b1f"
                    }
                </div>
            </canvas>
        </div>
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? plugin.sides[0] : plugin.sides[1] %}
                {% set color = (lang === 'ar') ?  'color-4dac54' : 'color-d11b1f' %}
                {% set bg = (lang === 'ar') ?  'bg-4dac54' : 'bg-d11b1f' %}
                <div
                    class="sp-block six text-left js-side-percentage h4 {{ color }} padding-bottom-sm side-percentage-right"
                    data-side-id="{{ side.id }}"
                >
                    {{ side.perc ? side.perc ~ '%' : '0%' }}
                </div>
                <div class="sp-block six text-right color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes ? side.votes : 0 }}</span> {{ t._('votes') }}
                </div>
            </div>
            <div class="progress circular-radius vote-progress no-shadow no-border">
                <div
                    class="progress-bar circular-radius {{ bg }} no-shadow no-border js-progress-bar arg-progress-bar-right"
                    style="width: {{side.perc ? side.perc ~ '%' : '0%'}}"
                    data-side-id="{{ side.id }}"
                >
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : '0%' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="sp-block-group text-center">
            {% set argumentsboxURL = '/newsfeed/argumentsbox?url=' %}
            {% set argumentsboxURL = argumentsboxURL ~ plugin.data.url | url_encode %}
            {% set argumentsboxURL = argumentsboxURL ~ '&app=' %}
            {% set argumentsboxURL = argumentsboxURL ~ plugin.app.id %}
            {% set argumentsboxURL = argumentsboxURL ~ '&lang=' %}
            {% set argumentsboxURL = argumentsboxURL ~ lang %}
            {% if plugin.data.code is defined and plugin.data.code %}
                {% set argumentsboxURL = argumentsboxURL ~ '&uuid=' %}
                {% set argumentsboxURL = argumentsboxURL ~ plugin.data.code %}
            {% endif %}
        <a 
            href="{{ argumentsboxURL }}"
            class="{{ isLoggedIn ? '' : 'js-login-button' }} anchor-reset margin-center inline-block btn-default conversation-button bg-white radius-5 border-acb6bb"
        >
            {{ t._('join-conversation') }}
        </a>
    </div>
</div>
