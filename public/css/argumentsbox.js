/**
 * @file
 * Arguments Box.
 * UI / UX
 * 
 * infraLayer | The Infrastructure Company (http://www.infraLayer.com/)
 * Copyright © 2014, infraLayer (http://www.infraLayer.com/)
 *
 * This file is part of infraLayer validation plugin.
 * ® Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright © 2014 infraLayer (http://www.infraLayer.com/)
 * @link          http://www.infraLayer.com/
 * @package       infraLayer
 * @subpackage    API
 * @version       1.0
 * @since         2014-03-24 Happy day :)
 * @license
 * @author        infraLayer <info@infralayer.com>
 * @modified      2014-08-02
 */

var t;
var parent;
var hasMore;
(function ($) {

    if (typeof (ArgumentsBox) === "undefined") {

        /**
         * 
         * @type _L22@pro;S|_L22@pro;settings
         */
        var _this;


        /**
         * 
         * @type _L22@pro;S
         */
        var S;
        /**
         * 
         * @type type
         */
        var ArgumentsBox = {
            /**
             * 
             */
            settings: {
                webUrl: phpVars.weburl + 'apps/api/',
                voteup: 'section.usr_action div.vt_btns a.vt_up',
                votedn: 'section.usr_action div.vt_btns a.vt_dwn',
                classVoteUp: 'vt_up',
                classVoteDn: 'vt_dwn',
                voteArgumentsBoxIDAttr: 'data-argumentsbox-id',
                voteSideIDAttr: 'data-side-id',
                fblogin: '.not_logged_tb .facebook_icon, .fb_login',
                glogin: '.not_logged_tb .gPlus_icon, .ggl_login',
                classDimmed: 'dimmed',
                cons: '.argumentBox_comments .pros_cons_percentbars .cons_percentbar .number strong',
                pros: '.argumentBox_comments .pros_cons_percentbars .pros_percentbar .number strong',
                prosPercentage: '.pros_percentbar',
                consPercentage: '.cons_percentbar',
                vtsTap: '.vts a span',
                votingCircle: '.argumentBox_comments .voting_circle .naqeshny-canvas .data',
                prosColor: '#166e16',
                consColor: '#980a0b',
                canvases: '.naqeshny-canvas',
                submitArg: '.usr_action .comment_on_action .comment_area .sbmt-arg',
                submitReply: '.subcomment_submit',
                textAreaArg: '.comment_area .tA_container .write_comment',
                fileArg: '.comment_area .add_img',
                idArg: '.comment_area #argumentbox_id',
                linkArg: '.comment_area #link',
                side: '.argumentBox_comments .side',
                sideIDAttr: 'data-side-id',
                argument: '.argumentBox_comments .side ul li',
                divComments: '.argumentBox_comments',
                reportBtn: '.social_buttons .report_button.report-arg-btn',
                muteBtn: '.mute-reply-btn',
                muteArgBtn: '.mute-arg-btn',
                thumbUpBtn: '.arg_votes_up',
                thumbDnBtn: '.arg_votes_down',
                nestedReplyForm: '.reply-form-nested',
                nestedReplyContiners: '.nested_comments',
                ClassArgVoted: 'voted',
                btnRepliesParent: '.reply',
                btnReplies: '.comment_unit .replies_actions .reply .reply_button',
                repliesContainer: '.comment_unit .replies_actions .reply .reply_container',
                ClassActiveRepliesContainer: 'active',
                btnCloseReplies: '.comment_area .img_comment .close_btn',
                btnSubmitReply: '.replies_actions .reply .reply_container .comment_on_action .comment_area .sbmt-reply',
                tagReplyform: 'form',
                replyForm: '.comment_unit .replies_actions .reply .reply_container .comment_on_action .comment_area form',
                argIDAttr: 'data-arg-id',
                parentReply: 'data-parent-reply',
                argumentUnit: 'div.comment_unit',
                repliesContainerArg: '.replies-container',
                btnMuteReply: '.comment_unit .replies_actions .reply .reply_container .replies-container .comment_unit .social_buttons .mute_button.mute-reply-btn',
                btnReportReply: '.comment_unit .replies_actions .reply .reply_container .replies-container .comment_unit .social_buttons .report_button.report-reply-btn',
                thumbUpReplyBtn: '.reply_votes_up',
                thumbDNReplyBtn: '.reply_votes_down',
                voteActions: '.vote_action',
                voteUp: '.votes_up',
                voteDn: '.votes_down',
                repliesActions: '.replies_actions',
                moreComments: '.comment_unit .replies_actions .reply .reply_container .showMore',
                nestedComments: '.nested_reply_button',
                moreArgsComments: '.showArgsComments',
                pageAttr: 'data-comments-page',
                perPage: 10,
                repliesCounter: '.reply_button span',
                socialContainer: '.social_shares',
                elemDisabled: 'element-disabled'
            },
            /**
             * 
             * @returns {undefined}
             */
            init: function () {
                // constructor
                _this = this;
                S = _this.settings;

                function inIframe() {
                    try {
                        return window.self !== window.top;
                    } catch (e) {
                        return true;
                    }
                }

                if (inIframe()) {
                window.parent.postMessage(JSON.stringify({
                    'id': 'iframeMessenger:ldsklk',
                    'type' : 'init'
                }),'*');
                window.addEventListener('message', function(e) {
                    try {
                        var message = JSON.parse(e.data);
                        if(message.id && message.indexOf('iframeMessenger') > -1 && message.type === 'init') {
                                parent.title = message.title;
                                parent.image = message.image;
                                parent.parent_url = message.parent_url;
                        }
                    } catch(e) {

                    }
                });
                    
                    _this.assginSocialUrls();
                    _this.bindUIActions();
                } else {
                    parent.title =  '';
                    parent.image =  '';
                    parent.parent_url = document.URL;
                    _this.assginSocialUrls();
                    _this.bindUIActions();
               }
            },
            /**
             * 
             * @returns {undefined}
             */
            isDisabled: function (elem) {

                if ($(elem).hasClass(S.elemDisabled)) {
                    return true;
                } else {
                    $(elem).addClass(S.elemDisabled);
                    return false;
                }

            },
            getAllSides: function() {
                var sideIDs = $('.argumentBox_comments [data-side-id]').map(function() {
                    return $(this).attr("data-side-id");
                }).toArray();
                return sideIDs;
            },
            enableDisabled: function (elem) {
                if ($(elem).hasClass(S.elemDisabled)) {
                    $(elem).removeClass(S.elemDisabled);
                }

            },
            unauthorizedHandler: function (objElement) {
                _this.enableDisabled(objElement);
                window.parent.postMessage(JSON.stringify({
                    'id': 'iframeMessenger:dfjdkfjd',
                    'type': 'popup'
                }), '*');
            },
            errrorHandler: function (text) {
                $('#errorbox').modal('hide');
                $('#errorbox #error-message').html(text);
                $('#errorbox').modal('show');
            },
            addReadMore: function() {
                $(".readmore").readmore('destroy');
                $(".readmore").readmore({embedCSS: false, collapsedHeight: 200});
            },
            showReplyContainer: function(element) {
                var $this = $(element);
                var popupContainer = $this.parent().find('.reply_container');
                popupContainer.css("width", $('.argumentBox_comments').width() - 30);
                popupContainer.toggleClass('active');
                popupContainer.position({
                    my: "top",
                    at: "bottom+10",
                    of: $this,
                    collision: "fit none"
                });
                popupContainer.find('.tip').position({
                    my: "top",
                    at: "bottom-10",
                    of: $this,
                    collision: "fit none"
                });
                var argumentID = $this.parents('[' + S.argIDAttr + ']').first().attr(S.argIDAttr);

                _this.Arguments.handleLoadReplies(argumentID, $this);

                $('body').append('<div class="overlay" />');
            },
            updateOnlineUsers: function() {
                var params = {
                    url: '/users/checkonlineusers',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        sides: _this.getAllSides()
                    },
                    success: function(data, textStatus, jqXHR) {
                        if(data.data) {
                            $('.owner-status.border-green').removeClass('border-green').addClass('border-light-grey');
                            $('.owner-status-text.green').text('Offline');
                            $('.owner-status-text.green').removeClass('green').addClass('light-grey');
                            $.each(data.data, function(i, id) {
                                var ownerStatus = $('[data-owner-id=' + id  + '] .owner-status');
                                var ownerStatusText = $('[data-owner-id=' + id  + '] .owner-status-text'); 
                                ownerStatus.removeClass("border-light-grey").addClass("border-green");
                                ownerStatusText.removeClass("light-grey").addClass("green");
                                ownerStatusText.text("Online");
                            });
                           
                        }
                    },
                };

                setInterval(function() {
                    $.ajax(params);
                }, 10000);

            },
            assginSocialUrls: function () {
                
                window.parent.postMessage(JSON.stringify({
                    'id': 'iframeMessenger:ldsklk',
                    'type' : 'parent-url'
                }),'*');

                window.addEventListener('message' , function(e) {
                    var message = JSON.parse(e.data);
                    if(message.type === 'parent-url') {
                        var parent_url = message.parent_url;
                        console.log('inside');
                        if ($(S.socialContainer).length && parent && parent_url) {
                            $(S.socialContainer).find('a.social-change').click(function (e) {
                                e.preventDefault();
                                var strUrl = $(this).attr('href');
                                var strWindowName = 'facebook-window';
                                var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,height=450,width=500,left=450,top=220";
                                var windowObjectReference = window.open(strUrl, strWindowName, strWindowFeatures);
                            });
                        }
                    }
                });
            },
            addSortDropdownEvents: function() {
                $(".sortby-dropdown .dropdown-menu a").on('click', function() {
                    var $this = $(this);
                    if($('.sort-order').attr('data-value') === $this.attr('data-value')) {
                        return;
                    }

                    var params = {};
                    params.side_ids = $(".side").map(function() {
                        return $(this).attr('data-side-id');
                    }).get();
                    params.order_by = $this.attr('data-value');
                    params.limit = 10;
                    _this.Arguments.changeSortOrder(params);
                    
                    $('.sort-order')
                    .text($this.text())
                    .attr('data-value', $this.attr('data-value'));
                });
            },
            bindUIActions: function () {
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();

                $(S.voteup).click(function () {
                    return _this.isDisabled(this) ? false : _this.Sides.handleVote($(this));
                });
                $(S.votedn).click(function () {
                    return _this.isDisabled(this) ? false : _this.Sides.handleVote($(this));
                });
                $(S.submitArg).click(function () {
                    return _this.isDisabled(this) ? false : _this.Sides.handleSubmitArg($(this));
                });
                $(S.divComments).delegate(S.reportBtn, 'click', function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();
                    if (!$(this).hasClass('reported')) {
                        return _this.Arguments.handleReportBtn($(this));
                    } else {
                        return _this.Arguments.handleUnReportBtn($(this));
                    }
                });

                $('body').delegate('.add_img', 'change', function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    var preview = '.arg_box_photo img';
                    var element = this;

                    $('.arg_box_photo').attr('src', '');
                    $('.arg_box_photo').hide();

                    if (this.files && this.files[0]) {

                        var reader = new FileReader();

                        if (!this.files[0].type.match('image.*')) {
                            ArgumentsBox.errrorHandler(t.ufmbio);
                            $(element).val('');
                            return false;
                        }

                        reader.onload = function (e) {

                            $(element).parents('.comment_area').find(preview).attr('src', e.target.result);
                            $(element).parents('.comment_area').find('.arg_box_photo').show();
                        }
                        reader.readAsDataURL(this.files[0]);
                    }
                    return false;
                });


                $(S.divComments).on('click', S.muteArgBtn, function (e) {
                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();
                    if (!$(this).hasClass('is-muted')) {
                        return _this.Arguments.handleMuteBtn($(this));
                    } else {
                        return _this.Arguments.handleUnMuteBtn($(this));
                    }
                });
                $(S.divComments).delegate(S.thumbUpBtn, 'click', function () {
                    return _this.isDisabled(this) ? false : _this.Arguments.handleThumbUpBtn($(this), $(this).attr('href'));
                });
                $(S.divComments).delegate(S.thumbDnBtn, 'click', function () {
                    return _this.isDisabled(this) ? false : _this.Arguments.handleThumbDnBtn($(this), $(this).attr('href'));
                });
                $(S.divComments).on('click', S.btnReplies, function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();
                    _this.showReplyContainer(this);
                });

                $(S.divComments).on('click', S.nestedComments, function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();

                    var argumentID = $(this).parents('[' + S.argIDAttr + ']').first().attr(S.argIDAttr);
                    var parentReply = $(this).attr(S.parentReply);

                    if (!$(this).attr('data-old-text')) {
                        $('.comment_unit_parent_' + parentReply).show();
                        _this.Arguments.handleNestedReplies(parentReply, argumentID, $(this));
                    } else {
                        $('.comment_unit_parent_' + parentReply).hide();
                        var $txt = $(this).attr('data-old-text');
                        $(this).text($txt);
                        $(this).removeAttr('data-old-text');
                    }

                    return false;
                });

                $(S.divComments).delegate(S.btnCloseReplies, 'click', function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.stopPropagation();
                    $(this).parents(S.repliesContainer).each(function (index, element) {
                        $(element).hide();
                        $(element).removeClass(S.ClassActiveRepliesContainer);
                    });
                    return false;
                });
                $(S.divComments).delegate(S.replyForm, 'submit', function () {
                    return _this.isDisabled(this) ? false : _this.Replies.handleSubmitReply($(this));
                });
                $(S.divComments).delegate(S.btnReportReply, 'click', function (e) {
                    if (_this.isDisabled(this)) {
                        return false;
                    }
                    e.preventDefault();
                    if (!$(this).hasClass('reported-reply')) {
                        return _this.Replies.handleReportBtn($(this));
                    } else {
                        return _this.Replies.handleUnReportBtn($(this));
                    }
                });
                $(S.divComments).delegate(S.btnMuteReply, 'click', function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();
                    if (!$(this).hasClass('muted-reply')) {
                        return _this.Replies.handleMuteBtn($(this));
                    } else {
                        return _this.Replies.handleUnMuteBtn($(this));
                    }
                });
                $(S.divComments).delegate(S.thumbUpReplyBtn, 'click', function () {
                    return _this.isDisabled(this) ? false : _this.Replies.handleThumbUpBtn($(this), $(this).attr('href'));
                });
                $(S.divComments).delegate(S.thumbDNReplyBtn, 'click', function () {
                    return _this.isDisabled(this) ? false : _this.Replies.handleThumbDnBtn($(this), $(this).attr('href'));
                });
                $(S.divComments).delegate(S.moreComments, 'click', function (e) {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    e.preventDefault();
                    var elem = $(this);
                    var params = new Object();
                    
                    params.argumentID = $(this).parents('[' + S.argIDAttr + ']').first().attr(S.argIDAttr);
                    params.limit = S.perPage;
                    params.element = $(this);
                    params.showMore = elem;
                    
                    $(this).attr('data-old', $.trim($(this).text()));
                    $(this).text('Loading..');

                    if ($(this).hasClass('nested_show_more')) {
                        params.parentReply = $(this).attr(S.parentReply);
                        params.page = parseInt($(this).attr(S.pageAttr)) + 1;
                        _this.Arguments.handlePaginateNestedReplies(params);
                    } else {
                        params.page = parseInt($(this).parents('[' + S.pageAttr + ']').first().attr(S.pageAttr)) + 1;
                        _this.Arguments.handlePaginateReplies(params);
                    }

                    return false;
                });

                $(S.divComments).delegate(S.moreArgsComments, 'click', function () {

                    if (_this.isDisabled(this)) {
                        return false;
                    }
                    var elem = $(this);
                    var pagBar = {};
                    $(this).attr('data-old', $.trim($(this).text()));
                    $(this).text('Loading..');
                    $(this).parents('div[' + S.voteSideIDAttr + ']').each(function () {

                        var params = new Object();
                        params.sideId = $(this).attr(S.voteSideIDAttr);
                        params.page = parseInt($(this).attr(S.pageAttr)) + 1;
                        params.limit = S.perPage;
                        params.element = $(this);
                        params.showMore = elem;

                        _this.Arguments.handlePaginateArguments(params);
                    });
                    return false;
                });
                // _this.updateOnlineUsers();
                _this.addSortDropdownEvents();
                
            },
            Sides: {
                handleVote: function (objElement) {
                    var intArgumentsBoxID = objElement.attr(S.voteArgumentsBoxIDAttr);
                    var intSideID = objElement.attr(S.voteSideIDAttr);

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/argumentsbox/vote/' + intArgumentsBoxID + '/' + intSideID
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        beforeSend: function () {

                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    var votes = data.data;
                                    _this.Misc.switchVoteClasses(objElement);
//                                    _this.Sides.recalculateVotes(objElement);
                                    _this.Sides.recalculateVotes(objElement, votes);
                                    $('.comment_on_action').show();
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };



                    if (!$(S.idArg).val()) {
                        _this.Sides.createArgumentBox(objElement, object);
                    } else {
                        $.ajax(object);
                    }
                    return false;
                },
                handleSubmitArg: function (objElement) {

                    var objFormData = new FormData();
                    objFormData.append('argument_id', $(S.idArg).val());
                    objFormData.append('context', $(S.textAreaArg).val());
                    objFormData.append('link', $(S.linkArg).val());
                    objFormData.append('file', $(S.fileArg).prop('files')[0]);
                    objFormData.append('action', '/arguments/insert');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: objFormData,
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        beforeSend: function () {
                            $(S.submitArg).attr('disabled', 'disabled');
                            $(S.submitArg).data('origin-text', $(S.submitArg).val());
                            $(S.submitArg).val(t.submt);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    $(S.submitArg).removeAttr('disabled');
                                    $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                    $(S.textAreaArg).val('');
                                    _this.Sides.appendArgumentToSide(data.data.side_id, data.data.argument_id.id);
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                if(data.responseJSON.message) {
                                    ArgumentsBox.errrorHandler(data.responseJSON.message);
                                } else {
                                    ArgumentsBox.errrorHandler(data.responseJSON.messages['0']);
                                }
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                _this.unauthorizedHandler(objElement);
                                // $('#loginbox').modal('show');
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.emt);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);

                    return false;
                },
                createArgumentBox: function (objElement, elemData) {

                    var objFormData = new FormData();

                    objFormData.append('app_id', phpVars.app_id);
                    objFormData.append('url', phpVars.url);
                    objFormData.append('width', phpVars.width);
                    objFormData.append('description', phpVars.description);
                    objFormData.append('token', phpVars.token);
                    if(!$("#noTitle").val()) {
                        objFormData.append('title', $('.title-text').text().trim());
                    }

                    objFormData.append('action', '/argument-box/create');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: objFormData,
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    _this.Sides.showArgumentBox(objElement, elemData);
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages['0']);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                _this.unauthorizedHandler(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                // $('#loginbox').modal('show');
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);

                    return false;
                },
                showArgumentBox: function (objElement, elemData) {

                    var object = {
                        url: S.webUrl,
                        type: 'GET',
                        // processData: false,
                        contentType: false,
                        data: {
                            action: '/argument-box/show/url',
                            url: phpVars.url
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {

                                    var $argData = data.data['0'];

                                    var intArgumentsBoxID = $argData.id;

                                    $(S.idArg).val($argData.id);

                                    $('.' + S.classVoteUp).attr(S.voteArgumentsBoxIDAttr, $argData.id);
                                    $('.' + S.classVoteDn).attr(S.voteArgumentsBoxIDAttr, $argData.id);

                                    $('.' + S.classVoteUp).attr(S.voteSideIDAttr, $argData.sides['0'].id);
                                    $('.' + S.classVoteDn).attr(S.voteSideIDAttr, $argData.sides['1'].id);

                                    $('.side:eq(0)').attr(S.voteSideIDAttr, $argData.sides['0'].id);
                                    $('.side:eq(1)').attr(S.voteSideIDAttr, $argData.sides['1'].id);

                                    if ($(objElement).hasClass('vt_up')) {
                                        var intSideID = $argData.sides['0'].id;
                                    } else {
                                        intSideID = $argData.sides['1'].id;
                                    }

                                    elemData.url = S.webUrl,
                                            elemData.data.action = '/argumentsbox/vote/' + intArgumentsBoxID + '/' + intSideID,
                                            $.ajax(elemData);

                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages['0']);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                // $('#loginbox').modal('show');
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);

                    return false;
                },
                recalculateVotes: function (objElement, votes) {
                    // refactored by Mohamed Ragab Dahab
                    var intPros = parseInt(votes[0]['votes']);
                    var intCons = parseInt(votes[1]['votes']);

//                    var intPros = parseInt($(S.pros).text());
//                    var intCons = parseInt($(S.cons).text());
//                    if (objElement.hasClass(S.classVoteUp)) {
//                        //increment pros
//                        intPros++;
//                        //decrement cons
//                        (intCons > 0 ? intCons-- : 0);
//                    } else if (objElement.hasClass(S.classVoteDn)) {
//                        //increment cons
//                        intCons++;
//                        //decrement pros
//                        (intPros > 0 ? intPros-- : 0);
//                    } else {
//                        //do nothing
//                    }
                    //update circle
                    var objCanvas = {
                        "pro-votes": intPros,
                        "con-votes": intCons,
                        "pro-color": S.prosColor,
                        "con-color": S.consColor
                    };

                    var total = intPros + intCons;

                    var prosPercentage = Math.round((intPros * 100) / total) + '%';
                    var consPercentage = Math.round((intCons * 100) / total) + '%';

                    $(S.votingCircle).text(JSON.stringify(objCanvas));
                    _this.Sides.updateVotingCircle();
                    //update pros & cons
                    $(S.pros).text(intPros);
                    $(S.cons).text(intCons);
                    $(S.prosPercentage).find('.percent').text(prosPercentage);
                    $(S.consPercentage).find('.percent').text(consPercentage);
                    $(S.vtsTap).text(total);
                },
                updateVotingCircle: function () {
                    // voting circle canvas
                    $(S.canvases).each(function (index, obj) {
                        $(obj).debateVersus();
                    });
                },
                appendArgumentToSide: function (_sideID, _argumentID) {

                    $(S.side).each(function (index, element) {
                        if ($(element).attr(S.sideIDAttr) == _sideID) {
                            var object = {
                                url: '/arguments/render/argument',
                                type: 'GET',
                                data: {
                                    'argument_id': _argumentID
                                },
                                success: function (data, textStatus, jqXHR) {
                                    data = '<li>' + data + '</li>';
                                    if ($(data).find('.context_img').length) {
                                        
                                        $('.vwrs_num').text(t.ids);    
                                    } else {
                                        $('.vwrs_num').text(t.ads);
                                    }
                                    $(element).children('ul').first().prepend(data);
                                    $(".vwrs_num").hide();
                                    $(".vwrs_num").fadeIn();
                                    _this.addReadMore();
                                    setTimeout(function () {
                                        $(".vwrs_num").fadeOut();
                                    }, 3000);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    ArgumentsBox.errrorHandler(jqXHR.responseText);
                                },
                            };

                            $.ajax(object);
                            return false;
                        }
                    });
                }
            },
            Arguments: {
                handleReportBtn: function (objElement) {
                    var _argumentID = objElement.attr('href');
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/arguments/' + _argumentID + '/report'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.addClass('reported');
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleMuteBtn: function (objElement) {

                    var _argumentID = objElement.attr('href');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/arguments/' + _argumentID + '/mute'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.removeClass('mute_button');
                                    objElement.addClass('is-muted follow_button');
                                    objElement.attr('title', 'muted');
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uham);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleUnReportBtn: function (objElement) {
                    var _argumentID = objElement.attr('href');
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            'method': 'DELETE',
                            action: '/arguments/' + _argumentID + '/unreport'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.removeClass('reported');
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleUnMuteBtn: function (objElement) {

                    var _argumentID = objElement.attr('href');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            'method': 'DELETE',
                            action: '/arguments/' + _argumentID + '/unmute'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.removeClass('follow_button');
                                    objElement.addClass('mute_button');
                                    objElement.attr('title', 'mute');
                                    objElement.removeClass('is-muted');
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uham);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleThumbUpBtn: function (objElement, _argumentID) {
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/arguments/' + _argumentID + '/thumb_up'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {

                                    _this.Misc.recalculateThumbs(objElement, true);
                                    _this.Misc.switchArgThumbsClasses(objElement);
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                                objElement.removeClass('voted');
                                var val = parseInt(objElement.text());
                                val = val ? val - 1 : 0;
                                objElement.text(val);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                // $('#loginbox').css('top', objElement[0].getBoundingClientRect().top);
                                // $('#loginbox').css('margin-top', '-250px');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleThumbDnBtn: function (objElement, _argumentID) {
                    var thatthat = this;
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/arguments/' + _argumentID + '/thumb_down'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {

                                    _this.Misc.recalculateThumbs(objElement, false);
                                    _this.Misc.switchArgThumbsClasses(objElement);
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                                objElement.removeClass('voted');
                                var val = parseInt(objElement.text());
                                val = val ? val - 1 : 0;
                                objElement.text(val);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show')
                                // $('#loginbox').css('top', objElement[0].getBoundingClientRect().top);
                                // $('#loginbox').css('margin-top', '-250px');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleLoadReplies: function (_argumentID, objElement) {
                    var object = {
                        url: '/arguments/render/replies/',
                        type: 'GET',
                        data: {
                            'argument_id': _argumentID,
                        },
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.parents(S.repliesActions).find(S.repliesContainerArg).first().html(data);
                                _this.addReadMore();
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                    return false;
                },
                handleNestedReplies: function (_replyParent, _argumentID, objElement) {

                    var object = {
                        url: '/arguments/render/nested-replies/',
                        type: 'GET',
                        data: {
                            'parent_reply': _replyParent,
                            'argument_id': _argumentID
                        },
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data) {
                                    objElement.parents('.replies-parent_' + _replyParent).find(S.nestedReplyContiners).find('.comments_level').not('.show-more-container').remove();
                                    objElement.parents('.replies-parent_' + _replyParent).find(S.nestedReplyContiners).prepend(data);
                                }

                                objElement.attr('data-old-text', objElement.text());
                                objElement.text('Hide ^');

                                objElement.parents('.replies-parent_' + _replyParent).find('.reply-form-nested:first, .nested_comments:first').show();

                                _this.addReadMore();
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                    return false;
                },
                /**
                 * 
                 * @param {type} _data {argument_id, page, limit, element}
                 * @returns {Boolean}
                 */
                handlePaginateReplies: function (_data) {

                    var objElement = _data.element;

                    var object = {
                        url: '/arguments/render/replies/',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            'argument_id': _data.argumentID,
                            'page': _data.page,
                            'limit': _data.limit
                        },
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(_data.showMore);
                                data = data.data;
                                if (data) {
                                    $html = $("<div>" + data.html + "</div>");
                                    _data.element.show();
                                    _data.element.parents(S.argumentUnit).find(S.repliesContainerArg).append($html.html());
                                    _data.element.parents('[' + S.pageAttr + ']').first().attr(S.pageAttr, _data.page);
                                    if(!data.hasmore) {
                                        _data.element.remove();
                                    } else {
                                        _data.element.text(_data.showMore.attr('data-old'));
                                    }
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                    return false;
                },
                /**
                 *
                 * @param {type} _data {argument_id, page, limit, element}
                 * @returns {Boolean}
                 */
                handlePaginateNestedReplies: function (_data) {

                    var objElement = _data.elemnt;

                    var object = {
                        url: '/arguments/render/nested-replies/',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            'argument_id': _data.argumentID,
                            'parent_reply': _data.parentReply,
                            'page': _data.page,
                            'limit': _data.limit
                        },
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(_data.showMore);
                                data = data.data;
                                if (data) {
                                    $html = $("<div>" + data.html + "</div>");
                                    _data.element.show();
                                    _data.element.parents('.comment_unit_parent_' + _data.parentReply).find('.nested_comments div:last').before($html.html());
                                    _data.element.attr(S.pageAttr, _data.page);
                                    if(!data.hasmore) {
                                        _data.element.remove();
                                    } else {
                                        _data.element.text(_data.showMore.attr('data-old'));
                                    }
                                }

                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                    return false;
                },
                /**
                 *
                 * @param {type} _data {argument_id, page, limit, element}
                 * @returns {Boolean}
                 */
                handlePaginateArguments: function (_data) {

                    var objElement = _data.showMore;

                    var object = {
                        url: '/arguments/render/arguments/',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            'side_id': _data.sideId,
                            'page': _data.page,
                            'limit': _data.limit,
                            'order_by': $('.sort-order').attr('data-value')
                        },
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                data = data.data;
                                if (data) {
                                    $html = $("<div>" + data.html + "</div>");
                                    _data.element.find('ul').append($html.html());
                                    _this.addReadMore();
                                    _data.element.attr(S.pageAttr, _data.page);
                                    if(!data.hasmore) {
                                        _data.showMore.remove();
                                    } else {
                                        _data.showMore.text(_data.showMore.attr('data-old'));
                                    }

                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                if (data.responseText.messages) {
                                    _this.enableDisabled(objElement);
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                    return false;
                },
                changeSortOrder: function(params) {
                    var object = {
                        url: '/arguments/changesort/',
                        type: 'GET',
                        dataType: 'json',
                        data: params,
                        success: function (data, textStatus, jqXHR) {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ArgumentsBox.errrorHandler(jqXHR.responseText);
                        },
                        statusCode: {
                            200: function (data) {
                                // _this.enableDisabled(objElement);
                                data = data.data;
                                if (data) {
                                    $('.side ul').html("");
                                    data.forEach(function(el, i) {
                                        $html = $("<div>" + el.html + "</div>");
                                        var side = $('.side[data-side-id="' + el.side_id  + '"]');
                                        if(!side.find('.showArgsComments').length && el.hasmore) {
                                            var showMore = '<a class="showArgsComments" href="javascript:">Show more</a>';
                                            side.find('.show_arg_container').append(showMore);
                                        }
                                        side.attr('data-comments-page', 1);
                                        side.find('ul').append($html.html());
                                    });
                                    _this.addReadMore();
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.br);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            },
                            500: function (data) {
                                if (data.responseText.messages) {
                                    _this.enableDisabled(objElement);
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        }
                    };

                    $.ajax(object);
                },
            },
            Replies: {
                handleMuteBtn: function (objElement) {

                    var _replyID = objElement.attr('href');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/replies/' + _replyID + '/mute'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.addClass('muted-reply');
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uham);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                // $('#loginbox').css('top', objElement[0].getBoundingClientRect().top);
                                // $('#loginbox').css('margin-top', '-250px');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleReportBtn: function (objElement) {
                    var _replyID = objElement.attr('href');
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/replies/' + _replyID + '/report'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.addClass('reported-reply')
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                // $('#loginbox').css('top', objElement[0].getBoundingClientRect().top);
                                // $('#loginbox').css('margin-top', '-250px');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleUnMuteBtn: function (objElement) {

                    var _replyID = objElement.attr('href');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            'method': 'DELETE',
                            action: '/replies/' + _replyID + '/unmute'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.removeClass('muted-reply');
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.uham);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                                _this.unauthorizedHandler(objElement);
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleUnReportBtn: function (objElement) {
                    var _replyID = objElement.attr('href');
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            'method': 'DELETE',
                            action: '/replies/' + _replyID + '/unreport'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.removeClass('reported-reply')
                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                _this.unauthorizedHandler(objElement);
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleThumbUpBtn: function (objElement, _replyID) {
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/replies/' + _replyID + '/thumb_up'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {

                                    _this.Misc.recalculateThumbs(objElement, true);
                                    _this.Misc.switchThumbsClasses(objElement);
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                                objElement.removeClass('voted');
                                var val = parseInt(objElement.text());
                                val = val ? val - 1 : 0;
                                objElement.text(val);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                _this.unauthorizedHandler(objElement);
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                handleThumbDnBtn: function (objElement, _replyID) {
                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: {
                            action: '/replies/' + _replyID + '/thumb_down'
                        },
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {

                                    _this.Misc.recalculateThumbs(objElement, false);
                                    _this.Misc.switchThumbsClasses(objElement);
                                }
                            },
                            302: function () {
                                _this.enableDisabled(objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                                objElement.removeClass('voted');
                                var val = parseInt(objElement.text());
                                val = val ? val - 1 : 0;
                                objElement.text(val);
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function () {
                                _this.unauthorizedHandler(objElement);
                                // _this.enableDisabled(objElement);
                                // $('#loginbox').modal('show');
                            },
                            409: function () {
                                _this.enableDisabled(objElement);
                                ArgumentsBox.errrorHandler(t.ec);
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                /**
                 * 
                 * @param {type} objElement
                 * @returns {Boolean}
                 */
                handleSubmitReply: function (objElement) {

                    var argumentID = null, parentReply = null;
                    var objFormData = new FormData();
                    objElement.children('input').each(function (index, element) {
                        objFormData.append($(element).attr('name'), $(element).val());
                        if ($(element).attr('type') == 'file') {
                            objFormData.append('file', $(element).prop('files')[0]);
                        }
                        if ($(element).attr('name') == 'argument_id') {
                            argumentID = $(element).val();
                        }
                        if ($(element).attr('name') == 'parent_reply') {
                            parentReply = $(element).val();
                        }
                    });
                    objElement.find('textarea').each(function (index, element) {
                        objFormData.append($(element).attr('name'), $(element).val());
                    });

                    objFormData.append('action', '/replies/create');

                    var object = {
                        url: S.webUrl,
                        type: 'POST',
                        data: objFormData,
                        processData: false,
                        contentType: false,
                        success: function (data, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        beforeSend: function () {
                            objElement.find(S.submitReply).attr('disabled', 'disabled');
                            objElement.find(S.submitReply).data('origin-text', objElement.find(S.submitReply).val());
                            objElement.find(S.submitReply).val(t.submt);
                        },
                        statusCode: {
                            200: function (data) {
                                _this.enableDisabled(objElement);
                                if (data.status == 'OK') {
                                    objElement.find(S.submitReply).removeAttr('disabled');
                                    objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                    objElement.find('textarea').each(function (index, element) {
                                        $(this).val('');
                                    });

                                    if (parentReply == null) {
                                        _this.Replies.appendReplyToArg(argumentID, data.data);
                                    } else {
                                        _this.Replies.appendNestedReply(argumentID, parentReply, data.data);
                                    }

                                    var objRepliesCounter = objElement.parents(S.btnRepliesParent).find(S.repliesCounter).first();
                                    var numReplies = parseInt(objRepliesCounter.html());
                                    objRepliesCounter.html(++numReplies);

                                }
                            },
                            302: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.uhav);
                            },
                            400: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                if(data.responseJSON.message) {
                                    ArgumentsBox.errrorHandler(data.responseJSON.message);
                                } else {
                                    ArgumentsBox.errrorHandler(t.br);
                                }
                            },
                            403: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                ArgumentsBox.errrorHandler(data.responseJSON.messages);
                            },
                            404: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.rnf);
                            },
                            401: function (data) {
                                _this.unauthorizedHandler(objElement);
                                // _this.enableDisabled(objElement);
                                // objElement.find(S.submitReply).removeAttr('disabled');
                                // objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                // $('#loginbox').modal('show');
                            },
                            409: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                ArgumentsBox.errrorHandler(t.emt);
                            },
                            500: function (data) {
                                _this.enableDisabled(objElement);
                                objElement.find(S.submitReply).removeAttr('disabled');
                                objElement.find(S.submitReply).val(objElement.find(S.submitReply).data('origin-text'));
                                if (data.responseText.messages) {
                                    ArgumentsBox.errrorHandler(data.responseText.messages);
                                }
                            }
                        },
                        dataType: 'JSON'
                    };

                    $.ajax(object);
                    return false;
                },
                appendReplyToArg: function (_argumentID, _replyID) {
                    $(S.argumentUnit).each(function (index, element) {
                        if ($(element).attr(S.argIDAttr) == _argumentID) {
                            var object = {
                                url: '/arguments/render/reply',
                                type: 'GET',
                                data: {
                                    'argument_id': _argumentID,
                                    'reply_id': _replyID
                                },
                                success: function (data, textStatus, jqXHR) {
                                    _this.enableDisabled(element);
                                    $(element).find(S.repliesContainerArg).first().prepend(data);
                                    _this.addReadMore();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    ArgumentsBox.errrorHandler(jqXHR.responseText);
                                }
                            };

                            $.ajax(object);
                            return false;
                        }
                    });
                },
                appendNestedReply: function (_argumentID, _parentReply, _replyID) {
                    $(S.argumentUnit).each(function (index, element) {
                        if ($(element).attr(S.argIDAttr) == _argumentID) {
                            var object = {
                                url: '/arguments/render/reply',
                                type: 'GET',
                                data: {
                                    'reply_id': _replyID,
                                    'parent_reply': _parentReply,
                                    'argument_id': _argumentID,
                                    'nested': 1
                                },
                                success: function (data, textStatus, jqXHR) {
                                    _this.enableDisabled(element);
                                    $('.comment_unit_parent_' + _parentReply).show();
                                    $('.replies-parent_' + _parentReply).find('.comments_level:first').prepend(data);
                                    _this.addReadMore();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    ArgumentsBox.errrorHandler(jqXHR.responseText);
                                }
                            };

                            $.ajax(object);
                            return false;
                        }
                    });
                }
            },
            Misc: {
                switchArgThumbsClasses: function (objElment) {
                    $(objElment).parents(S.argumentUnit).find(S.thumbDnBtn).removeClass(S.ClassArgVoted);
                    $(objElment).parents(S.argumentUnit).find(S.thumbUpBtn).removeClass(S.ClassArgVoted);
                    objElment.addClass(S.ClassArgVoted);
                },
                switchVoteClasses: function (objElement) {
                    $(S.voteup).removeClass(S.classDimmed);
                    $(S.votedn).removeClass(S.classDimmed);
                    objElement.addClass(S.classDimmed);
                },
                recalculateThumbs: function (objElement, boolIncr) {
                    var numThumbs = parseInt(objElement.text());
                    objElement.text(++numThumbs);
                    if (boolIncr) {
                        // If has voted to thumb down
                        // decrement thumbs down
                        var downObj = objElement.parents(S.voteActions).first().find(S.voteDn).first();
                        if (downObj.hasClass(S.ClassArgVoted)) {
                            var downCnts = parseInt(downObj.text());
                            downObj.text(--downCnts);
                        }
                    } else {
                        //if has voted to thumb up
                        // decrease thumbs up
                        var upObj = objElement.parents(S.voteActions).first().find(S.voteUp).first();
                        if (upObj.hasClass(S.ClassArgVoted)) {
                            var upCnts = parseInt(upObj.text());
                            upObj.text(--upCnts);
                        }
                    }
                },
                switchThumbsClasses: function (objElement) {
                    $(objElement.parents(S.voteActions).first().find(S.voteDn).first()).removeClass(S.ClassArgVoted);
                    $(objElement.parents(S.voteActions).first().find(S.voteUp).first()).removeClass(S.ClassArgVoted);
                    objElement.addClass(S.ClassArgVoted);
                }
            },

        };
        
        ArgumentsBox.init();
    }
}(jQuery));
/* $(document).ready(function(){
 var $container = $('.content_wrapper');
 $(window).scroll(function(){
 if($(document).scrollTop()> 40){
 $container.addClass('fixed');
 } else {
 $container.removeClass('fixed');
 }
 });
 }); */
$(document).on('click', '.overlay', function (event) {
    event.preventDefault();
    $(this).remove();
    $('.reply_container.active').removeClass('active');

});

$(document).on('click', '[data-target="#loginbox"]', function (event) {
    $('#loginbox').css('top', '15%')
});
