<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Confirmation;
use \Phalcon\Validation\Validator\InclusionIn;
class UsersEditValidation extends SpeakolValidation {
    public function __construct($rules = false, $exclude = false) {
        $this->loadCustomTrans("user");
        $this->validations = array("first_name" => array(new PresenceOf(array('message' => $this->t->_('fname-required')))), "last_name" => array(new PresenceOf(array('message' => $this->t->_('lname-required')))), 'subscribe' => array(new InclusionIn(array('message' => $this->t->_('subscribe-invalid'), 'domain' => array(1, 0))),), 'newsletter' => array(new InclusionIn(array('message' => $this->t->_('newsletter-invalid'), 'domain' => array('0', '1', '2'))),), 'new_password' => array(new PresenceOf(array('message' => $this->t->_('new-password-required'))), new StringLength(array('messageMinimum' => $this->t->_('password-min-length'), 'min' => 8)),), 'confirm_password' => array(new Confirmation(array('message' => $this->t->_('password-not-match'), 'with' => 'new_password'))), "email" => array(new Email(array('message' => $this->t->_('email-not-valid'))), new PresenceOf(array('message' => $this->t->_('email-required'))),), "confirm_email" => array(new Confirmation(array('message' => $this->t->_('email-not-match'), 'with' => 'email'))),);
        parent::__construct($rules, $exclude);
    }
    public function initialize() {
        parent::initialize();
    }
}
