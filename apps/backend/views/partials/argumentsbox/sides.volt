<div class="sp-block-group padding-bottom-sm relative sides-container width-95 margin-center">
    {% if argumentsbox.sides is defined %}
    {% if lang === "ar" %}
        {% set sides = [argumentsbox.sides[1] , argumentsbox.sides[0] ]  %}
    {% else  %}
        {% set sides = argumentsbox.sides  %}
    {% endif  %}
    {% if argumentsbox.ads is defined %}
    {% if lang === "ar" %}
        {% set ads = [argumentsbox.ads[1] , argumentsbox.ads[0] ]  %}
    {% else  %}
        {% set ads = argumentsbox.ads  %}
    {% endif  %}
    {% endif %}
        {% for key, side in sides %}
            {% set right = (loop.first) ? false : true %}
            <div class="js-side six sp-block side  {{ loop.first ? "vertical-separator-right" : "vertical-separator-left" }}"  data-side-id="{{ side.id }}" data-comments-page="1" {{ right ? 'data-align="right"' : '' }}>
                <ul>
                    {% if ads is defined %}
                        {%set ad = ads[loop.index0] %}
                        {% if ad is defined %}
                        <li>
                            {% include "partials/argumentsbox/ad" with ['ad': ad, 'argumentsBoxURL':argumentsbox.url, 'right': right] %}
                        </li>
                        {% endif %}
                    {% endif %}
                </ul>
                <ul class="js-arguments">
                    {% if side.arguments.args is defined %}
                        {% for argument in side.arguments.args %}
                            <li>
                                {% include "partials/argumentsbox/argument" with ['argument': argument, 'argumentsBoxURL':argumentsbox.url, 'right': right] %}
                            </li>
                        {% endfor %}
                    {% endif %}
                </ul>
                {%  if side.arguments.hasmore is defined and side.arguments.hasmore %}
                <div class="sp-block-group text-center {{ lang === 'ar' ? 'rtl' : '' }}">
                    <button class="js-show-more  bg-647a88 font-13 radius-5 inline-block width-78 padding-top-xs padding-bottom-xs white border-none" data-loading="{{ t._('loading') }}">{{ t._('show-more') }} </button>
                </div> 
                {% endif %}
            </div>
        {% endfor %}
    {% else %}
        
            <div class="js-side six sp-block side vertical-separator-right"  data-side-id="{{ side.id }}" data-comments-page="1">
                <ul class="js-arguments">
                </ul>
                
            </div>
            <div class="js-side six sp-block side vertical-separator-left"  data-side-id="{{ side.id }}" data-comments-page="1" data-align="right">
                <ul class="js-arguments">
                </ul>
            </div>
    {% endif %}
</div>
