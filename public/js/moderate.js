
(function ($) {
    
    function ModerationMethods(options){
        
        
        var s = $.extend({
            
        }, options);

        var _this = this;

        _this.init = function () {};
        
        _this.content = function (elem){

            return $(this).parent().find('.users').html();
        };
        
        _this.addNewAdmin = function(query){
            
            var response = {};
            
            $.ajax({
                url : '/moderates/getusers',
                data: {
                    query : query
                },
                async: false,
                success: function(data){
                    response = data['data'];
                }
            });

            return response;
        };
        
        _this.beforeXHR = function (elem, params){
            
            var type = $(elem).attr('data-type');
            var val = parseInt($(elem).attr('data-'+type)) + 1;
            $(elem).attr('data-'+$(elem).attr('data-type'),val);
            
            if(type == 'all'){
                var data = {
                    debate:val,
                    comparison:val,
                    argumentsbox:val,
                    all:val,
                    reply:val,
                };
            }else{
                var data = {
                    debate      : $(elem).attr('data-debate'),
                    comparison  : $(elem).attr('data-comparison'),
                    argumentsbox : $(elem).attr('data-argumentbox'),
                    all         : $(elem).attr('data-all'),
                    reply       : $(elem).attr('data-reply') && parseInt($(elem).attr('data-reply')),
                    argument    : $(elem).attr('data-argument') && parseInt($(elem).attr('data-argument'))
                };
            }
            if(data.reply){
                $(elem).attr('data-reply',data.reply + 1);
            }
            if(data.argument){
                $(elem).attr('data-argument',data.argument + 1);
            }
            return { page: data};
        }
        
        _this.renderItem = function (items) {
            var that = this;
            items = $(items).map(function (i, item) {
                i = $(that.options.item).attr({"data-value":item['value'],"data-text":item['name'],"data-image":item['image']});
                i.find("a").html(that.highlighter(item['name']));
                i.find("a").prepend('<div style="display:inline-block;margin-right: 10px;" class="img_circl_container"><span class="img_circl"><img src="'+item['image']+'" ></span></div>');
                return i[0];
            });
            if (this.autoSelect) {
                items.first().addClass("active")
            }
            
            this.$menu.html(items);
            return this
        };
        
        _this.select = function () {
            
            var val = this.$menu.find(".active").data("value");
            var name = this.$menu.find(".active").data("text");
            var image = this.$menu.find(".active").data("image");
            
            $('#add-user-cicrle').html('<img src="'+image+'" />')
            $('#add-user-hidden').val(val);
            
            if (this.autoSelect || name) {
                this.$element.val(this.updater(name)).change();
                this.$element.attr('disabled','disabled');
            }
        }
        
        _this.matcher = function (item) {
            
            return~item['name'].toLowerCase().indexOf(this.query.toLowerCase())
        }
        
        _this.sorter = function (items) {
            var beginswith = [], caseSensitive = [], caseInsensitive = [], item;
            while (item = items.shift()) {
                if (!item['name'].toLowerCase().indexOf(this.query.toLowerCase()))
                    beginswith.push(item);
                else if (~item['name'].indexOf(this.query))
                    caseSensitive.push(item);
                else
                    caseInsensitive.push(item)
            }
            return beginswith.concat(caseSensitive, caseInsensitive)
        };
        
        _this.highlighter = function (item) {
            
            var html = $("<div></div>");
            var query = this.query;
            var i = item.indexOf(query);
            var len, leftPart, middlePart, rightPart, strong;
            len = query.length;
            if (len == 0) {
                return html.text(item).html()
            }
            while (i > -1) {
                leftPart = item.substr(0, i);
                middlePart = item.substr(i, len);
                rightPart = item.substr(i + len);
                strong = $("<strong></strong>").text(middlePart);
                html.append(document.createTextNode(leftPart)).append(strong);
                item = rightPart;
                i = item.indexOf(query)
            }
            return html.append(document.createTextNode(item)).html()
        };
        
        _this.beforeSucess = function (data){
            
            var debates = $(data.data).filter('#debates').html();
            if(data.type == "debate" || data.type == "all") {
                $('#debates').append(debates);
            }
            
            var comparisons = $(data.data).filter('#comparisons').html();
            if(data.type == "comparison" || data.type == "all") {
                $('#comparisons').append(comparisons);
            }
            
            var argumentboxes = $(data.data).filter('#argumentbox').html();
            if(data.type == "argumentbox" || data.type == "all") {
                $('#argumentbox').append(argumentboxes);
            }
            
            if(!data.hasmore['all']){
                $('.pag-types').remove();
                return false;
            }
            
            if(!data.hasmore[data.type]){
                $('.pag-types').addClass('hide');
            }
            
            $('.pag-types').attr('data-all', data.page['all']);
            $('.pag-types').attr('data-debate', data.page['debate']);
            $('.pag-types').attr('data-comparison', data.page['comparison']);
            $('.pag-types').attr('data-argumentbox', data.page['argumentbox']);
            
        };
        
        return this.init(function () {

            _this.init();
        });
    }
    
    Moderate = new ModerationMethods();
    
    $(document).ready(function(){
        
        $('.add_button').click(function(e){
            
            var stop = false;
            
            $(this).parents('form').find('input').each(function(){
                
                if(!$(this).val()){
                    stop = true;
                }
            });
            
            e.preventDefault();
            
            if(stop){
                alert('Please select a user!')
                return false;
            }
            
            $(this).parents('form').submit();
        });
        
        addUser = $('#add_admin_user').typeahead({
                name: 'Users',
                source: Moderate.addNewAdmin,
                render: Moderate.renderItem,
                matcher: Moderate.matcher,
                sorter: Moderate.sorter,
                highlighter: Moderate.highlighter,
                select: Moderate.select,
            }
        );
        
        $(".js-yourcontent a").click(function (e) {
            
            e.preventDefault();
            
            if($(this).hasClass('paginate-me')){
                
                var type = $(this).data('type');
                var url  = $('.pag-types').data('url');
                var check = $('.pag-types').data(type);

                if(pag){
                    pag.setParams('ajaxUrl', url+'?type='+type);
                }

                $('.pag-types').attr('data-type',type);
                $('.pag-types').addClass('hide');
                
                if(check){
                    $('.pag-types').removeClass('hide');
                }
            }
            
            $(this).tab('show');
        });
        
        $('.showAll').click(function (e) {
            $('.js-types').find('.tab-pane').addClass('active in');
        });
        
        if($('.pag-types').length){
            
            pag = $('.pag-types').pagination({
                parentClass     : '.js-types',
                ajaxUrl         : $('.pag-types').attr('data-ajax-url'),
                beforeSuccess   : Moderate.beforeSucess,
                beforeXHR       : Moderate.beforeXHR
            });
            
        }else{
            pag = false;
        }
        
        if($('.pag-replies').length){
            replies = $('.pag-replies').pagination({
                parentClass     : '.js-reply-items',
                ajaxUrl         : $('.pag-replies').attr('data-ajax-url'),
                beforeXHR       : Moderate.beforeXHR
            });
        }
        
        if($('.pag-arguments').length){
            replies = $('.pag-arguments').pagination({
                parentClass     : '.js-arg-items',
                ajaxUrl         : $('.pag-arguments').attr('data-ajax-url'),
                beforeXHR       : Moderate.beforeXHR
            });
        }
        
    });
}(jQuery));
