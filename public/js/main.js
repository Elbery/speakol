var cropper = {};
main = {
    init: function () {
        main.bind();
        main.deleteDebate();
        main.initTooltips();
        main.readMore();
        main.deleteComparison();
        main.deleteArgumentsbox();
        main.replyDelete();
        main.agrumentDelete();
        main.removeModerator();
        main.switchLang();
        main.previewClick();
        main.logout();
        main.updateStats();
        main.onStatsIntervalChanged();
        main.reachedQuota();
        main.addDomain();
        main.removeDomain();
    },
    bind: function(){
        // jQuery('.tooltip_container a').length && jQuery('.tooltip_container a').popover();
        jQuery('[data-toggle="popover"]:not(.js-argumentsbox-popover):not(.js-image-popover)').popover();
        jQuery('[data-toggle="tooltip"]').tooltip();
        if(jQuery.fn.swipebox) {
            jQuery('.swipebox').swipebox();
        }
        var argPopoverTemplate = '';
        argPopoverTemplate += '<div class="popover argumentsbox-popover" role="tooltip">';
        argPopoverTemplate += '<div class="arrow"></div>';
        argPopoverTemplate += '<h3 class="popover-title"></h3>';
        argPopoverTemplate += '<div class="popover-content margin-top-xs"></div>';
        argPopoverTemplate += '</div>';
        jQuery('.js-argumentsbox-popover:not(.js-image-popover)').popover({
            template: argPopoverTemplate
        });

        var imgPopoverTemplate = '';
        imgPopoverTemplate += '<div class="popover argumentsbox-popover image-popover" role="tooltip">';
        imgPopoverTemplate += '<div class="arrow"></div>';
        imgPopoverTemplate += '<h3 class="popover-title"></h3>';
        imgPopoverTemplate += '<div class="popover-content"></div>';
        imgPopoverTemplate += '</div>';
        jQuery('.js-argumentsbox-popover.js-image-popover').popover({
            template: imgPopoverTemplate,
            content: function() {
                return '<img src="' + $(this).attr('data-image') + '"' +  '/>';
            },
            html: true
        });


    },
    initTooltips: function() {
        jQuery('[data-toggle="tooltip"]').tooltip();
    },
    readMore: function(){
        if(jQuery.fn.readmore) {
          jQuery('.readmore').readmore({ 
              embedCSS: false, 
              collapsedHeight: 200,
              moreLink: '<a href="#" class="font-13">more</a>',
              lessLink: '<a href="#" class="font-13">Close</a>'
          });
          jQuery('.debate-readmore').readmore({
              embedCSS: false, 
              collapsedHeight: 60,
              moreLink: '<a href="#" class="font-13 hide-xxs">Read more</a>',
              lessLink: '<a href="#" class="font-13 hide-xxs">Close</a>'
          });

          jQuery('.debate-readmore-lg').readmore({
              embedCSS: false, 
              collapsedHeight: 158,
              moreLink: '<a href="#" class="h5">Read more</a>',
              lessLink: '<a href="#" class="h5">Close</a>'
          });
        }  
    },
    deleteDebate: function () {
        jQuery(document).on('click', '.remove_debate', function (e) {
            var _this = jQuery(this);
            e.preventDefault();
            var debateSlug = _this.attr('data-debate-slug');
            var confirmation = confirm('Are you sure to delete debates: ' + debateSlug);
            if (confirmation) {
                jQuery.ajax({
                    url: '/debates/delete/' + debateSlug,
                    type: 'delete',
                    success: function (e) {
                        var e = JSON.parse(e);
                        if (e.status == 'SUCCESS') {
                            jQuery(_this).parents('.js-debate').fadeOut(500, function () {
                                jQuery(this).remove();
                            });
                        } else if (e.status == 'ERROR') {
                            alert('Error: Can\'t delete this debate');
                        }
                    }
                });
            }

        });
    },
    logout: function () {
        jQuery(document).on('click', '.logout', function (e) {
            var _this = jQuery(this);
            e.preventDefault();
            var debateSlug = _this.attr('data-debate_slug');
            _this.after('<img style="width:auto;margin: 0 3% 1%;" src="/img/speakol-loading.gif">');
            
            jQuery.ajax({
                url: _this.attr("href"),
                type: 'post',
                success: function (e) {
                    var e = JSON.parse(e);
                    if (e.status == 'OK') {
                        window.location.reload();
                        if(window.parent) {
                            window.parent.postMessage(JSON.stringify({
                                'id': 'iframeMessenger:dfjdkfjd',
                                'type': 'reload'
                            }), "*");
                        }
                    } else if (e.status == 'ERROR') {
                        alert('Error: Can\'t Logout');
                    }
                }
            });

        });
    },
    deleteArgumentsbox: function() {
        jQuery(document).on('click', '.js-remove-argumentsbox', function(e) {
            var $this = jQuery(this);
            e.preventDefault();
            var argumentsboxURL = $this.attr('data-argumentsbox-url'),
                uuid = $this.attr('data-uuid') ? $this.attr('data-uuid') : '',
                url = '/argumentsbox/delete?argumentsbox_url=' + argumentsboxURL,
                confirmationMsg = 'Are you sure to delete arguments box at: ' + argumentsboxURL;
            if(uuid) {
                url += '&uuid=' + uuid;
                confirmationMsg += 'with uuid ' + uuid;
            }
            var confirmation = confirm(confirmationMsg);
            if(confirmation) {
                jQuery.ajax({
                    url: url,
                    type: 'delete',
                    success: function(data) {
                        var e = JSON.parse(data);
                        if(e.status === 'OK') {
                            $this.parents('.js-argumentsbox').fadeOut(500, function() {
                                jQuery(this).remove();
                            });
                        } else{
                            alert('Error: Can\'t delete this argumentsbox');
                        }
                    }
                });
            }
        });
    },
    deleteComparison: function () {
        jQuery(document).on('click', '.remove_comparison', function (e) {
            var _this = jQuery(this);
            e.preventDefault();
            var comparisonSlug = _this.attr('data-comparison-slug');
            var confirmation = confirm('Are you sure to delete comparison: ' + comparisonSlug);
            if (confirmation) {
                jQuery.ajax({
                    url: '/comparisons/delete/' + comparisonSlug,
                    type: 'delete',
                    success: function (data) {
                        var e = JSON.parse(data);
                        if (e.status == 'SUCCESS') {
                            _this.parents('.js-comparison').fadeOut(500, function () {
                                jQuery(this).remove();
                            });
                        } else if (e.status == 'ERROR') {
                            alert('Error: Can\'t delete this comparison');
                        }
                    }
                });
            }
        });
    },
    agrumentDelete: function () {

        jQuery(document).on('click', '.arg_delete', function (e) {
            e.preventDefault();
            if (!confirm('Are you sure you want to delete this agrument?')) {
                return false;
            }
            var _this = jQuery(this);
            var argumentId = jQuery(_this).attr('data-argument-id');

            jQuery.ajax({
                url: '/arguments/delete/' + argumentId,
                type: 'delete',
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == "error") {
                        return false;
                    }
                    jQuery(_this).parents('.js-argument').css("background-color", "#eff0f1").fadeOut(500, function () {
                        jQuery(this).remove();
                    });

                    jQuery(_this).parents('.comment_unit').css("background-color", "#eff0f1").fadeOut(500, function () {
                        jQuery(this).remove();
                    });
                }
            })

        });
    },
    replyDelete: function () {
        jQuery(document).on('click', '.reply_delete', function (e) {
            e.preventDefault();
            if (!confirm('Are you sure you want to delete this reply?')) {
                return false;
            }
            var _this = jQuery(this);
            var replyId = jQuery(_this).attr('data-reply-id');
            var url = '/replies/delete/' + replyId ;
            if(_this.attr('data-ad') === 'true') {
                url += '?ad=true' ;
            } 
            jQuery.ajax({
                url: url,
                type: 'delete',
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == "error") {
                        return false;
                    }
                    jQuery(_this).parents('.js-reply').css("background-color", "#eff0f1").fadeOut(500, function () {
                        jQuery(this).remove();
                    });

                    jQuery(_this).parents('.comment_unit').first().css("background-color", "#eff0f1").fadeOut(500, function () {
                        jQuery(this).remove();
                    });
                }
            })

        });
    },
    switchLang: function () {
        jQuery(document).on('change', '#arg-box-lang', function () {
            var lang = jQuery(this).val();
            var codeText = jQuery(jQuery('.modal.fade.in code').text()); // convert code text into html object
            codeText = $("<div></div>").append(codeText);
            codeText.find('div').attr('data-lang', lang);
            jQuery('.modal.fade.in code').text(codeText.html());
        });
    },
    avatarCroping: function () {
        var options =
                {
                    thumbBox: '.thumbBox',
                    spinner: '.spinner',
                    imgSrc: 'avatar.png'
                }
        var cropper = jQuery('.imageBox').cropbox(options);
        jQuery('.upload').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                options.imgSrc = e.target.result;
                cropper = jQuery('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        jQuery('#btnCrop').on('click', function () {
            var img = cropper.getDataURL()
            jQuery('.img_circl > img').attr('src', img);
            var data = cropper.getData();
            data = JSON.stringify(data);
            jQuery('.logo_crop_data').val(data);

        })
    },
    removeModerator: function () {
        jQuery(document).on('click', '.remove-moderator', function (e) {
            var _this = jQuery(this);
            e.preventDefault();
            var userId = _this.attr('data-user-id');
            var confirmation = confirm('Are you sure to Remove This Moderator?');
            if (confirmation) {
                jQuery.ajax({
                    url: '/moderates/remove/' + userId,
                    type: 'delete',
                    success: function (data) {
                        if (data.status == 'OK') {
                            _this.parents('.form_row').fadeOut(500, function () {
                                jQuery(this).remove();
                            });
                        }

                    },
                    error: function (xhr) {
                        var response = JSON.parse(xhr.responseXML);
                        alert(response.messages);
                    }
                });
            }

        });
    },
    previewClick: function() {
        jQuery('.preview-btn').on('click', function (e) {
            e.preventDefault();
            var $this = jQuery(this);
            var argumentbox = ( $this.attr("data-argumentbox") === "true");
            var pluginDivContent = jQuery("#gnrtd_code code").text();
            var app = jQuery(pluginDivContent).attr("data-app");
            var title = jQuery(pluginDivContent).attr("data-title"); 
            var no_title = jQuery(pluginDivContent).attr("data-no-title"); 
            var option = jQuery(pluginDivContent).attr("data-option"); 
            var lang = jQuery(pluginDivContent).attr("data-lang");
            var uuid = jQuery(pluginDivContent).attr("data-uuid");
            var url;
            if(argumentbox) {
                url = "/arguments/render?app=" + app;
                url += (no_title) ? "&no_title=true" : ("&title=" + title);
                url += "&option=" ;
                url += (option) ? option : '1' ;
                url += (uuid) ? "&uuid=" + uuid : '';
            } else {
                url = jQuery(pluginDivContent).attr("data-href");
                url += ( "&url=" + encodeURIComponent(window.location.origin) ); 
            }

            url += ("&lang=" + lang);
            var width =jQuery(pluginDivContent).attr("data-width"); 
            window.open(url, '','width='+ width  +',height=700,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); 
        }); 
    },
    onStatsIntervalChanged: function() {
        jQuery('.js-stats-select').on('change', function() {
                var rangeName = $(this).find('option:selected').attr('data-range-name');
                jQuery.ajax({
                    url: '/dashboard/updateStats',
                    type: 'get',
                    data: {
                        time: jQuery('.js-stats-select').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.status === 'OK') {
                            var data = data.data;
                            main.renderStats(data ,rangeName);
                        }
                    },
                    error: function (xhr) {
                        var response = JSON.parse(xhr.responseText);
                        alert(response.messages);
                    }
                });
        });
    },
    updateStats: function() {
        if(jQuery('.js-stats-select').length) {
            var rangeName = $(this).find('option:selected').attr('data-range-name');
            jQuery.ajax({
                url: '/dashboard/updateStats',
                type: 'get',
                data: {
                    time: jQuery('.js-stats-select').val()
                },
                dataType: 'json',
                success: function (data) {
                    if (data.status === 'OK') {
                        var data = data.data;
                        main.renderStats(data, rangeName);
                    }
                },
                error: function (xhr) {
                    var response = JSON.parse(xhr.responseXML);
                    alert(response.messages);
                }
            });
        }
    },
    renderStats: function(data, rangeName) {
        jQuery('.js-stats-range').html(rangeName);
        jQuery('.js-number-comments').html(data.arguments);
        jQuery('.js-number-votes').html(data.votes);
        jQuery('.js-number-activeusers').html(data.active_users);
        main.renderStatsIndicatorTemplate(jQuery('.js-stats-comments'), data.arguments_stat);
        main.renderStatsIndicatorTemplate(jQuery('.js-stats-votes'), data.votes_stat);
        main.renderStatsIndicatorTemplate(jQuery('.js-stats-activeusers'), data.users_stat);
    }, 
    renderStatsIndicatorTemplate: function(object, number) {
        number = parseInt(number);
        var template = "";
        if(number < 0) {
            number = 0 - number;
            if(phpVars.lang === 'ar') {
                template = '<i class="fa fa-increase-line fa-flip-vertical red"></i>&nbsp;<span class="weight-bold">' + number + '%</span> نقص ';  
            } else {
                template = '<span class="weight-bold">' + number + '%</span> decrease &nbsp;<i class="fa fa-increase-line fa-flip-vertical red"></i>';  
            }

        } else if (number === 0) {
            template = '<span class="weight-bold">' + number + "%</span>";
        } else {
            if(phpVars.lang === 'ar') {
                template = '<i class="fa fa-increase-line color-32a044"></i>&nbsp;<span class="weight-bold">' + number + '</span>% زيادة';  
            } else {
                template = '<span class="weight-bold">' + number + '%</span> increase &nbsp;<i class="fa fa-increase-line color-32a044"></i>';  
            }
        }
        object.html(template);
    },
    reachedQuota: function() {
        if($('#reachedQuota').length) {
            $('input, select, textarea').prop('disabled', true);
        }
    },
    addDomain: function() {
        $('.js-add-domain').on('click', function(e) {
            e.preventDefault();
            var $el = $(this),
                $websitescontainer = $el.parents('.js-websites-container').first(),
                $websites = $websitescontainer.find('.js-website'),
                noOfWebsites = $websites.length,
                planID = +$('#planId').val(),
                currentWebsiteLimit = 0,
                websiteLimits = {
                    1: 1,
                    2: 2,
                    3: 3,
                    4: 4
                };
            planID = (planID <=1 || planID > 4) ? 1 : planID;
            currentWebsiteLimit = websiteLimits[planID];
            if(noOfWebsites < currentWebsiteLimit) {
                var $newWebsite = $websites.last().clone();
                $newWebsite.find('.js-website-input').val('');
                $newWebsite.addClass('margin-top-sm');
                if(!$newWebsite.find('.js-remove-domain').length) {
                    var html = '';
                    html +='<button ';
                    html +='class="js-remove-domain remove-domain btn-reset no-radius absolute color-6e6f6f"';
                    html +='>';
                    html +='<i class="fa fa-times fa-lg"></i>';
                    html +='</button>';
                    $newWebsite.append(html);
                }
                $websitescontainer.append($newWebsite);
                noOfWebsites++;
            }
            
            if(noOfWebsites >= currentWebsiteLimit) {
                $el.prop('disabled', true).addClass('hide');
            }


        });
    },
    removeDomain: function() {
        $(document).on('click', '.js-remove-domain', function(e) {
            e.preventDefault();
            var $el = $(this),
                $website = $el.parents('.js-website').first(),
                $addDomain = $('.js-add-domain');
            $website.remove();
            $addDomain.removeClass('hide').prop('disabled', false);
        });
    }
};
function validateForm(){
    if($("#enableImages").is(':checked')) {
        var number_of_comparisons =  jQuery('.js-comparison-item').length;
        for(i=1;i<number_of_comparisons;i++){  
            if(jQuery("#logo_"+i).attr("name")=="logo_"+i){
                var cropButton = false;
                jQuery("#error_message_image_"+i).remove();
                var is_tools_tag_exist = jQuery("#logo_"+i).parent().find('.dev-tools .tools').length;
                var image = jQuery("#logo_" + i).attr('data-image');
                if(is_tools_tag_exist  || image){
                    if( jQuery("#logo_"+i).parent().find('.dev-tools .final').length || image){
                        cropButton = true;
                    }else {
                        var error_message = $('<div id="error_message_image_'+i+'" class="side_1_logoformError parentFormundefined formError" style="opacity: 0.87; position: absolute; top: 180px; left: 125px; margin-top: -52px;"><div class="formErrorContent">* Adjust your image and click the green button<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>');
                        jQuery("#error_"+i).append(error_message);
                        return false;
                    }   
                }else{
                    jQuery('.dropzone').addClass("error-message");
                    return false;
                }
            }
        }
        return true;
    }
}
jQuery(document).ready(function () {
    /* ------- my scripts ------- */
    main.init();

    // user notifications box Hover to open
    //=======================================
    jQuery('.popup a').click(function (e) {
        jQuery(this).parent().toggleClass('active').find('.popup_container').toggle().closest('#page-content-wrapper').toggleClass('dimmed');
    });

    if (jQuery.validationEngine) {
        jQuery('form').validationEngine({ 
            validateNonVisibleFields: true,
            onValidationComplete:function(form,status){
                return status;
            },
            scroll: false
        });
    }
   jQuery('.password-toggle').click(function (e) {
        e.preventDefault();
        jQuery('.password-toggle-container').toggleClass('hide');
    });

    // Aside full page height
    var pageHeight = jQuery(document).height();
    jQuery('.outer_aside').height(pageHeight);
    jQuery('.ht').height(pageHeight);
    jQuery(window).resize(function () {
        jQuery('.outer_aside').height(pageHeight);
        jQuery('.ht').height(pageHeight);
    });

    // Apps,  logo
    jQuery(document).on('change', '.js-upload-button' ,function () {
        var $uploadContainer = $(this).parents('.js-upload-container');
        var $preview = $uploadContainer.find('.js-preview');
        var $value = $uploadContainer.find('.js-picture-value');
        readURL(this, $preview, $value);
    });

    // Debates, Debater A profile
    jQuery("#uploadBtnA").change(function () {
        var img = "<img class='img-circle' src='' />";
        jQuery('#debaterA-img').css("opacity:", "1");
        jQuery('#debaterA-img').html(img);
        readURL(this, '#debaterA-img img');
    });

    // Debates, Debater B profile
    jQuery("#uploadBtnB").change(function () {
        var img = "<img class='img-circle' src='' />";
        jQuery('#debaterB-img').css("opacity", "1");
        jQuery('#debaterB-img').html(img);
        readURL(this, '#debaterB-img img');
    });

    jQuery("#enableImages").on('change' , function() {
        if(!$(this).is(':checked')) {
            $(this).parent().addClass('padding-bottom-30');
            $('.cropImage').addClass('hide');
            $('.js-remove-item').addClass('static');
            $('.js-description-input').addClass('margin-bottom-lg');
            $('.js-change-image-container').addClass('hide');
            $('.js-remove-item-container').removeClass('margin-top-sm');
        } else {
            $(this).parent().removeClass('padding-bottom-30');
            $('.cropImage').removeClass('hide');
            $('.js-remove-item').removeClass('static');
            $('.js-description-input').removeClass('margin-bottom-lg');
            $('.js-change-image-container').removeClass('hide');
            $('.js-remove-item-container').addClass('margin-top-sm');
        } 
    });

    var copy_sel = jQuery('.cp-generated-code').length && jQuery('.cp-generated-code');
    if (copy_sel) {
        copy_sel.click(function (e) {

            e.preventDefault();
        });

        copy_sel.clipboard({
            path: '/js/clipboard.swf',
            copy: function () {
                jQuery(this).text('Text Copied');
                return  jQuery('#gnrtd_code').text();
            }
        });

        jQuery('#noTitle').on('change', function(e) {
            $("#app_title").attr('disabled', $(this).is(":checked"));
        });
        jQuery('#arg-form').on('submit', function (e) {
            e.preventDefault();
            if(!$(this).validationEngine('validate')) {
                return;
            }
            var appId, appUrl, appWidth, appDesc, code, appTitle, noTitle, multipleUse, uuid;
            appId = jQuery('#app_id').attr('data-app_id');
            appUrl = jQuery('#app_url').val();
            appWidth = parseInt(jQuery('#app_width').val());
            appTitle = $("#app_title").val();
            appTitle = htmlEntities(appTitle);
            appDesc = jQuery('#app_desc').val();
            noTitle = jQuery("#noTitle").is(":checked");
            multipleUse = jQuery("#multipleUse").is(":checked");
            appOption = jQuery('#app_option').val();
            appOption = +appOption ? appOption : 1;
            code = '';
            code += '<div class="speakol-arguments-box" data-app="' + appId + '"';
            code += ' data-width="' + appWidth + '"';
            code += ' data-option="' + appOption + '"';
            code += ' data-lang="en"';
            if(noTitle) {
                code += ' data-no-title="true"'
            } else {
                code += ' data-title="' + appTitle  +  '"';
            }
            if(multipleUse) {
                code += ' data-uuid="' + window.uuid() + '"';
            }
            code += '></div>';
            code += "<script>";
            code += "(function(doc, sc, id) {";
            code += "var js, sjs = document.getElementsByTagName(sc)[0];";
            code += "if(doc.getElementById(id)) return;";
            code += "js = document.createElement(sc);";
            code += "js.src = '//plugins.speakol.com/js/sdk.js'; js.id=id;";
            code += "sjs.parentNode.insertBefore(js, sjs);";
            code += "})(document, 'script', 'speakol-sdk');";
            code += "</script>";
            jQuery('#gnrtd_code code').text(code);
            jQuery('.cp-generated-code').text('Copy Generated Code');
            jQuery('.modal').modal();


        });

    }
    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    // Comparison 1...3 logos
//    comparisonsLogoPerview(".upload");

    addComparison();
    deleteComparisonItem();
    avatarCroping();
    jQuery(document).on('submit', 'form', function (e) {
        _imageCrop();
    });
    function get_available_index(){
        for(var i=1;i<9;i++){
           if (jQuery("#comparison-items").find("#logo_"+i).length==0){
               return i;
           }
        }
    }
    // add more comparison  
    jQuery(document).on('click', '.js-change-image', function(e) {
        e.preventDefault();
        var $this = jQuery(this);
        $this.parents('.js-dropzone-container').find('input[type=file]').trigger('click');
        // $this.parents('.js-change-image-container').addClass('hide');
    });

    jQuery('input[type=file]').on('change', function() {
        var $dropzoneContainer = jQuery(this).parents('.js-dropzone-container');
        $dropzoneContainer.find('.js-change-image-container').addClass('hide');
        $dropzoneContainer.find('.js-remove-item-container').removeClass('margin-top-sm');
    });

    jQuery(document).on('click', '.tools .btn-cancel, .tools .btn-del', function(e) {
        var $dropzoneContainer = $('.js-dropzone-container').not(':has(.dev-tools .tools)');
        var $input = $dropzoneContainer.find('input[type=file]');
        $input.on('change', function() {
            var $dropzoneContainer = jQuery(this).parents('.js-dropzone-container');
            $dropzoneContainer.find('.js-change-image-container').addClass('hide');
            $dropzoneContainer.find('.js-remove-item-container').removeClass('margin-top-sm');
        });

        $dropzoneContainer.find('.js-change-image-container').removeClass('hide');
        $dropzoneContainer.find('.js-remove-item-container').addClass('margin-top-sm');
    });
    function addComparison() {
        jQuery('.js-new-comparison .js-remove-item').addClass('hide');
        jQuery('.js-add-item').click(function (e) {
            e.preventDefault();
            var isEdit = jQuery(this).hasClass('js-add-edit');
            var align =  +$('[name="align"]:checked').val(),
                acceptedLength = align ? 4 : 8;
            jQuery('.js-remove-item').removeClass('hide');
            var comparisonsLength = jQuery('.js-comparison-item').length;
            var available_index = comparisonsLength;
            if (comparisonsLength < acceptedLength) {
                //clone the comparison item 
                var clone = jQuery('.js-comparison-item').last().clone(true, true);
                if (jQuery(".js-comparison-item").find("#logo_"+comparisonsLength).length){
                    available_index = get_available_index();
                }
                jQuery("#data_order").val(jQuery("#data_order").val()+","+available_index);

                // set placeholder value for new comparisons

                clone.find('.js-id-input').attr('name', 'sides[' + (available_index - 1) +'][id]').val(0);
                clone.find('.js-title-input input[type=text]').attr({
                    name: isEdit ? 'sides[' + (available_index - 1) + '][title]': 'side_' + available_index + '_title',
                    id: 'side_' + available_index + '_title',
                });
                clone.find('.js-title-input input[type=text]').prop('value',"");
                clone.find('.js-description-input textarea').prop('value', "");
                clone.find('.js-description-input textarea').attr({
                    name: isEdit ? 'sides[' + (available_index - 1) + '][description]' : 'side_' + available_index + '_description',
                    id: 'side_' + available_index + '_description',
                });
                if($('#enableImages').is(':checked')) {
                    clone.find('.js-change-image-container').removeClass('hide');
                }
                clone.find('.dropzone').attr({
                    name: 'logo_' + available_index ,
                    id: 'logo_' + available_index ,
                    'data-image': '',
                });
                clone.find('.dropzone input[type=file]').attr({
                    name: 'file_logo_'+ available_index,
                    id:   'file_logo_'+ available_index,
                });

                clone.find('.cropImage input[type=hidden]').attr({
                    name: isEdit ? 'sides[' + (available_index - 1)+ '][logo]': 'side_' + available_index + '_logo',
                    id: 'side_' + available_index + '_logo',
                });
                // clone.find('.validation_message').attr({
                //     id: 'validation_message_' + available_index,
                // });
                clone.find('.cropImage input[type=hidden]').prop('value',"");
                clone.find('.error_message').attr({
                    id: 'error_'+ available_index,
                });
                clone.find('.cropWrapper').remove();
                clone.find('.dropzone input[type=file]').attr({
                    style:"position: absolute;"
                });
                clone.find('.final').remove();
                clone.find('.tools').remove();
                clone.find('.dropzone').html5imageupload();
                clone.find('.dropzone img').remove();
                clone.find('.dropzone input[type=file]').on('change', function() {
                    jQuery(this).parents('.js-dropzone-container').find('.js-change-image-container').addClass('hide');
                });
                jQuery('#comparison-items').append(clone);
                comparisonsLength++;
                
            }
            if (comparisonsLength === acceptedLength) {
                jQuery('.js-add-item').addClass('hide');
            }
        });
    }
    function remove_item_from_data_order(list,item_id){
         var list_of_items_id  = list.split(",");
         var result = "";
         for(i=0;i<list_of_items_id.length;i++)
         {
           if(list_of_items_id[i]!=item_id)
               {
                   result += list_of_items_id[i] + ",";
               }
         }
         return result;
    }
    function deleteComparisonItem() {
        jQuery(document).on('click', '.js-remove-item', function (e) {
         
            e.preventDefault();
            var comparisonsLength = jQuery('.js-comparison-item').length;
            var isEdit = jQuery('.js-add-item').hasClass('js-add-edit');
           
                
            if (comparisonsLength > 2) {
                var item_id = jQuery(this).parents('.js-comparison-item').find('.dropzone').attr("id").split("_")[1];
                jQuery("#data_order").val(remove_item_from_data_order(jQuery("#data_order").val(),item_id)); 
                jQuery('.js-remove-item').removeClass('hide');
                jQuery(this).parents('.js-comparison-item').remove();
                comparisonsLength--;
                jQuery('.js-add-item').removeClass('hide');
            }
            if (comparisonsLength <= 2) {
                jQuery('.js-add-item').css("margin-top", "0px");
                jQuery('.js-remove-item').addClass('hide');
                if(!isEdit) {
                    jQuery('.js-remove-item').addClass('hide');
                }
            }
        });
    }
    function avatarCroping() {
        var options =
                {
                    thumbBox: '.thumbBox',
                    spinner: '.spinner',
                    imgSrc: 'avatar.png'
                }

        jQuery(document).on('change', '.uploadBtn', function () {
            var _this = this;
            var reader = new FileReader();
            reader.onload = function (e) {
                options.imgSrc = e.target.result;
                var indx = jQuery(_this).parents('li').children('.imageBox').index('.imageBox');
                cropper[indx] = jQuery(_this).parents('li').children('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        });
    }
    function _imageCrop() {
        jQuery('.uploadBtn').each(function () {

            var _this = jQuery(this);

            var indx = jQuery(_this).parents('li').find('.imageBox').index('.imageBox');
            var crop = cropper[indx];

            if (!crop) {
                return;
            }

            jQuery(_this).parents('li').find('.cropprev').attr('src', crop.getDataURL());

            var d = JSON.stringify(crop.getData());
            jQuery(_this).parents('li').find('.logo_crop_data').val(d);
        });
    }
    // preview comparison logo
    function comparisonsLogoPerview(selector) {

        jQuery(document).on('change', selector, function () {
            var _this = jQuery(this);

            if (this.files && this.files[0]) {
                if (!this.files[0].type.match('image.*')) {
                    jQuery(element).val('');
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    _this.parent().css('background-image', 'url("' + e.target.result + '")');
                }
                reader.readAsDataURL(this.files[0]);
            }

        });


    }
    //Modifed By Mohamed Ragab
    function readURL(input, preview, value) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var img = new Image();
                img.src = e.target.result;
                img.onload = function() {
                    var canvas = document.createElement('canvas');
                    canvas.width = 256;
                    canvas.height = 256;
                    var context = canvas.getContext("2d");
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(img, 0, 0 , 256, 256);
                    var finalImg = canvas.toDataURL("image/png");
                    if(jQuery(value).length) {
                        jQuery(value).val(finalImg);
                    }

                    jQuery(preview).attr('src', finalImg);
                };
            };

            reader.readAsDataURL(input.files[0]);
        }

    }

    $('[name="align"]').on('change', function(e) {
       var $this =$(this);
       var value = +$this.val();
       var $comparisonItems = $('.js-comparison-item');
       var isEdit = $('.js-add-item').hasClass('js-add-edit');
       if(value) {
           $comparisonItems.find('.media').addClass('col-sm-3').addClass('overflow-visible');
           $('.js-dropzone-container').removeClass('pull-left').removeClass('pull-right');
           $('.js-title-input').removeClass('horizontal').removeClass('six')
           .addClass('width-78').addClass('margin-center').removeClass('pull-right');
           $('.js-description-input textarea').removeClass('horizontal');
           if(isEdit) {
               $('.js-description-input textarea').removeClass('description-edit');
           }
           $('.js-description-input').addClass('width-78').addClass('margin-center');
           $('.js-add-item-container').removeClass('row').addClass('col-sm-3');
           $('.js-add-item').removeClass('horizontal');
           $('.js-remove-item').removeClass('horizontal');
           $('.js-change-image-container').addClass('margin-center').addClass('width-78').removeClass('padding-right-5').addClass('margin-bottom-lg')
           .removeClass('margin-bottom-sm');
           $('.js-remove-item-container').addClass('margin-center').addClass('width-78').removeClass('horizontal').removeClass('margin-bottom-sm');
           if(isEdit) {
               $('.js-remove-item-container').addClass('vertical').removeClass('padding-right-5').removeClass('padding-left-5');
               $('.js-description-input').addClass('vertical');
           }
           if($comparisonItems.length >= 4) {
               $items = $comparisonItems.toArray();
               $items.splice(0,4);
               $items.forEach(function($item) {
                   $item.remove();
               });
               $('.js-add-item').addClass('hide');
           }
           
           
       } else {

           $('.js-comparison-item').find('.media').removeClass('col-sm-3').removeClass('overflow-visible');
           $('.js-title-input').addClass('horizontal').addClass('six')
           .removeClass('width-78').removeClass('margin-center');
           $('.js-description-input textarea').addClass('horizontal');
           if(isEdit) {
               $('.js-description-input textarea').addClass('description-edit');
               $('.js-description-input').removeClass('vertical');
           }
           $('.js-description-input').removeClass('width-78').removeClass('margin-center');
           $('.js-add-item-container').addClass('row').removeClass('col-sm-3');
           $('.js-add-item').addClass('horizontal');
           $('.js-remove-item').addClass('horizontal');
           $('.js-change-image-container').removeClass('margin-center').removeClass('width-78').addClass('padding-right-5').removeClass('margin-bottom-lg')
           .addClass('margin-bottom-sm');
           $('.js-remove-item-container').removeClass('margin-center').removeClass('width-78').addClass('horizontal').addClass('margin-bottom-sm');
           if(isEdit) {
               $('.js-remove-item-container').removeClass('vertical').addClass('padding-right-5').addClass('padding-left-5');
           }
           if($comparisonItems.length < 8) {
               $('.js-add-item').removeClass('hide');
           }

           if($('#lang').val() === 'ar') {
               $('.js-dropzone-container').addClass('pull-right');
               $('.js-title-input').addClass('pull-right');
           } else {
               $('.js-dropzone-container').addClass('pull-left');
           }

       }
    });

});
