<div class="row helvetica-font">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
<header class="index-header helvetica-font container no-padding margin-top-sm margin-bottom-sm padding-top-sm padding-bottom-sm">
    <div class="home-logo pull-left">
        <a href="https://plugins.speakol.com/"> 
            {{ image("img/logo3.png", "alt": "Speakol for websites", "title": "Speakol for websites") }}
        </a>
    </div>
    <div class="dropdown pull-right">
        <button class="btn-reset border-e4e4e4 no-padding radius-3 dropdown-toggle text-center color-acb6bb lang-dropdown" type="button" id="langDropdown" data-toggle="dropdown" aria-expanded="true">
            {{ lang === 'ar' ? 'Ar' : 'En' }}&nbsp;<i class="sp-icon sp-caret"></i>
        </button>
        <ul class=" small-tip border-transparent radius-5 dropdown-menu right-0 left-auto color-565656 community-dropdown-menu" role="menu" aria-labelledby="langDropdown">
            <li role="prensentation" class="community-dropdown-item lang-halflength border-halflength halflength-f5f4f4"><a class="{{ lang === 'ar' ? '' : 'weight-bold' }}" href="http://speakol.dev/public/?lang=en" title="" >English</a></li>
            <li role="prensentation" class="padding-bottom-sm padding-top-sm community-dropdown-item"><a class="{{ lang === 'ar' ? 'weight-bold' : '' }}" href="http://speakol.dev/public/?lang=ar" title="" >العربية</a></li>
        </ul>
    </div>
    <div class="pull-right">
        <a href="http://speakol.dev/public/apps/login" class="anchor-reset inline-block h4 home-login color-57b25f radius-3 border-57b25f">Login</a>
        <a href="http://speakol.dev/public/pricing" class="green-anchor anchor-reset inline-block weight-bold h4 home-create nowrap white radius-3 bg-57b25f border-57b25f">Create an accoun</a>
    </div>
</header>
<div class="row add-container helvetica-font">
    <div class="add-content text-center margin-center">
        <h1 class="white font-50 margin-bottom-sm weight-thin">
{% if introParagraph is defined and introParagraph %}
{{ introParagraph }}
{% else %}
            Build your community and increase user engagement on your website
{% endif %}
        </h1>
        <a href="http://speakol.dev/public/pricing/" class="weight-thin anchor-reset add-link inline-block h4 nowrap white radius-3 bg-57b25f border-57b25f green-anchor">Add Speakol to your website</a>
    </div>
</div>
<div class="row edge-top bg-46ab5b helvetica-font">
    <div class="">
        <section class="row plugin-description margin-center">
            <div class="col-sm-6 spacing-right">
                <div class="media">
                    <div class="pull-left">
                        <i class="home-icon debate-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="weight-thin margin-top-sm">Debates</h2>
                    </div>
                </div>
                <p class="margin-top-sm padding-top-sm weight-light">
                    Provide an organized platform allowing users to engage in online debates by supporting or refuting various topics and statements.
                </p>
                <p class="">
                    <a class="weight-light anchor-reset inline-block radius-5 create-link color-3c4851 bg-white" href="http://speakol.dev/public/pricing/">Create your first debate</a>
                </p>
            </div>
            <div class="col-sm-6">
                <a href="#debateModal" data-toggle="modal">
                    <img src="/public/img/home-debate.png" alt="debate example" class="debate-example">
                </a>
            </div>
        </section>
    </div>
    <div class="plugin-description-container">
        <section class="row plugin-description margin-center plugin-padding">
            <div class="col-sm-6">
                <a href="#comparisonModal" data-toggle="modal">
                    <img src="/public/img/home-comparison.png" alt="comparison example" class="comparison-example">
                </a>
            </div>
            <div class="col-sm-6 spacing-left margin-top-lg">
                <div class="media margin-top-lg padding-top-lg">
                    <div class="pull-left">
                        <i class="home-icon comparison-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="weight-thin margin-top-sm">Comparisons</h2>
                    </div>
                </div>
                <p class="weight-light font-22 italic margin-top-sm padding-top-sm">
                    Give users the chance to express themselves!
                </p>
                <p class="no-margin font-20 weight-light">
                     Users may now respond to polls by selecting their favorite items, providing percentage ratings for up to four items, and even sharing their thoughts.
                </p>
                <p class="">
                    <a class="weight-light anchor-reset inline-block radius-5 create-link color-3c4851 bg-white" href="http://speakol.dev/public/pricing/">Create your first comparison</a>
                </p>
            </div>
        </section>
    </div>
    <div class="plugin-description-container">
        <section class="row plugin-description plugin-padding margin-center">
            <div class="col-sm-6 spacing-right">
                <div class="media">
                    <div class="pull-left">
                        <i class="home-icon argumentsbox-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="weight-thin margin-top-xs">Argument Box</h2>
                    </div>
                </div>
                <p class="margin-top-sm padding-top-sm weight-light">
                    Manage how your users interact with your content, allowing them to vote for or against various topics with the ability to support their positions.
                </p>
                <p class="">
                    <a class="weight-light anchor-reset inline-block radius-5 create-link color-3c4581 bg-white" href="http://speakol.dev/public/pricing/">Create your first Argument Box</a>
                </p>
            </div>
            <div class="col-sm-6">
                <a href="#argumentModal" data-toggle="modal">
                    <img src="/public/img/home-argumentsbox.png" alt="argumentsbox example" class="argumentsbox-example">
                </a>
            </div>
        </section>
    </div>
</div>
<article class="padding-top-lg helvetica-font row bg-f7f7f7">
    <div class="margin-top-lg padding-top-sm row color-29303b text-center margin-center why-header">
        <h2 class="font-50 margin-top-sm weight-thin">Why Speakol?</h2>
        <p class="h3 margin-top-lg padding-top-sm weight-light">We’re far from a below the line, traditional and linear
commenting system. We’re different, there's no debating that.</p>
    </div>
    <div class="row why-container margin-center">
        <div class="col-sm-6">
            <section class="margin-top-lg why-section engage-section">
                <h3 class="margin-top-xs weight-bold">Engage your readers</h3>
                <p class="no-margin">Give readers a structured platform to become part of your content.</p>
            </section>
            <section class=" margin-top-lg why-section learn-section">
                <h3 class="margin-top-xs weight-bold">Learn about readers</h3>
                <p class="no-margin">View reader sentiment instantly and access the vital numbers too.</p>
            </section>
            <section class=" margin-top-lg why-section build-section">
                <h3 class="margin-top-xs weight-bold">Build, don’t replace</h3>
                <p class="no-margin">Use alongside your existing commenting systems like Disqus or LiveFyre.</p>
            </section>
        </div>
        <div class="col-sm-6">
            <section class=" margin-top-lg why-section easy-section">
                <h3 class="margin-top-xs weight-bold">Easy to use</h3>
                <p class="no-margin">It takes a few minutes to register and integrated into your website.</p>
            </section>
            <section class=" margin-top-lg why-section socially-section">
                <h3 class="margin-top-xs weight-bold">Socially friendly</h3>
                <p class="no-margin">Easily share debates to traditional social networks and drive traffic to your site.</p>
            </section>
            <section class=" margin-top-lg why-section moderate-section">
                <h3 class="margin-top-xs weight-bold">Moderate activity</h3>
                <p class="no-margin">Easy to use moderation tools to ensure healthy debates and discussions.</p>
            </section>
        </div>
    </div>
    <div class="row text-center why-footer">
        <h4 class="h3 margin-top-sm color-525861 weight-light">What are you waiting for?</h4>
        <p class="margin-bottom-lg padding-bottom-lg">
            <a href="http://speakol.dev/public/pricing" class="anchor-reset add-link inline-block margin-bottom-lg h4 nowrap white radius-3 bg-57b25f border-57b25f green-anchor">Add Speakol to your website</a>
        </p>
    </div>
</article>
<footer class="index-footer no-margin helvetica-font">
    <div class="row footer-content margin-center">
        <div class="row footer-connect">
            <div class="col-sm-4">
                <a class="inline-block margin-top-sm" href="http://speakol.dev/public">{{ image("../public/img/logo_white.png", "alt": "Speakol", "title": "Speakol") }}</a>
            </div>
            <div class="col-sm-4">
                <!-- Begin MailChimp Signup Form -->
            
                <form action="//speakol.us9.list-manage.com/subscribe/post?u=4867705d0bad713092d2d1928&amp;id=ab84493550" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <label for="mail_list" class="font-17 white weight-normal margin-bottom-sm padding-bottom-sm color-c3c5ca">Join our mailing list</label>
                    <input type="text" id="mail_list" name="EMAIL" placeholder="your email address" class="mail-input"/>
                    <input type="submit" value="JOIN" class="btn radius-5 bg-57b25f mail-submit green-anchor"/>
                </form>
            </div>
            <div class="col-sm-4 social-connect">
                <h3 class="font-17 white weight-normal margin-bottom-sm padding-bottom-sm color-c3c5ca"> Get connected </h3>
                <a target="_blank" class="soc_cnnct facebook" href="https://facebook.com/Speakol" title="Facebook"> Facebook</a>
                <a target="_blank" class="soc_cnnct twitter" href="http://www.twitter.com/speakol_" title="Twitter"> Twitter</a>
                <a target="_blank" class="soc_cnnct gPlus" href="https://plus.google.com/115379961634886743482/" title="Google plus"> google pluse</a>
                <a target="_blank" class="soc_cnnct instagram" href="http://instagram.com/speakol_" title="Instagram"> Instagram</a>
            </div>
        </div>
        <div class="row f-links">
            {{ linkTo('pricing', 'Pricing') }}
            {{ linkTo('about', 'About Us') }}
            {{ linkTo('contactus', 'Contact Us') }}
            {{ linkTo('terms', 'Terms') }}
            {{ linkTo('privacy', 'Privacy') }}
            {{ linkTo('apps', 'Apps') }}
            {{ linkTo('careers', 'Careers') }}
            {{ linkTo('press', 'Press') }}
        </div>
        <p class="weight-light text-center color-c3c5ca h5">
            Copyright © 2015 - All rights reserved, Speakol | part of Naqeshny for Information Technology | 36 Mussadak Street Dokki, Giza, Egypt. 
        </p>
        <p class="weight-light text-center color-c3c5ca h5 margin-top-sm">
            Speakol UK Ltd | Company Number 9234670 | Registered in England and Wales
        </p>
    </div>
</footer>
<div class="modal fade sample-modal helvetica-font" id="debateModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">Create Debate</h4>
            </div>
            <div class="modal-body"> 
                <img src="/public/img/debate-sample.png" alt="Create debate at Speakol"/>
                <a class="green-anchor weight-thin" href="http://speakol.dev/public/pricing" title="Create debate at Speakol">Start now and create your debate</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade sample-modal helvetica-font" id="comparisonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">Create Comparison</h4>
            </div>
            <div class="modal-body"> 
                <img src="/public/img/comparison-sample.png" alt="Create comparison at Speakol"/>
                <a class="green-anchor weight-thin" href="https://plugins.speakol.com/pricing" title="Create comparison at Speakol">Start now and create your comparison</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade sample-modal helvetica-font" id="argumentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">Create Argument Box</h4>
            </div>
            <div class="modal-body">
                <img src="/public/img/argumentbox-sample.png" alt="Create Argument Box at Speakol"/>
                <a class="green-anchor weight-thin" href="https://plugins.speakol.com/pricing" title="Create Argument Box at Speakol">Start now and create your argument box</a>
            </div>
        </div>
    </div>
</div>
