<?php
namespace Speakol\Backend\Controllers;
class AnalysisController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->currentUserIs(array('2', '3'), 'apps/login');
    }
    public function indexAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        return $this->response->redirect('dashboard');
        $token = $this->getAppToken();
        set_exception_handler(array($this, 'exceptionHandler'));
        $hdrs = array('Authorization:' . $token,);
        $intPage = $this->request->get('page');
        $intLimit = 5;
        $params = array('app_id' => $this->getAppData()->id, 'module' => $this->request->get('module', 'string', ''), 'order' => $this->request->get('order'), 'page' => ($intPage) ? $intPage : 1, 'limit' => ($intLimit) ? $intLimit : 2, 'token' => $token);
        $objAnal = $this->utility->iCurl('/apps/' . $params['app_id'] . '/stats/' . $params['page'] . '/' . $params['limit'], 'get', $hdrs, $params, fasl, true);
        if (is_null($objAnal) || $objAnal['status'] == 'ERROR') {
            $this->utility->throwCustomException($objAnal['messages']);
        }
        $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
        if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
            $objUserProfile = false;
        }
        $this->view->title = 'Speakol | Analysis';
        $this->view->analysis = $objAnal['data'];
        $this->view->linkfor = '/analysis?app=' . $params['app_id'] . '&page=' . $params['page'] . '&order=' . $params['order'];
        $this->view->linkforfilter = '/analysis?app=' . $params['app_id'] . '&page=' . $params['page'] . '&order=' . $params['order'] . '&module=';
        $this->view->linkfororder = '/analysis?app=' . $params['app_id'] . '&module=' . $params['module'] . '&page=' . $params['page'] . '&order=';
        $this->view->nextp = '/analysis?app=' . $params['app_id'] . '&module=' . $params['module'] . '&page=' . (intval($params['page']) + 1) . '&order=' . $params['order'];
        $this->view->perpage = $intLimit;
        $this->view->fburl = $this->utility->getAbsoluteURL('users', 'facebook');
        $this->view->twitterurl = $this->utility->getAbsoluteURL('users', 'twitter');
        $this->view->googleurl = $this->utility->getAbsoluteURL('users', 'google-plus');
        $this->view->isLoggedIn = $objUserProfile ? true : false;
        $this->view->logout = $objUserProfile ? $this->url->get("users/logout") : '';
        $this->view->LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data : false;
        $this->view->jsVars = array('headers' => $this->utility->prepareHeadersForJS($hdrs),);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string)->addCss('css/moderate.css?' . $this->config->application->cache_string);
        $this->assets->collection('header')->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->collection('header-ie9')->addJs('http://html5shim.googlecode.com/svn/trunk/html5.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main-analysis-external.js?' . $this->config->application->cache_string)->addJs('js/core.js?' . $this->config->application->cache_string)->addJs('js/analysis-external.js?' . $this->config->application->cache_string);
    }
    function exceptionHandler($exception) {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->view->errorMessage = $exception->getMessage();
        var_dump(__FUNCTION__);
        exit(0);
        $this->view->pick("argumentsbox/error");
    }
}
