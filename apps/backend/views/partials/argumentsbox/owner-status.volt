<span class="inline-block circle owner-status hide-xs {{ (status) ? 'border-green' : 'border-light-grey'  }}"></span>
<span class="owner-status-text hide-xs {{ (status) ? "green" : "light-grey"  }}">{{ (status) ? "" : ""  }}</span>
