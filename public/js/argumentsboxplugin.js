(function() {
    var ArgumentsboxPlugin = function() {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = phpVars.lang;
        this.websiteURL = phpVars.url;
        this.audio = $('#audioEnabled').length;
        this.recorder = speakolRecorder;
        this.timer = new Timer(45, undefined, function() {
            this.recorder.stop();
            this.timer.$display.parents('.js-record-bar').first().find('.js-recording-text').addClass('hide');
            var finishedText = this.timer.$display.attr('data-finished');
            this.timer.$display.html(finishedText);
        }.bind(this));
        this.bindEvents();
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };


    ArgumentsboxPlugin.Events = {
        '.js-vote': 'vote',
        '.js-unvote': 'unvote',
        '.js-submit-arg': 'submitArg',
        '.js-record-button': 'startRecording',
        '.js-cancel-record': 'cancelRecording',
        '.js-send-record': 'sendRecording',
        '.js-edited': 'showEditedTime'
    };

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(window.location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function updateVotes(votes) {
        var pros = parseInt(votes[0]['votes']);
        var cons = parseInt(votes[1]['votes']);
        var canvasData = {
            "pro-votes": pros,
            "con-votes": cons,
            "pro-color": '#4dac54',
            "con-color": '#d11b1f'
        };
        $('.js-voting-circle .js-data').text(JSON.stringify(canvasData));
        $('.js-voting-circle').debateVersus();
        var total = votes.reduce(function(prev, next) {
            var prevVote = +prev.votes;
            var nextVote = +next.votes;
            return prevVote + nextVote;
        });
        votes.forEach(function(vote) {
            $('.js-side-votes[data-side-id="'+ vote.side_id  + '"]').text(vote.votes);
            if(total) {
                vote.votes = +vote.votes;
                var percentage = Math.round((vote.votes * 100) / total) + '%';
            } else {
                percentage = '0%';
            }
            $('.js-side-percentage[data-side-id="'+ vote.side_id  + '"]').text(percentage);
            var $progressBar = $('.js-progress-bar[data-side-id="' + vote.side_id + '"]');
            $progressBar.css('width', percentage)
            .find('.js-progress-sr')
            .text(percentage);
        });
    }


    ArgumentsboxPlugin.prototype.submitArg = function($el, e, audio) {
        audio = audio || false;
        var $argumentForm = $el.parents('.js-argument-form'),
            argumentId = $argumentForm.find('input[name="argumentbox_id"]').val(),
            context = $argumentForm.find('textarea[name="comment"]').val(),
            link = $argumentForm.find('input[name="link"]').val(),
            lang = this.lang,
            url = this.url,
            sideId = '',
            audioEnabled = this.audio;
        $argumentForm.find('textarea, input, button').prop('disabled', true);
        var formData = new FormData();
        formData.append('context', context);
        var file;
        if(audio) {
            if(this.recorder.checkIfNoAudio()) {
                var message = lang === 'ar' ? 'لا يوجد صوت' : 'No audio detected';
                this.showErrorMsg(message);
                return;
            }
            formData.append('audio', 1);
            file = this.recorder.exportOgg(); 
        } else {
            file = $argumentForm.find('input[name="attached"]').prop('files')[0];
            if(!!file && file.size > 512 * 1024) {
                var message = lang === 'ar' ? 'يجب أن يكون حجم الصورة أقل من 512 كيلوبايت' : 'Image size must be smaller than 512 Kilobytes';
                this.showErrorMsg(message);
                return;
            }
        }
        formData.append('file', file);
        formData.append('argument_id', argumentId);
        formData.append('link', phpVars.url ? phpVars.url : '');
        formData.append('action', '/arguments/insert');
        formData.append('lang', lang);
        $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: formData
        }).then(function(data) {
            if(data.status === 'OK') {
                $argumentForm.find('textarea, input, button').prop('disabled', false);
                $argumentForm.find('textarea[name="comment"]').val('');
                var argumentId = data.data.argument_id;
                sideId = data.data.side_id;
                var right = $('.js-side[data-side-id="'+ sideId + '"]').attr('data-align') === 'right';
                return $.ajax({
                    url: '/arguments/render/argument',
                    type: 'GET',
                    data: {
                        'argument_id': argumentId,
                        'lang': lang,
                        'right': right,
                        'audio': audioEnabled
                    }
                });

            }
        }).done(function(data) {
            data = '<li>' + data + '</li>';
            $('.js-side[data-side-id="' + sideId +'"]')
            .find('.js-arguments')
            .prepend(data);
            addReadMore();
        }).fail(this.errorHandler.bind(this));
    };


    ArgumentsboxPlugin.prototype.startRecording = function($el) {
      var $argumentForm = $el.parents('.js-argument-form'),
          $recordBar = $argumentForm.find('.js-record-bar'),
          $normalSubmit = $argumentForm.find('.js-normal-submit'),
          $timer = $recordBar.find('.js-timer');
      $normalSubmit
      .addClass('hide')
      .find('input, button')
      .prop('disabled', true);
      $recordBar
      .removeClass('hide')
      .find('button')
      .prop('disabled', false);
      $recordBar.find('.js-recording-text').removeClass('hide');
      this.timer.setDisplay($timer);
      var start = function() {
          this.timer.set();
          this.recorder.record();
      }.bind(this);

      if(this.recorder.initialized) {
          if(this.recorder.recording) {
              this.recorder.stop();
          }
          start();
      } else {
          this.recorder.init(start);
      }

    };

    ArgumentsboxPlugin.prototype.cancelRecording = function($el) {
        removeRecordBar($el);
        this.recorder.stop();
        this.timer.reset();
    };


    function removeRecordBar($el) {
        var $argumentForm = $el.parents('.js-argument-form'),
            $recordBar = $argumentForm.find('.js-record-bar'),
            $normalSubmit = $argumentForm.find('.js-normal-submit');
      $normalSubmit
      .removeClass('hide')
      .find('input, button')
      .prop('disabled', false);
      $recordBar
      .addClass('hide')
      .find('button')
      .prop('disabled', true);
    }


    ArgumentsboxPlugin.prototype.sendRecording = function($el, e) {
        $el.text('Sending....');
        this.recorder.stop();
        this.timer.reset();
        setTimeout(function() {
            $el.text('Send');
            removeRecordBar($el);
            if(this.recorder.error) {
                var errorMsg = this.lang === 'ar' ? 'لم تقم بالسماح للميكروفون بالعمل' : 'You haven\'t allowed the microphone';
                this.showErrorMsg(errorMsg); 
            } else {
                this.submitArg($el, e, true);
            }
        }.bind(this), 200);
    };


    ArgumentsboxPlugin.prototype.vote = function($el) {

        var argumentId = $el.attr('data-argumentsbox-id'),
            sideId =$el.attr('data-side-id'),
            lang = this.lang,
            url = this.url,
            promise;
        $el.prop('disabled', true);
        if(!argumentId) {
            promise = this.createArgumentsbox($el).then(function(data) {
                if(data.status === 'OK') {
                    data = data.data['0'];
                    argumentId = data.id;
                    var sides = [data.sides['0'].id, data.sides['1'].id];
                    updateSides(argumentId, sides, lang);
                    sideId = $el.attr('data-side-id');
                } 
            });
        } else {
            promise = $.Deferred().resolve().promise();
        }

        promise.then(function() {

            return $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: '/argumentsbox/vote/' + argumentId + '/' + sideId,
                    lang: lang,
                    link: phpVars.url ? phpVars.url : ''
                }
            });
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                var votes = data.data;
                updateVotes(votes);
                var $argumentForm = $('.js-argument-form[data-side-id="'+ sideId  +'"]');
                var $unvoteContainer = $('.js-unvote-container[data-side-id="'+ sideId  +'"]');
                $('.js-unvote-container').addClass('hide');
                $('.js-argument-form').addClass('hide');
                $('.js-argument-form').addClass('opacity-0');
                $('.js-vote').removeClass('hide');
                $unvoteContainer.removeClass('hide');
                ramjet.transform($el.get(0), $unvoteContainer.get(0), {
                    done: function() {
                        $unvoteContainer.removeClass('hide');
                        setTimeout(function() {
                            $argumentForm.removeClass('opacity-0');
                        }, 1);
                    },
                    easing: quadInOutEasing,
                    duration: 300

                });
                $el.addClass('hide');
                $unvoteContainer.addClass('hide');
                $argumentForm.removeClass('hide');
            }
        }).fail(this.errorHandler.bind(this));
    };


    ArgumentsboxPlugin.prototype.createArgumentsbox = function($el) {
        var formData = new FormData(),
            url = this.url,
            lang = this.lang,
            pluginOwner = +$('#pluginOwner').val(),
            paidPlan = +$('#paidPlan').val(),
            websiteURL = this.websiteURL,
            postDataObject = {},
            uuid = '';
        uuid = getParameterByName('uuid');
        formData.append('app_id', phpVars.app_id);
        postDataObject['app_id'] = phpVars.app_id;
        formData.append('url', phpVars.url);
        postDataObject['url'] =  phpVars.url;
        formData.append('width', phpVars.width);
        postDataObject['width'] = phpVars.width;
        formData.append('description', phpVars.description);
        postDataObject['description'] = phpVars.description;
        formData.append('token', phpVars.token);
        postDataObject['token'] = phpVars.token;
        formData.append('option', $("#option").val());
        postDataObject['option'] = $("#option").val();
        formData.append('code', uuid);
        postDataObject['code'] = uuid;
        if(!$("#noTitle").val()) {
            formData.append('title', $('.js-title-text').text().trim());
            postDataObject['title'] = $('.js-title-text').text().trim();
        }
        formData.append('action', '/argument-box/create');
        postDataObject['action'] = '/argument-box/create';
        formData.append('lang', lang);
        postDataObject['lang'] = lang;

        var $voteButtons = $('.js-vote');
        var direction = '';
        if($voteButtons.eq(0).get(0) === $el.get(0)) {
            direction = 'left';
        } else if($voteButtons.eq(1).get(0) === $el.get(0)) {
            direction = 'right';
        }
        return $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: formData,
            createAction: true,
            direction: direction,
            postDataObject: postDataObject
        }).then(function(data) {
            if(data.status === 'OK') {
                if(pluginOwner && !$('#editPluginLink').length && paidPlan) {
                    var editPluginText = lang === 'ar' ? 'عدل' : 'Edit',
                        link = $('input[name="link"]').first().val(),
                        editPluginHTML = '<li role="presentation"><a role="menuitem" tabindex="-1" href="/argumentsbox/edit?url='+ encodeURIComponent(websiteURL) + '" target="_blank">' + editPluginText +'</a></li>',
                        $userDropdown = $('.js-user-dropdown');
                    $userDropdown.prepend(editPluginHTML);
                }
                return $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    contentType: false,
                    data: {
                        action: '/argument-box/show/url',
                        url: phpVars.url,
                        code: uuid,
                        lang: lang,
                        "app_id": phpVars.app_id
                    }
                });
            }
        });
    }

    ArgumentsboxPlugin.prototype.unvote = function($el) {
        var argumentId = $el.attr('data-argumentsbox-id'),
            sideId =$el.attr('data-side-id'),
            lang = this.lang,
            url = this.url;
        $el.prop('disabled', true);
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: '/unvote',
                method: 'DELETE',
                side_id: sideId,
                id: argumentId,
                type: 'argumentbox',
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                var votes = data.data;
                updateVotes(votes);
                var $argumentForm = $('.js-argument-form[data-side-id="'+ sideId  +'"]');
                var $unvoteContainer = $el.parents('.js-unvote-container');
                var $voteButton = $('.js-vote[data-side-id="' + sideId  + '"]');
                $('.js-unvote-container').addClass('hide');
                $('.js-argument-form').addClass('hide');
                $('.js-argument-form').addClass('opacity-0');
                $('.js-vote').removeClass('hide');
                $unvoteContainer.removeClass('hide');
                ramjet.transform($unvoteContainer.get(0), $voteButton.get(0), {
                    done: function() {
                        $voteButton.removeClass('hide');
                        setTimeout(function() {
                            $argumentForm.addClass('hide');
                        }, 1);
                    },
                    easing: quadInOutEasing,
                    duration: 300

                });
                $voteButton.addClass('hide');
                $unvoteContainer.addClass('hide');
                $argumentForm.removeClass('opacity-0');

            }
        }).fail(this.errorHandler.bind(this));
    };

    ArgumentsboxPlugin.prototype.showEditedTime = function($el) {
        $editedTime = $('.js-edited-time');
        $editedTime.toggleClass('hide');
    }

    function updateSides(argumentId, sides, lang) {
        var $voteButtons = $('.js-vote'),
            $unvoteButtons = $('.js-unvote'),
            $sides = $('.js-side'),
            $unvoteContainers = $('.js-unvote-container'),
            $argumentForms = $('.js-argument-form'),
            $sideVotes = $('.js-side-votes'),
            $sidePercentages = $('.js-side-percentage'),
            $progressBars = $('.js-progress-bar'),
            swtichedElements = [ $voteButtons, $unvoteButtons , $unvoteContainers , $argumentForms],
            elements = [ $sides, $sideVotes , $sidePercentages , $progressBars ];
        swtichedElements.forEach(function(el, i) {
            el.eq(0).attr('data-side-id', sides[0]);
            el.eq(1).attr('data-side-id', sides[1]);
        });

        if(lang === 'ar') {
            sides = [sides[1], sides[0]];
        }
        elements.forEach(function(el, i) {
            el.eq(0).attr('data-side-id', sides[0]);
            el.eq(1).attr('data-side-id', sides[1]);
        });
        $voteButtons.attr('data-argumentsbox-id', argumentId);
        $unvoteButtons.attr('data-argumentsbox-id', argumentId);
        $('input[name="argumentbox_id"]').val(argumentId);
    }

    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }
    ArgumentsboxPlugin.prototype.bindEvents = function() {
        var _this = this;
        $.each(ArgumentsboxPlugin.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    ArgumentsboxPlugin.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler(jqXHR.settings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.message,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages,
            610: response.message
        };

        this.showErrorMsg(errorMsgs[status]);
    };

    ArgumentsboxPlugin.prototype.showErrorMsg = function(message) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(message);
        $('#errorbox').modal('show');
    };




    ArgumentsboxPlugin.prototype.unauthorizedHandler = function(ajaxSettings) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        if(ajaxSettings) {
            var lang = this.lang;
            var lastAction = {
                url: ajaxSettings.url,
                type: ajaxSettings.type,
                dataType: ajaxSettings.dataType,
                data: ajaxSettings.data,
                processData: ajaxSettings.processData,
                contentType: ajaxSettings.contentType,
                createAction: ajaxSettings.createAction,
                argumentsboxURL: phpVars.url,
                direction: ajaxSettings.direction,
                lang: lang,
                postDataObject: ajaxSettings.postDataObject
            };

            localStorage.setItem('lastAction', JSON.stringify(lastAction));
        }
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    function quadInOutEasing (t) {
        t /= 0.5;
        if (t < 1) return 0.5*t*t;
        t--;
        return -0.5 * (t*(t-2) - 1);
    }
    var argumentsboxPlugin = new ArgumentsboxPlugin();

})();
