{% if not debates and not comparisons and not argboxes %}
    <h3>{{ t._('no-content-found') }}!</h3>
{% endif %}
<div class="tab-pane fade active in" id="debates">

    <ul class="js-debates">
        {% for item in debates %}
            {% include "partials/moderates/debate.volt" %}
        {% endfor %}
    </ul>
</div>
<div class="tab-pane fade active in" id="comparisons">

    <ul class="js-comparisons">
        {% for item in comparisons %}
            {% include "partials/moderates/comparison.volt" %}
        {% endfor %}
    </ul>
</div>
<div class="tab-pane fade active in" id="argumentbox">

    <ul class="js-argumentsboxes">
        {% for item in argboxes %}
            {% include "partials/moderates/argumentbox.volt" %}
        {% endfor %}
    </ul>
</div>
