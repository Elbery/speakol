(function() {
    function inIframe () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }
    window.addEventListener('message', function(e) {
        var message = JSON.parse(e.data);
        if(message.type === 'close-popup') {
            var loginContainer = document.getElementById('speakol-login-container');
            if(!loginContainer) {
                return;
            }

            loginContainer.style.width = "481px";
            window.getComputedStyle(loginContainer).width;
            loginContainer.style.width = "0px";
            if(message.reload) {
                $('.speakol-arguments-box .speakol,  .speakol-debate .speakol, .speakol-comparison .speakol').each(function() {
                    $(this).attr('src', $(this).attr('src'));
                });
            }
        }
        if(message.type === 'popup' && inIframe()) {
            window.parent.postMessage(e.data,'*');
        }
    });
})();
