notifications = {
    init: function () {
        notifications.bindEvents();
        notifications.view();
        notifications.seen();
    },
    bindEvents: function() {
        jQuery(document).on('click', '.js-notification-item', function() {
            jQuery(this).find('.js-notification-url').get(0).click();
        });

        jQuery(document).on('click', '.js-notification-url', function(e) {
            e.stopPropagation();
        });
    },
    view: function () {
        function run() {
            var notificationBlock = jQuery('.js-notifications').length;
            if (notificationBlock === 0) {
                return false;
            }
            jQuery.ajax({
                url: '/notifications/view',
                type: 'GET',
                success: function (response) {
                    if (response == null || response == "") {
                        return false;
                    }
                    var response = JSON.parse(response);
                    var totalNotifications = +response.data.new_items;
                    if(totalNotifications) {
                        jQuery('.js-notification-indicator').removeClass('hide');
                    } else {
                        jQuery('.js-notification-indicator').addClass('hide');
                    }
                    // var html = '';
                    // jQuery.each(response.data.data, function (index, value) {
                    //     html += '<li class="font-13">' + value + '</li>';
                    // });
                    // html += '';
                    // if (totalNotifications > 0) {
                    //     jQuery('.js-notifications-button').find('.sp-icon').addClass('color-5bb767').removeClass('color-9ca7ad');
                    // }
                    jQuery('.js-notifications').html(response.data.html);
                    jQuery('.js-notification-indicator').html(totalNotifications);
                }
            });
        }
        run();
        setInterval(run, 5000);
    },
    seen: function () {
        jQuery('.js-notifications-button').on('click', function (e) {
            var $currentItems = jQuery('.js-notifications').find('li');
            var currentItemsValue = +$currentItems.length;
            var dropdownOpen = jQuery('.js-notifications').parents('.dropdown').hasClass('open');
            if(!dropdownOpen) {
                if (currentItemsValue != 0) {
                    jQuery.ajax({
                        url: '/notifications/updatebyrecipient',
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success') {
                            }
                        }
                    });
                }

            } else {
                 jQuery('.js-notifications-button').find('.sp-icon').removeClass('color-5bb767').addClass('color-9ca7ad');
            }
        });



    },
}
jQuery(document).ready(function () {
    notifications.init();
});
