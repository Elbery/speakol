<div class="tab-pane fade" id="fltrs">
    <div class="blc hlf">
        <h2>Restricted Words</h2>
        <p>Please separate using a comma, Arguments and replies containing any of these words will not be published until approved.</p>
    </div>
    <div class="blc hlf">
        <textarea name="" id="" rows="5"></textarea>
    </div>
    <hr>
    <div class="blc hlf">
        <h2>Black and white lists</h2>
        <p>Control who is able to participate within your community. You can specify items such as users, emails, IP addresses. Blacklisted items prevent certain people from posting, while whitelisted items allow users to bypass certain moderation filters (such as spam).
        </p>
    </div>
    <div class="blc hlf">
        <ul class="nav nav-tabs" id="Tabs_fltr">
            <li class="active"><a href="#wt_lst" data-toggle="tab">
                    White List
                </a></li>
            <li><a href="#blk_lst" data-toggle="tab">
                    Black List
                </a></li>
        </ul>
        <div class="tab-content Tabs_fltr">
            <div class="tab-pane fade in active" id="wt_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>IP Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="tab-pane fade" id="blk_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>IP Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="button" value="Save" class="btn blue_btn"/>
    </div>
</div>