/* avoid conflicts with other versions or libraries */
jQuery.noConflict();
(function ($) {

    if (NotificationClass === "undefined") {
        var _self, selectors;

        // Decalring Notification Class
        var NotificationClass = function (_options) {
            // Notification Class refefence, to prevent conflicts of other references
            _self = this;

            this.defaults = {
                selectors: {
                    nItemsList: '.notification_items_list',
                    nDrpdwn: '.drpdwn',
                    nTotal: '.total_notifications',
                }
            }

        };

        // acts like class constructor 
        NotificationClass.prototype.init = function () {
            selectors = _self.defaults.selectors;
        };

        // acts like class constructor 
        NotificationClass.prototype.assignEvents = function () {
            $(selectors.nDrpdwn).hover(function () {
                var currentItems = $(this).children(_self.selectors.nTotal);
                var currentItemsValue = parseInt($(currentItems).text());
                if (!isNaN(currentItemsValue) && typeof currentItemsValue !== 'undefined' && parseInt(currentItemsValue) != 0) {
                    $.ajax({
                        url: '/notifications/updatebyrecipient',
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success') {
                                $(currentItems).html('');
                            }
                        }
                    });
                }
                return false;
            });
        };

        // view notifications 
        NotificationClass.prototype.view = function () {

        };

        // mark viewed notifications as seen
        NotificationClass.prototype.seen = function () {

        };

    }


}(jQuery));