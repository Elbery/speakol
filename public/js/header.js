(function() {
    var Header = function(lang) {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = lang;
        this.audio = $('#audioEnabled').length;
        this.bindEvents();
        this.bindUploadImageEvent();
        setInterval(function() {
            this.updateOnlineUsers();
        }.bind(this), 90000);
    };

    Header.Events = {
        '.js-sort-button': 'sortArguments',
        '.js-social-share': 'showSocial',
        '.js-edited-indicator': 'showEditedDate'
    };


    Header.prototype.showEditedDate = function($el) {
        $('.js-edited-date').toggleClass('hide');
    };

    
    Header.prototype.getLargestPage= function(sides) {
        var pages = sides.map(function(side) {
            var page =  $('.js-side[data-side-id= "' + side  + '"]').attr('data-comments-page');
            return +page;
        });
        return Math.max.apply(null, pages);
    };

    function getAllRightSides() {
        var sideIDs = $('.js-side').filter('[data-align="right"]').map(function() {
            return $(this).attr("data-side-id");
        }).toArray();
        return sideIDs.filter(Boolean);
    }

    Header.prototype.updateOnlineUsers= function() {
        var sides = getAllSides();
        if(sides.length) {
            var params = {
                url: '/users/checkonlineusers',
                type: 'GET',
                dataType: 'json',
                data: {
                    sides: sides, 
                    argument_id: $('.js-replies:not(.hide)').parents('.js-argument').first().attr('data-arg-id'),
                    page: this.getLargestPage(sides),
                    limit: 10,
                    order_by: $('.js-current-sort').attr('data-value')
                },
                success: function(data, textStatus, jqXHR) {
                    if(data.data && data.data.length) {
                        $('.js-owner-status').removeClass("color-53a960").addClass("color-bebebe");
                        $.each(data.data, function(i, id) {
                            var $ownerStatus = $('.js-owner-status[data-owner-id=' + id  + ']');
                            $ownerStatus.removeClass("color-bebebe").addClass("color-53a960");
                        });

                    }
                },
            };

            $.ajax(params);
        }

    };


    Header.prototype.showSocial = function($el) {
        var strUrl = $el.attr('href'),
            strWindowName = 'facebook-window',
            strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,height=450,width=500,left=450,top=220";
        var windowObjectReference = window.open(strUrl, strWindowName, strWindowFeatures);
    };

    Header.prototype.previewImage = function($el) {
        var file = $el.prop('files')[0],
            reader = new FileReader(),
            $imageForm = $el.parents('.js-image-form').first(),
            $image = $imageForm.find('.js-image'),
            $imageUploadContainer = $imageForm.find('.js-image-upload-container'),
            $imageContainer = $imageForm.find('.js-image-container');
        reader.onload = function (e) {
            $imageUploadContainer.addClass('hide');
            $imageContainer.removeClass('hide');
            var img = new Image();
            img.src = e.target.result;
            img.onload = function() {
                var canvas = document.createElement('canvas');
                canvas.width = 256;
                canvas.height = 256;
                var context = canvas.getContext("2d");
                context.clearRect(0, 0, canvas.width, canvas.height);
                context.drawImage(img, 0, 0 , 256, 256);
                var finalImg = canvas.toDataURL("image/png");
                $image.attr('src', finalImg);
            };

        }
        reader.readAsDataURL(file);
    };

    Header.prototype.deleteImage = function($el) {
        var $imageForm = $el.parents('.js-image-form').first(),
            $image = $imageForm.find('.js-image'),
            $imageUploadContainer = $imageForm.find('.js-image-upload-container'),
            $imageContainer = $imageForm.find('.js-image-container'),
            $uploadButton = $imageForm.find('.js-image-upload');

        $image.attr('src', '');
        $imageUploadContainer.removeClass('hide');
        $imageContainer.addClass('hide');
        $uploadButton.val('');
    }

    Header.prototype.bindUploadImageEvent = function($el) {
        var uploadImageEvents = {
            '.js-image-upload': {
                'event': 'change',
                'fn': 'previewImage' 
            },
            '.js-delete-image': {
                'event': 'click',
                'fn': 'deleteImage'
            },
            '.js-image-loggedout': {
                'event': 'click',
                'fn': 'unauthorizedHandler'
            }
        }


        var _this = this;
        $.each(uploadImageEvents, function(key, value) {
            $(document).on(value.event, key, function(e) {
                if(value.event === 'click') {
                    e.preventDefault();
                }
                var $el = $(this);
                _this[value.fn]($el);
            });
        });
    };

    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }

    Header.prototype.bindEvents = function() {
        var _this = this;
        $.each(Header.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    function getAllSides() {
        var sideIDs = $('.js-side').map(function() {
            return $(this).attr("data-side-id");
        }).toArray();
        return sideIDs.filter(Boolean);
    }

    Header.prototype.sortArguments = function($el) {
        var $currentSort = $('.js-current-sort'),
            sides = getAllSides(),
            rightSides = getAllRightSides(),
            params = {};
        if($el.attr('data-value') === $currentSort.attr('data-value')) {
            return;
        }
        params.side_ids = sides;
        params.right_sides = rightSides;
        params.order_by = $el.attr('data-value');
        params.limit = 10;
        params.lang = this.lang;
        params.comparison = $('.js-comparison-side').length > 0;
        params.audio = this.audio;
        $.ajax({
            url: '/arguments/changesort/',
            type: 'GET',
            dataType: 'json',
            data: params
        }).done(function(data) {
            data = data.data;
            if(data) {
                data.forEach(function(el, i) {
                    var $html = $('<div>' + el.html + '</div>'),
                        $side = $('.js-side[data-side-id="' + el.side_id+ '"]'),
                        $showMoreButton = $side.find('.js-show-more');
                    if(el.hasmore && $showMoreButton.hasClass('hide')) {
                        $showMoreButton
                        .removeClass('hide')
                        .prop('disabled', false);
                    }


                    $side.attr('data-comments-page', 1);
                    var $comparisonContent = $side.find('.js-comparison-content');
                    if($comparisonContent.length && $comparisonContent.hasClass('comparison-content-vertical')) {
                        $('.js-play-button').addClass('small');
                    } 

                    $side.find('.js-arguments').html($html.html());
                    addReadMore();
                });
                $currentSort.attr('data-value', $el.attr('data-value'));
            }
        }).fail(this.errorHandler.bind(this));

    };

    Header.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler();
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: t.br,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages
        };
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(errorMsgs[status]);
        $('#errorbox').modal('show');
    };

    Header.prototype.unauthorizedHandler = function() {
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    var header = new Header(phpVars.lang);
})();
