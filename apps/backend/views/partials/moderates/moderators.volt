<div class="tab-pane fade in" id="mdrtrs">

    {% if moderators %}
        {% for user in moderators %}
            <div class="form_row">
                <div class="img_circl_container">
                    <span class="img_circl">
                        {% if user['role_id'] == 4 %}
                            {% if user['profile_picture'] %}
                                <a href="{{ config.application.main_website ~ url('user/profile/'~user['slug']) }}">
                                    {{ image( config.application.webservice ~ user['profile_picture'] , "alt": "Speakol", "title": "Speakol") }}
                                {% else %}
                                    {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol") }}
                                {% endif %}
                            </a>
                        {% else %}
                            <a href="{{ config.application.main_website ~ url('app/profile/'~app.slug) }}">
                                {% if app.image %}
                                    {{ image( config.application.webservice ~ app.image , "alt": "Speakol", "title": "Speakol") }}
                                {% else %}
                                    {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol") }}
                                {% endif %}
                            </a>
                        {% endif %}
                    </span>
                    <span class="nameAndDate">
                        {% if user['role_id'] == 4 %}
                            <a href="{{ config.application.main_website ~ url('user/profile/'~user['slug']) }}"> {{ user['name']}} </a>
                        {% else %}
                            <a href="{{ config.application.main_website ~ url('app/profile/'~app.slug) }}"> {{ app.name}} </a>
                        {% endif %}
                    </span>
                    {% if user['role_id'] is 4 %}
                        <ul class="actions_buttons">
                            <li><a title="Delete" href="#" data-user-id="{{ user['id']  }}" class="remove-moderator delete_button">{{ t._('dlt') }}</a></li>
                        </ul>
                    {% endif %}
                </div>
            </div>
        {% endfor %}
    {% endif %}

    <div class="form_row">
        {{ form('moderates/add') }}
        <div class="img_circl_container">
            <span id="add-user-cicrle" class="img_circl add"></span>
            <span class="nameAndDate">
                <input type="text" id="add_admin_user" name="email" autocomplete="off"/>
                <div>
                    <input type="checkbox" id="admin" name="is_admin"/>
                    <label for="admin"> {{ t._('admins') }}</label>
                </div>
            </span>
            <ul class="actions_buttons">
                <li><a title="Add" href="#" class="add_button">{{ t._('add') }}</a></li>
            </ul>
        </div>
        {{ endForm() }}
    </div>

</div>
