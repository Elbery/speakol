<?php
namespace Speakol\Backend\Controllers;
class ArgumentsboxController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('argumentbox');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function indexAction() {
        $userSession = $this->getAppData();
        if (is_null($userSession)) {
            return $this->response->redirect('apps/login');
        }
        $header = array('Authorization:' . $userSession->user->token);
        $apiResponse = $this->utility->iCurl('/argument-boxes', 'get', $header, array('app_id' => $userSession->user->app_id));
    }
    public function showAction() {
    }
    public function createAction() {
    }
    public function editAction() {
        $url = $this->request->get('url');
        if (!$url) {
            $this->flash->error('No URL has been given');
            return $this->response->redirect('dashboard');
        }
        $app = $this->getAppData();
        if (!$app) {
            $this->flash->error('You should login first');
            return $this->response->redirect('apps/login');
        }
        if ($app->plan_id && $app->plan_id === 1) {
            return $this->response->redirect('dashboard');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        if ($this->request->isPost()) {
            $title = $this->request->getPost('title') ? $this->request->getPost('title') : ' ';
            $option = $this->request->getPost('option');
            $uuid = '';
            if ($this->request->get('uuid')) {
                $uuid = $this->request->get('uuid');
            }
            $params = array('app_id' => $app->id, 'url' => $url, 'title' => $title, 'option' => $option, 'code' => $uuid,);
            $response = $this->utility->iCurl('/argument-box/update', 'POST', $hdrs, $params, false, false, true);
            if ($response->status !== 'OK') {
                $this->flash->error($response->message);
            } else {
                $this->flash->success($this->t->_('update-succeeded'));
            }
        }
        $uuid = '';
        if ($this->request->get('uuid')) {
            $uuid = $this->request->get('uuid');
        }
        $params = array('app_id' => $app->id, 'url' => $url, 'code' => $uuid,);
        $response = $this->utility->iCurl('/argument-box/show/info', 'GET', $hdrs, $params, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('dashboard');
        }
        $optionsJSON = $this->utility->iCurl('/argument-box/options', 'GET', $hdrs, array());
        if ($optionsJSON->status !== 'OK') {
            $this->flash->error("UnExpected Error");
            return $this->response->redirect('dashboard');
        }
        $this->view->options = $optionsJSON->data;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->view->setVar('app_id', $userSession->id);
        $this->view->argumentsbox = $response->data;
        $this->view->pick('argumentsbox/code');
    }
    public function codeAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $userSession = $this->getAppData();
        if (is_null($userSession)) {
            $this->flash->error("You should login first");
            return $this->response->redirect('apps/login');
        }
        if (empty($userSession->id)) {
            $this->flash->error('You should create application first.');
            return $this->response->redirect('apps/create');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $optionsJSON = $this->utility->iCurl('/argument-box/options', 'GET', $hdrs, array());
        if ($optionsJSON->status !== 'OK') {
            $this->flash->error("UnExpected Error");
            return $this->response->redirect('dashboard');
        }
        $this->view->options = $optionsJSON->data;
        $this->view->planID = $userSession->plan_id;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/uuid.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->view->setVar('app_id', $userSession->id);
    }
    public function renderAction() {
        header('Content-Type: text/html; charset=utf-8');
        $url = $this->request->get('url');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $authorized = true;
        if ($this->cookies->has(session_name())) {
            $error = $this->session->get('error_socail');
            if ($error) {
                $this->flash->error($error);
                $this->session->remove('error_socail');
            }
        }
        $newsfeed = $this->request->get('newsfeed');
        $newsfeed = $newsfeed === '1' ? true : false;
        $this->view->newsfeed = $newsfeed;
        $this->view->isAppLoggedIn = false;
        $this->view->appData = array();
        $this->view->freePlan = true;
        if ($this->getAppData()) {
            $this->view->isAppLoggedIn = true;
            $this->view->appData = $this->getAppData();
            $planID = $this->view->appData->plan_id;
            if ($planID > 1) {
                $this->view->freePlan = false;
            }
        }
        $token = $this->getToken();
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        } else {
            $hdrs = array('Authorization:');
        }
        $uuid = '';
        if ($this->request->get('uuid')) {
            $uuid = $this->request->get('uuid');
        }
        $params = array('app_id' => $this->request->get('app'), 'url' => $this->request->get('url'), 'order_by' => 'most_voted', 'code' => $uuid,);
        $objJSON = $this->utility->iCurl('/argument-box/show/url', 'get', $hdrs, $params);
        if ($objJSON->status === 'ERROR' && $objJSON->unauthorized_domain) {
            $authorized = false;
        }
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                $this->setLang($objUserProfile->data->user->locale);
            }
        } else {
            $objUserProfile = false;
        }
        if (!$authorized && !$newsfeed) {
            $this->view->authorized = false;
        } else {
            $this->view->authorized = true;
            $this->view->isFreePlan = $objJSON->free_plan;
            $this->view->optionID = $this->request->get('option') ? $this->request->get('option') : '1';
            if ($this->view->isFreePlan) {
                $this->view->options = array($this->t->_('I Agree'), $this->t->_('I Disagree'));
            } else if ($objJSON->status === 'OK' && $objJSON->data) {
                $this->view->options = $objJSON->data[0]->option;
            } else {
                $optionID = $this->view->optionID;
                $optionsJSON = $this->utility->iCurl('/argument-box/options', 'GET', $hdrs, array());
                if ($optionsJSON->status !== 'OK') {
                    $this->view->options = array('I Agree', 'I Disagree');
                } else {
                    $option = array_filter($optionsJSON->data, function ($var) use ($optionID) {
                        return $var->id === $optionID;
                    });
                    $option = array_pop($option);
                    $this->view->options = array($option->option_1, $option->option_2);
                }
            }
            $this->view->title = 'Speakol Argumentbox';
            $redirectUrl = '1';
            $this->view->argumentsbox = $objJSON->data[0];
            if (!$this->isUrlHttps($url) && !$newsfeed) {
                $this->view->argumentsbox->audio = false;
            }
            $created_at = $this->view->argumentsbox->created_at;
            $updated_at = $this->view->argumentsbox->updated_at;
            $this->view->edited = strtotime($updated_at) > strtotime($created_at);
            $this->view->fburl = $this->utility->getAbsoluteURL('users', 'facebook?r_url=' . $redirectUrl);
            $this->view->twitterurl = $this->utility->getAbsoluteURL('users', 'twitter');
            $this->view->googleurl = $this->utility->getAbsoluteURL('users', 'google?r_url=' . $redirectUrl);
            $this->view->isLoggedIn = $objUserProfile ? true : false;
            $this->view->logout = $objUserProfile ? $this->url->get("users/logout") : '';
            if ($this->getAppData()) {
                $this->view->logout = $this->url->get('apps/logout');
            }
            $this->view->lang = $this->request->get('lang') ? $this->request->get('lang') : $this->getLang();
            $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'url' => $this->request->get('url'), 'width' => $this->request->get('width'), 'description' => $this->request->get('desc'), 'app_id' => $this->request->get('app'), 'lang' => $this->getLang(),);
            $this->view->replies = array();
        }
        $objApp = new \stdClass();
        $objApp->URL = $this->request->get('url');
        $objApp->DESC = $this->request->get('desc');
        $this->view->app = $objApp;
        $this->view->url = $this->request->get('url');
        if ($newsfeed) {
            $this->view->url = urlencode($this->config->application->main_website . 'newsfeed/argumentsbox?url=' . urlencode($this->view->url));
        }
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->collection('header')->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->collection('header-ie9')->addJs('http://html5shim.googlecode.com/svn/trunk/html5.js?' . $this->config->application->cache_string);
        $this->assets->collection('footer')->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/mediaelement-and-player.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string);
        if ($newsfeed || $authorized) {
            $this->assets->collection('footer')->addJs('vendor/js/audiorecorder.js?' . $this->config->application->cache_string)->addJs('vendor/js/speexextensions.js?' . $this->config->application->cache_string)->addJs('js/recorder.js?' . $this->config->application->cache_string)->addJs('js/audioplayer.js?' . $this->config->application->cache_string)->addJs('vendor/js/ramjet.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/argumentsboxplugin.js?' . $this->config->application->cache_string)->addJs('js/header.js?' . $this->config->application->cache_string)->addJs('js/arguments.js?' . $this->config->application->cache_string)->addJs('js/replies.js?' . $this->config->application->cache_string)->addJs('js/loginbox.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string);
        }
        $this->view->pick("argumentsbox/show");
    }
    public function changesortAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        }
        $sideIds = $this->request->get('side_ids');
        $sideIds = is_array($sideIds) ? $sideIds : array($sideIds);
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $objUserProfile = false;
            }
        } else {
            $objUserProfile = false;
        }
        $sides = array();
        $rightSides = $this->request->get('right_sides');
        foreach ($sideIds as $sideId) {
            $right = in_array($sideId, $rightSides);
            $params = array('side_id' => $sideId, 'page' => 1, 'limit' => $this->request->get('limit'), 'order_by' => ($this->request->get('order_by')) ? : 'most_voted',);
            $url = '/arguments/' . $params['side_id'];
            if (isset($params['page']) && !empty($params['page'])) {
                $url.= '/' . $params['page'];
            }
            if (isset($params['limit']) && !empty($params['limit'])) {
                $url.= '/' . $params['limit'];
            }
            $objArgumentJSON = $this->utility->iCurl($url, 'get', $hdrs, $params);
            $arguments = !empty($objArgumentJSON->data) ? $objArgumentJSON->data : false;
            if ($arguments) {
                $side = new \stdClass();
                $side->side_id = $sideId;
                $side->html = $this->utility->renderTemplate("partials", "argumentsbox/arguments", array("replies" => array(), "sideId" => $sideId, "argumentsBoxURL" => $this->request->get('url'), "LoggedInUserData" => !empty($objUserProfile->data) ? $objUserProfile->data->user : false, "arguments" => $arguments, "right" => $right, "lang" => $this->request->get('lang'), "comparison" => $this->request->get('comparison') === 'true' ? true : false, "audio" => intval($this->request->get('audio')),));
                $side->hasmore = $arguments->hasmore;
                $sides[] = $side;
            } else {
                return false;
            }
        }
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent(array('status' => 200, 'data' => $sides));
        return $this->response;
    }
    public function renderArgumentAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $params = array('argument_id' => $this->request->get('argument_id'),);
        $objArgumentJSON = $this->utility->iCurl('/arguments/' . $params['argument_id'] . '/show', 'get', $hdrs, $params, true);
        $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, array(), false, false);
        if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
            $objUserProfile = false;
        }
        $this->view->replies = array();
        $this->view->argumentsBoxURL = $this->request->get('url');
        $this->view->LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
        $this->view->argument = $objArgumentJSON->data;
        $this->view->argumentID = $this->request->get('argument_id');
        $this->view->argumentsBoxURL = $this->request->get('url');
        $this->view->right = $this->request->get('right') === 'false' ? false : true;
        $this->view->lang = $this->request->get('lang');
        $this->view->comparison = $this->request->get('comparison') === 'true' ? true : false;
        $this->view->audio = intval($this->request->get('audio'));
        $this->view->pick("partials/argumentsbox/argument");
    }
    public function renderReplyAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $adID = $this->request->get('ad_id');
        $url = '/replies/show/';
        if (isset($adID) && !empty($adID)) {
            $url = '/ads/replies/show/';
        }
        $params = array('reply_id' => $this->request->get('reply_id'));
        $objArgumentJSON = $this->utility->iCurl($url . $params['reply_id'], 'get', $hdrs, $params);
        $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
        if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
            $objUserProfile = false;
        }
        $this->view->argumentsBoxURL = $this->request->get('url');
        $this->view->LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
        $this->view->audio = intval($this->request->get('audio'));
        if ($this->request->get('nested')) {
            $this->view->not_nested_reply = '0';
        } else {
            $this->view->not_nested_reply = 1;
        }
        $this->view->reply = $objArgumentJSON->data;
        $this->view->parentReply = $this->request->get('parent_reply');
        if (empty($this->view->parentReply)) {
            $this->view->notNestedReply = true;
        }
        $this->view->argumentID = $this->request->get('argument_id');
        $this->view->adID = $this->request->get('ad_id');
        $this->view->argumentsBoxURL = $this->request->get('url');
        $this->view->right = $this->request->get('right') === 'false' ? false : true;
        $this->view->comparison = $this->request->get('comparison') === 'true' ? true : false;
        $this->view->pick("partials/argumentsbox/reply");
    }
    public function renderRepliesAction() {
        $ajax = $this->request->get('ajax') === 'false' ? false : true;
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        }
        $adID = $this->request->get('ad_id');
        $url = "";
        if (isset($adID) && !empty($adID)) {
            $params = array('ad_id' => $this->request->get('ad_id'), 'reply_parent' => $this->request->get('reply_parent'), 'page' => $this->request->get('page') ? $this->request->get('page') : 1, 'limit' => $this->request->get('limit') ? $this->request->get('limit') : 10);
            $url = '/ads/replies/' . $params['ad_id'];
            if (isset($params['page']) && !empty($params['page'])) {
                $url.= '/' . $params['page'];
            }
            if (isset($params['limit']) && !empty($params['limit'])) {
                $url.= '/' . $params['limit'];
            }
        } else {
            $params = array('argument_id' => $this->request->get('argument_id'), 'reply_parent' => $this->request->get('reply_parent'), 'page' => $this->request->get('page'), 'limit' => $this->request->get('limit'));
            $url = '/replies/' . $params['argument_id'];
            if (isset($params['page']) && !empty($params['page'])) {
                $url.= '/' . $params['page'];
            }
            if (isset($params['limit']) && !empty($params['limit'])) {
                $url.= '/' . $params['limit'];
            }
        }
        $objArgumentJSON = $this->utility->iCurl($url, 'get', $hdrs, $params);
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $objUserProfile = false;
            }
        }
        $replies = $objArgumentJSON->data;
        $argumentsBoxURL = $this->request->get('url');
        $LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
        $replies = $objArgumentJSON->data;
        $argumentID = $this->request->get('argument_id');
        if (!$replies) {
            return false;
        }
        if (!$ajax) {
            $this->view->argumentsBoxURL = $argumentsBoxURL;
            $this->view->LoggedInUserData = $LoggedInUserData;
            $this->view->replies = $replies;
            $this->view->argumentID = $argumentID;
            $this->view->adID = $adID;
            $this->view->argumentsBoxURL = $argumentsBoxURL;
            $this->view->right = $this->request->get('right') === 'false' ? false : true;
            $this->view->comparison = $this->request->get('comparison') === 'true' ? true : false;
            $this->view->audio = intval($this->request->get('audio'));
            $this->view->pick("partials/argumentsbox/replies");
        } else {
            $data->html = $this->utility->renderTemplate('partials', 'argumentsbox/replies', array("argumentsBoxURL" => $this->request->get('url'), "LoggedInUserData" => !empty($objUserProfile->data) ? $objUserProfile->data->user : false, "replies" => $replies, "argumentID" => $this->request->get('argument_id'), "adID" => $this->request->get('ad_id'), "argumentsBoxURL" => $this->request->get('url'), "right" => $this->request->get('right') === 'false' ? false : true, "comparison" => $this->request->get('comparison') === 'true' ? true : false, "audio" => intval($this->request->get('audio')),));
            $data->hasmore = $replies->hasmore;
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 200, 'data' => $data));
            return $this->response;
        }
    }
    public function renderNestedRepliesAction() {
        $ajax = $this->request->get('ajax') === 'false' ? false : true;
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        }
        $params = array('parent_reply' => $this->request->get('parent_reply'), 'page' => $this->request->get('page') ? $this->request->get('page') : 1, 'limit' => $this->request->get('limit') ? $this->request->get('limit') : 10);
        $adID = $this->request->get('ad_id');
        if (isset($adID) && !empty($adID)) {
            $url = '/ads/replies/nested-reply/' . $params['parent_reply'];
            if (isset($params['page']) && !empty($params['page'])) {
                $url.= '/' . $params['page'];
            }
            if (isset($params['limit']) && !empty($params['limit'])) {
                $url.= '/' . $params['limit'];
            }
        } else {
            $url = '/replies/nested-reply/' . $params['parent_reply'];
            if (isset($params['page']) && !empty($params['page'])) {
                $url.= '/' . $params['page'];
            }
            if (isset($params['limit']) && !empty($params['limit'])) {
                $url.= '/' . $params['limit'];
            }
        }
        $objArgumentJSON = $this->utility->iCurl($url, 'get', $hdrs, $params);
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $objUserProfile = false;
            }
        } else {
            $objUserProfile = false;
        }
        $argumentsBoxURL = $this->request->get('url');
        $LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
        $argumentID = $this->request->get('argument_id');
        $replies = !empty($objArgumentJSON->data) ? $objArgumentJSON->data : array();
        if (empty($replies)) {
            return $this->response;
        }
        if (!$ajax) {
            $this->view->argumentsBoxURL = $argumentsBoxURL;
            $this->view->LoggedInUserData = $LoggedInUserData;
            $this->view->argumentID = $argumentID;
            $this->view->adID = $adID;
            $this->view->replies = $replies;
            $this->view->right = $this->request->get('right') === 'false' ? false : true;
            $this->view->pick("partials/argumentsbox/replies");
            $this->view->comparison = $this->request->get('comparison') === 'true' ? true : false;
            $this->view->audio = intval($this->request->get('audio'));
        } else {
            $data->html = $this->utility->renderTemplate('partials', 'argumentsbox/replies', array("argumentsBoxURL" => $argumentsBoxURL, "LoggedInUserData" => $LoggedInUserData, "adID" => $adID, "argumentID" => $argumentID, "replies" => $replies, "right" => $this->request->get('right') === 'false' ? false : true, "comparison" => $this->request->get('comparison') === 'true' ? true : false, "audio" => intval($this->request->get('audio')),));
            $data->hasmore = $replies->hasmore;
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 200, 'data' => $data));
            return $this->response;
        }
    }
    public function renderArgumentsAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        set_exception_handler(array($this, 'exceptionHandler'));
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        }
        $params = array('app_id' => $this->request->get('app_id'), 'side_id' => $this->request->get('side_id'), 'page' => $this->request->get('page'), 'limit' => $this->request->get('limit'), 'order_by' => ($this->request->get('order_by')) ? : 'most_voted',);
        $url = '/arguments/' . $params['side_id'];
        if (isset($params['page']) && !empty($params['page'])) {
            $url.= '/' . $params['page'];
        }
        if (isset($params['limit']) && !empty($params['limit'])) {
            $url.= '/' . $params['limit'];
        }
        $objArgumentJSON = $this->utility->iCurl($url, 'get', $hdrs, $params);
        $arguments = !empty($objArgumentJSON->data) ? $objArgumentJSON->data : false;
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $objUserProfile = false;
            }
        } else {
            $objUserProfile = false;
        }
        if ($arguments) {
            $data->html = $this->utility->renderTemplate("partials", "argumentsbox/arguments", array("replies" => array(), "sideId" => $this->request->get('side_id'), "argumentsBoxURL" => $this->request->get('url'), "LoggedInUserData" => !empty($objUserProfile->data) ? $objUserProfile->data->user : false, "arguments" => $arguments, "right" => $this->request->get('right') === 'false' ? false : true, "lang" => $this->request->get('lang'), "comparison" => $this->request->get('comparison') === 'true' ? true : false, "audio" => intval($this->request->get('audio')),));
            $data->hasmore = $arguments->hasmore;
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(array('status' => 200, 'data' => $data));
        } else {
            return false;
        }
        return $this->response;
    }
    function exceptionHandler($exception) {
        $this->view->errorMessage = $exception->getMessage();
        $this->view->pick("argumentsbox/error");
    }
    public function thumbupAction($argument_id) {
        $this->response->setHeader("Content-Type", "application/json");
        if (NULL == $this->utility->getLoggedInUser()) {
            $this->response->setStatusCode(401, "Unauthorized");
            $this->response->setJsonContent(array('status' => 'ERROR', 'data' => 0));
        }
        return $this->response;
        $hdrs = array('Authorization:xxxxxxxxxxx');
        $params = array('app_id' => $this->request->get('app'), 'url' => $this->request->get('url'), 'width' => $this->request->get('width'), 'description' => $this->request->get('desc'),);
        $objJSON = $this->utility->iCurl('/argument-box/create', 'post', $hdrs, $params);
        $params = array('url' => $this->request->get('url'),);
        $objJSON = $this->utility->iCurl('/argument-box/show', 'get', $hdrs, $params);
        $this->view->argumentsbox = $objJSON->data[0];
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->collection('header')->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->collection('header-ie9')->addJs('http://html5shim.googlecode.com/svn/trunk/html5.js?' . $this->config->application->cache_string);
        $this->assets->collection('footer')->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/argumentsbox.js?' . $this->config->application->cache_string);
        $this->view->pick("argumentsbox/show");
    }
    public function deleteAction() {
        if (!$this->request->isDelete() || !$this->request->isAjax() || !$this->getAppToken()) {
            $this->response->setJsonContent(array('status' => 'ERROR'));
            return $this->response;
        }
        $argumentsboxURL = $this->request->get('argumentsbox_url');
        if (!$argumentsboxURL) {
            $this->response->setJsonContent(array('status' => 'ERROR'));
            return $this->response;
        }
        $uuid = $this->request->get('uuid') ? $this->request->get('uuid') : '';
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $params = array('url' => $argumentsboxURL,);
        if ($uuid) {
            $params['code'] = $uuid;
        }
        $result = $this->utility->iCurl('/argument-box/delete', 'DELETE', $hdrs, $params, false, false, true);
        if ($result->status !== 'OK') {
            $this->response->setJsonContent(array('status' => 'ERROR'));
        } else {
            $this->response->setJsonContent(array('status' => 'OK'));
        }
        return $this->response;
    }
}
