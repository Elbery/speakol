    {% if replies.data is defined %}
        {% for reply in replies.data %}
            {% if adID is defined and adID %}
                <li>
                    {% include "partials/argumentsbox/reply" with ['reply': reply, 'adID': adID, 'argumentsBoxURL': argumentsBoxURL, 'not_nested_reply':'1', 'noUnderline': loop.last] %}
                </li>
            {% else %}
                <li>
                    {% include "partials/argumentsbox/reply" with ['reply': reply, 'argumentID': argumentID, 'argumentsBoxURL': argumentsBoxURL, 'not_nested_reply':'1', 'noUnderline': loop.last] %}
                </li>
            {% endif %}
        {% endfor %}
    {% endif %}
