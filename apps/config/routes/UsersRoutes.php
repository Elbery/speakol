<?php
namespace Speakol\Routes;
class UsersRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'users',));
        $this->setPrefix('/users');
        $this->add("/index", array('action' => 'index',))->setName("user-index");
        $this->add("/logout", array('action' => 'logout',))->setName("user-logout");
        $this->add("/login/social", array('action' => 'socialLogin',))->setName("user-login-social");
        $this->add("/social-response", array('action' => 'socialResponse',))->setName("user-social-response");
        $this->add("/facebook", array('action' => 'facebook',))->setName("user-facebook");
        $this->add("/twitter", array('action' => 'twitter',))->setName("user-twitter");
        $this->add("/google-plus", array('action' => 'googlePlus',))->setName("user-google-plus");
        return $this;
    }
}
