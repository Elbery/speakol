{% set pluginTemplates = ['argumentboxes' : 'partials/newsfeed/argumentsbox' , 'comparisons': 'partials/newsfeed/comparison', 'debates': 'partials/newsfeed/debate']%}

{% if highlightedPlugins | length %}
    {% for plugin in highlightedPlugins %}
        {% include pluginTemplates[plugin.type] with ['plugin': plugin]%}
    {% endfor %}
{% endif %}
{% if plugins | length  %}
    {% for plugin in plugins %}
        {% include pluginTemplates[plugin.type] with ['plugin': plugin]%}
    {% endfor %}
    <a href="{{ nextUrl }}" class="js-feed-paginate hide">next</a>

{% else %}
    {% if not paginate %}
         <h3 class="text-center">{{ t._('no-content') }}</h3>
    {% endif %}
{% endif %}
