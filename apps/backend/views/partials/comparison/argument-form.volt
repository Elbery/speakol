<div class="usr_action">

    <div class="comment_on_action clearfix">
        <div class="img_circl_container">
            <span class="img_circl">
                {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                    <a href=""> {{ image(config.application.webservice~LoggedInUserData.profile_picture) }}</a>
                {% else %}
                    <a href=""> {{ image("img/user_default.gif") }}</a>
                {% endif %}
            </span>
        </div>
        <div class="comment_area">
            <div class="tA_container">
                {{ text_area("comment", "placeholder": t._('choose-side'), "class":"write_comment", "cols": "6", "rows": 20) }}
            </div>
            <p class="vwrs_num" style="display:none; background: #53a960;margin: 6px 0 0 0;padding: 2px 17px;color: #fff;border-radius: 5px;"></p>
            {{ submit_button("post", "value": t._('sbmt-arg'), "class":"sbmt-arg") }}
            <div class="arg_box_photo">
                <img src="#" alt="">
            </div>
            {{ file_field("attached", "class":"add_img",'accept':'.png,.jpeg,.jpg,.gif') }}
            {{ hidden_field("container_id", "value": comparison.id) }}
            {{ hidden_field("container_type", "value": comparison.type) }}

        </div>
    </div>

</div>
