<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Confirmation;
class UsersValidation extends SpeakolValidation {
    public function __construct($rules = false, $exclude = false) {
        $this->loadCustomTrans("user");
        $this->validations = array("first_name" => array(new PresenceOf(array('message' => $this->t->_('fname-required')))), "last_name" => array(new PresenceOf(array('message' => $this->t->_('lname-required')))), "dob" => array(new PresenceOf(array('message' => $this->t->_('dob-required')))), "country_id" => array(new PresenceOf(array('message' => $this->t->_('country-required')))), "email" => array(new Email(array('message' => $this->t->_('email-not-valid'))), new PresenceOf(array('message' => $this->t->_('email-required'))),), "password" => array(new PresenceOf(array('message' => $this->t->_('password-required'))), new StringLength(array('messageMinimum' => $this->t->_('password-min-length'), 'min' => 8)),), "confirm_email" => array(new Confirmation(array('message' => $this->t->_('email-not-match'), 'with' => 'email'))), "confirm_password" => array(new Confirmation(array('message' => $this->t->_('password-not-match'), 'with' => 'password'))));
        parent::__construct($rules, $exclude);
    }
    public function initialize() {
        parent::initialize();
    }
}
