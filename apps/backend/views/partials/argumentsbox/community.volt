<section class="top_discussion">
    <h3>{{ t._('top-discussions') }}</h3>
    <ul class="nws_itms_lst">
        {% if community.top %}
            {% for top in community.top %}

                <li>
                    <a href="{{ top.url }}" target="_blank">
                        <span class="itm_img">
                            {% if top.image is defined and top.image %}
                                {{ image( top.image ) }}
                            {% else %}
                                {{ image("img/user_default.gif") }}
                            {% endif %}
                        </span>
                        <span class="item_info">
                            <span class="itm_title">{{ top.title }}</span>
                            {#                    <span class="itm_shares">250 shares</span>#}
                            <span class="itm_arguments">{{ top.arguments_count}} {{ t._('arguments') }}</span>
                        </span>
                    </a>
                </li>
            {% endfor %}
        {% else %}
            <li>
            <span class="item_info">
                <span class="itm_title">{{ t._('no-result-found') }}!</span>
            </span>
            </li>{{ t._('open') }}
        {% endif %}

    </ul>
{#    <a class="showMore" href="#">Show more discussions</a>#}
</section>

<section class="ldrbrd">
    <h3>{{ t._('leaderboard') }}</h3>
    <ul class="usrs_lb">
        {% if community.leaderboard %}
        {% for user in community.leaderboard %}
        <li>
            <div class="user_unit clearfix">
                <div class="img_circl_container">
                    <span class="img_circl">
                        <a target="_blank" href="{{ config.application.main_website ~ url('user/profile/'~user.slug) }}">
                            {% if user.profile_picture is defined and user.profile_picture %}
                                {{ image( config.application.webservice ~ user.profile_picture ) }}
                            {% else %}
                                {{ image("img/user_default.gif") }}
                            {% endif %}
                        </a>
                    </span>
                </div>
                <div class="User_info">
                    <a target="_blank" class="userName" href="{{ config.application.main_website ~ url('user/profile/'~user.slug) }}">{{ user.name }}</a>
                    <span class="arg_num">{{ user.total_arguments}} {{ t._('arguments') }}</span>
{#                    <a class="followUser" href="#">Follow</a>#}
                </div>
            </div>
        </li>
        {% endfor %}
        {% else %}
            <li>
            <span class="item_info">
                <span class="itm_title">{{ t._('no-result-found') }}!</span>
            </span>
            </li>
        {% endif %}
    </ul>
</section>
