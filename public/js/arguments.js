(function() {
    var Arguments = function(lang) {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = lang;
        this.audio = $('#audioEnabled').length;
        this.bindEvents();
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };

    Arguments.Events = {
        '.js-thumbs-up': 'thumbsUp',
        '.js-show-more': 'showMore',
        '.js-show-replies': 'showReplies',
        '.js-thumbs-down': 'thumbsDown'
    };


    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }
    Arguments.prototype.bindEvents = function() {
        var _this = this;
        $.each(Arguments.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    Arguments.prototype.showMore = function($el) {
        var $side = $el.parents('.js-side'),
            params = {},
            page = +$side.attr('data-comments-page'),
            argumentsLength = $side.find('.js-arguments').find('.js-argument').length;
        $el.attr('data-old', $.trim($el.text()));
        $el.text($el.attr('data-loading'));
        params.side_id = $side.attr('data-side-id');
        params.page = page + 1;
        params.limit = argumentsLength / page;
        params.lang = this.lang;
        // params.order_by = $('.js-sort-order').attr('data-value');
        params.order_by = 'most_voted';
        params.right = ($side.attr('data-align') === 'right');
        params.audio = this.audio;
        if($side.hasClass('js-comparison-side')) {
            params.comparison = true;
        }
        $.ajax({
            url: '/arguments/render/arguments/',
            type: 'GET',
            dataType: 'json',
            data: params
        }).done(function(data) {
            data = data.data;
            if (data) {
                $html = $("<div>" + data.html + "</div>");
                $side.find('.js-arguments').append($html.html());
                $side.attr('data-comments-page', params.page);
                if(!data.hasmore) {
                    $el.addClass('hide').prop('disabled', true);
                } else {
                    $el.text($el.attr('data-old'));
                }
                addReadMore();

            }
        }).fail(this.errorHandler);
    };


    Arguments.prototype.showReplies = function($el) {
        var $argument = $el.parents('.js-argument'),
            $repliesContainer = $argument.find('.js-replies-container'),
            $repliesShowMore = $repliesContainer.find('.js-more-replies'),
            lang = this.lang,
            argumentId = '',
            adId = '',
            right = $repliesContainer.parents('.js-side').attr('data-align') === 'right',
            comparison = $repliesContainer.parents('.js-side').hasClass('js-comparison-side'),
            audio = this.audio;

        if(!$repliesContainer.hasClass('hide')) {
            $repliesContainer.addClass('opacity-0');
            setTimeout(function() {
                $repliesContainer.addClass('hide');
            },200);
            $repliesShowMore
            .removeClass('hide')
            .prop('disabled', false);
            $repliesContainer.attr('data-comments-page', 1);
            return;
        }

        $('.js-replies-container').addClass('hide');
        $repliesContainer.removeClass('hide');
        setTimeout(function() {
            $repliesContainer.removeClass('opacity-0');
        }, 10);
        if($el.attr('data-ad') === 'true') {
            adId = $argument.attr('data-ad-id');
        } else {
            argumentId = $argument.attr('data-arg-id');
        }

        $.ajax({
            url: '/arguments/render/replies/',
            type: 'GET',
            data: {
                'argument_id': argumentId,
                'ad_id': adId,
                'lang': lang,
                'ajax': false,
                'right': right,
                'comparison': comparison,
                'audio': audio
            }
        }).done(function(data) {
            // _this.enableDisabled(objElement);
            $repliesContainer.find('.js-replies').html(data);
            addReadMore();
        }).fail(this.errorHandler);

    };


    Arguments.prototype.thumbsUp = function($el) {
        var action = "",
            argumentId = $el.attr('data-argument-id'),
            lang = this.lang,
            url = this.url,
            adId = '';
        $el.prop('disabled', true);
        if($el.attr('data-ad') === 'true') {
            adId = $el.attr('data-ad-id');
            action = '/ads/' + adId + '/thumb_up';
        } else {
            action = '/arguments/' + argumentId + '/thumb_up';
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: action,
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        })
        .done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                updateThumbs($el);
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 302) {
                $el.prop('disabled', false);
                removeThumb($el);
            } else {
                this.errorHandler(jqXHR, textStatus, errorThrown);
            }
        }.bind(this));

    };

    Arguments.prototype.thumbsDown = function($el) {
        var action = "",
            argumentId = $el.attr('data-argument-id'),
            lang = this.lang,
            url = this.url,
            adId = '';
        $el.prop('disabled', true);
        if($el.attr('data-ad') === 'true') {
            adId = $el.attr('data-ad-id');
            action = '/ads/' + adId + '/thumb_down';
        } else {
            action = '/arguments/' + argumentId + '/thumb_down';
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: action,
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        })
        .done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                updateThumbs($el);
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 302) {
                $el.prop('disabled', false);
                removeThumb($el);
            } else {
                this.errorHandler(jqXHR, textStatus, errorThrown);
            }
        }.bind(this));
    };


    function removeThumb($el) {
        var numThumbs = +$el.attr('data-thumbs'),
            $elText = $el.find('span'),
            isThumbsUp = $el.hasClass('js-thumbs-up'),
            colorClass = isThumbsUp ? 'color-5bb767' : 'color-d11b1f';
        numThumbs = numThumbs - 1;
        $el
        .removeClass('js-voted')
        .removeClass(colorClass)
        .addClass('color-c1cacf')
        .attr('data-thumbs', numThumbs);
        $elText.text(numThumbs);
    }


    function updateThumbs($el) {
        var $argumentActions = $el.parents('.js-argument-actions'),
            numThumbs = +$el.attr('data-thumbs'),
            $elText = $el.find('span'),
            isThumbsUp = $el.hasClass('js-thumbs-up'),
            oppositeClass = isThumbsUp ? 'js-thumbs-down' : 'js-thumbs-up',
            $oppositeEl = $argumentActions.find('.' + oppositeClass);
            colorClass = isThumbsUp ? 'color-5bb767' : 'color-d11b1f',

        numThumbs = numThumbs + 1;
        $elText.text(numThumbs);
        $el.addClass('js-voted')
        .addClass(colorClass)
        .removeClass('color-c1cacf')
        .attr('data-thumbs', numThumbs);

        if($oppositeEl.hasClass('js-voted')) {
            var oppositeThumbs = +$oppositeEl.attr('data-thumbs');
            var oppositeColorClass = isThumbsUp ?  'color-d11b1f':'color-5bb767' ;
            var $oppositeElText = $oppositeEl.find('span');
            oppositeThumbs--;
            $oppositeEl
            .removeClass('js-voted')
            .removeClass(oppositeColorClass)
            .addClass('color-c1cacf')
            .attr('data-thumbs', oppositeThumbs);
            $oppositeElText.text(oppositeThumbs);
        }
    }

    Arguments.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status,
            ajaxSettings = jqXHR.settings;
        if(status === 401) {
            this.unauthorizedHandler(ajaxSettings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: t.br,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages
        };
        $('.js-thumbs-up, .js-thumbs-down').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(errorMsgs[status]);
        $('#errorbox').modal('show');
    };

    Arguments.prototype.unauthorizedHandler = function(ajaxSettings) {
        $('.js-thumbs-up, .js-thumbs-down').prop('disabled', false);
        if(ajaxSettings) {
            var lastAction = {
                url: ajaxSettings.url,
                type: ajaxSettings.type,
                dataType: ajaxSettings.dataType,
                data: ajaxSettings.data,
                processData: ajaxSettings.processData,
                contentType: ajaxSettings.contentType,
            };

            localStorage.setItem('lastAction', JSON.stringify(lastAction));
        }
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    var args = new Arguments(phpVars.lang);
})();
