<div class="row header h4 bg-34383c padding-top-xs padding-bottom-sm">
    <div class="dropdown col-sm-3">
        <button class="h5 color-d6d7d8 text-left bg-34383c border-none dropdown-toggle width-all padding-top-xs padding-bottom-xs" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="col-sm-3">
                    {% if appImage %}
                        {{ image( appImage, "alt": "Speakol", "title": "Speakol", "class": "circle profile-picture-sm no-padding") }}
                    {% else %}
                        {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol", "class": "circle profile-picture-sm no-padding") }}
                    {% endif %}
                </span>
            <span class="col-sm-8 margin-top-sm dashboard-user-name">
                {{ user.name }}
            </span>
            <span class="col-sm-1 text-right margin-top-sm">
                <i class="fa fa-caret-down color-d6d7d8"></i>
            </span>
        </button>
        <ul class="dropdown-menu bg-34383c no-radius no-shadow width-all border-none" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"class="padding-top-xs padding-bottom-xs border-halflength"><a role="menuitem" class="color-d6d7d8 h5  pl30" tabindex="-1" href="/apps/settings"><i class="fa fa-cogs"></i>&nbsp; {{ t._('settings') }}</a></li>
            <li role="presentation"class="padding-top-xs padding-bottom-xs border-halflength"><a role="menuitem" class="color-d6d7d8 h5  pl30" tabindex="-1" href="/billing"><i class="fa fa-usd"></i>&nbsp; {{ t._('billing-history') }}</a></li>
            <li role="presentation"class="padding-top-xs padding-bottom-xs"><a role="menuitem" class="color-d6d7d8 h5  pl30" tabindex="-1" href="/apps/logout"><i class="fa fa-sign-out fa-flip-horizontal"></i>&nbsp; {{ t._('sign-out') }}</a></li>
        </ul>
    </div>
    <div class="pull-right">
        {%if isFreePlan %}
            <a href="/pricing" class="upgrade-free-button radius-2 inline-block margin-top-sm font-16 bg-green anchor-reset white">
                    <i class="sp-icon sp-up-arrow"></i> {{ t._('upgrade-free-plan') }}
            </a>
        {% endif %}
    </div>
</div>
<div class="row">
{% if cookies.has(sessionName()) and not noAlerts  %}
    {{ flash.output() }}
{% endif %}
</div>
