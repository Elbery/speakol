<?php
namespace Speakol\Backend\Controllers;
class CategoriesController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('debates');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function showAction($categoryId) {
        if (!$categoryId) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'No Category Specified', 'code' => 400));
        }
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->communityPage = true;
        $type = $this->request->get('type');
        $page = $this->request->get('page');
        $lastCreatedDate = $this->request->get('last_date');
        $page = $page ? $page : 1;
        $type = $type ? $type : 'all';
        $limit = 10;
        $params = array('page' => $page, 'limit' => $limit, 'type' => $type, 'category' => $categoryId);
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $this->view->isLoggedIn = false;
        } else {
            $this->view->isLoggedIn = true;
        }
        $plugins = $this->getPlugins($params);
        $this->view->plugins = $plugins;
        $lastCreatedDate = $plugins[count($plugins) - 1]->created_at;
        $lastCreatedDate = str_replace(' ', '+', $lastCreatedDate);
        $url = $this->request->get('_url');
        $nextUrl = $url;
        $url.= '?page=' . ($page);
        $nextUrl.= '?page=' . ($page + 1);
        if ($type !== 'all') {
            $url.= '&type=' . $type;
            $nextUrl.= '&type=' . $type;
        }
        $url.= '&last_date=' . $lastCreatedDate;
        $nextUrl.= '&last_date=' . $lastCreatedDate;
        $url.= '&lang=' . $this->view->lang;
        $nextUrl.= '&lang=' . $this->view->lang;
        $this->view->url = $url;
        $this->view->nextUrl = $nextUrl;
        $this->view->type = $type;
        $favorites = array();
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->view->superAdmin = $this->view->LoggedInUserData->role_id === 2;
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites ? $objUserProfile->data->favorites : array();
            }
        } else {
            $objUserProfile = false;
        }
        $this->view->paginate = false;
        if ($this->request->isAjax()) {
            $this->view->highlightedPlugins = array();
            $this->view->paginate = true;
            $this->view->pick('partials/newsfeed/feed');
            return;
        }
        $categoriesData = $this->formatCategories($favorites, $categoryId);
        foreach ($categoriesData as $key => $data) {
            $this->view->{$key} = $data;
        }
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        if ($this->view->superAdmin) {
            $this->assets->addJs('js/highlight.js?' . $this->config->application->cache_string);
        }
        $this->view->pick('categories/show');
    }
    public function starAction($categoryId) {
        if (!$categoryId) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong URL', 'code' => 400));
        }
        if (!$this->request->isPost()) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Method', 'code' => 400));
        }
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Unauthorized', 'code' => 401));
        }
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $response = $this->utility->iCurl('/user/favoritecategories/add', 'POST', $hdrs, array('category_id' => $categoryId), true, false, true);
        if ($response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => $response->message, 'code' => $response->code));
        } else {
            return $this->sendResponse(array('status' => 'OK', 'message' => 'SUCCESS', 'code' => 200));
        }
    }
    public function unstarAction($categoryId) {
        if (!$categoryId) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong URL', 'code' => 400));
        }
        if (!$this->request->isDelete()) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Method', 'code' => 400));
        }
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Unauthorized', 'code' => 401));
        }
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $response = $this->utility->iCurl('/user/favoritecategories/remove', 'DELETE', $hdrs, array('category_id' => $categoryId), true);
        if ($response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => $response->message, 'code' => $response->code));
        } else {
            return $this->sendResponse(array('status' => 'OK', 'message' => 'SUCCESS', 'code' => 200));
        }
    }
}
