{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': t._('settings') ]  %}
        <div class="row margin-top-lg">
            {{ form('apps/update', 'method': 'post', 'enctype': 'multipart/form-data', 'class': 'form-horizontal col-sm-11 color-686a6c') }}
                <div class="form-group js-upload-container">
                    <div class="col-sm-3 text-right">
                        {% if appImage %}
                            {{ image(
                                appImage,
                                "alt": "Speakol",
                                "title": "Speakol",
                                "class": "profile-picture img-circle js-preview"
                            ) }}
                        {% else %}
                            {{ image(
                                "img/symbol.png",
                                "alt": "Speakol",
                                "title": "Speakol",
                                "class": "profile-picture img-circle js-preview"
                            ) }}
                        {% endif %}
                    </div>
                    <div class="col-sm-3 " style="width:20%">
                        <div class="width-all fileUpload btn btn-default padding-bottom-sm btn-primary no-radius blue_btn black-btn border-none font-15">
                            <span>{{ t._('upload-logo') }}</span>
                            {{ hiddenField({"app_logo", 'class' :'js-picture-value'})  }}
                            {{ fileField({
                                "",
                                "class": "form-control no-radius js-upload-button"
                            }) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 text-right margin-top-xs font-15" for="">{{ t._('app_id')  }}</label>
                    <div class="col-sm-3 margin-top-xs font-15">{{ app_id  }}</div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{ t._('app_name')  }}</label>
                    <div class="col-sm-3">
                        {{ textField({
                            "app_name",
                            "class": "form-control no-radius font-15 arial-font"
                        }) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{ t._('website-URL') }}</label>
                    <div class="col-sm-3 js-websites-container">
                        {% if not websitesLength %}
                            <div class="js-website relative  {{ loop.index0 ? ' margin-top-sm' : '' }}">
                                {{ textField({
                                    "website[]",
                                    "value": "",
                                    "class": "js-website-input form-control no-radius font-15 no-padding-height"
                                }) }}
                            </div>
                        {% endif %}
                        {% for website in websites %}
                            <div class="js-website relative  {{ loop.index0 ? ' margin-top-sm' : '' }}">
                                {{ textField({
                                    "website[]",
                                    "value": website,
                                    "class": "js-website-input form-control no-radius font-15 no-padding-height"
                                }) }}
                                {% if loop.index0 %}
                                    <button 
                                        class="js-remove-domain remove-domain btn-reset no-radius absolute color-6e6f6f"
                                    >
                                        <i class="fa fa-times fa-lg"></i>
                                    </button>
                                {% endif %}
                            </div>
                        {% endfor %}
                        {% if (not isFreePlan) %}
                        <button 
                            class="js-add-domain absolute add-domain btn-reset no-radius color-57b25f {{ websitesLength < websiteLimit ? '' : 'hide'}}" 
                            {{ websitesLength < websiteLimit ? '' : 'disabled' }}
                        >
                            <i class="fa fa-plus fa-lg"></i>
                        </button>
                        <input type="hidden" id="planId" value="{{ planID }}">
                        {% endif %}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('app_category') }}</label>
                    <div class="col-sm-3">
                        {{ select(
                            "category",
                            categories,
                            'using': ['id', 'name'],
                            'class': 'form-control no-radius font-15 no-padding-height arial-font'
                        ) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('newsletter') }}</label>
                    <div class="col-sm-3">
                        <select 
                            id="newsletter" 
                            name="newsletter" 
                            class="form-control no-radius font-15 no-padding-height arial-font"
                        >
                            {% set options = [ '2': t._('monthly'), '1': t._('weekly'), '0': t._('no-email') ] %}
                            {% for key, value in options %}
                                <option value="{{ key }}" {{ key === newsletter ? 'selected' : '' }}>{{ value }}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('app_lang') }}</label>
                    <div class="col-sm-3">
                        {{ select(
                            "lang",
                            langs,
                            'using': ['id', 'name'],
                            'class': 'form-control no-radius font-15 no-padding-height arial-font'
                        ) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-3 margin-bottom-lg">
                        <a href="#" class="password-toggle">{{ t._('change-password')  }}</a>
                    </div>
                </div>
                <div class="row password-toggle-container hide">
                    <div class="row">
                        <div class="col-sm-3 text-right">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-info fa-stack-1x white"></i>
                            </span>
                        </div>
                        <div class="col-sm-8 h5">
                            <p>Please choose a secure and memorable password, but one you feel comfortable sharing with your colleagues. You will all use these login details to access Speakol and create debates within your website</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('old-passwd') }}</label>
                        <div class="col-sm-3">
                            {{ passwordField({
                                "old_password",
                                "class": "form-control no-radius font-15 arial-font"
                            }) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('nw-passwd') }}</label>
                        <div class="col-sm-3">
                            {{ passwordField({
                                "password",
                                "class": "form-control no-radius font-15 arial-font"
                            }) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('cnfrm-passwd') }}</label>
                        <div class="col-sm-3">
                            {{ passwordField({
                                "confirm_password",
                                "class": "form-control no-radius font-15 arial-font"
                            }) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ submitButton({
                        "submit",
                        "value": t._('updt-accnt'),
                        "class": "font-15 col-sm-2 col-sm-offset-3 btn blue_btn no-radius bg-light-blue"
                    }) }}
                </div>
            {{ endForm() }}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
