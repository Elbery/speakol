<?php
use Speakol\Routes\ArgumentsBoxRoutes, Speakol\Routes\UsersRoutes, Speakol\Routes\AppsRoutes, Speakol\Routes\DashboardRoutes, Speakol\Routes\ComparisonsRoutes, Speakol\Routes\DebatesRoutes, Speakol\Routes\ModeratesRoutes, Speakol\Routes\IndexRoutes, Speakol\Routes\NotificationsRoutes, Speakol\Routes\ArgumentsRoutes, Speakol\Routes\StaticRoutes;
$router = new \Phalcon\Mvc\Router();
$router->removeExtraSlashes(true);
$router->setDefaultModule("backend");
$router->mount(new ArgumentsBoxRoutes());
$router->mount(new UsersRoutes());
$router->mount(new AppsRoutes());
$router->mount(new DashboardRoutes());
$router->mount(new ComparisonsRoutes());
$router->mount(new DebatesRoutes());
$router->mount(new ModeratesRoutes());
$router->mount(new IndexRoutes());
$router->mount(new NotificationsRoutes());
$router->mount(new ArgumentsRoutes());
$router->mount(new StaticRoutes());
return $router;