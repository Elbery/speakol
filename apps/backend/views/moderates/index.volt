{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': t._('moderate') ]  %}
        <div class="row margin-top-lg">
            <div class="col-sm-9">
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a class="font-15"href="#arguments" aria-controls="arguments" role="tab" data-toggle="pill">{{ t._('arguments') }}</a></li>
                    <li role="presentation"><a class="font-15" href="#replies" aria-controls="replies" role="tab" data-toggle="pill">{{ t._('replies') }}</a></li>
                    <li role="presentation"><a class="font-15" href="#your_content" aria-controls="your_content" role="tab" data-toggle="pill">{{ t._('your-content')  }}</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <div class="row">
            <div class="tab-content">
                <div class="tab-pane fade in active" role="tabpanel" id="arguments">
                    {% include "partials/moderates/arguments.volt"  %}
                    {% if hasMore['argument'] %}
                        <div class="row">
                            <a class="h5 pag-arguments more_cont no-radius light-blue border-none col-sm-6 col-sm-offset-2 padding-top-lg padding-bottom-lg weight-bold bg-e8e8e8" data-argument="2" data-page="1" data-ajax-url="/moderates/index?type=argument" href="#">{{ t._('show-next-20')  }}</a>
                        </div>
                    {% endif %}
                </div>
                <div class="tab-pane fade" role="tabpanel" id="replies">
                    {% include "partials/moderates/replies.volt"  %}
                    {% if hasMore['reply'] %}
                        <div class="row">
                            <a class="h5 pag-replies more_cont no-radius light-blue border-none col-sm-6 col-sm-offset-2 padding-top-lg padding-bottom-lg weight-bold bg-e8e8e8" data-reply="2" data-page="0" data-ajax-url="/moderates/index?type=reply" href="#">{{ t._('show-next-20')  }}</a>
                        </div>
                    {% endif %}
                </div>
                <div class="tab-pane fade" role="tabpanel" id="your_content">
                    {% include "partials/moderates/yourcontent.volt"  %}
                </div>
            </div>
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
