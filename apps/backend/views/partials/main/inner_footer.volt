{# <footer> #}
{#     <ul class="footer_links col-lg-8 clearfix"> #}
{#         <li>{{ linkTo('static/about', t._('abts-us')) }}</li> #}
{#         <li>{{ linkTo('pdf/Speakol-Apps.pdf', t._('help')) }}</li> #}
{#         <li>{{ linkTo('static/terms', t._('terms')) }}</li> #}
{#         <li>{{ linkTo('static/privacy', t._('privacy')) }}</li> #}
{#         <li>{{ linkTo('static/apps', t._('apps')) }}</li> #}
{#         <li>{{ linkTo('static/publishers', t._('publishers')) }}</li> #}
{#     </ul> #}
{#     <span class="copyRights col-lg-4 text-right"></span> #}
{# </footer> #}

<footer class="bg-f6f6f6 row width-all footer h5 margin-top-lg padding-top-lg">
    <div class="footer-container thick-overline">
        <div class="col-sm-7 margin-top-sm">
            <ul class="list-inline">
                <li>{{ link_to('static/about', t._('abts-us'), 'class' : 'color-a2a1a1') }}</li>
                <li>{{ link_to('pdf/Speakol-Apps.pdf', t._('help'), 'class': 'color-a2a1a1') }}</li>
                <li>{{ link_to('static/terms', t._('terms'), 'class': 'color-a2a1a1' ) }}</li>
                <li>{{ link_to('static/privacy', t._('privacy'), 'class' : 'color-a2a1a1' ) }}</li>
                <li>{{ link_to('static/apps', t._('apps'), 'class': 'color-a2a1a1') }}</li>
                <li>{{ link_to('static/contactus', t._('contact-us'), 'class': 'color-a2a1a1') }}</li>
            </ul>
        </div>
        <div class="col-sm-5 text-right margin-top-sm {{ lang === "ar" ? "rtl" : ""  }}">
            {{ t._('cpright') }} © {{ date('Y') }} - {{ t._('all-rits-resrvd') }}, {{ t._('speakol') }}.
        </div>
    </div>
</footer>
