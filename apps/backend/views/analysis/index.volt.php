<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            <?php foreach ($jsVars as $key => $value) { ?>
                phpVars.<?php echo $key; ?> = '<?php echo $value; ?>';
            <?php
} ?>
        </script>

        <?php echo $this->assets->outputCss(); ?>
        <?php echo $this->assets->outputJs('header'); ?>
        <!--[if lt IE 9]>
        <?php echo $this->assets->outputJs('header-ie9'); ?>
    <![endif]-->
    </head>
    <body class="clearfix">
        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper">

            <!-- Sidebar -->
            <nav id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    
<?php $controller = $this->dispatcher->getControllerName(); ?>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar_brand"><?php $speakolLogo = $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?><?php echo $this->tag->linkto('index', $speakolLogo); ?></li>
        <?php if ($this->session->app_data) { ?>
        <li class="sidebar_dshbrd <?php echo ($controller == 'dashboard' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('dashboard', 'dashboard'); ?></li>
        <li class="sidebar_dbt <?php echo ($controller == 'debates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('debates/code', 'Create Debate'); ?></li>
        <li class="sidebar_cmprsn <?php echo ($controller == 'comparisons' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('comparisons/code', 'Create Comparison'); ?></li>
        <li class="sidebar_argmnt <?php echo ($controller == 'arguments' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('arguments/code', 'Create Argument'); ?></li>
        <li class="sidebar_mdrt <?php echo ($controller == 'moderates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('moderates/index', 'Moderate'); ?></li>
        <li class="sidebar_anlsys <?php echo ($controller == 'analysis' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('analysis', 'Analysis'); ?></li>
        <li class="sidebar_aignout"><?php echo $this->tag->linkto('apps/logout', 'Sign Out'); ?></li>
        <?php
} ?>
    </ul>
</nav> 
                </ul>
            </nav>

            <!-- Page content -->
            <div id="page-content-wrapper">

                <!-- Keep all page content within the page-content inset div! -->
                <div class="page-content inset clearfix">
                    <div class="col-lg-12 no_padding">
                        <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                        <div class="story_container analysis">
                            <div class="top_bar">
                                <div class="img_circl_container">
                                    <span class="img_circl">
                                        <a href=""><img src="img/user_default.gif" alt="" title=""></a>
                                    </span>
                                    <span><h1>Analysis</h1></span>
                                </div>
                            </div>
                            <div class="blc">
                                <div class="shw_fltr">
                                    <ul class="nav nav-pills">
                                        <li><span>Show: </span></li>
                                        <li><a href="<?php echo $linkfor; ?>">All</a></li>
                                        <li class="active"><a href="<?php echo $linkforfilter; ?>debate">Debates</a></li>
                                        <li class="active"><a href="<?php echo $linkforfilter; ?>comparison">Comparisons</a></li>
                                        <li class="active"><a href="<?php echo $linkforfilter; ?>argumentboxes">Argument Boxes</a></li>
                                    </ul>
                                    <div class="drpdwn_cont">
                                        <span> Sort By:</span>
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                                                Date (Newest)
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $linkfororder; ?>created_at">Date (Newest)</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $linkfororder; ?>">Name</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <?php $this->partial('partials/analysis/items', array('analysis' => $analysis)); ?>

                            </div>
                        </div>

                    </div>
                </div>

            </div>


        </div>

        <?php echo $this->assets->outputJs('footer'); ?>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <!--<script>-->
        <!--var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];-->
        <!--(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];-->
        <!--g.src='//www.google-analytics.com/ga.js?1l3kd93m';-->
        <!--s.parentNode.insertBefore(g,s)}(document,'script'));-->
        <!--</script>-->
    </body>
</html>