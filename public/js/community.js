(function() {
    var Community = function() {
        this.lang = phpVars.lang;
        this.bindEvents();
        this.bindPostMsgHandlers();
        this.bindPopupCloseEvent();
        this.initInfiniteScrolling('.js-feed-container', '.js-feed-paginate');
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };

    Community.Events = {
        '.js-login-button': 'openLoginForm',
        '.js-register-button': 'openRegisterForm',
        '.js-category-star': 'handleStarringCategory',
        '.js-hamburger-menu': 'toggleSidebar',
    };

    Community.prototype.bindEvents = function() {
        var _this = this;
        $.each(Community.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    Community.prototype.initInfiniteScrolling = function(container, paginate) {
        $(container).jscroll({
            'nextSelector': paginate,
            'loadingHtml': $('.js-svg-spinner').html(),
            'callback': function() {
                if (($(".naqeshny-canvas")).length) {
                    $(".naqeshny-canvas").each(function (index, obj) {
                        $(obj).debateVersus();
                    });
                }
            }
        });  
    };


    Community.prototype.bindPostMsgHandlers = function() {
        window.addEventListener('message', function(e) {
            var iframes = document.getElementsByClassName('speakol');
            for(var i = 0 ; i < iframes.length ; i++ ) {
                if(e.source === iframes[i].contentWindow) {
                    var message = JSON.parse(e.data);
                    handlePostMessage(message, iframes[i]);
                }
            }
        });
    };

    Community.prototype.handleStarringCategory = function($el) {
        if($el.hasClass('js-category-starred')) {
            this.unStarCategory($el);
        } else {
            this.starCategory($el);
        } 
    };


    Community.prototype.starCategory = function($el) {
        var $category = $el.parents('.js-category'),
            $categoriesList = $el.parents('.js-categories'),
            $favoriteCategories = $categoriesList.find('.js-category-highlighted'),
            favoriteCategories = $favoriteCategories.toArray(),
            $categoryIcon = $el.find('.js-category-icon'),
            $newsfeedLink = $categoriesList.find('.js-newsfeed'),
            categoryId = $category.attr('data-category-id');

        $.ajax({
            url: '/categories/star/' + categoryId,
            type: 'POST',
            dataType: 'json',
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.addClass('js-category-starred');
                $categoryIcon.removeClass('sp-star').addClass('sp-star-highlight');
                $category.addClass('js-category-highlighted');
                favoriteCategories.push($category);
                favoriteCategories.sort(sortByName);
                $favoriteCategories.remove();
                $category.remove();
                $newsfeedLink.after(favoriteCategories);
            }
        }).fail(this.errorHandler.bind(this));
    };


    Community.prototype.unStarCategory = function($el) {
        var $category = $el.parents('.js-category'),
            $categoriesList = $el.parents('.js-categories'),
            $normalCategories = $categoriesList.find('.js-category:not(.js-category-highlighted)'),
            normalCategories = $normalCategories.toArray(),
            $categoryIcon = $el.find('.js-category-icon'),
            $newsfeedLink = $categoriesList.find('.js-newsfeed'),
            categoryId = $category.attr('data-category-id');
        $.ajax({
            url: '/categories/unstar/' + categoryId,
            type: 'DELETE',
            dataType: 'json'
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.removeClass('js-category-starred');
                $categoryIcon.removeClass('sp-star-highlight').addClass('sp-star');
                $category.removeClass('js-category-highlighted');
                normalCategories.push($category);
                normalCategories.sort(sortByName);
                $normalCategories.remove();
                $category.remove();
                $categoriesList.append(normalCategories);
            } 
        }).fail(this.errorHandler.bind(this));
        
    };

    Community.prototype.openLoginForm = function($el) {
        openPopup(true);
    };

    Community.prototype.openRegisterForm = function($el) {
        openPopup(false);
    };

    Community.prototype.bindPopupCloseEvent = function() {
        $(document).on('transitionend', '#speakol-login-container', function(e) {
            if( parseInt($(this).width()) === 0) {
                $('#speakol-login-container').remove();
                $('#speakol-style').remove();
            }
        });
    };

    Community.prototype.toggleSidebar = function($el) {
        var $sidebarContainer = $('.js-community-sidebar-container'),
            $headerContainer = $('.js-community-header-container'),
            $contentContainer = $('.js-community-content-container');

        $sidebarContainer.toggleClass('active');
        $headerContainer.toggleClass('active');
        $contentContainer.toggleClass('active');
    }

    Community.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler(jqXHR.settings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.message,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages
        };
        $('.js-normal-submit input,textarea').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(errorMsgs[status]);
        $('#errorbox').modal('show');
    };
    Community.prototype.unauthorizedHandler = function() {
        var lastAction = {
            url: ajaxSettings.url,
            type: ajaxSettings.type,
            dataType: ajaxSettings.dataType,
            data: ajaxSettings.data,
            processData: ajaxSettings.processData,
            contentType: ajaxSettings.contentType,
        };

        localStorage.setItem('lastAction', JSON.stringify(lastAction));
        openPopup(true);
    };

    function sortByName(prev, next) {
        var prevName = $(prev).attr('data-category-name').toLowerCase();
        var nextName = $(next).attr('data-category-name').toLowerCase();
        return prevName.localeCompare(nextName);
    }
    function openPopup(login) {
        login = login === true;
        if($("#speakol-login-container").length) {
            return;
        }
        var styles = "#speakol-login-container {"
        styles += "top: 0;";
        styles += "right: 0;";
        styles += "overflow-x: hidden;";
        styles += "overflow-y: auto;";
        styles += "position: fixed;";
        styles += "width: 0px;";
        styles += "height: 100%;";
        styles += "background: white;";
        styles += "transition: width 0.5s ease;";
        styles += "z-index: 99999999999 !important;";
        styles += "border-left: 1px solid rgba(91, 183, 103, .4);";
        styles += "}";
        var css = document.createElement('style');
        css.type = "text/css";
        css.id = "speakol-style";
        css.appendChild(document.createTextNode(styles));
        document.getElementsByTagName('head')[0].appendChild(css);
        var loginIframe = document.createElement('iframe');
        var width = ($(window).width() > 480) ? 480 : 320;
        var route = "/users/register?redirect_url=" + encodeURIComponent(document.location.href) + '&lang=' + phpVars.lang;
        if(login) {
            var route = "/users/login?redirect_url=" + encodeURIComponent(document.location.href) + '&lang=' + phpVars.lang;
        }
        loginIframe.setAttribute('src', document.location.origin + route);
        loginIframe.setAttribute('width', width + 'px');
        loginIframe.setAttribute("class", 'speakol');
        loginIframe.setAttribute("id", 'speakol-login');
        loginIframe.setAttribute("scrolling", "no");
        loginIframe.setAttribute("style", "border:none;");
        html =$( '<div id="speakol-login-container"></div>' );
        html.append(loginIframe);
        $("body").append(html);
        var loginContainer = document.getElementById('speakol-login-container');
        window.getComputedStyle(loginContainer).width;
        loginContainer.style.width = (width + 1) + "px";
    }

    function handlePostMessage(message, iframe) {
        if(message.id.indexOf("iframeMessenger") <= -1) {
            return;
        }
        var msgHandlers = {};
        msgHandlers['close-popup'] = function() {
            var loginContainer = document.getElementById('speakol-login-container');
            if(!loginContainer) {
                return;
            }

            loginContainer.style.width = "481px";
            window.getComputedStyle(loginContainer).width;
            loginContainer.style.width = "0px";
        };

        msgHandlers['set-height'] = function() {
            var plugin;
            if($(iframe).parents('.speakol-arguments-box').length) {
                plugin = $(iframe).parents('.speakol-arguments-box');

            } else if( $(iframe).parents('.speakol-debate').length) {
                plugin = $(iframe).parents('.speakol-debate');

            } else if ($(iframe).parents('.speakol-comparison').length) {
                plugin = $(iframe).parents('.speakol-comparison');
            }
            if(plugin && plugin.find('.speakol-loading').length) {
                plugin.find('.speakol-loading').remove();
                $(iframe).css('visibility', 'visible');
            }
            $(iframe).height(message.value);
        };
        
        msgHandlers['reload'] = function() {
            window.location.reload();
        }

        if(msgHandlers[message.type]) {
            msgHandlers[message.type]();
        }
        if(message.reload) {
            msgHandlers['reload']();
        }
    }

    var community = new Community();
})();
