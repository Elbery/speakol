<?php
namespace Speakol\Social;
class SocialProvidersFactory {
    public static function create($type) {
        switch ($type) {
            case 'google':
                return new GoogleSocialProvider();
            break;
            case 'facebook':
                return new FacebookSocialProvider();
            break;
        }
    }
}
