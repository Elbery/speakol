<footer>
    <ul class="footer_links clearfix">
        <li>{{ linkTo('static/about', t._('abts-us')) }}</li>
        <li>{{ linkTo('pdf/Speakol-Apps.pdf', t._('help') ) }}</li>
        <li>{{ linkTo('static/terms', t._('terms')) }}</li>
        <li>{{ linkTo('static/privacy', t._('privacy')) }}</li>
        <li>{{ linkTo('static/apps', t._('apps')) }}</li>
        <li>{{ linkTo('static/careers', t._('careers')) }}</li>
    </ul>
    <span class="copyRights">{{ t._('cpright') }} © {{ date('Y') }} - {{ t._('all-rits-resrvd') }}, {{ t._('speakol') }}.</span>
</footer>