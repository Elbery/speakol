<?php
namespace Speakol\Backend\Controllers;
class NewsfeedController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('debates');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function renderItem($type, $id, $appId) {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->type = $type;
        $this->view->communityPage = true;
        $this->view->url = $this->request->get('_url');
        if ($appId && ctype_digit($appId)) {
            $this->view->appId = $appId;
        }
        $favorites = array();
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites ? $objUserProfile->data->favorites : array();
            }
        }
        $categoriesData = $this->formatCategories($favorites);
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        $this->view->pick('newsfeed/item');
    }
    public function argumentsboxAction() {
        $argumentsboxUrl = $this->request->get('url');
        $appId = $this->request->get('app');
        $sthis->view->uuid = '';
        if ($this->request->get('uuid')) {
            $this->view->uuid = $this->request->get('uuid');
        }
        if ((!$argumentsboxUrl && !$appId) || !ctype_digit($appId)) {
            return $this->response->redirect('newsfeed');
        }
        $this->view->argumentsboxUrl = $argumentsboxUrl;
        $this->renderItem('argumentsbox', $argumentsboxUrl, $appId);
    }
    public function debateAction() {
        $slug = $this->request->get('slug');
        $appId = $this->request->get('app');
        if ((!$slug && !$appId) || !ctype_digit($appId)) {
            return $this->response->redirect('newsfeed');
        }
        $this->view->slug = $slug;
        $this->renderItem('debate', $slug, $appId);
    }
    public function comparisonAction() {
        $slug = $this->request->get('slug');
        $appId = $this->request->get('app');
        if ((!$slug && !$appId) || !ctype_digit($appId)) {
            return $this->response->redirect('newsfeed');
        }
        $this->view->slug = $slug;
        $this->renderItem('comparison', $slug, $appId);
    }
    public function indexAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $type = $this->request->get('type');
        $page = $this->request->get('page');
        $lastCreatedDate = $this->request->get('last_date');
        $page = $page ? $page : 1;
        $type = $type ? $type : 'all';
        $limit = 10;
        $this->view->homePage = true;
        $this->view->communityPage = true;
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $this->view->isLoggedIn = false;
        } else {
            $this->view->isLoggedIn = true;
        }
        $params = array('page' => $page, 'limit' => $limit, 'type' => $type,);
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        $plugins = $this->getPlugins($params);
        $this->view->plugins = $plugins;
        $lastCreatedDate = $plugins[count($plugins) - 1]->created_at;
        $lastCreatedDate = str_replace(' ', '+', $lastCreatedDate);
        $url = $this->request->get('_url');
        $nextUrl = $url;
        $url.= '?page=' . ($page);
        $nextUrl.= '?page=' . ($page + 1);
        if ($type !== 'all') {
            $url.= '&type=' . $type;
            $nextUrl.= '&type=' . $type;
        }
        $url.= '&last_date=' . $lastCreatedDate;
        $nextUrl.= '&last_date=' . $lastCreatedDate;
        $url.= '&lang=' . $this->view->lang;
        $nextUrl.= '&lang=' . $this->view->lang;
        $this->view->url = $url;
        $this->view->nextUrl = $nextUrl;
        $this->view->type = $type;
        $favorites = array();
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->view->superAdmin = $this->view->LoggedInUserData->role_id === 2;
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites ? $objUserProfile->data->favorites : array();
            }
            if (!$objUserProfile) {
                $introParagraph = $this->utility->iCurl('/speakol/intro/show', 'GET', $hdrs, array('lang' => $this->getLang()), true);
                $this->view->introParagraph = $introParagraph->data;
            }
        } else {
            $introParagraph = $this->utility->iCurl('/speakol/intro/show', 'GET', $hdrs, array('lang' => $this->getLang()), true);
            $this->view->introParagraph = $introParagraph->data;
            $objUserProfile = false;
        }
        $this->view->paginate = false;
        if ($this->request->isAjax()) {
            $this->view->highlightedPlugins = array();
            $this->view->paginate = true;
            $this->view->pick('partials/newsfeed/feed');
            return;
        }
        $categoriesData = $this->formatCategories($favorites);
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->view->highlightedPlugins = $this->getHighlightedPlugins(array('type' => $type, 'lang' => $this->getLang()));
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        if ($this->view->superAdmin) {
            $this->assets->addJs('js/highlight.js?' . $this->config->application->cache_string);
        }
        $this->view->pick('newsfeed/index');
    }
    public function highlightAction($pluginId) {
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getAppData())) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'UnAuthorized', 'code' => 401));
        }
        $app = $this->getAppOwnerData();
        if ($app->role_id !== 2) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'UnAuthorized', 'code' => 401));
        }
        if (!$this->request->isPost() && !$this->request->isAjax()) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Method', 'code' => 400));
        }
        $type = $this->request->get('type');
        if (!$type || !$pluginId) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Parameters', 'code' => 400));
        }
        $params = array('plugin_type' => $type, 'plugin_id' => $pluginId,);
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $response = $this->utility->iCurl('/speakol/highlight/add', 'POST', $hdrs, $params, false, false, true);
        if ($response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => $response->message, 'code' => 400));
        }
        return $this->sendResponse(array('status' => 'OK', 'message' => $response->data, 'code' => 200));
    }
    public function unhighlightAction($pluginId) {
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getAppData())) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'UnAuthorized', 'code' => 401));
        }
        $app = $this->getAppOwnerData();
        if ($app->role_id !== 2) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'UnAuthorized', 'code' => 401));
        }
        if (!$this->request->isDelete() && !$this->request->isAjax()) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Method', 'code' => 400));
        }
        $type = $this->request->get('type');
        if (!$type || !$pluginId) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Parameters', 'code' => 400));
        }
        $params = array('plugin_type' => $type, 'plugin_id' => $pluginId,);
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $response = $this->utility->iCurl('/speakol/highlight/remove', 'DELETE', $hdrs, $params, true);
        if ($response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => $response->message, 'code' => 400));
        }
        return $this->sendResponse(array('status' => 'OK', 'message' => $response->data, 'code' => 200));
    }
}
