{% if replies.data is defined %}
    {% for reply in replies.data %}
        {% include "partials/argumentsbox/nested-reply" with ['reply': reply, 'argumentID': argumentID, 'adID': adID, 'argumentsBoxURL': argumentsBoxURL, 'not_nested_reply':'0'] %}
    {% endfor %}
{% endif %}
