{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title':  t._('ads') ]  %}
        <div class="row margin-top-lg">
            {{ form('ads', 'method': 'post', 'enctype': 'multipart/form-data', 'class': 'form-horizontal col-sm-11 color-686a6c', 'id': 'adsForm') }}
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Text</label>
                    <div class="col-sm-3">
                        {{ textField({"text", "class": "form-control no-radius font-15"}) }}
                    </div>
                </div>
                <div class="form-group js-upload-container">
                    <div class="col-sm-3 text-right">
                        {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol", "class": "profile-picture img-circle js-preview") }}
                    </div>
                    <div class="col-sm-3 " style="width:20%">
                        <div class="width-all fileUpload btn btn-default padding-bottom-sm btn-primary no-radius blue_btn black-btn border-none font-15">
                            <span>Image</span>
                            {{ fileField({"image", "class": "form-control no-radius js-upload-button"}) }}
                        </div>
                    </div>
                </div>
                <div class="form-group js-upload-container">
                    <div class="col-sm-3 text-right">
                        {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol", "class": "profile-picture img-circle js-preview") }}
                    </div>
                    <div class="col-sm-3 " style="width:20%">
                        <div class="width-all fileUpload btn btn-default padding-bottom-sm btn-primary no-radius blue_btn black-btn border-none font-15">
                            <span>Owner photo</span>
                            {{ fileField({"owner_photo", "class": "form-control no-radius js-upload-button"}) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Owner name</label>
                    <div class="col-sm-3">
                        {{ textField({"owner_name", "class": "form-control no-radius font-15"}) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Owner email</label>
                    <div class="col-sm-3">
                        {{ textField({"owner_email", "class": "form-control no-radius font-15"}) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Owner language</label>
                    <div class="col-sm-3">
                        {{ select("owner_lang", langs, 'using': ['id', 'name'],  'class': 'form-control no-radius font-15 no-padding-height') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">URL</label>
                    <div class="col-sm-3">
                        {{ textField({"url", "class": "form-control no-radius font-15"}) }}
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Plugin Type</label>
                    <div class="col-sm-3">
                        <select id="" name="plugin_type" class="form-control no-radius font-15 no-padding-height">
                            <option value="comparison">Comparison</option>
                            <option value="debate">Debate</option>
                            <option value="argumentbox">Argumentsbox</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">Plugin slug or URL</label>
                    <div class="col-sm-3">
                        {{ textField({"plugin", "class": "form-control no-radius font-15"}) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ submitButton({"submit", "value": t._('add-ads'), "class": "font-15 col-sm-2 col-sm-offset-3 btn blue_btn no-radius bg-light-blue"}) }}
                </div>
            {{ endForm() }}

    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>


