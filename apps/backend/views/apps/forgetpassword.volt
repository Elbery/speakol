<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">

<div class="row">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container login">
                    <div class="top_bar">
                        <span>{{ t._('frst-time-spkl') }} ?</span>
                        {{ linkTo({'apps/create', t._('create-accnt-now'), "class": "btn blue_btn"}) }}
                    </div>
                    <div class="login_container">
                        <h2>{{ t._('forget-password') }}</h2>
                        {{ form('apps/forgetpassword') }}
                        <div class="form_cont">

                            <div class="login_form">
                                {#  <p class="warning">
                                      {{ t._('forget-password-notice') }}
                                  </p>#}
                                <div class="input-group">
                                    <span class="input-group-addon email_ico"></span>
                                    {{ textField( {
                                        "email", 
                                        'placeholder': t._('email-addr'), 
                                        'class': 'form-control', 
                                        "data-validation-engine" :"validate[required, custom[email]]",
                                        "data-errormessage-value-missing": t._('email-required'),
                                        "data-errormessage-custom-error": t._('email-not-valid')
                                    } ) }}
                                </div>
                                <div class="form_row">
                                    {{ submitButton(t._('send-verifi-code')) }}
                                </div>
                            </div>
                        </div>
                        {{ endForm() }}

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
