{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30 row margin-bottom-lg">
        <div class="col-sm-11">
            {% include "partials/dashboard/stats.volt"  %}
            {% if maxPlugins !== 'Unlimited' %}
                {% if remainingPlugins <=  0 %}
                    {% set background = 'bg-f9c6c6' %}
                    {% set color = 'color-9b3535' %}
                    {% set icon = 'speakol-times-circle color-dc5959'%}
                {% elseif remainingPlugins > 5 %}
                    {% set background = 'bg-bee7f1' %}
                    {% set color = 'color-217088' %}
                    {% set icon = 'speakol-info-circle color-49aec7'%}
                {% else %}
                    {% set background = 'bg-fbe2a7' %}
                    {% set color = 'color-966f1d' %}
                    {% set icon = 'speakol-exclamation-triangle color-e6b729'%}
                {% endif %}
                <div class="row margin-top-sm padding-top-xs padding-bottom-xs padding-left-10 {{ background }} {{ color }}">
                    <div class="pull-left">
                        <i class="speakol-icon speakol-icon-md {{ icon }}"></i>
                    </div>
                    <div class="media-body font-15 margin-top-xs">
                        <p class="padding-left-10 no-margin">{{ quotaText }} <a class="{{ color }} decoration-underline weight-bold" href="/pricing">{{ t._('upgrade-limit') }}</a></p>
                    </div>
                </div>
            {% endif %}
            {% if downgradeStatus.downgraded %}
                <div class="row margin-top-sm padding-top-sm padding-bottom-sm padding-left-10 bg-f9c6c6 color-9b3535">
                    <div class="pull-left margin-top-xs">
                        <i class="speakol-icon speakol-icon-md speakol-times-circle color-dc5959"></i>
                    </div>
                    <div class="media-body font-15">
                        <p class="padding-left-10 no-margin">
                            {{ t._('charge-account-fail') }}
                            {% if downgradeStatus.card_number is defined and downgradeStatus.payment_method is defined  %}
                                {{ t._('using') }}  {{ downgradeStatus.payment_method }} {{ t._('ending-with') }} {{ downgradeStatus.card_number }}
                            {% endif %}
                            ,{{ t._('automaitcally-downgraded') }},
                            <a class="color-9b3535 decoration-underline weight-bold" href="/billing">{{ t._('update-payment-method') }}</a>
                        </p>
                    </div>
                </div>
            {% endif %}
            <div class="row">
                <div class="col-sm-8">
                    {% include "partials/dashboard/pending_arguments.volt" %}
                </div>
                <div class="col-sm-4">
                    {% include "partials/dashboard/latest_content.volt" %}
                </div>
            </div>
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>

<script type="text/javascript">
    var phpVars = new Object();
    {% for key, value in jsVars %}
        phpVars.{{key}} = '{{value}}';
    {% endfor %}
</script>
