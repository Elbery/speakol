<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}
                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}

    {#     <!--[if lt IE 9]> #}
    {#     {{ assets.outputJs('header-ie9') }} #}
    {# <![endif]--> #}
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">
<div class="js-svg-spinner hide">
<div class="sp-block-group text-center">
    {% include 'partials/newsfeed/spinner.volt' %}
</div>
</div>
{% include 'partials/newsfeed/header.volt' %}


<div class="sp-block-group community-content-container bg-f5f5f6 js-community-content-container">
    <div class="sp-block four bg-f5f5f6 community-sidebar-container js-community-sidebar-container">
        <div class="sp-block seven-xxl five-offset-xxl color-777777 h5">
            {% include 'partials/newsfeed/sidebar.volt' %}
        </div>
    </div>
    <div class="sp-block eight four-offset bg-white height-all feed-container js-feed-container">
        {% for flashType, messages in flash.getMessages() %}
            {% for message in messages %}
                {% set classes = ['notice': 'bg-4fc0e8', 'success' : 'bg-57b25f', 'error': 'bg-d11b1f']%}
                {% set icons = ['notice': 'sp-exclamation', 'success' : 'sp-check', 'error': 'sp-exclamation']%}
                <div class="{{ classes[flashType] }} font-13 white radius-5 media padding-top-sm padding-bottom-sm margin-top-xl margin-bottom-sm">
                    <div class="pull-left">
                        <i class="sp-icon {{ icons[flashType] }} margin-left-5"></i>
                    </div>
                    <div class="media-body">
                        <p class="no-margin">
                            {{ message }}
                        </p>
                    </div>
                </div>
            {% endfor%}
        {% endfor %}
        <div class="media margin-top-lg padding-top-lg">
            <div class="pull-left">
                <img class="community-profile-image radius-5" src="{{ appImage }}" alt="">
            </div>
            <div class="media-body">
                <h2 class="font-28 no-margin margin-bottom-sm color-333333 margin-top-xs weight-bold">{{ appName }}</h2>
                <div class="sp-block-group text-left h5 padding-bottom-sm">
                    <span class="inline-block profile-info profile-info-first ">
                        <strong class="weight-bold">{{ appInteractions.debates_count }}</strong>&nbsp;{{ t._('debates-small') }}
                    </span>
                    <span class="inline-block profile-info ">
                        <strong class="weight-bold">{{ appInteractions.comparisons_count }}</strong>&nbsp;{{ t._('comparisons-small') }}
                    </span>
                    <span class="inline-block profile-info ">
                        <strong class="weight-bold">{{ appInteractions.argumentboxes_count }}</strong>&nbsp;{{ t._('argumentsboxes') }}
                    </span>
                    <span class="inline-block profile-info profile-info-last">
                        <strong class="weight-bold">{{ appInteractions.interactions_count }}</strong>&nbsp;{{ t._('interactions') }}
                    </span>
                </div>
            </div>
        </div>
        <div class="sp-block-group margin-bottom-lg margin-top-sm">
            <div class="pull-right">
            <div class="dropdown">
                <label for="filterDropdown" class="inline-block margin-right-5 weight-normal color-909090">{{ t._('display') }}</label>
                {% set filterTexts = ['debates':  'Debates', 'comparisons': 'Comparisons', 'argumentboxes': 'Argument Boxes', 'all' : 'All Content']%}
                <button class="h5 btn-reset border-e4e4e4 no-padding padding-5 radius-5 dropdown-toggle no-radius margin-top-sm text-center color-909090" type="button" id="filterDropdown" data-toggle="dropdown" aria-expanded="true">
                    {{ t._(type) }}&nbsp;<i class="sp-icon sp-caret"></i>
                </button>
                <ul class="padding-bottom-sm small-tip border-transparent radius-5 dropdown-menu right-0 left-auto color-585858 community-dropdown-menu" role="menu" aria-labelledby="filterDropdown">
                    <li role="prensentation" class=" padding-bottom-sm padding-top-sm community-dropdown-item border-halflength halflength-f5f4f4">
                        <a class="no-padding-height font-13 {{ type === 'debates' ? 'weight-bold' : '' }}" href="{{ url }}&type=debates&lang={{ lang }}" title=""  >{{ t._('debates') }}</a>
                    </li>
                    <li role="prensentation" class=" padding-bottom-sm padding-top-sm community-dropdown-item border-halflength halflength-f5f4f4">
                        <a class="no-padding-height font-13 {{ type === 'comparisons' ? 'weight-bold' : '' }}" href="{{ url }}&type=comparisons" title=""  >{{ t._('comparisons') }}</a>
                    </li>
                    <li role="prensentation" class=" padding-bottom-sm padding-top-sm community-dropdown-item border-halflength halflength-f5f4f4">
                        <a class="no-padding-height font-13 {{ type === 'argumentboxes' ? 'weight-bold' : '' }}" href="{{ url }}&type=argumentboxes" title=""  >{{ t._('argumentboxes') }}</a>
                    </li>
                    <li role="prensentation" class="padding-bottom-sm padding-top-sm community-dropdown-item">
                        <a class="no-padding-height font-13 {{ type === 'all' ? 'weight-bold' : '' }}" href="/apps/profile/{{ appId }}" title="" >{{ t._('all') }}</a>
                    </li>
                </ul>
            </div>
            </div>
        </div>
        {% include 'partials/newsfeed/feed.volt' %}
    </div>
</div>

{{ assets.outputJs() }}


<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-56411923-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
// voting circle canvas
    if (($(".naqeshny-canvas")).length) {

        $(".naqeshny-canvas").each(function (index, obj) {
            $(obj).debateVersus();
        });
    }
</script>
</body>
</html>
