(function(iframeMessenger, $) {
    if(iframeMessenger) {
        iframeMessenger.enableAutoResize({
            absoluteHeight: true
        });
    }




    window.addEventListener('message',function(e) {
        var message = JSON.parse(e.data);
        var msgHandlers = {};

        msgHandlers['iframe-show' ] = function() {
            if($('.naqeshny-canvas').length > 0 && $.fn.debateVersus) {
                $('.naqeshny-canvas').debateVersus();
            }
            if($('.readmore').length > 0) {
                $('.readmore').readmore({ embedCSS: false, collapsedHeight: 200 });
            }
        };

        msgHandlers['reload-all'] = function() {
            window.parent.postMessage(JSON.stringify({
                'id': 'iframeMessenger:dfjdkfjd',
                'type': 'reload'
            }), "*");
        };

        msgHandlers['reload-plugins'] = function() {
            window.parent.postMessage(JSON.stringify({
                'id': 'iframeMessenger:dshdfjsdk',
                'type': 'close-popup',
                'reload': true
            }), '*');
        };



        if(msgHandlers[message.type]) {
            msgHandlers[message.type]();
        }

    });




})(iframeMessenger, jQuery);
