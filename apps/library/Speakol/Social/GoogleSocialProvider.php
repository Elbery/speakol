<?php
namespace Speakol\Social;
class GoogleSocialProvider extends \Phalcon\Mvc\User\Plugin implements SocialProvider {
    public function getUser() {
    }
    public function getAccessToken($code, $state_code_a, $redirectURI) {
        $this->google->setRedirectUri($redirectURI);
        $this->google->authenticate($code);
        $response = json_decode($this->google->getAccessToken());
        return $response->access_token;
    }
    public function getLoginURL($redirectURI) {
        $this->google->setRedirectUri($redirectURI);
        return $this->google->createAuthUrl();
    }
    public function getSocialId() {
        $oauth2 = new \Google_Service_Oauth2($this->google);
        $user = $oauth2->userinfo->get();
        return ($user) ? $user['id'] : false;
    }
}
