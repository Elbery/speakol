<div class="row thick-underline">
    <div class="col-sm-9">
        {% if lang === "ar"  %}
            <h3 class="font-20 weight-bold"> {{ t._('activity')  }} <span class="js-stats-range">{{ t._('todays')  }}</span></h3>
        {% else %}
            <h3 class="font-20 weight-bold"><span class="js-stats-range">{{ t._('todays')  }}</span> {{ t._('activity') }}</h3>
        {% endif  %}
    </div>
    <div class="col-sm-3">
        <div class="select-dropdown margin-top-sm">
            <label for="" class="width-all font-13 color-c8c8c8 weight-normal">
                {{ t._('display')  }}: 
            <select id="" name="" class="inline-block bg-transparent border-none margin-top-xs js-stats-select color-6e6f6f">
                <option value="0" selected data-range-name="{{ t._('todays')  }}">{{ t._('today')  }}</option>
                <option value="1" data-range-name="{{ t._('yesterdays')  }}">{{ t._('yesterday')  }}</option>
                <option value="7" data-range-name="{{ t._('week-agos')  }}">{{ t._('week-ago')  }}</option>
                <option value="30" data-range-name="{{ t._('last-months')  }}">{{ t._('last-month')  }}</option>
                <option value="1000" data-range-name="{{ t._('all-times')  }}">{{ t._('all-time')  }}</option>
            </select>
            </label>
        </div>
    </div>
</div>
<div class="row dashboard-stats">
    <div class="col-sm-4 stats-indicator-right">
        <div class="row text-right">
            <h4 class="h1-lg js-number-comments weight-bold"></h4>
            <h4 class="font-16">{{ t._('comments')  }}</h4>
        </div>
        <div class="row text-center bg-medium-grey">
            <h4 class="js-stats-comments h5"></h4>
        </div>
    </div>
    <div class="col-sm-4 stats-indicator-left stats-indicator-right">
        <div class="row text-right">
            <h4 class="h1-lg js-number-votes weight-bold"></h4>
            <h4 class="font-16">{{ t._('votes') }}</h4>
        </div>
        <div class="row text-center bg-medium-grey">
            <h4 class="js-stats-votes h5"></h4>
        </div>
    </div>
    <div class="col-sm-4 stats-indicator-left">
        <div class="row text-right">
            <h4 class="h1-lg js-number-activeusers weight-bold"></h4>
            <h4 class="font-16">{{ t._('activeusers') }}</h4>
        </div>
        <div class="row text-center bg-medium-grey">
            <h4 class="js-stats-activeusers h5"></h4>
        </div>
    </div>
</div>
