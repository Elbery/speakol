{% if adID is defined and adID %}
    <div class="sp-block-group js-reply reply margin-top-sm" data-reply-id="{{ reply.id }}" data-owner-id="{{ reply.owner_id  }}" data-ad-id="{{ adID  }}">
{% else %}
    <div class="sp-block-group js-reply reply margin-top-sm" data-reply-id="{{ reply.id }}" data-owner-id="{{ reply.owner_id  }}" data-argument-id="{{ argumentID }}">
{% endif %}
    <div class="sp-block-group no-margin">
        <div class="media sp-block eleven {{ right ? 'one-push' : '' }}">
          <div class="{{ right ? "pull-right" : "pull-left" }}">
                {% if reply.owner_photo is defined and reply.owner_photo != "" %}
                    <a class="js-profile-link" href="{{ reply.admin_at ? config.application.main_website ~ 'apps/profile/' ~ reply.admin_at : config.application.main_website ~ 'users/profile/' ~ reply.owner_id }}"><img class="media-object profile-picture-sm radius-5" src="{{reply.owner_photo}}" alt="" title=""></a>
                {% else %}
                    <a class="js-profile-link" href="{{ reply.admin_at ? config.application.main_website ~ 'apps/profile/' ~ reply.admin_at : config.application.main_website ~ 'users/profile/' ~ reply.owner_id }}">{{ image("img/user_default.gif" , 'class': 'media-object profile-picture-sm radius-5') }}</a>
                {% endif %}
            </div>
          <div class="media-body {{ right ? 'text-right' : '' }}">
                {% set statusColor = (reply.owner_status) ? "color-53a960" : "color-bebebe" %}
                {% set ownerStatus = '<i class="fa fa-circle-o font-11 hide-xxs js-owner-status ' ~ statusColor ~  '" data-owner-id="' ~ reply.owner_id~ '"></i>'%}
                {% if right %}
                    {% set headingContent = ownerStatus ~ '&nbsp;' ~ reply.owner_name %} 
                {% else %}
                    {% set headingContent = reply.owner_name ~  '&nbsp;' ~ ownerStatus %} 
                {% endif %}
                <a class="js-profile-link" href="{{ reply.admin_at ? config.application.main_website ~ 'apps/profile/' ~ reply.admin_at : config.application.main_website ~ 'users/profile/' ~ reply.owner_id }}" target="_blank" class="media-heading h5 light-blue display-block {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                    {% if not_nested_reply == 0 %}
                        &nbsp;<span class="font-11 color-aebac1"><i class="fa fa-reply"></i>&nbsp;{{reply.reply_to_user}}</span>
                    {% endif %}
                </a>
                {# {% if reply.url %} #}
                {# <a href="{{ utility.isFullURL(reply.url) ?   reply.url : 'http://' ~ reply.url }}" target="_blank" class="media-heading h5 light-blue display-block {{ right ? "text-right" : "" }}"> #}
                {#     {{ headingContent }} #}
                {#     {% if not_nested_reply == 0 %} #}
                {#         &nbsp;<span class="font-11 color-aebac1"><i class="fa fa-reply"></i>&nbsp;{{reply.reply_to_user}}</span> #}
                {#     {% endif %} #}
                {# </a> #}
                {# {% else %} #}
                {# <h4 class="media-heading h5 light-blue {{ right ? "text-right" : "" }}"> #}
                {#     {{ headingContent }} #}
                {#     {% if not_nested_reply == 0 %} #}
                {#         &nbsp;<span class="font-11 color-aebac1"><i class="fa fa-reply"></i>&nbsp;{{reply.reply_to_user}}</span> #}
                {#     {% endif %} #}
                {# </h4> #}
                {# {% endif %} #}
                <p class="font-11 color-aebac1 {{ right ? "text-right rtl" : "" }}">{{ t._('on') }} {{ reply.created_at }}</p>
          </div>
        </div>
        {% if LoggedInUserData.id == reply.owner_id %}
        <div class="one sp-block {{ right ? 'eleven-pull' : '' }}">
            <button class="btn-reset reply_delete transition-opacity opacity-0 opacity-hover color-c1cacf" {{ adID is defined and adID ? 'data-ad="true"' : '' }} data-reply-id="{{ reply.id }}">
                <i class="fa fa-times"></i>
            </button>
        </div>
        {% endif %}
</div>
<div class="sp-block-group no-margin">
    <p class="readmore font-13 break-word no-margin {{ lang === 'ar' and comparison ? 'text-right' : ''}}">{{ utility.insertHashtagHTML(reply.context) }}</p>
    {% if reply.attached_photos %}
    {% for file in reply.attached_photos %}
                    {% set extension = substr(file.url, -3)  %}
                    {% if extension === "ogg"  %}
                        <div class="js-player player">
                            <input type="hidden" value="{{ file.url  }}">
                            <button class="js-play-button play-button clearfix">
                                <span class="play-icon fa fa-play pull-left"></span>
                                <span class="js-play-text play-text font-13 pull-left" data-text="{{ t._('click-play-small')  }}" data-text-small="{{ t._('click-play-small')  }}"></span>
                            </button>
                        </div>
                    {% else %}
                        {{ image(file.url, "class":"img-responsive margin-bottom-sm margin-top-sm") }}
                    {% endif %}
    {% endfor %}
    {% endif %}
</div>
    <div class="sp-block-group text-center margin-bottom-sm h6 js-reply-actions margin-top-sm {{ lang === 'ar' and comparison ? 'flip' : '' }}">
        <div class="sp-block four-xs  vertical-separator-right {{ comparison ? 'text-left one two-sm' : 'text-left two three-sm' }}">
            <button data-reply-id="{{ reply.id }}" class="js-reply-thumbs-up btn-reset no-padding {{ (reply.currentUserhasVoted and reply.currentUserVote) ? "js-voted color-5bb767" : "color-c1cacf"  }}" title="{{ t._('vote_up') }}" data-thumbs="{{ reply.count_thumbs_up }}" {{ adID is defined and adID ? 'data-ad="true"' : ''  }}>
                <i class="sp-icon sp-thumbs-up {{ lang === 'ar' and comparison ? 'flip' : '' }}"></i>&nbsp;
                <span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{ reply.count_thumbs_up}}</span>
            </button>
        </div>
        <div class="sp-block four-xs  {{ comparison ? 'text-right one two-sm' : 'text-right two three-sm' }}">
            <button data-reply-id="{{ reply.id }}" class="js-reply-thumbs-down btn-reset no-padding {{ (reply.currentUserhasVoted and !reply.currentUserVote) ? "js-voted color-d10b1f" : "color-c1cacf"  }}" title="{{ t._('vote_down') }}" data-thumbs="{{ reply.count_thumbs_dn }}" {{ adID is defined and adID ? 'data-ad="true"' : ''  }}>
                <i class="sp-icon sp-thumbs-down {{ lang === 'ar' and comparison ? 'flip' : '' }}"></i>&nbsp;
                <span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{ reply.count_thumbs_dn}}</span>
            </button>
        </div>
        {% if reply.reply_level <= 2 %}
            <div class="sp-block four-xs {{comparison ? 'two eight-offset-lg two-sm six-offset-sm' : 'two six-offset-lg three-sm three-offset-sm' }} text-right">
                <a class="js-show-nested-replies hover-c1cacf color-c1cacf" href="javascript:" {{ adID is defined and adID ? 'data-ad="true"' : ''  }}><span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{reply.replies_count}}</span>&nbsp;<i class="sp-icon sp-comment {{ lang === 'ar' and comparison ? 'flip' : '' }}"></i></a>
            </div>
        {% endif %}
    </div>

    <div class="js-nested-replies-container transition-opacity opacity-0 {{ right or (lang === 'ar' and comparison) ? 'one-offset-right' : 'one-offset' }} hide" data-comments-page="1" data-total-replies-pages="{{total_replies_pages}}">

        {% if adID is defined and adID %}
            {% include "partials/argumentsbox/reply-form" with ['adID': adID,'parent_reply': reply.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:''] %}
        {% else %}
            {% include "partials/argumentsbox/reply-form" with ['argumentID': argumentID,'parent_reply': reply.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:''] %}
        {% endif %}
        <div class="sp-block-group">
            <ul class="js-nested-replies">
            </ul>
        </div>
        {% if reply.replies_count and reply.replies_count > 2 %}
            <div class="sp-block-group text-center thin-overline margin-bottom-sm padding-top-sm">
                <button class="js-more-nested-replies  bg-647a88 font-13 radius-5 inline-block width-78 padding-top-xs padding-bottom-xs white border-none" {{ adID is defined and adID ? 'data-ad="true"' : ''}}>{{ t._('show-more') }} </button>
            </div>
        {% endif %}
    </div>
</div>
