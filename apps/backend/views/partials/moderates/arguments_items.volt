    {% for element in arguments %}
        <li class="js-argument {{ loop.last ? "" : "thin-underline"  }} {{ (loop.first and ajax) ? "thin-overline" : ""  }}">
            <div class="row margin-top-lg">
                <div class="col-xs-10">
                    <div class="media">
                        <div class="pull-left">
                            {% if element['owner_photo'] is defined and element['owner_photo'] != "" %}
                                {{ image( element['owner_photo'] , "class" : "media-object img-circle" ) }}
                            {% else %}
                                {{ image("img/user_default.gif", "class": "media-object img-circle" ) }}
                            {% endif %}
                        </div>
                        <div class="media-body">
                            {% if element['url'] %}
                            <a href="{{ utility.isFullURL(element['url']) ? element['url'] : "http://" ~ element['url'] }}" target="_blank">
                                <h4 class="media-heading light-blue weight-bold font-15">{{ element['owner_name'] }}</h4>
                            </a>
                            {% else %}
                                <h4 class="media-heading light-blue weight-bold font-15">{{ element['owner_name'] }}</h4>
                            {% endif %}
                            <p class="{{ lang === 'ar' ? 'rtl text-left' : ''  }} h6">
                            {% set title = element['plugin_title'] ? element['plugin_title'] : element['plugin_url']%}
                            <time><span>{{ element['date'] }}</span></time>
                            {% if element['plugin_url'] %}
                            ,  <a href="{{ utility.isFullURL(element['plugin_url']) ? element['plugin_url'] : 'http://' ~ element['plugin_url'] }}" target="_blank">
                                 <span class="light-blue"> {{ t._('in')  }} {{ title  }}</span> 
                               </a>
                            {% else %}
                                 ,<span class="light-blue"> {{ t._('in')  }} {{ title  }}</span> 
                            {% endif %}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 text-right">
                    <button class="btn btn-default bg-white arg_delete border-none no-radius" data-argument-id="{{ element['id'] }}"><i class="fa fa-times red fa-lg padding-bottom-xs"></i></button>
                </div>
            </div>
            <div class="row margin-bottom-sm">
                <p class="h5">{{ element['context'] }}</p>
                {% if element['attached_photos'] %}
                    {% set extension = substr(element['attached_photos'], -3)  %}
                    {% set file = element['attached_photos'] %}
                    {% if extension === "ogg"  %}
                        <div class="js-player player">
                            <input type="hidden" value="{{ file  }}">
                            <button class="js-play-button play-button clearfix">
                                <span class="play-icon glyphicon glyphicon-play pull-left"></span>
                                <span class="play-text" data-text="Click here to play audio comment" data-text-small="Play comment"></span>
                            </button>
                        </div>
                    {% else %}
                        {{ image(file, "class":"margin-bottom-xs") }}
                    {% endif %}
                {% endif %}
            </div>
        </li>
    {% endfor %}
