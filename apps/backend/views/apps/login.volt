<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">

        <!-- Keep all page content within the page-content inset div! -->
<div class="row">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container login">
                    <div class="top_bar">
                        <span>{{ t._('frst-time-spkl') }} ?</span>
                        {{ linkTo({'pricing', t._('create-accnt-now'), "class": "btn blue_btn"}) }}
                    </div>
                    <div class="login_container">
                        <h2>{{ t._('login') }}</h2>
                        {{ form('apps/login') }}
                            {% if redirectURL %}
                                <input type="hidden" name="redirect_url" value="{{ redirectURL }}">
                            {% endif %}
                        <div class="form_cont">
                            <div class="input-group">
                                <span class="input-group-addon email_ico"></span>
                                    {{ textField( {
                                        "email", 
                                        'placeholder': t._('email-addr'), 
                                        'class': 'form-control', 
                                        "data-validation-engine" :"validate[required, custom[email]]",
                                        "data-errormessage-value-missing": t._('email-required'),
                                        "data-errormessage-custom-error": t._('email-not-valid')
                                    } ) }}
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon password_ico"></span>
                                    {{ passwordField( {
                                        "password", 
                                        'size': 45, 
                                        'placeholder': t._('passwd'), 
                                        'class': 'form-control', 
                                        "autocomplete": "off", 
                                        "data-validation-engine" :"validate[required]",
                                        "data-errormessage-value-missing": t._('password-required')
                                    } ) }}
                            </div>
                            <div class="row">
                                <input type="checkbox" id="rememberMe"/>
                                <label for="rememberMe"> {{ t._('remember-me') }}</label>
                                {{ submitButton({t._('login'), "class": "btn blue_btn"}) }}
                            </div>
                            <div class="row">
                                <a class="forgot" href="{{ url('apps/forgetpassword') }}"> {{ t._('forgot-passwd') }}? </a>
                            </div>
                        </div>
                        {{ endForm() }}
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>
