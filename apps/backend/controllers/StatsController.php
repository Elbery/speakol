<?php
namespace Speakol\Backend\Controllers;
class StatsController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/moderate.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery-1.10.1.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap3-typeahead.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/stats.js?' . $this->config->application->cache_string);
    }
    public function indexAction() {
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $page = ($this->request->get('page')) ? $this->request->get('page') : 1;
        $params = array('page' => $page,);
        $response = $this->utility->iCurl('/apps/list/users', 'GET', $hdrs, $params);
        if ($this->request->isAjax()) {
            $this->response->setHeader("Content-Type", "application/json");
            if ($response->status === 'OK') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $response->data));
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $response->messages));
            }
            return $this->response;
        }
        if ($response->status !== 'OK') {
            $this->flash->error($response->messages);
            $this->response->redirect('dashboard/index');
        } else {
            $this->view->users = $response->data->users;
            $this->view->apps = $response->data->apps;
            $this->view->page = $page;
        }
        $this->tag->appendTitle(' Stats');
    }
    public function sortAction($sortItem) {
        $possibleSorts = array('apps', 'users');
        if (!in_array($sortItem, $possibleSorts) && $this->request->isAjax()) {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => 'Invalid Action'));
            return $this->response;
        }
        if (!in_array($sortItem, $possibleSorts) && !$this->request->isAjax()) {
            return $this->response->redirect('dashboard');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $page = ($this->request->get('page')) ? $this->request->get('page') : 1;
        $limit = ($this->request->get('limit')) ? $this->request->get('limit') : 10;
        $sort = ($this->request->get('sort')) ? $this->request->get('sort') : 'asc';
        $sortKey = $this->request->get('sort_key');
        $params = array('page' => $page, 'limit' => $limit, 'sort' => $sort, 'sort_key' => $sortKey,);
        if ($sortItem === 'users') {
            $action = '/apps/stats/users';
        } else {
            $action = '/apps/stats/apps';
        }
        $response = $this->utility->iCurl($action, 'GET', $hdrs, $params);
        if ($this->request->isAjax()) {
            $this->response->setHeader("Content-Type", "application/json");
            if ($response->status === 'OK') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $response->data));
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $response->messages));
            }
            return $this->response;
        }
    }
}
