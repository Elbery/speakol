{{ form('comparisons/edit?slug=' ~ comparison.slug, 'enctype': 'multipart/form-data', "class": "js-edit-comparison form-horizontal col-sm-11 comparison-form","onsubmit":"return validateForm();") }}
    <input type="hidden" id="data_order" name="data_order" value="1,2"/>
    <div class="form-group">
        {{ textField({'comparison_title', 'placeholder': t._('comprsn_title'), 'class': 'jitter-fix arial-font form-control font-22 weight-bold input-lg no-radius no-padding', "data-validation-engine" :"validate[required]", "value": comparison.title}) }}
        <a href="#" class="info-tooltip bg-yellow black text-center absolute circle " data-toggle="tooltip" title="{{ t._('exmp_comprsn') }}" data-animation="true" data-placement="top" data-trigger="hover" >?</a>
    </div>
    <div class="form-group margin-bottom-xs">
        <label class="radio-inline {{ lang === 'ar' ? 'rtl' : '' }}" for="horizontalAlign">
            <input id="horizontalAlign" type="radio" name="align" value="0" {{  comparison.align == 0 ? 'checked' : '' }}> {{ t._('horizontal-align') }} 
        </label>
        <label class="radio-inline {{ lang === 'ar' ? 'rtl' : '' }}" for="verticalAlign">
            <input id="verticalAlign" type="radio" name="align" value="1" {{ comparison.align == 1  ? 'checked' : '' }}> {{ t._('vertical-align') }} 
        </label>
        <p class="margin-left-20 margin-top-xs color-8a8a8a">{{ t._('horizontal-layout') }}</p>
    </div>
    <div class="form-group margin-top-xs">
            <label for="enableImages" class="h5 checkbox {{ no_images ? 'padding-bottom-30' : '' }}">
                    <input id="enableImages" type="checkbox" name="enable_images" {{ no_images ? '' : 'checked' }}>{{ t._('enable-images') }}
            </label>
    </div>
    <div class="form-group">
        <ul id="comparison-items" class="no-margin">
            {% for side in comparison.sides %}
                {% set meta = side.meta[0]%}
            <li class="js-comparison-item">
                <div class="media {{ comparison.align == '1' ? 'col-sm-3 overflow-visible' : '' }}">
                    <input type="hidden" name="sides[{{ loop.index0 }}][id]" value="{{ side.id }}" class="js-id-input">
                    {% if comparison.align == '1' %}
                    <div class="js-dropzone-container relative">
                    {% else %}
                    <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }} js-dropzone-container relative">
                    {% endif %}
                        {# <button class="btn-reset js-remove-item absolute top-0 remove-item {{ comparison.align == 1 ? '' : 'horizontal' }} color-686a6c opacity-50"><i class="fa fa-times"></i></button> #}
                        <div class="row text-center cropImage container {{ no_images ? 'hide' : '' }}">
                            <div class="dropzone row bg-white dropzone-edit" id="logo_{{ loop.index }}" name="logo_{{ loop.index }}" data-width="165" data-height="165" data-url="" data-originalsize="false" data-text="{{ t._('optional') }}" data-image="{{ meta.image }}">
                                <input type="file" id="file_logo_{{ loop.index }}" name="file_logo_{{ loop.index }}" style="z-index: 10;">
                            </div>
                            <div class="row error_message margin-bottom-xs margin-top-xs" id="error{{loop.index}}"></div>
                            <div class="row dev-tools margin-bottom-xs margin-top-xs"></div>
                            <input id="side_{{loop.index}}_logo" name="sides[{{loop.index0}}][logo]" type="hidden" value="">
                        </div>
                        <div class="row {{ comparison.align == 1  ? 'margin-center width-78 margin-bottom-lg' : 'padding-right-5 margin-bottom-sm' }} js-change-image-container  {{ no_images ? 'hide' : '' }}">
                            <button class="js-change-image btn-reset no-radius no-border bg-white color-8d8d8d padding-top-xs padding-bottom-xs width-all {{ lang === 'ar' ? 'text-right rtl' : 'text-left' }}">
                                <i class="sp-icon sp-change-image margin-left-5"></i>&nbsp;{{ t._('change-image') }}
                            </button>
                        </div>
                        {% if comparison.align == 1 %}
                        <div class="row margin-center width-78 {{ no_images ? '' : 'margin-top-sm'}} js-remove-item-container remove-item-edit vertical">
                        {% else %}
                        <div class="row padding-left-5 padding-right-5 {{ no_images ? '' : 'margin-top-sm'}} margin-bottom-sm js-remove-item-container remove-item-edit">
                        {% endif %}
                                <button class="js-remove-item btn-reset no-radius no-border text-center bg-white color-c10707 border-c10707 padding-top-xs padding-bottom-xs width-all text-left">
                                        {{ t._('remove-item') }}
                                </button>
                        </div>
                    </div>
                    <div class="media-body">
                    {% if comparison.align == '1' %}
                        <div class="row margin-bottom-xs js-title-input title-input margin-center width-78 {{ lang === 'ar' ? 'rtl' : '' }}">
                    {% else %}
                        <div class="row margin-bottom-xs js-title-input title-input six horizontal {{ lang === 'ar' ? 'pull-right rtl' : '' }}">
                    {% endif %}
                        {{ textField( {'id':"side_" ~ loop.index ~ "_title", 'name': 'sides['~ loop.index0 ~ '][title]', 'placeholder': t._('item-name'), 'class': 'jitter-fix arial-font form-control no-radius weight-bold inline-block h4 no-padding title-input arial-font', "data-validation-engine": "validate[required]", "value" : meta.title } )}}
                    </div>
                    <div class="row js-description-input description-input {{ lang === 'ar' ? 'rtl' : '' }} {{ comparison.align == 1 ? 'margin-center width-78 vertical' : '' }}">
                        {{ textarea( {'id' : "side_" ~ loop.index ~ "_description", 'name': 'sides[' ~ loop.index0 ~ '][description]', 'placeholder':t._('description-optional'), "class": "border-none no-radius no-resize h5 description-input-textarea " ~ ( comparison.align == 1 ? '' : 'horizontal description-edit' ), "value" : meta.description } )}}
                    </div>
                    </div>
                </div>
            </li>
            {% endfor %}
        </ul>
        {% set fullItems = (comparison.sides | length === 8 and comparison.align == 0 ) or (comparison.sides | length === 4 and comparison.align == 1)%}
        <div class="js-add-item-container {{ comparison.align == 1 ? 'col-sm-3' : 'row' }}">
            <div class="row text-center container " id="add_more_comparison">
                <div class="add-item add-edit js-add-item js-add-edit border-dashed row cursor-pointer bg-white {{ fullItems ? 'hide' : '' }} {{ comparison.align == 1 ? '' : 'horizontal' }}">
                        <i class="fa fa-2x-sm fa-5x fa-plus color-6dad78"></i>
                        <div class="color-868889 font-16">{{ t._('add-item') }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-2 text-right">
            <label for="comparisonComments" class="h5 margin-right-10">{{ t._('Comments') }}:</label>
        </div>
        <div class="col-sm-10">
            <label for="comparisonComments" class="h5 checkbox">
                    <input id="comparisonComments" type="checkbox" name="hide_comment" {{ comparison.hide_comment == 1  ? 'checked' : '' }}> {{ t._('hide-comments') }} 
            </label>
        <p class="margin-top-xs color-8a8a8a">{{ t._('if-enabled') }}</p>
        </div>
    </div>

    <div class="form-group {#lang === "ar" ? "rtl" : ""#}">
        <div class="col-sm-2  text-right {#lang === "ar" ? "pull-right" : ""#}">
            <label for="debtCategory" class="h5 margin-right-10 {#lang === "ar" ? "rtl" : ""#}">{{ t._('cmprsn-crt') }}:</label>
        </div>
        <div class="col-sm-2 width-18 {#lang === "ar" ? "pull-right" : ""#}">
            {{ select("category", categories, 'using': ['id', 'name'], "useEmpty" : true, 'emptyText': t._('choose...'),"id": "debtCategory", 'class': 'form-control no-radius no-padding-height ' ~ (lang === "ar" ? "rtl" : ""), "data-validation-engine" :"validate[required]", 'data-prompt-position': lang === 'ar' ? 'topLeft' : '') }}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-2 col-lg-2 width-18 {# lang ==="ar" ? 'col-sm-offset-6 col-lg-offset-8' : 'col-sm-offset-3 col-lg-offset-2'  #}">
            {{ submitButton({  t._('update'), 'class': 'form-control no-radius bg-light-blue h5 padding-top-xs padding-bottom-xs' } )}}
        </div>
    </div>
{{ endform() }}

