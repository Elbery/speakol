
$(document).ready(function() {
    /* ------- my scripts ------- */
    //toggle sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });

    // tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // activated tab
        e.relatedTarget // previous tab
    })

    //top carousel
    $('.carousel').carousel({
        interval: 5000,
        wrap: true,
        pause:"hover"
    })

    // user notifications box Hover to open
    //=======================================
    $('.popup').hover(function () {
        $(this).find('.popup_container').show();
    }, function () {
        $(this).find('.popup_container').hide();
    });

    // rearrange outer aside after inner aside
    //=======================================
    jQuery(window).resize(function(){

        var windowsize = jQuery(window).width();
        if (windowsize < 1199) {
            jQuery(".outer_aside").insertAfter(jQuery(".inner_aside"));
        } else {
            jQuery(".col-lg-3.bg_drkGrey").append(jQuery(".outer_aside"));
        }
    });

    // disable click on current nav item
    //=========================================
    $(".sidebar-nav li.current a").click(function(e){
        e.preventDefault();
    });

    $('.vote_up, .vote_down, .compare_one,.compare_two').click(function(e){
        e.preventDefault();
        $(this).addClass('voted');
        $(this).siblings('a').addClass('dimmed').off( "click").attr('href','javascript:');
    });


    // Search box keep visible on focus
    //=====================================================

    $(".searchbox").focus(function(){
        $(this).parent().addClass('active');
    });
    $(".searchbox").blur(function(){
        $(this).val("").parent().removeClass('active');
        $(this).next(".inast_srch_dd").removeClass('opened');
    });
    $(".searchBar span").on("click",function(){
        $(".searchbox").focus();
    });
    $('.searchbox').keypress(function() {
        $(this).next(".inast_srch_dd").addClass('opened');
    });
    $(".inast_srch_dd").click(function(e){
        e.stopPropagation()
        $(".searchbox").focus();
        $(".inast_srch_dd").addClass('opened');
    });

    // Side menu toggle button
    //=====================================================
    $(".navicon-button").click(function(){
        $(this).toggleClass("open");
        $("h1").addClass("fade");
    });


    // delete image in a comment when click on close button
    //=====================================================
    $(".close_btn").click( function(e){
        e.preventDefault();
        $(this).parent().remove();
    });


    // toggle replies container when click on reply button
    //=====================================================

    // search results type toggle button
    //=====================================================
    $(".srch_type_toggl").click(function(e){
        e.preventDefault();
        $(this).toggleClass("active");
    });


});