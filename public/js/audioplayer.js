(function($, speakolRecorder) {
    var recorder = speakolRecorder;
    $(document).on('click', '.js-play-button', function() {
        var $this = $(this);
        $this.prop('disabled', true);
        var $audio = $("<audio></audio>");
        $audio.css('width', '100%').css('height', '100%');
        var $parent = $(this).parent();
        var url = $parent.find('input').val();
        recorder.convertToWav(url, function(blob) {
            $audio.attr("src", URL.createObjectURL(blob));
            $this.remove();
            $parent.append($audio);
            $audio.mediaelementplayer({
                type: 'audio/wav',
                features: ['playpause', 'current', 'progress', 'duration', 'volume'],
                audioWidth: $parent.width(),
                audioVolume: 'vertical'
            });
        });
    });
})(jQuery, speakolRecorder);
