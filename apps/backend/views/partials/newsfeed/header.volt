<div class="sp-block-group community-header-container js-community-header-container">
    <header class="sp-block-group community-header">
        <div class="sp-block four-xxl seven five-xs">
            <div class="sp-block two hide-xxl padding-top-xs">
                <button class="js-hamburger-menu btn-reset hamburger-menu"></button>
            </div>
            <div class="sp-block padding-top-xs community-speakol-logo six-offset-xxl">
                <a href="/newsfeed"><img src="/img/speakol-logo-slogan.png" alt="Speakol Logo" class=""></a>
            </div>
        </div>
        <div class="margin-top-sm sp-block eight-xxl five seven-xs">
            
            <div class="dropdown pull-right">
                <button class="btn-reset border-e4e4e4 no-padding radius-5 dropdown-toggle no-radius margin-top-sm text-center color-acb6bb community-dropdown" type="button" id="langDropdown" data-toggle="dropdown" aria-expanded="true">
                    {{ lang === 'ar' ? 'Ar' : 'En' }}&nbsp;<i class="sp-icon sp-caret"></i>
                </button>
                <ul class=" small-tip border-transparent radius-5 dropdown-menu right-0 left-auto color-565656 community-dropdown-menu" role="menu" aria-labelledby="langDropdown">
                                <li role="prensentation" class="community-dropdown-item lang-halflength border-halflength halflength-f5f4f4"><a class="{{ lang === 'ar' ? '' : 'weight-bold' }}" href="{{ url }}&lang=en" title="" >English</a></li>
                                <li role="prensentation" class="padding-bottom-sm padding-top-sm community-dropdown-item"><a class="{{ lang === 'ar' ? 'weight-bold' : '' }}" href="{{ url }}&lang=ar" title="" >العربية</a></li>
                </ul>
            </div>
            {% if isLoggedIn %}
            <div class="dropdown plugin-dropdown pull-right">
                <button class="btn-reset dropdown-toggle no-radius js-notifications-button community-notification-button" type="button" id="notificationDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-bell sp-icon-lg color-9ca7ad"></i>
                </button>
                {% if isLoggedIn %}
                <ul class="tip notification-tip border-none dropdown-menu right-0 left-auto no-shadow no-radius color-9ca7ad js-notifications community-notifications-dropdown bg-f0f0f0" role="menu" aria-labelledby="notificationDropdown">
                </ul>
                {% endif %}
            </div>
            {% endif %}
            {% if isLoggedIn %}
                <div class="hide js-notification-indicator community-notification-indicator pull-right radius-5 bg-bb1111 white margin-top-sm padding-left-5 padding-right-5 weight-bold">0</div>
            {% endif %}
            <div class="js-community-search-container community-search-container relative transition-width pull-right margin-top-sm">
                <form action="/search" method="GET">
                    <input id="" type="search" name="q" class="js-community-search-input community-search-input absolute top-0 right-0 border-none outline-none width-all margin-0 radius-none color-c8c4c4" placeholder="{{ t._('type-search') }}">
                    <input type="hidden" name="lang" value="{{ lang }}">
                    <input type="submit" value="" class="community-search-submit display-block absolute right-0 top-0 cursor-pointer text-center no-margin no-padding border-none">
                    <input type="hidden" name="category" value="{{ currentCategory.id }}">
                    <input type="hidden" name="type" value="{{ type }}">
                    <i class="js-community-search-icon community-search-icon sp-icon absolute right-0 top-0 cursor-pointer text-center no-margin no-padding bg-white"></i>
                </form>
            </div>
        </div>
    </header>
</div>
