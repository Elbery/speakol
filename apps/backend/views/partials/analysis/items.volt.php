<ul class="ul_vert item_typs">
    <?php if (isset($analysis['data']) && $analysis['data']) { ?>
        <?php foreach ($analysis['data'] as $item) { ?>
            <?php if ($item['data']['module'] == 'comparison') { ?>
                <?php if ($item) { ?>
                    <?php $this->partial('partials/analysis/cmp_item', array('item' => $item)); ?>
                <?php
            } else { ?>
                    No comparison found!
                <?php
            } ?>
            <?php
        } elseif ($item['data']['module'] == 'debate') { ?>
                <?php if ($item) { ?>
                    <?php $this->partial('partials/analysis/dbt_item', array('item' => $item)); ?>
                <?php
            } else { ?>
                    No debates found!
                <?php
            } ?>
            <?php
        } elseif ($item['data']['module'] == 'argumentsbox') { ?>
                <?php if ($item) { ?>
                    <?php $this->partial('partials/analysis/arg_item', array('item' => $item)); ?>
                <?php
            } else { ?>
                    No argumentboxes found!
                <?php
            } ?>
            <?php
        } ?>
        <?php
    } ?>
    <?php
} else { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No data added yet!</small>
                </div>
            </ul>
        </div>
        
    <?php
} ?>
</ul>
<?php if ($analysis['extra']['hasmore'] == true) { ?>
    <a class="more_cont" href="<?php echo $nextp; ?>">Show next <?php echo $perpage; ?> items</a>
<?php
} ?>