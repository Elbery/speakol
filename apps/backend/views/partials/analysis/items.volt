<ul class="ul_vert item_typs">
    {% if analysis['data'] is defined and analysis['data'] %}
        {% for item in analysis['data'] %}
            {% if item['data']['module'] == "comparison" %}
                {% if item %}
                    {% include "partials/analysis/cmp_item" with ['item':item] %}
                {% else %}
                    {{ t._('no-comparsn') }}!
                {% endif %}
            {% elseif item['data']['module'] == "debate" %}
                {% if item %}
                    {% include "partials/analysis/dbt_item" with ['item':item] %}
                {% else %}
                    {{ t._('no-debts') }}!
                {% endif %}
            {% elseif item['data']['module'] == "argumentsbox" %}
                {% if item %}
                    {% include "partials/analysis/arg_item" with ['item':item] %}
                {% else %}
                    {{ t._('no-argbox') }}!
                {% endif %}
            {% endif %}
        {% endfor %}
    {% else %}
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>{{ t._('no-data') }}!</small>
                </div>
            </ul>
        </div>

    {% endif %}
</ul>
{% if analysis['extra']['hasmore'] == true %}
    <a class="more_cont" href="{{ nextp }}">{{ t._('shw-nxt') }} {{ perpage }} {{ t._('items') }}</a>
{% endif %}
