(function() {
    var ComparisonPlugin = function() {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = phpVars.lang;
        this.recorder = speakolRecorder;
        this.audio = $('#audioEnabled').length;
        this.timer = new Timer(45, undefined, function() {
            this.recorder.stop();
            this.timer.$display.parents('.js-record-bar').first().find('.js-recording-text').addClass('hide');
            var finishedText = this.timer.$display.attr('data-finished');
            this.timer.$display.html(finishedText);
        }.bind(this));
        this.bindEvents();
        this.editPlayButtonText();
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };


    ComparisonPlugin.Events = {
        '.js-comparison-accordion': 'showContent',
        '.js-comparison-tab': 'showContent',
        '.js-comparison-content': 'showContent',
        '.js-vote': 'vote',
        '.js-unvote': 'unvote',
        '.js-submit-arg': 'submitArg',
        '.js-record-button': 'startRecording',
        '.js-cancel-record': 'cancelRecording',
        '.js-send-record': 'sendRecording',
        '.js-profile-link': 'stopPropagation',
        '.fileUpload': 'stopPropagation',
        'html': 'resetSides'
    };

    ComparisonPlugin.prototype.stopPropagation = function($el, e) {
        e.stopPropagation();
    }

    ComparisonPlugin.prototype.resetSides = function($el, e) {
        var $target = $(e.target),
            accordion = $target.hasClass('js-comparison-accordion') || $target.parents('.js-comparison-accordion').length,
            tab = $target.hasClass('js-comparison-tab') || $target.parents('.js-comparison-tab').length,
            side = $target.parents('.js-side').length;
        if(accordion || tab || side) {
            return;
        }

        $('.js-side').removeClass('width-all').removeClass('hide-lg').removeClass('hide');
        $('.js-small-argument-form').removeClass('hide');
        $('.js-comparison-content').removeClass('content-vertical-focused').removeClass('content-horizontal-focused').removeClass('hide');
        $('.js-comparison-description').addClass('hide');
        $('.js-argument-form-container').addClass('hide');
        $('.js-replies-container').addClass('hide');
        $('.js-comparison-tab').removeClass('comparison-tab-focused rounded-border-left rounded-border-right');
        $('.js-comparison-accordion').removeClass('comparison-accordion-focused');
        if($('#hide_comments').length) {
            $('.js-comparison-content').addClass('hide');
        }

        addReadMore();

        this.editPlayButtonText();

    }

    ComparisonPlugin.prototype.sendRecording = function($el, e) {
        $el.text('Sending....');
        this.recorder.stop();
        this.timer.reset();
        setTimeout(function() {
            $el.text('Send');
            removeRecordBar($el);
            if(this.recorder.error) {
                var errorMsg = this.lang === 'ar' ? 'لم تقم بالسماح للميكروفون بالعمل' : 'You haven\'t allowed the microphone';
                this.showErrorMsg(errorMsg); 
            } else {
                this.submitArg($el, e, true);
            }
        }.bind(this), 200);
    };
    ComparisonPlugin.prototype.cancelRecording = function($el) {
        removeRecordBar($el);
        this.recorder.stop();
        this.timer.reset();
    };


    function removeRecordBar($el) {
        var $argumentForm = $el.parents('.js-argument-form'),
            $recordBar = $argumentForm.find('.js-record-bar'),
            $normalSubmit = $argumentForm.find('.js-normal-submit');
      $normalSubmit
      .removeClass('hide')
      .find('input, button')
      .prop('disabled', false);
      $recordBar
      .addClass('hide')
      .find('button')
      .prop('disabled', true);
    }


    function resetForms() {
      $('.js-record-bar').each(function() {
          var $this = $(this);
          $this.addClass('hide');
          $this.find('button').prop('disabled', true);
      });
      $('.js-normal-submit').each(function() {
          var $this = $(this);    
          $this.removeClass('hide');
          $this.find('input, button').prop('disabled', false);
      });
    }

    ComparisonPlugin.prototype.startRecording = function($el) {
      var $argumentForm = $el.parents('.js-argument-form'),
          $recordBar = $argumentForm.find('.js-record-bar'),
          $normalSubmit = $argumentForm.find('.js-normal-submit'),
          $timer = $recordBar.find('.js-timer');
      resetForms();
      $normalSubmit
      .addClass('hide')
      .find('input, button')
      .prop('disabled', true);
      $recordBar
      .removeClass('hide')
      .find('button')
      .prop('disabled', false);
      $recordBar.find('.js-recording-text').removeClass('hide');
      this.timer.setDisplay($timer);
      var start = function() {
          this.timer.set();
          this.recorder.record();
      }.bind(this);

      if(this.recorder.initialized) {
          if(this.recorder.recording) {
              this.timer.reset();
              this.recorder.resetClip();
              this.recorder.stop(start);
          } else {
              start();
          }
      } else {
          this.recorder.init(start);
      }

    };
    ComparisonPlugin.prototype.submitArg = function($el, e, audio) {
        audio = audio || false;
        var $argumentForm = $el.parents('.js-argument-form'),
            argumentId = $argumentForm.find('input[name="comparison_id"]').val(),
            context = $argumentForm.find('textarea[name="comment"]').val(),
            containerId = $argumentForm.find('input[name="container_id"]').val(),
            containerType = $argumentForm.find('input[name="container_type"]').val(),
            lang = this.lang,
            url = this.url,
            $side = $argumentForm.parents('.js-side'),
            sideId =   $side.attr('data-side-id'),
            audioEnabled = this.audio,
            postDataObject = {};
        $argumentForm.find('textarea, input, button').prop('disabled', true);
        var formData = new FormData();
        var file;
        formData.append('context', context);
        postDataObject['context'] = context;
        if(audio) {
            if(this.recorder.checkIfNoAudio()) {
                var message = lang === 'ar' ? 'لا يوجد صوت': 'No audio detected';
                this.showErrorMsg(message);
                return;
            }
            formData.append('audio', 1);
            file = this.recorder.exportOgg(); 
        } else {
            file = $argumentForm.find('input[name="attached"]').prop('files')[0];
            if(!!file && file.size > 512 * 1024) {
                var message = lang === 'ar' ? 'يجب أن يكون حجم الصورة أقل من 512 كيلوبايت' : 'Image size must be smaller than 512 Kilobytes';
                this.showErrorMsg(message);
                return;
            }
            postDataObject['file'] = file;
        }
        formData.append('id', containerId);
        postDataObject['id'] = containerId;
        postDataObject['type'] = containerType;
        formData.append('type', containerType);
        formData.append('file', file);
        formData.append('side_id', sideId);
        postDataObject['side_id'] = sideId;
        formData.append('action', '/arguments/factory/make');
        postDataObject['action'] = '/arguments/factory/make';
        formData.append('lang', lang);
        postDataObject['lang'] = lang;
        formData.append('link', phpVars.url ? phpVars.url : '');
        postDataObject['link'] = phpVars.url ? phpVars.url : '';
        $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: formData,
            postDataObject: postDataObject
        }).then(function(data) {
            if(data.status === 'OK') {
                $argumentForm.find('textarea, input, button').prop('disabled', false);
                $argumentForm.find('textarea[name="comment"]').val('');
                var argumentId = data.data.argument_id.id,
                    right = $side.attr('data-align') === 'right',
                sideId = data.data.side_id;
                return $.ajax({
                    url: '/arguments/render/argument',
                    type: 'GET',
                    data: {
                        'argument_id': argumentId,
                        'lang': lang,
                        'right': right, 
                        'comparison': true,
                        'audio': audioEnabled
                    }
                });

            }
        }).done(function(data) {
            data = '<li>' + data + '</li>';
            $('.js-side[data-side-id="' + sideId +'"]')
            .find('.js-arguments')
            .prepend(data);
            addReadMore();
        }).fail(this.errorHandler.bind(this));
    };

    ComparisonPlugin.prototype.showContent = function($el) {
        var sideId = $el.attr('data-side-id'),
            $comparisonDescription = $('.js-comparison-description[data-side-id="' + sideId + '"]'),
            $comparisonTab, $comparisonAccordion, $comparisonContent, $argumentFormContainer, $smallArgumentForm, $side;

        if($el.hasClass('js-comparison-accordion')) {
            $comparisonAccordion = $el;
        } else {
            $comparisonAccordion = $('.js-comparison-accordion[data-side-id="' + sideId + '"]');
        }

        if($el.hasClass('js-comparison-tab')) {
            $comparisonTab = $el;
        } else {
            $comparisonTab = $('.js-comparison-tab[data-side-id="' + sideId + '"]');
        }

        if($el.hasClass('js-comparison-content')) {
            $comparisonContent = $el;
        } else {
            $comparisonContent = $('.js-comparison-content[data-side-id="' + sideId + '"]');
        }

        $argumentFormContainer = $comparisonContent.find('.js-argument-form-container');
        $smallArgumentForm = $comparisonAccordion.find('.js-small-argument-form');
        $side = $comparisonContent.parents('.js-side');

        $('.js-comparison-content').addClass('hide').removeClass('content-vertical-focused content-horizontal-focused');
        $('.js-comparison-description').addClass('hide');
        $('.js-argument-form-container').addClass('hide');
        $('.js-comparison-tab').removeClass('comparison-tab-focused rounded-border-left rounded-border-right');
        $('.js-comparison-accordion').removeClass('comparison-accordion-focused');
        $comparisonTab.addClass('comparison-tab-focused');
        $comparisonAccordion.addClass('comparison-accordion-focused');
        $comparisonContent.removeClass('hide');
        $comparisonDescription.removeClass('hide');
        $side.addClass('width-all');
        $side.find('.js-play-button').removeClass('small');
        if($side.hasClass('js-vertical')) {
            $('.js-side').addClass('hide-lg');
            $comparisonContent.addClass('content-vertical-focused in');
            $side.removeClass('hide-lg');
            positionContent($comparisonTab, $comparisonContent);
            $argumentFormContainer.removeClass('hide');
            $smallArgumentForm.addClass('hide');
        } else {
            $comparisonContent.addClass('content-horizontal-focused');
            $argumentFormContainer.removeClass('hide');
            ramjet.transform($smallArgumentForm.get(0), $argumentFormContainer.get(0), {
                done: function() {
                    $argumentFormContainer.removeClass('hide');
                    $argumentFormContainer.find('textarea').focus();
                },
                easing: quadInOutEasing,
                duration: 500

            });
            $argumentFormContainer.addClass('hide');
            $smallArgumentForm.addClass('hide');
        }
        addReadMore();
    };

    ComparisonPlugin.prototype.showAccordionContent = function($el) {
        var sideId = $el.attr('data-side-id'),
            $comparisonContent = $('.js-comparison-content[data-side-id="' + sideId + '"]'),
            $comparisonDescription = $('.js-comparison-description[data-side-id="' + sideId + '"]'),
            $argumentFormContainer = $comparisonContent.find('.js-argument-form-container'),
            $side = $comparisonContent.parents('.js-side'),
            $smallArgumentForm = $el.find('.js-small-argument-form');
        $('.js-comparison-content').addClass('hide').removeClass('content-horizontal-focused');
        $('.js-comparison-description').addClass('hide');
        $('.js-argument-form-container').addClass('hide');
        $('.js-comparison-accordion').removeClass('comparison-accordion-focused');
        $el.addClass('comparison-accordion-focused');
        $comparisonContent.addClass('content-horizontal-focused');
        addReadMore();
        $comparisonContent.removeClass('hide');
        $comparisonDescription.removeClass('hide');
        $argumentFormContainer.removeClass('hide');
        ramjet.transform($smallArgumentForm.get(0), $argumentFormContainer.get(0), {
            done: function() {
                $argumentFormContainer.removeClass('hide');
                $argumentFormContainer.find('textarea').focus();
            },
            easing: quadInOutEasing,
            duration: 500

        });
        $argumentFormContainer.addClass('hide');
        $smallArgumentForm.addClass('hide');
    };


    function quadInOutEasing (t) {
        t /= 0.5;
        if (t < 1) return 0.5*t*t;
        t--;
        return -0.5 * (t*(t-2) - 1);
    }
    function positionContent($el, $comparisonContent) {
        var $comaprisonTabs = $('.js-comparison-tab'),
            elIndex = $comaprisonTabs.index($el) + 1,
            className = 'content-translate-' + elIndex + '-' + $comaprisonTabs.length;

        $comparisonContent.removeClass(function() {
            var classes = $(this).attr('class').split(' ');
            classes =  classes.filter(function(klass) {
                if (klass.indexOf('content-translate') !== -1) {
                    return true;
                }
                return false;
            });
            return classes.join(' ');
        });

        $comparisonContent.addClass(className);
        $el.toggleClass('rounded-border-left', elIndex > 1);
        $el.toggleClass('rounded-border-right', elIndex < $comaprisonTabs.length);

    }
    ComparisonPlugin.prototype.showTabContent = function($el) {
        var sideId = $el.attr('data-side-id'),
            $comparisonContent = $('.js-comparison-content[data-side-id="' + sideId + '"]');
            $comparisonDescription = $('.js-comparison-description[data-side-id="' + sideId + '"]'),
            $argumentFormContainer = $comparisonContent.find('.js-argument-form-container'),
            $side = $comparisonContent.parents('.js-side');
        $('.js-side').removeClass('width-all').addClass('hide');
        $('.js-comparison-content').addClass('hide').removeClass('content-vertical-focused');
        $('.js-comparison-description').addClass('hide');
        $('.js-argument-form-container').addClass('hide');
        $('.js-comparison-tab').removeClass('comparison-tab-focused rounded-border-left rounded-border-right');
        $el.addClass('comparison-tab-focused');
        $comparisonContent.addClass('content-vertical-focused').addClass('in');
        positionContent($el, $comparisonContent);
        addReadMore();
        $comparisonContent.removeClass('hide');
        $comparisonDescription.removeClass('hide');
        $argumentFormContainer.removeClass('hide');
        $side.addClass('width-all').removeClass('hide');
        $side.find('.js-play-button').removeClass('small');
    };

    function getUrl() {

        var results = new RegExp('[\?&]url=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        } else{
            return decodeURIComponent(results[1]) || window.location.href;
        }
    }

    ComparisonPlugin.prototype.vote = function($el) {
        var comparisonId = $el.attr('data-comparison-id'),
            sideId = $el.attr('data-side-id'),
            lang = this.lang,
            url = this.url,
            comparisonUrl = $("#url").val();
        if(!comparisonUrl) {
            comparisonUrl = getUrl();
            comparisonUrl = encodeURIComponent(comparisonUrl);
            $("#url").val(comparisonUrl);
        }
        $el.prop('disabled', true);
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                'action': '/vote',
                'type': 'comparison',
                'id': comparisonId,
                'side_id': sideId,
                'type': 'comparison',
                'lang': lang,
                'url' : comparisonUrl,
                'link': phpVars.url ? phpVars.url : ''
            }
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                var votedHTML = '<i class="fa fa-times"></i>&nbsp; ' + (lang === 'ar' ? 'تم التصويت' : 'Voted');
                var voteHTML = '<i class="fa fa-thumbs-up"></i>&nbsp; ' + (lang === 'ar' ? 'صوت' : 'Vote');
                $('.js-unvote')
                .removeClass('js-unvote')
                .removeClass('white')
                .removeClass('bg-b5b5b5')
                .addClass('js-vote')
                .addClass('color-5bb767')
                .addClass('bg-white')
                .html(voteHTML);
                var $voteButtons = $('.js-vote[data-side-id="' + sideId + '"]');
                $voteButtons
                .removeClass('js-vote')
                .removeClass('color-5bb767')
                .removeClass('bg-white')
                .html(votedHTML)
                .addClass('js-unvote')
                .addClass('white')
                .addClass('bg-b5b5b5');

                updateVotes(data.data); 

            }
        }).fail(this.errorHandler.bind(this));
    };


    function updateVotes(sides) {
        if(sides.constructor === Array) {
            var total = sides.map(function(side) {
                return +side.votes;
            }).reduce(function(prev, next) {
                return prev + next;
            });
        } else {
            var total = Object.keys(sides).map(function(sideId) {
                return +sides[sideId];
            }).reduce(function(prev, next) {
                return prev + next;
            });
        }
        $.each(sides, function(key, value) {
            votes = sides.constructor === Array ? +value.votes : +value;
            var percentage = votes === 0 ? 0 : Math.round((votes * 100) / total),
                sideIdSelector = sides.constructor === Array ?  '[data-side-id="' + value.side_id + '"]': '[data-side-id="'+ key +'"]';
            $('.js-progress-bar' + sideIdSelector)
            .css('left', percentage + '%')
            .attr('aria-valuenow', percentage)
            $('.js-progress-indicator' + sideIdSelector).html(percentage + '%');

            $('.js-side-votes' + sideIdSelector).html(votes);
        });  
    };


    
    ComparisonPlugin.prototype.unvote = function($el) {
        var comparisonId = $el.attr('data-comparison-id'),
            sideId =$el.attr('data-side-id'),
            lang = this.lang,
            url = this.url;
        $el.prop('disabled', true);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: '/unvote',
                    method: 'DELETE',
                    side_id: sideId,
                    id: comparisonId,
                    type: 'comparison',
                    lang: lang,
                    link: phpVars.url ? phpVars.url : ''
                }
            }).done(function(data) {
                if(data.status === 'OK') {
                    $el.prop('disabled', false);
                    var voteHTML = '<i class="fa fa-thumbs-up"></i>&nbsp; ' + (lang === 'ar' ? 'صوت' : 'Vote');
                    var $voteButtons = $('.js-unvote[data-side-id="' + sideId + '"]');
                    $voteButtons
                    .removeClass('js-unvote')
                    .removeClass('white')
                    .removeClass('bg-b5b5b5')
                    .html(voteHTML)
                    .addClass('js-vote')
                    .addClass('bg-white')
                    .addClass('color-5bb767');
                    updateVotes(data.data); 
                }
            }).fail(this.errorHandler.bind(this));
    };




    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }
    ComparisonPlugin.prototype.bindEvents = function() {
        var _this = this;
        $.each(ComparisonPlugin.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                if(key !== 'html' && key !== '.js-profile-link' && key !== '.fileUpload') {
                    e.preventDefault();
                }
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    ComparisonPlugin.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler(jqXHR.settings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.message,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages
        };
        this.showErrorMsg(errorMsgs[status]);
    };

    ComparisonPlugin.prototype.showErrorMsg = function(message) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(message);
        $('#errorbox').modal('show');
    };


    ComparisonPlugin.prototype.editPlayButtonText = function() {
        if(!$('.js-comparison-tab').hasClass('hide')) {
            $('.js-play-button').addClass('small');
        }
    };




    ComparisonPlugin.prototype.unauthorizedHandler = function(ajaxSettings) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        if(ajaxSettings) {
            var lastAction = {
                url: ajaxSettings.url,
                type: ajaxSettings.type,
                dataType: ajaxSettings.dataType,
                data: ajaxSettings.data,
                processData: ajaxSettings.processData,
                contentType: ajaxSettings.contentType,
            };

            if(ajaxSettings.postDataObject) {
                var postDataObject = ajaxSettings.postDataObject;
                if(postDataObject['file']) {
                    var fileReader = new FileReader();
                    fileReader.onload = function(e) {
                        postDataObject['file'] = e.target.result;
                        lastAction.postDataObject = postDataObject;
                        localStorage.setItem('lastAction', JSON.stringify(lastAction));
                    }
                    fileReader.readAsDataURL(postDataObject['file']);
                } else {
                    lastAction.postDataObject = ajaxSettings.postDataObject;
                    localStorage.setItem('lastAction', JSON.stringify(lastAction));
                }
            } else {
                localStorage.setItem('lastAction', JSON.stringify(lastAction));
            }

        }
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    function quadInOutEasing (t) {
        t /= 0.5;
        if (t < 1) return 0.5*t*t;
        t--;
        return -0.5 * (t*(t-2) - 1);
    }
    var comparisonPlugin = new ComparisonPlugin();

})();
