<div class="row helvetica-font rtl">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
<header class="index-header helvetica-font container no-padding margin-top-sm margin-bottom-sm padding-top-sm padding-bottom-sm">
    <div class="home-logo pull-left">
        <a href="http://speakol.dev/"> 
            {{ image("img/logo3.png", "alt": "Speakol for websites", "title": "Speakol for websites") }}
        </a>
    </div>
    <div class="dropdown pull-right">
        <button class="btn-reset border-e4e4e4 no-padding radius-3 dropdown-toggle text-center color-acb6bb lang-dropdown" type="button" id="langDropdown" data-toggle="dropdown" aria-expanded="true">
            {{ lang === 'ar' ? 'Ar' : 'En' }}&nbsp;<i class="sp-icon sp-caret"></i>
        </button>
        <ul class=" small-tip border-transparent radius-5 dropdown-menu right-0 left-auto color-565656 community-dropdown-menu" role="menu" aria-labelledby="langDropdown">
            <li role="prensentation" class="community-dropdown-item lang-halflength border-halflength halflength-f5f4f4"><a class="{{ lang === 'ar' ? '' : 'weight-bold' }}" href="http://speakol.dev/public/?lang=en" title="" >English</a></li>
            <li role="prensentation" class="padding-bottom-sm padding-top-sm community-dropdown-item"><a class="{{ lang === 'ar' ? 'weight-bold' : '' }}" href="http://speakol.dev/?lang=ar" title="" >العربية</a></li>
        </ul>
    </div>
    <div class="pull-right">
        <a class="anchor-reset inline-block h4 home-login color-57b25f radius-3 border-57b25f" href="http://speakol.dev/public/apps/login">تسجيل الدخول</a>  
        {{ linkTo({
        'pricing',
        'إنشاء حساب',
        "class": "green-anchor anchor-reset inline-block weight-bold h4 home-create nowrap white radius-3 bg-57b25f border-57b25f"
        }) }}
    </div>
</header>
<div class="row rtl add-container helvetica-font">
    <div class="add-content text-center margin-center">
        <h1 class="white font-50 margin-bottom-sm">
{% if introParagraph is defined and introParagraph %}
{{ introParagraph }}
{% else %}
قم ببناء مجتمعك وقم بزيادة تفاعل المستخدمين في موقعك 
{% endif %}
        </h1>
        <a href="http://speakol.dev/public/pricing" class="anchor-reset add-link inline-block h4 nowrap white radius-3 bg-57b25f border-57b25f">ضف Speakol لموقعك</a>
    </div>
</div>
<div class="row rtl edge-top bg-46ab5b helvetica-font">
    <div class="">
        <section class="row plugin-description margin-center">
            <div class="col-sm-6 spacing-right">
                <div class="media">
                    <div class="pull-right">
                        <i class="home-icon debate-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2>مناقشات</h2>
                    </div>
                </div>
                <p class="margin-top-sm padding-top-sm">
         بعمل مناظرات حول أي موضوع على موقعك. سوف يصوت القراء بين وجهتي نظر ويساهمون بآرائهم.
                </p>
                <p class="">
                    <a class="anchor-reset inline-block radius-5 create-link  color-3c4581 bg-white" href="http://.speakol.dev/public/pricing">قم بإنشاء أول مناقشة لك</a>
                </p>
            </div>
            <div class="col-sm-6">
                <a href="#debateModal" data-toggle="modal">
                    <img src="../public/img/home-debate.png" alt="debate example" class="debate-example">
                </a>
            </div>
        </section>
    </div>
    <div class="plugin-description-container">
        <section class="row plugin-description margin-center plugin-padding">
            <div class="col-sm-6">
                <a href="#comparisonModal" data-toggle="modal">
                    <img src="../public/img/home-comparison.png" alt="comparison example" class="comparison-example">
                </a>
            </div>
            <div class="col-sm-6 spacing-left margin-top-lg">
                <div class="media margin-top-lg padding-top-lg">
                    <div class="pull-right">
                        <i class="home-icon comparison-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2>مقارنات</h2>
                    </div>
                </div>
                <p class="weight-bold italic margin-top-sm padding-top-sm">
        إعط مستخدمينك الفرصة للتعبير عن أنفسهم
                </p>
                <p class="no-margin font-20">
انس الأسلوب القديم لاستطلاعات الرأي. اسمح للقراء أن يصوتوا إلى ما يصل إلى أربعة اختيارات ومن ثم يشرحون اختياراتهم.
                </p>
                <p class="">
                    <a class="anchor-reset inline-block radius-5 create-link color-3c4581 bg-white" href="http://speakol.dev/public/pricing">قم بإنشاء أول مقارنة لك</a>
                </p>
            </div>
        </section>
    </div>
    <div class="plugin-description-container">
        <section class="row plugin-description plugin-padding margin-center">
            <div class="col-sm-6 spacing-right">
                <div class="media">
                    <div class="pull-right">
                        <i class="home-icon argumentsbox-icon"></i>
                    </div>
                    <div class="media-body">
                        <h2>صناديق حِجَّة</h2>
                    </div>
                </div>
                <p class="margin-top-sm padding-top-sm">
        وت القراء إما لصالح أو ضد سؤال أو عبارة ما ويدعمون تصويتهم بالإدلاء بحجتهم.
                </p>
                <p class="">
                    <a class="anchor-reset inline-block radius-5 create-link color-3c4581 bg-white" href="http://speakol.dev/public/pricing">قم بإنشاء أول صندوق حِجَّة لك</a>
                </p>
            </div>
            <div class="col-sm-6">
                <a href="#argumentModal" data-toggle="modal">
                    <img src="../public/img/home-argumentsbox.png" alt="argumentsbox example" class="argumentsbox-example">
                </a>
            </div>
        </section>
    </div>
</div>
<article class="rtl padding-top-lg helvetica-font row bg-f7f7f7">
    <div class="margin-top-lg padding-top-sm row color-29303b text-center margin-center why-header">
        <h2 class="font-50 margin-top-sm">لماذا Speakol؟</h2>
        <p class="h3 margin-top-lg padding-top-sm">نحن أبعد ما نكون عن أنظمة التعليق الدعائية والعرضية التقليدية. نحن مختلفون لا جدال في ذلك.</p>
    </div>
    <div class="row why-container margin-center">
        <div class="col-sm-6">
            <section class=" margin-top-lg why-section engage-section">
                <h3 class="margin-top-xs weight-bold"> أشرك قرّائك</h3>
                <p class="no-margin">أعط القراء منصة منظمة لكي يصبحوا جزءا من المحتوى الذي تقدمه.</p>
            </section>
            <section class=" margin-top-lg why-section learn-section">
                <h3 class="margin-top-xs weight-bold"> تعرف علي قرائك</h3>
                <p class="no-margin">شاهد مؤشرات اتجاهات القراء بشكل فوري واحصل على أرقام حيوية أيضا.</p>
            </section>
            <section class=" margin-top-lg why-section build-section">
                <h3 class="margin-top-xs weight-bold"> أضف, لا تستبدل</h3>
                <p class="no-margin">استخدم (Speakol) جنبا إلى جنب مع أنظمة التعليق الخاصة بك مثل Disqus وLiveFyre.</p>
            </section>
        </div>
        <div class="col-sm-6">
            <section class=" margin-top-lg why-section easy-section">
                <h3 class="margin-top-xs weight-bold"> سهل الاستعمال</h3>
                <p class="no-margin">يستغرق الأمر بضع دقائق للتسجيل والإدماج إلى موقعك.</p>
            </section>
            <section class=" margin-top-lg why-section socially-section">
                <h3 class="margin-top-xs weight-bold"> تواصل إجتماعيا</h3>
                <p class="no-margin">شارك المناظرات بسهولة عبر شبكات التواصل الاجتماعي التقليدية وادفع مستوى الزيارات على موقعك الإلكتروني</p>
            </section>
            <section class=" margin-top-lg why-section moderate-section">
                <h3 class="margin-top-xs weight-bold"> راقب أنشطتك</h3>
                <p class="no-margin">استخدام سهل لأدوات الاعتدال لتأمين مناظرات ومناقشات صحية.</p>
            </section>
        </div>
    </div>
    <div class="row text-center why-footer">
        <h4 class="h3 margin-top-sm color-525861">ماذا تنتظر ؟</h4>
        <p class="margin-bottom-lg padding-bottom-lg">
            <a href="http://speakol.dev/public/pricing" class="anchor-reset add-link inline-block margin-bottom-lg h4 nowrap white radius-3 bg-57b25f border-57b25f green-anchor">ضف Speakol لموقعك</a>
        </p>
    </div>
</article>
<footer class="rtl index-footer no-margin helvetica-font">
    <div class="row footer-content margin-center">
        <div class="row footer-connect">
            <div class="col-sm-4 social-connect">
                <h3 class="font-17 white weight-normal margin-bottom-sm padding-bottom-sm color-c3c5ca">تواصل معنا</h3>
                <a target="_blank" class="soc_cnnct facebook" href="https://facebook.com/Speakol" title="Facebook"> Facebook</a>
                <a target="_blank" class="soc_cnnct twitter" href="http://www.twitter.com/speakol_" title="Twitter"> Twitter</a>
                <a target="_blank" class="soc_cnnct gPlus" href="https://plus.google.com/115379961634886743482/" title="Google plus"> google pluse</a>
                <a target="_blank" class="soc_cnnct instagram" href="http://instagram.com/speakol_" title="Instagram"> Instagram</a>
            </div>
            <div class="col-sm-4">
                <!-- Begin MailChimp Signup Form -->
            
                <form action="//speakol.us9.list-manage.com/subscribe/post?u=4867705d0bad713092d2d1928&amp;id=ab84493550" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <label for="mail_list" class="font-17 white weight-normal margin-bottom-sm padding-bottom-sm color-c3c5ca">إنضم إلي قائمتنا البريدية</label>
                    <input type="text" id="mail_list" name="EMAIL" placeholder="بريدك الإلكتروني" class="mail-input"/>
                    <input type="submit" value="إنضم" class="btn radius-5 bg-57b25f mail-submit green-anchor"/>
                </form>
            </div>
            <div class="col-sm-4">
                <a class="inline-block margin-top-sm" href="http://speakol.dev/public">{{ image("img/logo_white.png", "alt": "Speakol", "title": "Speakol") }}</a>
            </div>
        </div>
        <div class="row f-links">
            {{ linkTo('pricing',    'اﻷسعار' )}}
            {{ linkTo('about',    'عننا' )}}
            {{ linkTo('contactus','اتصل بنا' )}}
            {{ linkTo('terms',    'الشروط' )}}
            {{ linkTo('privacy',  'الخصوصية' )}}
            {{ linkTo('apps',     'التطبيقات' )}}
            {{ linkTo('careers',  'وظائف' )}}
            {{ linkTo('press',    'صحافة' )}}
        </div>
        <p class="ltr weight-light text-center color-c3c5ca h5">
            Copyright © 2015 - All rights reserved, Speakol | part of Naqeshny for Information Technology | 36 Mussadak Street Dokki, Giza, Egypt. 
        </p>
        <p class="ltr weight-light text-center color-c3c5ca h5 margin-top-sm">
            Speakol UK Ltd | Company Number 9234670 | Registered in England and Wales
        </p>
    </div>
</footer>
<div class="modal fade sample-modal helvetica-font" id="debateModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">انشئ مناقشة</h4>
            </div>
            <div class="modal-body"> 
                <img src="../public/img/debate-sample.png" alt="Create debate at Speakol"/>
                <a class="rtl" href="http://speakol.dev/public/pricing" title="Create debate at Speakol">ابدأ الآن وانشئ مناظرة خاصة بك</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade sample-modal helvetica-font" id="comparisonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">انشئ مقارنة</h4>
            </div>
            <div class="modal-body"> 
                <img src="../public/img/comparison-sample.png" alt="Create comparison at Speakol"/>
                <a class="rtl" href="http://speakol.dev/public/pricing" title="Create comparison at Speakol">ابدأ الآن وانشئ مفارنة خاصة بك</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade sample-modal helvetica-font" id="argumentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title weight-bold">انشئ صندوق حجة</h4>
            </div>
            <div class="modal-body">
                <img src="../public/img/argumentbox-sample.png" alt="Create Argument Box at Speakol"/>
                <a class="rtl" href="http://speakol.dev/public/pricing" title="Create Argument Box at Speakol">ابدأ الآن وانشئ صندوق حجة خاص بك</a>
            </div>
        </div>
    </div>
</div>
