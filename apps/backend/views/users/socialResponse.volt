{% if refresh %}
    <script type="text/javascript">
        window.isRefresh = {{refresh}};
                window.opener.location = window.opener.location;
        window.close();
    </script>
{% endif %}

{% if emailRequired %}
    <div class="story_container login">
        <div class="register_form">
            {{ form('users/social-response') }}
            <h2>{{ t._('registration') }}</h2>
            <div class="register_form">
                {% if error %}
                    <div class="input-group">
                        <span class="error"><b>ERROR: </b>{{ error }}</span>
                    </div>
                    <br/>
                {% endif %}
                <div class="input-group">
                    <span class="input-group-addon email_ico"></span>
                    {{ textField({"email", "placeholder": t._('email-address'), "class": "form-control"}) }}
                </div>
                <div class="form_row">
                    <br/>
                    {{ submitButton(t._('regis-complt')) }}
                </div>
            </div>
            {{ endForm() }}
        </div>
    </div>
{% endif %}