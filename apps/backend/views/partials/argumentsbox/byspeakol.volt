{# <div class="footer row"> #}
{#     <div class="col-xs-6 text-center"> #}
{#         <a class="footer-text by_speakol" target="_blank" href="http://www.speakol.com">{{ t._('powered-by') }} <span> {{ t._('speakol') }} </span> </a> #}
{#     </div> #}
{#     <div class="col-xs-6 text-center"> #}
{#         <a class="footer-text" target="_blank" href="http://plugins.speakol.com">{{ t._('add-spkl-to-ur-website') }}</a> #}
{#     </div> #}
{# </div> #}
<div class="sp-block-group margin-top-sm margin-bottom-sm thin-overline padding-top-lg padding-bottom-sm width-90 margin-center {{ lang === 'ar' ? 'rtl' : '' }}">
    <div class="sp-block six {{ lang === 'ar' ? 'six-push text-right' : 'text-left' }} text-left">
        <a class="color-b5b9bc font-13 text-left" target="_blank" href="http://plugins.speakol.com">{{ t._('add-spkl-to-ur-website') }}</a>
    </div>
    <div class="sp-block six {{ lang === 'ar' ? 'six-pull text-left' : 'text-right' }}">
        <a href="http://plugins.speakol.com" target="_blank"><img src="/img/logo-small.png" alt=""></a>
    </div>
</div>
