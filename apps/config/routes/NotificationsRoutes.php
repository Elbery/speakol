<?php
namespace Speakol\Routes;
class NotificationsRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'notifications',));
        $this->setPrefix('/notifications');
        $this->add("/view", array('action' => 'view',))->setName("notifications-view");
        $this->add("/updatebyrecipient", array('action' => 'updatebyrecipient',))->setName("notifications-updatebyrecipient");
        return $this;
    }
}
