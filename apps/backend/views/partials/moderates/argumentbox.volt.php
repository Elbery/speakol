<li class="feed_item_arg">
    <article>

        <div class="feed_item_content">
            <ul class="actions_buttons">
                <li><a class="delete_button" href="#" title="Delete">Delete</a></li>
            </ul>
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>

            <h1 class="item_title">
                <a target="blank" href="<?php echo $item['data']['url']; ?>"><?php echo $item['data']['recomended']['title']; ?></a>
            </h1>
            <div class="user_commented">
                <p>
                    <?php echo $item['data']['description']; ?>
                    <a target="blank" href="<?php echo $item['data']['url']; ?>">read more</a>
                </p>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

            </div>
        </div>
    </article>
</li>

<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'debates/render?app=' . $this->session->get('app_data')->user->app->id . '&debate=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
