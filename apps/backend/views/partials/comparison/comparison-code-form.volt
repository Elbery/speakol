{{ form('comparisons/code', 'enctype': 'multipart/form-data', "class": "js-new-comparison form-horizontal col-sm-11 comparison-form","onsubmit":"return validateForm();") }}
    <input type="hidden" id="data_order" name="data_order" value="1,2"/>
    <div class="form-group">
        {{ textField({'comparison_title', 'placeholder': t._('comprsn_title'), 'class': 'arial-font form-control font-22 weight-bold input-lg no-radius no-padding', "data-validation-engine" :"validate[required]"}) }}
        <a href="#" class="info-tooltip bg-yellow black text-center absolute circle " data-toggle="tooltip" title="{{ t._('exmp_comprsn') }}" data-animation="true" data-placement="top" data-trigger="hover" >?</a>
    </div>
    <div class="form-group margin-bottom-xs">
        <label class="radio-inline {{ lang === 'ar' ? 'rtl' : '' }}" for="horizontalAlign">
            <input id="horizontalAlign" type="radio" name="align" value="0" checked> {{ t._('horizontal-align') }} 
        </label>
        <label class="radio-inline {{ lang === 'ar' ? 'rtl' : '' }}" for="verticalAlign">
            <input id="verticalAlign" type="radio" name="align" value="1"> {{ t._('vertical-align') }} 
        </label>
        <p class="margin-left-20 margin-top-xs color-8a8a8a">{{ t._('horizontal-layout') }}</p>
    </div>
    <div class="form-group margin-top-xs">
            <label for="enableImages" class="h5 checkbox">
                    <input id="enableImages" type="checkbox" name="" checked>{{ t._('enable-images') }}
            </label>
    </div>
    <div class="form-group">
        <ul id="comparison-items" class="no-margin">
            <li class="js-comparison-item">
                <div class="media">
                    <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }} js-dropzone-container relative">
                        <button class="btn-reset js-remove-item absolute top-0 remove-item horizontal color-686a6c opacity-50"><i class="fa fa-times"></i></button>
                        <div class="row text-center cropImage container">
                            <div class="dropzone row bg-white" id="logo_1" name="logo_1" data-width="165" data-height="165" data-url="" data-originalsize="false" data-text="{{ t._('optional') }}">
                                <input type="file" id="file_logo_1" name="file_logo_1" style="">
                            </div>
                            <div class="row error_message margin-bottom-xs margin-top-xs" id="error1"></div>
                            <div class="row dev-tools margin-bottom-xs margin-top-xs"></div>
                            <input id="side_1_logo" name="side_1_logo" type="hidden">
                        </div>
                    </div>
                    <div class="media-body">
                    <div class="row margin-bottom-xs js-title-input six title-input horizontal {{ lang === 'ar' ? 'pull-right rtl' : '' }}">
                        {{ textField( {"side_1_title", 'placeholder': t._('item-name'), 'class': 'jitter-fix arial-font form-control no-radius weight-bold inline-block h4 no-padding title-input', "data-validation-engine": "validate[required]" } )}}
                    </div>
                    <div class="row js-description-input {{ lang === 'ar' ? 'rtl' : '' }}">
                        {{ textarea( {"side_1_description", 'placeholder':t._('description-optional'), "class": "border-none no-radius no-resize h5 description-input-textarea horizontal" } )}}
                    </div>
                    </div>
                </div>
            </li>
            <li class="js-comparison-item">
                <div class="media">
                    <div class="{{ lang=== 'ar' ? 'pull-right' : 'pull-left' }} js-dropzone-container relative">
                        <button class="btn-reset js-remove-item absolute top-0 remove-item horizontal color-686a6c opacity-50"><i class="fa fa-times"></i></button>
                        <div class="row text-center cropImage container">
                            <div class="dropzone row bg-white" id="logo_2" name="logo_2" data-width="165" data-height="165" data-url="" data-originalsize="false"  data-text="{{ t._('optional') }}">
                                <input type="file" id="file_logo_2" name="file_logo_2" style="">
                            </div>
                            <div class="row error_message margin-bottom-xs margin-top-xs" id="error1"></div>
                            <div class="row dev-tools margin-bottom-xs margin-top-xs"></div>
                            <input id="side_2_logo" name="side_2_logo" type="hidden">
                        </div>
                    </div>
                    <div class="media-body">
                    <div class="row margin-bottom-xs js-title-input six horizontal {{ lang==='ar' ? 'pull-right rtl' : '' }}">
                        {{ textField( {"side_2_title", 'placeholder': t._('item-name'), 'class': 'jitter-fix arial-font form-control no-radius weight-bold inline-block h4 no-padding title-input', "data-validation-engine": "validate[required]" } )}}
                    </div>
                    <div class="row js-description-input {{ lang === 'ar' ? 'rtl' : '' }}">
                        {{ textarea( {"side_2_description", 'placeholder': t._('description-optional'), "class": "border-none no-radius no-resize h5 description-input-textarea horizontal" } )}}
                    </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="row js-add-item-container">
            <div class="row text-center container " id="add_more_comparison">
                <div class="add-item js-add-item border-dashed row cursor-pointer bg-white horizontal">
                        <i class="fa fa-2x-sm fa-5x fa-plus color-6dad78"></i>
                        <div class="color-868889 font-16">{{ t._('add-item') }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-2 text-right">
            <label for="comparisonComments" class="h5 margin-right-10">{{ t._('Comments') }}:</label>
        </div>
        <div class="col-sm-10">
            <label for="comparisonComments" class="h5 checkbox">
                    <input id="comparisonComments" type="checkbox" name="hide_comment" > {{ t._('hide-comments') }} 
            </label>
        <p class="margin-top-xs color-8a8a8a">{{ t._('if-enabled') }}</p>
        </div>
    </div>

    <div class="form-group {#lang === "ar" ? "rtl" : ""#}">
        <div class="col-sm-2  text-right {#lang === "ar" ? "pull-right" : ""#}">
            <label for="debtCategory" class="h5 margin-right-10 {#lang === "ar" ? "rtl" : ""#}">{{ t._('cmprsn-crt') }}:</label>
        </div>
        <div class="col-sm-2 width-18 {#lang === "ar" ? "pull-right" : ""#}">
            {{ select("category", categories, 'using': ['id', 'name'], "useEmpty" : true, 'emptyText': t._('choose...'),"id": "debtCategory", 'class': 'form-control no-radius no-padding-height ' ~ (lang === "ar" ? "rtl" : ""), "data-validation-engine" :"validate[required]", 'data-prompt-position': lang === 'ar' ? 'topLeft' : '') }}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-2 col-lg-2 width-18  {# lang ==="ar" ? 'col-sm-offset-6 col-lg-offset-8' : 'col-sm-offset-3 col-lg-offset-2'  #}">
            {{ submitButton({  t._('get-code'), 'class': 'form-control no-radius bg-light-blue h5 padding-top-xs padding-bottom-xs' } )}}
        </div>
    </div>
{{ endform() }}

