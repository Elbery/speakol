<?php
namespace Speakol\Backend\Controllers;
use \Phalcon\Tag as Tag;
class ComparisonsController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('comparisons');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function indexAction() {
        $userSession = $this->getAppData();
        if (is_null($userSession)) {
            return $this->response->redirect('apps/login');
        }
        $header = array('Authorization:' . $this->getAppToken());
        $apiResponse = $this->utility->iCurl('/comparisons', 'get', $header, array());
    }
    public function codeAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->view->successfullyCreated = false;
        $this->tag->appendTitle(' Create Comparison');
        $this->view->comparison_slug = "";
        $userSession = $this->getAppData();
        if (is_null($userSession)) {
            $this->flash->error($this->t->_("login-first"));
            return $this->response->redirect('apps/login');
        }
        $appId = $userSession->id;
        if (empty($appId)) {
            $this->flash->error($this->t->_('crt-app-first'));
            return $this->response->redirect('apps/create');
        }
        $pluginsLimit = $this->checkPluginLimit();
        if ($pluginsLimit['errors']) {
            $this->flash->error($pluginsLimit['errors']);
            return $this->response->redirect('dashboard');
        }
        $this->view->reachedQuota = $pluginsLimit['remainingPlugins'] === 0;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/popups.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/html5imageupload.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/html5imageupload.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/dropzoneFile.js?' . $this->config->application->cache_string);
        $header = array('Authorization:' . $this->getAppToken());
        $result = $this->utility->iCurl('/categories', 'GET', $header, array());
        $categories = $result->messages;
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $this->view->setVar('categories', $categoryList);
        if ($this->request->isPost()) {
            $postFields = array();
            $item_order = $this->request->getPost("data_order");
            $item_order = trim($item_order, ",");
            $item_ids = explode(",", $item_order);
            $items = array();
            for ($j = 0;$j < count($item_ids);$j++) {
                if ($item_ids[$j] != "") {
                    array_push($items, $item_ids[$j]);
                }
            }
            for ($i = 1;$i <= count($items);$i++) {
                $postFields['side_' . $i . '_logo'] = $this->request->getPost('side_' . $items[$i - 1] . '_logo');
                $postFields['sides'][$i - 1]['title'] = $this->request->getPost('side_' . $items[$i - 1] . '_title');
                $postFields['sides'][$i - 1]['description'] = $this->request->getPost('side_' . $items[$i - 1] . '_description');
            }
            $userData = $this->getAppOwnerData();
            $postFields['sides'] = array_values($postFields['sides']);
            $postFields['sides'] = json_encode($postFields['sides']);
            $postFields['title'] = $this->request->getPost('comparison_title');
            $postFields['user_id'] = $userData->id;
            $postFields['category_id'] = $this->request->getPost('category');
            $postFields['app_id'] = $appId;
            $postFields['align'] = $this->request->getPost('align');
            $postFields['hide_comment'] = $this->request->getPost('hide_comment') ? 1 : 0;
            $apiResponse = $this->utility->iCurl('/comparisons/create', 'post', $header, $postFields);
            if (strtoupper($apiResponse->status) == "ERROR") {
                $error = is_array($apiResponse->messages) ? $apiResponse->messages['0'] : $apiResponse->messages;
                $error = $apiResponse->message;
                $this->flash->error($error);
            } else if (strtoupper($apiResponse->status) == "OK") {
                $this->view->setVar('app_id', $userSession->id);
                $this->view->setVar('comparison_slug', $this->config->application->webhost . "comparisons/render?app=" . $userSession->id . "&comparison=" . $apiResponse->data);
                $this->view->successfullyCreated = true;
            }
        }
    }
    public function editAction() {
        $slug = $this->request->get('slug');
        if (!$slug) {
            $this->flash->error('No slug has been given');
            return $this->response->redirect('dashboard');
        }
        $app = $this->getAppData();
        if (!$app) {
            $this->flash->error('You should login first');
            return $this->response->redirect('apps/login');
        }
        if ($app->plan_id && $app->plan_id === 1) {
            return $this->response->redirect('dashboard');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        if ($this->request->isPost()) {
            $sides = $this->request->getPost('sides');
            $sides = array_values($sides);
            $postFields = array('sides' => json_encode($sides), 'slug' => $slug, 'title' => $this->request->getPost('comparison_title'), 'hide_comment' => $this->request->getPost('hide_comment') === 'on' ? 1 : 0, 'align' => $this->request->getPost('align'), 'category_id' => $this->request->getPost('category'),);
            foreach ($sides as $index => $side) {
                if ($side['logo']) {
                    $postFields['side_' . ($index + 1) . '_logo'] = $side['logo'];
                }
            }
            $enableImages = $this->request->getPost('enable_images');
            if ($enableImages === 'on') {
                $postFields['no_images'] = 0;
            } else {
                $postFields['no_images'] = 1;
            }
            $response = $this->utility->iCurl('/comparisons/update', 'POST', $hdrs, $postFields, true);
            if ($response->status !== 'OK') {
                $this->flash->error($response->message);
            } else {
                $this->flash->success($this->t->_('update-succeeded'));
            }
        }
        $params = array('slug' => $slug,);
        $response = $this->utility->iCurl('/comparisons/show/info', 'GET', $hdrs, $params);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('dashboard');
        }
        $result = $this->utility->iCurl('/categories', 'GET', array('Authorization:' . $this->getAppToken()), array());
        $categories = $result->messages;
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        $this->view->setVar('categories', $categoryList);
        $this->view->comparison = $response->data;
        $sides = $this->view->comparison->sides;
        $this->view->no_images = false;
        foreach ($sides as $side) {
            if (!$side->meta[0]->image) {
                $this->view->no_images = true;
            }
        }
        Tag::setDefault('category', $this->view->comparison->category_id);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/popups.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/html5imageupload.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/create_content.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/html5imageupload.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/dropzoneFile.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->view->pick('comparisons/code');
    }
    public function renderAction() {
        $url = $this->request->get('url');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $newsfeed = $this->request->get('newsfeed');
        $newsfeed = $newsfeed === '1' ? true : false;
        $authorized = true;
        $this->view->newsfeed = $newsfeed;
        if ($this->cookies->has(session_name())) {
            $error = $this->session->get('error_socail');
            if ($error) {
                $this->flash->error($error);
                $this->session->remove('error_socail');
            }
        }
        $token = $this->getToken();
        set_exception_handler(array($this, 'exceptionHandler'));
        $this->view->isAppLoggedIn = false;
        if ($this->getAppData()) {
            $this->view->isAppLoggedIn = true;
            $this->view->appData = $this->getAppData();
        }
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        } else {
            $hdrs = array('Authorization:');
        }
        $params = array('app_id' => $this->request->get('app'), 'comparison_slug' => $this->request->get('comparison'), 'token' => $token, 'order_by' => 'most_voted', 'url' => $this->request->get('url'),);
        $objComparison = $this->utility->iCurl('/comparisons/' . $params['comparison_slug'] . '/show', 'get', $hdrs, $params, false);
        if ((is_null($objComparison) || $objComparison->status == 'ERROR') && !$objComparison->unauthorized_domain) {
        }
        if ($objComparison->status === 'ERROR' && $objComparison->unauthorized_domain) {
            $authorized = false;
        }
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->setLang($objUserProfile->data->user->locale);
            }
        } else {
            $objUserProfile = false;
        }
        $redirectUrl = '1';
        $this->view->title = 'Speakol | ' . $objComparison->data->title;
        $this->view->authorized = true;
        if (!$authorized && !$newsfeed) {
            $this->view->authorized = false;
        } else {
            $this->view->comparison = $objComparison->data;
            if (!$this->isUrlHttps($url) && !$newsfeed) {
                $this->view->debate->audio = false;
            }
            $created_at = $this->view->comparison->created_at;
            $updated_at = $this->view->comparison->updated_at;
            $this->view->edited = strtotime($updated_at) > strtotime($created_at);
            $this->view->fburl = $this->utility->getAbsoluteURL('users', 'facebook?r_url=' . $redirectUrl);
            $this->view->twitterurl = $this->utility->getAbsoluteURL('users', 'twitter');
            $this->view->googleurl = $this->utility->getAbsoluteURL('users', 'google?r_url=' . $redirectUrl);
            $this->view->isLoggedIn = $objUserProfile ? true : false;
            $this->view->logout = $objUserProfile ? $this->url->get("users/logout") : '';
            if ($this->getAppData()) {
                $this->view->logout = $this->url->get('apps/logout');
            }
            $this->view->url = urldecode($this->view->comparison->url);
            if ($newsfeed) {
                $this->view->url = urlencode($this->config->application->main_website . 'newsfeed/comparison?slug=' . $this->view->comparison->slug);
            }
            $this->view->LoggedInUserData = !empty($objUserProfile->data) ? $objUserProfile->data->user : false;
            $this->view->lang = $this->request->get('lang') ? $this->request->get('lang') : $this->getLang();
            $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(), 'url' => $this->request->get('url') ? $this->request->get('url') : '',);
        }
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->collection('header')->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->collection('header-ie9')->addJs('http://html5shim.googlecode.com/svn/trunk/html5.js?' . $this->config->application->cache_string);
        $this->assets->collection('footer')->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('vendor/js/mediaelement-and-player.min.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        if ($authorized || $newsfeed) {
            $this->assets->collection('footer')->addJs('vendor/js/ramjet.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('vendor/js/audiorecorder.js?' . $this->config->application->cache_string)->addJs('vendor/js/speexextensions.js?' . $this->config->application->cache_string)->addJs('js/recorder.js?' . $this->config->application->cache_string)->addJs('js/audioplayer.js?' . $this->config->application->cache_string)->addJs('js/comparisonplugin.js?' . $this->config->application->cache_string)->addJs('js/loginbox.js?' . $this->config->application->cache_string)->addJs('js/header.js?' . $this->config->application->cache_string)->addJs('js/arguments.js?' . $this->config->application->cache_string)->addJs('js/replies.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string);
        }
    }
    function exceptionHandler($exception) {
        $this->view->errorMessage = $exception->getMessage();
        var_dump(__FUNCTION__);
        exit(0);
        $this->view->pick("argumentsbox/error");
    }
    public function deleteAction($slug) {
        $this->tag->appendTitle(' Create Comparison');
        $result = $this->utility->iCurl('/comparisons/' . $slug . '/delete', 'delete', array('Authorization:' . $this->getAppToken()), array());
        if ($this->request->isAjax()) {
            if (strtoupper($result->status) == "SUCCESS") {
                $this->response->setJsonContent(array('status' => 'SUCCESS'));
            } else if (strtoupper($result->status) == "ERROR") {
                $this->response->setJsonContent(array('status' => 'ERROR'));
            }
            return $this->response;
        } else {
            if (strtoupper($result->status) == "SUCCESS") {
                $this->flash->success('Content deleted Successfully!');
            } else if (strtoupper($result->status) == "ERROR") {
                $this->flash->error('Some thing went wrong please try again later!');
            }
            return $this->response->redirect('admin/reports');
        }
    }
}
