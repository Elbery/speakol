<?php
namespace Speakol\Backend\Controllers;
use Phalcon\Mvc\Controller, Phalcon\Translate\Adapter\NativeArray;
class BaseController extends Controller implements \Phalcon\DI\InjectionAwareInterface {
    private $_token;
  private $_lang;
    private $_appToken;
    private $_userData;
    private $_appData;
    private $_appOwnerData;
    private $_perPage = 6;
    protected function initialize() {
        $userLang = $this->request->get('lang');
        if ($userLang) {
            if (isset($_COOKIE['lang'])) $_COOKIE['lang'] = $userLang;
            setcookie("lang", $userLang, time() + 3600);
        }
        $this->view->appImage = '';
        $this->tag->prependTitle('Speakol | ');
        $this->loadMainTrans();
        if ($this->cookies->has(session_name())) {
            if ($this->session->get('app_data')) {
                $userSession = $this->session->get('app_data')->user;
                $this->view->admin_mail = ($userSession->email === $this->config->application->admin_email);
                $this->view->isFreePlan = $userSession->app->plan_id === 1;
            }
            if (!empty($userSession) && !empty($userSession->profile_picture)) {
                $this->view->appImage = $userSession->profile_picture;
                $this->view->user = $userSession;
            }
            $this->setDataFromSession();
            $appData = $this->getAppOwnerData();
            if ($appData && $appData->app && !$appData->app->active && !$this->activeStatus()) {
                if ($this->dispatcher->getControllerName() == 'dashboard') {
                    if (!$this->flash->has('success')) {
                        $this->flash->error('You account need to be verified!. You can\'t use system features without account verification!');
                    }
                } else {
                    $arrs = array('comparisons', 'debates', 'argumentsbox', 'apps');
                    $actions = array('render', 'activateaccount');
                    $controller = $this->dispatcher->getControllerName();
                    $action = $this->dispatcher->getActionName();
                    if (in_array($controller, $arrs) && in_array($action, $actions)) {
                    } else {
                        $this->response->redirect('dashboard');
                        return $this->response;
                    }
                }
            }
            if ($appData && $appData->app && $appData->app->active && $this->dispatcher->getControllerName() == 'index') {
                $this->response->redirect('dashboard');
                return $this->response;
            }
        }
        $messages = array();
        $translationPath = $this->_getTransPath();
        require $translationPath . "/js.php";
        $this->view->jsTranslation = json_encode($messages);
    }
    protected function getToken() {
        return $this->_token;
    }
    protected function getAppToken() {
        return $this->_appToken;
    }
    public function getUserData() {
        return $this->_userData;
    }
    public function getAppData() {
        return $this->_appData;
    }
    public function getAppOwnerData() {
        return $this->_appOwnerData;
    }
    public function getPerPage() {
        return $this->_perPage;
    }
    public function setDataFromSession() {
        $userData = $this->session->get('user_data');
        $appData = $this->session->get('app_data');
        $this->_userData = !empty($userData->user) ? $userData->user : false;
        $this->_token = !empty($this->_userData->token) ? $this->_userData->token : false;
        if ($appData) {
            $this->_appOwnerData = !empty($appData->user) ? $appData->user : false;
            $this->_appData = !empty($appData->user) ? $appData->user->app : false;
            $this->_appToken = !empty($appData->user->token) ? $appData->user->token : false;
        }
    }
    public function loadMainTrans() {
        $translationPath = $this->_getTransPath();
        //die($translationPath);
        require $translationPath . "/main.php";
        $mainTranslate = new NativeArray(array("content" => $messages));
        $this->_messages = !empty($this->_messages) ? array_merge($this->_messages, $messages) : $messages;
        $this->t = $mainTranslate;
        $this->view->setVar("t", $mainTranslate);
    }
    public function loadCustomTrans($transFile) {
        $translationPath = $this->_getTransPath();
        require $translationPath . '/' . $transFile . '.php';
        $this->_messages = !empty($this->_messages) ? array_merge($this->_messages, $messages) : $messages;
        $controllerTranslate = new NativeArray(array("content" => $this->_messages));
        $this->t = $controllerTranslate;
        $this->view->setVar("t", $controllerTranslate);
    }
    public function setLang($userLang) {
        $this->_lang = ($userLang && in_array($userLang, array('ar', 'en'))) ? $userLang : $this->session->get("lang");
        $this->session->set('lang', $this->_lang);
    }
    public function getLang() {
        $controllerName = $this->dispatcher->getControllerName();
        $actionName = $this->dispatcher->getActionName();
        $controllers = array('comparisons', 'argumentsbox', 'debates', 'arguments');
        if ((in_array($controllerName, $controllers)) && ($actionName !== "code") && ($this->request->get('lang'))) {
            return $this->request->get('lang');
        }
        $routes = array(array('controller' => 'users', 'action' => 'update'), array('controller' => 'users', 'action' => 'profile'), array('controller' => 'apps', 'action' => 'profile'));
        foreach ($routes as $route) {
            if ($controllerName === $route['controller'] && $actionName === $route['action']) {
                return $this->request->get('lang') ? $this->request->get('lang') : 'en';
            }
        }
        if (in_array($controllerName, array('newsfeed', 'search', 'categories'))) {
            return $this->request->get('lang') ? $this->request->get('lang') : 'en';
        }
        if ($controllerName === "apps" && $actionName === "api") {
            return $this->request->get("lang");
        }
        if ($this->cookies->has(session_name()) && $this->session->get('lang')) {
            return $this->session->get('lang');
        }
        if ($this->cookies->has('lang')) {
            $lang = $_COOKIE['lang'];
            return trim($lang);
        }
        if ($this->request->get('lang')) {
            return $this->request->get('lang');
        }
        return "en";
    }
    protected function _getTransPath() {
        $translationPath = '../apps/backend/messages/';
        $language = $this->getLang();
        $this->view->lang = $language;
        if (!$language && $this->cookies->has(session_name())) {
            $this->session->set("lang", "en");
        }
        if ($language === 'sp' || $language === 'en' || $language === 'ar') {
            return $translationPath . $language;
        } else {
            return $translationPath . 'en';
        }
    }
    public function currentUserIs($roles, $redirectUrl = '/') {
        $arry = array('1' => 'admin', '2' => 'moderator', '3' => 'app', '4' => 'user', '5' => 'guest',);
        foreach ($roles as $role) {
            if (is_string($role) && intval($role) !== $role) {
                if ($check_role = array_search($role, $arry)) {
                    $role = $check_role;
                }
            }
            if (!empty($this->_appOwnerData) && $role == $this->_appOwnerData->role_id) {
                return array($role => $arry[$role]);
            }
        }
        if ($redirectUrl) {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . '/' . $this->url->get($redirectUrl));
            die();
        }
        return false;
    }
    public function redirect($item) {
    }
    public function showErrorMsgs($msgs) {
        $message = $msgs[0];
        $this->flash->error($message->__toString());
        \Phalcon\Tag::setDefault("password", "");
        \Phalcon\Tag::setDefault("confirm_password", "");
    }
    public function getTookTour() {
        $app_data = $this->session->get('app_data');
        return $app_data->user->took_tour;
    }
    public function setTookTour($tookTour) {
        $app_data = $this->session->get('app_data');
        $app_data->user->took_tour = $tookTour;
        $this->session->set('app_data', $app_data);
    }
    public function checkPluginLimit() {
        $app = $this->getAppData();
        if (!$app) {
            $this->flash->error('Invalid Action');
            return $this->response->redirect('apps/login');
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $response = $this->utility->iCurl('/apps/validate/pluginscreation', 'GET', $hdrs, array());
        return array('remainingPlugins' => $response->data->remaining_plugins, 'maxPlugins' => $response->data->max_no, 'errors' => ($response->status !== 'OK') ? $response->messages : false,);
    }
    public function activeStatus() {
        $appData = $this->session->get('app_data');
        if (!$appData) {
            return false;
        }
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $response = $this->utility->iCurl('/user/user_active', 'GET', $hdrs, array(), true);
        if ($response->data && strtolower($response->status) === "success") {
            $appData->user->app->active = 1;
            $this->session->set('app_data', $appData);
            return true;
        } else {
            return false;
        }
    }
    public function getPlugins($params = array()) {
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $hdrs = array('Authorization:xxxxxxxxxxx');
        } else {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        }
        $response = $this->utility->iCurl('/feeds/list', 'GET', $hdrs, $params);
        $plugins = $response->data;
        $plugins = $this->formatPlugins($plugins);
        return $plugins;
    }
    public function formatPlugins($plugins = array()) {
        foreach ($plugins as $plugin) {
            if ($plugin->type === 'argumentboxes') {
                $plugin->articleContent = $this->utility->getArticleContent($plugin->data->url);
            }
        }
        return $plugins;
    }
    public function getHighlightedPlugins($params = array()) {
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        } else {
            $hdrs = array('Authorization:xxxxxxxxxxx');
        }
        $response = $this->utility->iCurl('/feeds/highlighted', 'GET', $hdrs, $params);
        $plugins = $response->data;
        foreach ($plugins as $plugin) {
            if ($plugin->type === 'argumentboxes') {
                $plugin->articleContent = $this->utility->getArticleContent($plugin->data->url);
            }
            $plugin->highlighted = true;
        }
        return $plugins;
    }
    public function getUnseenCategories() {
        if (!$this->cookies->has(session_name())) {
            return array();
        }
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $response = $this->utility->iCurl('/feeds/newplugins', 'GET', $hdrs, array());
        if ($response->status !== 'OK') {
            return array();
        }
        $unseen = $response->data;
        return $unseen;
    }
    public function formatCategories($favorites = array(), $categoryId = '') {
        if (!$favorites) {
            $favorites = array();
        }
        $categories = $this->utility->iCurl('/categories', 'GET', array('Authorization:xxxxxxxxxxx'), array());
        $categories = $categories->messages;
        $hdrs = array('Authorization:');
        $favoriteCategories = array();
        $normalCategories = array();
        $currentCategory = '';
        if ($categoryId) {
            foreach ($categories as $category) {
                if ($category->id == $categoryId) {
                    $currentCategory = $category;
                    break;
                }
            }
        }
        $unseen = $this->getUnseenCategories();
        $categories = array_map(function ($category) use ($unseen) {
            $unseenCategory = array_filter($unseen, function ($c) use ($category) {
                return $c->id == $category->id;
            });
            $unseenCategory = $unseenCategory[0];
            $category->unseen = $unseenCategory && $unseenCategory->count ? $unseenCategory->count : 0;
            return $category;
        }, $categories);
        $favoriteCategories = array_filter($categories, function ($category) use ($favorites) {
            return in_array($category->id, $favorites);
        });
        $normalCategories = array_filter($categories, function ($category) use ($favorites) {
            return !in_array($category->id, $favorites);
        });
        usort($favoriteCategories, function ($prev, $next) {
            $prevString = strtolower($prev->name);
            $nextString = strtolower($next->name);
            return strcmp($prevString, $nextString);
        });
        usort($normalCategories, function ($prev, $next) {
            $prevString = strtolower($prev->name);
            $nextString = strtolower($next->name);
            return strcmp($prevString, $nextString);
        });
        return array('favoriteCategories' => $favoriteCategories, 'normalCategories' => $normalCategories, 'currentCategory' => $currentCategory,);
    }
    protected function sendResponse($response, $referer = false) {
        if (!$referer) {
            $referer = $this->request->getHTTPReferer();
        }
        if (!$this->request->isAjax() && $response['status'] === 'ERROR') {
            if ($response['message']) {
                $this->flash->error($response['message']);
            }
            return $this->response->redirect($referer, false, 301);
        }
        if (!$this->request->isAjax() && $response['status'] === 'OK') {
            $this->flash->success($response['message']);
            return $this->response->redirect($referer, false, 301);
        }
        $this->response->setHeader("Content-Type", "application/json");
        $this->response->setStatusCode($response['code'], $response['status']);
        $this->response->setJsonContent($response);
        return $this->response;
    }
    protected function isUrlHttps($url = '') {
        if (!$url) {
            return false;
        }
        if (preg_match("@^https://@", $url)) {
            return true;
        } else {
            return false;
        }
    }
}
