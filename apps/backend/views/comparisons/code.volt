{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <input type="hidden" name="lang" id="lang" value="{{ lang }}">
    <div class="main pl30  {{ reachedQuota ? 'relative' : ''}}">
        {% if reachedQuota %}
                <input type="hidden" name="" id="reachedQuota">
                <div class="row overlay bg-f6f6f6"></div>
                <div class="row">
                    <div class="col-sm-11  margin-top-sm padding-top-xs padding-bottom-xs padding-left-10 reached-quota bg-f9c6c6 color-9b3535">
                        <div class="pull-left">
                            <i class="speakol-icon speakol-icon-md speakol-times-circle"></i>
                        </div>
                        <div class="media-body font-15 margin-top-xs">
                            <p class="padding-left-10 no-margin">{{ t._('reached-quota') }} <a class="color-9b3535 decoration-underline weight-bold" href="/pricing">{{ t._('upgrade-limit') }}</a></p>
                        </div>
                    </div>
                </div>
        {% endif %}
        {% include "partials/main/page-title" with [ 'title': comparison ? t._('edit-comparison'): t._('crt_comprsn')]  %}
        <div class="row">
            {% if comparison is defined and comparison %} 
                {% include 'partials/comparison/comparison-edit-form.volt' %}
            {% else %}
                {% include 'partials/comparison/comparison-code-form.volt' %}
            {% endif %}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
{% if not comparison %}
  <div class="modal fade" id="get_code" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="raw_code_container">
                                            <div><span>{{ t._('choz-cntnt-lang') }}:</span> 
                                                <select id="arg-box-lang" name="arg-box-lang">
                                                    <option value="en">English</option>
                                                    <option value="ar">اللغة العربية</option>
                                                </select>
                                            </div>
                                                <div class="speakol-debate-box">
<pre id="gnrtd_code"><code>&lt;div class="speakol-comparison" data-lang="en" data-width="600" data-href="{{ comparison_slug }}"&gt;&lt;/div&gt; &lt;script type="text/javascript" &gt; (function(doc, sc, id) { var js, sjs = document.getElementsByTagName(sc)[0]; if(doc.getElementById(id)) return; js = document.createElement(sc); js.src = "{{config.application.sdk_url}}js/sdk.js";js.id=id; sjs.parentNode.insertBefore(js, sjs); })(document, 'script', 'speakol-sdk'); &lt;/script&gt;</code></pre>
                                                </div>
                                            <p>{{ t._('argbox-code') }}</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="form_row">
                                            <!--<span class="alert-success"> Copied </span>-->
                                            <a href="javascript:" class="btn bg-grey preview-btn">{{ t._('preview') }}</a>
                                            <a href="javascript:" class="btn blue_btn cp-generated-code">{{ t._('copy-code')  }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{% if successfullyCreated %}
    <input type="hidden" id="d_code_btn" value="Get Code" data-target="#get_code" data-toggle="modal">
    <script type="text/javascript" src="{{ this.config.application.static_js ~ '/vendor/js/jquery-1.10.1.min.js'}}"></script>
    <script>
        $(document).ready(function (e) {
            $('#d_code_btn').click();
        });
    </script>
{% endif %}
{% endif %}
