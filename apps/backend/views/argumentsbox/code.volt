{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title':  argumentsbox ? t._('edit-argbox') : t._('crt-argbox')  ]  %}
        <div class="row">
            {% if argumentsbox %}
                {{ form(
                    'argumentsbox/edit?url=' ~ argumentsbox.url|url_encode ~ '&uuid=' ~ argumentsbox.code,
                    'class': 'form-horizontal col-sm-11',
                    'enctype': 'multipart/form-data'
                ) }}
                    <input type="hidden" name="url" value="{{ argumentsbox.url }}">
                    {% set title = argumentsbox.title | trim %}
            {% else %}
                <form id="arg-form" class="form-horizontal col-sm-11" action="">
            {% endif %}
               <div class="form-group">
                   <div class="checkbox ">
                        <label for="noTitle" class="font-15 {{ isFreePlan ? 'color-686a6c' : '' }}">
                            <input 
                                id="noTitle"
                                type="checkbox"
                                name=""
                                {{ title === '' or (isFreePlan and title !== '') ? 'checked' : '' }}
                                {{ isFreePlan ? 'disabled' : '' }}
                            > 
                                {{ t._('disable-question')  }}
                        </label>
                   </div>
               </div>
               <div class="form-group">
                   <div class="col-sm-6">
                        <input 
                            id="app_title"
                            class="arial-font form-control font-22 weight-bold input-lg no-radius no-padding"
                            type="text"
                            name="{{argumentsbox ? 'title' : 'app_title' }}"
                            data-validation-engine="validate[required]"
                            placeholder="{{ t._('argbox-question')  }}"
                            value="{{ argumentsbox ? argumentsbox.title : '' }}"
                            {{ title === '' or isFreePlan ? 'disabled' : '' }}
                        >
                        {{ hiddenField({'data-app_id', 'id':'app_id', 'data-app_id': app_id }) }}
                    </div>
                    {% if isFreePlan %}
                    <div class="col-sm-6">
                        <a 
                            class="argumentsbox-upgrade radius-5 inline-block font-16 bg-green anchor-reset white"
                            href="/pricing"
                        >
                            <i class="sp-icon sp-up-arrow"></i>
                            {{ t._('upgrade') }}
                        </a>
                        <span 
                            class="js-argumentsbox-popover js-image-popover margin-left-10 padding-left-5 inline-block color-abacad anchor-reset"
                            data-toggle="popover"
                            data-trigger="hover"
                            data-placement="top"
                            data-image="/img/options-example.png"
                        >
                            <i class="fa fa-exclamation-circle"></i>
                        </span>
                    </div>
                    {% endif %}
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 margin-top-sm h5 {{ isFreePlan ? 'color-686a6c' : '' }}">{{ t._('answer-options') }}:</label>
                    <div class="col-sm-3 width-19 no-padding margin-top-xs">
                        <select
                            id="app_option"
                            name="{{ argumentsbox ? 'option' : '' }}"
                            class="form-control no-radius no-padding padding-left-5 arial-font"
                            {{ isFreePlan ? 'disabled' : '' }}
                        >
                            {% if planID  === 1 %}
                                <option value="1" {{ argumentsbox.option === '1' ? 'selected' : '' }}>{{ t._('I Agree') ~ ' / ' ~ t._('I Disagree') }}</option>
                            {% else %}
                                {% for option in options %}
                                    <option value="{{ option.id }}" {{ argumentsbox.option === option.id ? 'selected' : '' }}>{{ t._(option.option_1) ~ ' / ' ~ t._(option.option_2) }}</option>
                                {% endfor %}
                            {% endif %}
                        </select>
                    </div>
                    {% if isFreePlan %}
                    <div class="col-sm-6">
                        <a 
                            class="margin-left-5 argumentsbox-upgrade radius-5 inline-block font-16 bg-green anchor-reset white"
                            href="/pricing"
                        >
                            <i class="sp-icon sp-up-arrow"></i>
                            {{ t._('upgrade') }}
                        </a>
                        <span 
                            class="js-argumentsbox-popover margin-left-10 padding-left-5 inline-block color-abacad anchor-reset"
                            data-toggle="popover"
                            title="{{ t._('example') }}"
                            data-content="{{ t._('yes') }}/{{ t._('no') }}<br \>{{ t._('good') }}/{{ t._('bad') }}<br \>{{ t._('care') }}/{{ t._('not-care') }}"
                            data-html="true"
                            data-trigger="hover"
                            data-placement="top"
                        >
                            <i class="fa fa-exclamation-circle"></i>
                        </span>
                    </div>
                    {% endif %}
                </div>
                <div class="form-group {{ argumentsbox ? 'hide' : '' }}">
                    <label for="" class="col-sm-3 col-lg-2 margin-top-xs h5 argumentsbox-width">{{ t._('width-optional')  }}:</label>
                    <div class="col-sm-2 col-lg-1 form-inline">
                        {{ textField( {"app_width", "id": "app_width", "class": "form-control no-radius no-padding h5 arial-font","value": "650", "data-validation-engine":"validate[required, custom[integer], min[0]]"} )}}
                    </div>
                    <div class="col-sm-1 col-lg-1 height-all margin-top-xs h5">
                            px
                    </div>
                </div>
                {% if not argumentsbox %}
                <div class="form-group">
                    <label for="multipleUse" class="col-sm-2 margin-top-lg h5 {{ isFreePlan ? 'color-686a6c' : '' }}">{{ t._('multiple-use') }}:</label>
                    <div class="col-sm-3 width-30 no-padding margin-top-xs">
                        <label for="multipleUse" class="font-15 margin-top-xs {{ isFreePlan ? 'color-686a6c' : '' }}">
                            <input 
                                id="multipleUse"
                                type="checkbox"
                                name=""
                                {{ isFreePlan ? 'disabled' : '' }}
                            > 
                                {{ t._('activate-multiple-use')  }}
                        </label>
                    </div>
                    {% if isFreePlan %}
                    <div class="col-sm-6">
                        <a 
                            class="margin-left-5 argumentsbox-upgrade radius-5 inline-block font-16 bg-green anchor-reset white"
                            href="/pricing"
                        >
                            <i class="sp-icon sp-up-arrow"></i>
                            {{ t._('upgrade') }}
                        </a>
                        <span 
                            class="js-argumentsbox-popover margin-left-10 padding-left-5 inline-block color-abacad anchor-reset"
                            data-toggle="popover"
                            data-content="{{ t._('same-different') }}"
                            data-html="true"
                            data-trigger="hover"
                            data-placement="top"
                        >
                            <i class="fa fa-exclamation-circle"></i>
                        </span>
                    </div>
                    {% endif %}
                </div>
                {% endif %}
                <div class="form-group">
                    {% set buttonText = argumentsbox ? t._('update') : t._('get-code') %}
                    {{ submitButton( {buttonText, 'id': argumentsbox ? 'agr_code_btn' : '', 'class': 'btn btn-primary col-sm-2 col-sm-offset-2 col-lg-2 no-radius bg-light-blue font-15 width-19 argumentsbox-submit'   } )}}
                </div>
            </form>
        </div>
        <div class="row margin-top-lg">
            <div class="col-xs-2 col-lg-1 text-center">
                <span class="fa-stack fa-2x">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-info fa-stack-1x white"></i>
                </span>
            </div>
            <div class="col-xs-8 col-lg-9 h5 info-paragraph">
            <p>
                The default argument box title will appear as "What do you think?". This is used to spark a quick and simple debate regarding your article title and content. Readers will vote up or down and add their opinions.
            </p>
                <p>
            The frame pixel width determines how wide the Argument box will be. Your Digital Manager can help you determine this, but please email <a href="mailto:ask@speakol.com">ask@speakol.com</a> if you have any questions or queries.
                </p>
            </div>
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
<div class="modal fade" id="get_code" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="raw_code_container">
                    <div><span>{{ t._('choz-cntnt-lang') }}:</span>
                        <select id="arg-box-lang" name="arg-box-lang">
                            <option value="en">English</option>
                            <option value="ar">اللغة العربية</option>
                        </select>
                    </div>
                    <pre id="gnrtd_code"><code></code>
                    </pre>
                    <p>{{ t._('argbox-code') }}</p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form_row">
                    <!--<span class="alert-success"> Copied </span>-->
                    <a href="javascript:" class="btn bg-grey preview-btn" data-argumentbox="true">{{ t._('preview') }}</a>
                    <a href="javascript:void(0);" class="btn blue_btn cp-generated-code">{{ t._('copy-code') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
