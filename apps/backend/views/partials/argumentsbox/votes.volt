<div class="voting_circle">
    <canvas class="naqeshny-canvas" data-lang="{{ lang  }}" width="80" height="80">
        <div class="data" style="display:none;">
            {% if argumentsbox.votes is defined %}
            {
                "pro-votes": "{{ argumentsbox.votes.proVotes }}",
                "pro-color": "#166e16",
                "con-votes": "{{ argumentsbox.votes.conVotes }}",
                "con-color": "#980a0b"
            }
            {% else %}
            {
                "pro-votes": "0",
                "pro-color": "#166e16",
                "con-votes": "0",
                "con-color": "#980a0b"
            }
            {% endif %}
        </div>
    </canvas>
</div>
<div class="pros_cons_percentbars clearfix">
    {% if argumentsbox.votes.sides is defined %}
        {% for key, side in argumentsbox.votes.sides %}
            {% if loop.first %}
                <div class="pros_percentbar">
                    <span class="percent">{{side.percentage}}</span>
                    <span class="number">  <strong>{{side.votes}}</strong>{{ t._('agrees') }}</span>
                    <div class="pros_percent" style="width: {{side.percentage}};"></div>
                </div>
            {% else %}
                <div class="cons_percentbar">
                    <span class="percent">{{side.percentage}}</span>
                    <span class="number"> <strong>{{side.votes}}</strong> {{ t._('disagrees') }}</span>
                    <div class="cons_percent" style="width: {{side.percentage}};"> </div>
                </div>
            {% endif %}
        {% endfor %}
    {% else %}
    <div class="pros_percentbar">
        <span class="percent">0</span>
        <span class="number"><strong>0</strong> {{ t._('agrees') }}  </span>
        <div class="pros_percent" style="width: 0;"></div>
    </div>
    <div class="cons_percentbar">
        <span class="percent">0</span>
        <span class="number"> <strong>0</strong> {{ t._('disagrees') }}</span>
        <div class="cons_percent" style="width: 0"> </div>
    </div>
    {% endif %}
</div>
