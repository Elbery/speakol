<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Controllers\BaseController;
use Speakol\Backend\Validations as Validations;
use Speakol\Social as Social;
use Speakol\Recaptcha\Recaptcha;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use \Phalcon\Tag as Tag;
class UsersController extends BaseController {
    public function initialize() {
        $this->response->setHeader('Cache-Control', 'no-transform');
        parent::initialize();
        $actions = array('update', 'profile');
        if (!in_array($this->dispatcher->getActionName(), $actions)) {
            $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
            $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
            $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
            $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
            $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
            $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
            $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
            $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        }
        $this->loadCustomTrans('user');
    }
    public function indexAction() {
        $this->tag->appendTitle(' User Dashboard');
        $userData = $this->getUserData();
        $username = $userData->name;
        $this->view->setVar("username", $username);
    }
    public function activateaccountAction($token) {
        if (!$token) {
            $this->repsonse->redirect('');
        }
        $checkVerificationCode = $this->utility->iCurl('/user/confirm_mail/' . $token, 'get', NULL, array(), true, true);
        if ($checkVerificationCode['status'] === 'OK') {
            $this->flash->success('Your account has been activated successfully. You can login to Speakol and contribute to the discussions');
            if ($this->request->get('redirect_url')) {
                $this->view->redirectUrl = $this->request->get('redirect_url');
            }
            $this->view->pick('partials/user/redirection');
        } else {
            $err = is_array($checkVerificationCode['messages']) ? $checkVerificationCode['messages'][0] : $checkVerificationCode['messages'];
            $this->flash->error($err);
            return $this->response->redirect('');
        }
    }
    public function registerAction() {
        $userSession = $this->getUserData();
        if (!empty($userSession)) {
            $this->flash->error('You already got an account');
            return $this->response->redirect('');
        }
        $yourPrivateKey = $this->config->application->recaptcha->private_key;
        $recaptcha = Recaptcha::check($yourPrivateKey, $_SERVER['REMOTE_ADDR'], "", $this->request->getPost('g-recaptcha-response'));
        if ($this->request->isPost() && !$recaptcha) {
            $this->flash->error('Recaptcha code is invalid');
            return $this->response->redirect('users/register');
        }
        if ($this->request->isPost() && $recaptcha) {
            $postData = array('first_name' => $this->request->getPost('first_name'), 'last_name' => $this->request->getPost('last_name'), 'email' => $this->request->getPost('email'), 'password' => $this->request->getPost('password'), 'confirm_password' => $this->request->getPost('confirm_password'), 'gender' => $this->request->getPost('gender'), 'redirect_url' => $this->config->application->webhost . 'users/activateaccount/', 'image' => $this->request->getPost('picture'),);
            if ($this->request->get('redirect_url')) {
                $postData['plugin_url'] = $this->request->get('redirect_url');
            }
            $userValidator = new Validations\UsersValidation(array('name', 'email', 'password', 'confirm_password', 'gender'));
            $userMsgs = $userValidator->validate($postData);
            if (count($userMsgs)) {
                $this->showErrorMsgs($userMsgs);
            } else {
                unset($postData['confirm_password']);
                $response = $this->utility->iCurl('/user/create/account', 'POST', array('Authorization:xxxxxxxxxxx'), $postData, false, false, true);
                if ($response->status === 'OK') {
                    $this->view->refresh = false;
                    $this->view->redirectUrl = $this->config->application->webhost . 'users/login';
                    $this->flash->success('An Email has been sent to verify your account, you may now close this window');
                    $this->assets->addJs('js/login.js?' . $this->config->application->cache_string);
                    $this->view->pick('users/socialLogin');
                    return;
                } else {
                    \Phalcon\Tag::setDefault("password", "");
                    \Phalcon\Tag::setDefault("confirm_password", "");
                    $this->flash->error($response->message);
                }
            }
        }
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/messenger.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/login.js?' . $this->config->application->cache_string);
        $publicKey = $this->config->application->recaptcha->public_key;
        $this->view->publicKey = $publicKey;
        $this->tag->appendTitle(' Register new account');
    }
    public function loginAction() {
        $this->tag->appendTitle(' User login');
        if ($this->cookies->has(session_name())) {
            $userSession = $this->session->get('user_data');
            if (!empty($userSession)) {
                $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                $this->flash->error('You are already logged in');
                $this->view->refresh = true;
                $this->view->redirectUrl = $this->config->application->webhost;
                $this->view->pick('users/socialLogin');
                return;
            }
        }
        if ($this->request->isPost()) {
            $params = array('email' => $this->request->getPost('email'), 'password' => $this->request->getPost('password'), 'role' => 4);
            $userValidator = new Validations\UsersValidation(array('email', 'password'));
            $userMsgs = $userValidator->validate($params);
            if (count($userMsgs)) {
                $this->showErrorMsgs($userMsgs);
            } else {
                $response = $this->utility->iCurl('/user/signin', 'POST', NULL, $params, false, false, true);
                if ($response->status !== 'OK') {
                    $this->flash->error($response->message);
                    return $this->response->redirect('users/login');
                }
                $this->session->remove('app_data');
                if ($response->data->user->app) {
                    $this->session->set('app_data', $response->data);
                } else {
                    $this->session->set('user_data', $response->data);
                }
                $this->setLang($response->data->user->locale);
                $this->session->set('access_token', $response->data->user->token);
                $this->view->refresh = true;
                $this->view->redirectUrl = $this->config->application->webhost;
                $this->assets->addJs('js/login.js?' . $this->config->application->cache_string);
                $this->view->pick('users/socialLogin');
                return;
            }
        }
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/messenger.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/login.js?' . $this->config->application->cache_string);
        $this->view->fbUrl = $this->url->get('users/facebook');
        $this->view->googleUrl = $this->url->get('users/google');
    }
    public function logoutAction() {
        $token = $this->getToken();
        $appToken = $this->getAppToken();
        if ($appToken) {
            $this->utility->iCurl('/user/signout', 'delete', array('Authorization:' . $appToken), NULL);
        }
        if ($token) {
            $this->utility->iCurl('/user/signout', 'delete', array('Authorization:' . $token), NULL);
        }
        $this->session->remove('user_data');
        $this->session->remove('app_data');
        $this->session->destroy();
        $params = session_get_cookie_params();
        setcookie(session_name(), '', 0, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
        if ($this->request->isAjax()) {
            $this->response->setJsonContent(array('status' => 'OK', 'data' => true));
            return $this->response;
        } else {
            return $this->response->redirect($this->request->getHTTPReferer(), false, 301);
        }
    }
    public function tooktourAction() {
        $userSession = $this->getUserData();
        if (empty($userSession)) {
            $this->response->setStatusCode("401", "Unauthorized");
            $this->response->setJsonContent(array("status" => "ERROR", "messages" => "Unauthorized"));
        }
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $data = $this->utility->iCurl('/user/took-tour', 'put', $hdrs, array(), FALSE, false);
        if ($data->code === 200) {
            $this->setTookTour(1);
            $this->response->setStatusCode($data->code, "OK");
            $this->response->setJsonContent(array("status" => $data->code, "data" => $data->data));
        } else {
            $this->response->setStatusCode($data->code, "ERROR");
            $this->response->setJsonContent(array("status" => $data->code, "messages" => $data->messages));
        }
        return $this->response;
    }
    public function unsubscribeAction() {
        $email = $this->request->get('email');
        if (!$email) {
            return $this->response->redirect('/');
        }
        $params = array('email' => $email,);
        $response = $this->utility->iCurl('/user/unsubscribe', 'POST', array(), $params, true, false, true);
        if ($response->status === "OK") {
            $this->view->pick("users/unsubscribe-success");
        } else {
            $this->view->message = $response->message;
            $this->view->pick("users/unsubscribe-error");
        }
    }
    public function checkonlineusersAction() {
        $params = array('sides' => $this->request->get('sides'), 'argument_id' => $this->request->get('argument_id'), 'page' => $this->request->get('page'), 'limit' => $this->request->get('limit'), 'order_by' => $this->request->get('order_by'),);
        $data = $this->utility->iCurl('/user/users_status', 'GET', array('Authorization:xxxxxxxxxxx'), $params, FALSE, false);
        if ($data->code === 200) {
            $this->response->setStatusCode($data->code, "OK");
            $this->response->setJsonContent(array("status" => $data->code, "data" => $data->data));
        } else {
            $this->response->setStatusCode($data->code, "ERROR");
            $this->response->setJsonContent(array("status" => $data->code, "messages" => $data->messages));
        }
        return $this->response;
    }
    public function connectAction($provider_type) {
        if (!$provider_type) {
            $this->response->redirect('');
            return;
        }
        $userSession = $this->getUserData();
        if (!empty($userSession)) {
            $this->flash->error($this->t->_('alrdy-logged'));
            $this->view->refresh = true;
            $this->view->pick('user/socialLogin');
            return;
        }
        $socialProvider = Social\SocialProvidersFactory::create($provider_type);
        $code = $this->request->get('code');
        $redirectURI = $this->utility->getAbsoluteURL('users/connect', $provider_type);
        if (!$code) {
            $loginURL = $socialProvider->getLoginURL($redirectURI);
            $this->response->redirect($loginURL, true);
            return;
        }
        $state_code_a = $this->request->get('state_code_a');
        $accessToken = $socialProvider->getAccessToken($code, $state_code_a, $redirectURI);
        $params = array('access_token' => $accessToken, 'connect_type' => $provider_type,);
        $response = $this->utility->iCurl('/user/social/connect', 'POST', NULL, $params, true, false, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            $this->view->refresh = false;
            $this->view->pick('users/socialLogin');
            return;
        }
        $this->session->remove('app_data');
        if ($response->data->user->app) {
            $this->session->set('app_data', $response->data);
        } else {
            $this->session->set('user_data', $response->data);
        }
        $this->setLang($response->data->user->locale);
        $this->session->set('access_token', $response->data->user->token);
        $this->view->refresh = true;
        $this->view->pick('users/socialLogin');
    }
    public function setpasswordAction() {
        if ($this->request->isPost()) {
            $accessToken = $this->session->get('social_token');
            if (!$accessToken) {
                $this->flash->error('Unauthorized');
            } else {
                $params = array('password' => $this->request->getPost('password'), 'confirm_password' => $this->request->getPost('confirm_password'),);
                $userValidator = new Validations\UsersValidation(array('password', 'confirm_password'));
                $userMsgs = $userValidator->validate($params);
                if (count($userMsgs)) {
                    $this->showErrorMsgs($userMsgs);
                } else {
                    $params = array('access_token' => $accessToken, 'social' => 1, 'password' => $this->request->getPost('password'),);
                    $response = $this->utility->iCurl('/user/social/setpassword', 'POST', NULL, $params, false, false, true);
                    if ($response->status === 'OK') {
                        $this->session->remove('app_data');
                        $this->session->remove('social_token');
                        $this->session->set('user_data', $response->data);
                        $this->session->set('access_token', $response->data->user->token);
                        $this->view->refresh = true;
                        $this->view->pick('users/socialLogin');
                        return;
                    } else {
                        $this->flash->error($response->message);
                    }
                }
            }
        }
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main_site.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
    }
    public function forgetpasswordAction() {
        $this->tag->appendTitle(' | Forget Password');
        if ($this->request->isPost()) {
            $data = array("email" => $this->request->getPost('email'),);
            try {
                $userValidator = new Validations\UsersValidation(array("email"));
                $userMsgs = $userValidator->validate($data);
                if (count($userMsgs)) {
                    $message = $userMsgs[0]->__toString();
                    $this->flash->error($message);
                    return $this->response->redirect('users/login');
                } else {
                    $postData = array('redirect_url' => $this->config->application->webhost . 'apps/resetpassword/');
                    $forgetPassword = $this->utility->iCurl('/user/forgot_password/' . $data['email'], 'get', NULL, $postData, true, false);
                    if ($checkVerificationCode['status'] = 'OK') {
                        $this->flash->success($this->t->_('user-forget-password-success'));
                        return $this->response->redirect('users/login');
                    } else {
                        throw new Exception($checkVerificationCode['messages']);
                    }
                }
            }
            catch(\Exception $ex) {
                $message = $ex->getMessage();
                $this->flash->error($message);
            }
        }
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/login.js?' . $this->config->application->cache_string);
    }
    public function profileAction($userId) {
        if (!$userId || !ctype_digit($userId)) {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => 'Wrong Profile', 'code' => 400), 'newsfeed');
        }
        $this->loadCustomTrans('newsfeed');
        $this->loadCustomTrans('arguments');
        $this->view->communityPage = true;
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $type = $this->request->get('type');
        $page = $this->request->get('page');
        $lastCreatedDate = $this->request->get('last_date');
        $page = $page ? $page : 1;
        $type = $type ? $type : 'all';
        $limit = 10;
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $hdrs = array('Authorization:xxxxxxxxxxxxxx');
            $this->view->isLoggedIn = false;
        } else {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $this->view->isLoggedIn = true;
        }
        $params = array('page' => $page, 'limit' => $limit, 'type' => $type,);
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        $response = $this->utility->iCurl('/feeds/user/profile/' . $userId, 'GET', $hdrs, $params);
        if ($response && $response->status !== 'OK') {
            return $this->sendResponse(array('status' => 'ERROR', 'message' => '', 'code' => 401), 'newsfeed');
        }
        $userInfo = $response->data->info;
        $plugins = $response->data->info->plugins;
        $plugins = $this->formatPlugins($plugins);
        $this->view->userId = $userId;
        $this->view->plugins = $plugins;
        $this->view->userName = $response->data->name;
        $this->view->userImage = $response->data->image;
        $this->view->userProfile = true;
        $lastCreatedDate = $plugins[count($plugins) - 1]->created_at;
        $lastCreatedDate = str_replace(' ', '+', $lastCreatedDate);
        $url = $this->request->get('_url');
        $url.= '?page=' . ($page);
        $nextUrl.= '?page=' . ($page + 1);
        if ($type !== 'all') {
            $url.= '&type=' . $type;
            $nextUrl.= '&type=' . $type;
        }
        $url.= '&last_date=' . $lastCreatedDate;
        $nextUrl.= '&last_date=' . $lastCreatedDate;
        $url.= '&lang=' . $this->view->lang;
        $nextUrl.= '&lang=' . $this->view->lang;
        $this->view->url = $url;
        $this->view->nextUrl = $nextUrl;
        $this->view->type = $type;
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->view->superAdmin = $this->view->LoggedInUserData->role_id === 2;
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites;
            }
        } else {
            $objUserProfile = false;
        }
        $this->view->paginate = false;
        if ($this->request->isAjax()) {
            $this->view->highlightedPlugins = array();
            $this->view->paginate = true;
            $this->view->pick('partials/newsfeed/feed');
            return;
        }
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        $favorties = array();
        $categoriesData = $this->formatCategories($favorites);
        $this->view->userProfile = $plugins[0]->app;
        $this->view->userInteractions = $userInfo;
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->view->highlightedPlugins = array();
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        if ($this->view->superAdmin) {
            $this->assets->addJs('js/highlight.js?' . $this->config->application->cache_string);
        }
        $this->view->pick('users/profile');
    }
    public function updateAction() {
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken())) {
            return $this->response->redirect('newsfeed');
        }
        $this->loadCustomTrans('newsfeed');
        $this->view->url = $this->request->get('_url');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->isLoggedIn = true;
        if ($this->session->get('app_data')) {
            return $this->response->redirect('apps/settings');
        }
        $hdrs = $this->utility->getAuthorizationHeaders();
        if ($this->request->isPost()) {
            $params = array('first_name' => $this->request->get('first_name'), 'last_name' => $this->request->get('last_name'), 'newsletter' => $this->request->get('newsletter'), 'subscribe' => $this->request->get('subscription') === 'on' ? '1' : '0', 'url' => $this->config->application->webhost . 'users/verifyupdate',);
            $oldPassword = '';
            if ($this->request->get('current_password') || $this->request->get('password') || $this->request->get('confirm_password')) {
                $params['old_password'] = $this->request->get('current_password');
                $params['new_password'] = $this->request->get('password');
                $params['confirm_password'] = $this->request->get('confirm_password');
            }
            $confirmEmail = false;
            if ($this->request->get('email') || $this->request->get('confim_email')) {
                $confirmEmail = true;
                $params['email'] = $this->request->get('email');
                $params['confirm_email'] = $this->request->get('confirm_email');
            }
            $userValidator = new Validations\UsersEditValidation(array_keys($params));
            $userMsgs = $userValidator->validate($params);
            if ($this->request->get('image')) {
                $params['image'] = $this->request->get('image');
            }
            if (count($userMsgs)) {
                $this->showErrorMsgs($userMsgs);
            } else {
                unset($params['confirm_password']);
                unset($params['confirm_email']);
                $response = $this->utility->iCurl('/user/profile/edit', 'POST', $hdrs, $params, false, false, true);
                if ($response->status !== 'OK') {
                    $this->flash->error($response->message);
                } elseif ($confirmEmail) {
                    $this->flash->notice($this->t->_('email-confirmation-dialog'));
                } else {
                    $this->flash->success($this->t->_('user-profile-updated'));
                }
            }
            \Phalcon\Tag::setDefault("current_password", "");
            \Phalcon\Tag::setDefault("password", "");
            \Phalcon\Tag::setDefault("confirm_password", "");
        }
        $favorites = array();
        if ($this->cookies->has(session_name())) {
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending) {
                    $this->view->pending = true;
                    $this->flash->notice($this->t->_('email-confirmation-dialog'));
                }
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites ? $objUserProfile->data->favorites : array();
            }
        }
        $categoriesData = $this->formatCategories($favorites);
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $response = $this->utility->iCurl('/user/profile/show', 'GET', $hdrs, array(), true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('newsfeed');
        }
        $this->view->user = $response->data;
        $this->view->userEditProfile = true;
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        $this->view->pick('users/update');
    }
    public function verifyupdateAction($token) {
        $this->loadCustomTrans('newsfeed');
        if (!$token) {
            $this->flash->error('Invalid Action');
            return $this->response->redirect('/user/update');
        }
        $params = array('token' => $token,);
        $response = $this->utility->iCurl('/user/profile/email/activate', 'POST', array('Authorization:xxxxxxxxx'), $params, false, false, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
        } else {
            $this->flash->success($this->t->_('email-confirmation-updated'));
        }
        return $this->response->redirect('users/update');
    }
}
