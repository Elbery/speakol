<div class="story_container register">
    <div class="top_bar">
        <span>{{ t._('create-new-account') }}</span>
        <a class="link_btn" href="/user/login"> {{ t._('login-now') }}</a>
    </div>
    <div class="login_container">
        <h2>{{ t._('create-new-account') }}</h2>
        <div class="login_form">
            {{ form( 'user/register', 'method': 'post', 'enctype': 'multipart/form-data' ,'class':'ajaxfiy-me') }}
            <div class="form_row">
                <div class="input-group">
                    <span class="input-group-addon name_ico"></span>
                    {{ textField({"name", "placeholder": t._('name'), "class": "form-control"}) }}
                </div>
            </div>
            {% if not social_media %}
            <div class="form_row">
                <div class="input-group">
                    <span class="input-group-addon email_ico"></span>
                    {{ textField({"email", "placeholder": t._('emailaddress'), "class": "form-control"}) }}
                </div>
            </div>
            <div class="form_row">
                <div class="input-group">
                    <span class="input-group-addon email_ico"></span>
                    {{ textField({"confirm_email", "placeholder": t._('confirm-email'), "class": "form-control"}) }}
                </div>
            </div>
            
            <div class="form_row">
                <div class="input-group">
                    <span class="input-group-addon password_ico"></span>
                    {{ passwordField({"password", "placeholder": t._('password'), "class": "form-control"}) }}
                </div>
            </div>
            <div class="form_row">
                <div class="input-group">
                    <span class="input-group-addon password_ico"></span>
                    {{ passwordField({"confirm_password", "placeholder": t._('confirm-password'), "class": "form-control"}) }}
                </div>
            </div>
            {% endif %}
            <div class="form_row full mt">
                <div class="input-group">
                    <span class="input-group-addon country_ico"></span>
                    {{ select("country", countries, 'using': ['id', 'name'], "useEmpty" : true, 'class': 'form-control') }}
                </div>
            </div>
            <div class="form_row full mt">
                <div class="input-group">
                    <span class="input-group-addon occupation_ico"></span>
                    {{ textField({"occupation", "placeholder": t._('occupation'), "class": "form-control"}) }}
                </div>
            </div>
            <div class="form_row full">
                <div class="input-group">
                    <span class="input-group-addon Birthdate_ico"></span>
                    {{ dateField({"birthdate", "placeholder": t._('birthdate'), "class": "form-control"}) }}
                </div>
            </div>
                
            {% if social_media %}
                {% include "partials/user/social.volt" %}
            {% endif  %}
                
            <div class="form_row">
                {{ submit_button('Create account') }}
            </div>
            {{ endForm() }}    
        </div>
        <div class="social_connect">
            <a class="fb_login" href="{{ fbUrl }}"> {{ t._('login-with-facebook') }}</a>
            <a class="ggl_login" href="{{ googleUrl }}"> {{ t._('login-with-google') }}</a>
        </div>
    </div>
</div>
