/**
 * Pagination
 * @param {type} options
 * @returns {admin_L2.$.fn@call;each}
 */

(function ($) {
    
    $.fn.pagination = function (options) {

        var s = $.extend({
            parentClass:    "",
            ajaxUrl:        "",
            pageAttr:       "data-page",
            methodParams:   [],
            beforeXHR:      function() { return true; },
            beforeSuccess:   function() { return true; },
            afterSuccess:   function() { return true; },
        }, options);

        var _this = this;

        _this.init = function () {
            _this.click(_this.handleClick);
        };
        
        _this.handleClick = function (e) {

            e.preventDefault();

            var page = _this.attr('data-page'), nextPage = 1;
            page = parseInt(page) < 2 ? 2 : parseInt(page);
            nextPage = page + 1;
            _this.addLoadingIcon();
            
            viewMore = true;
            
            var params = {'page': page};
            
            var ret = s.beforeXHR(_this,params);
            
            if( ret && typeof ret == 'object' ){
                params = ret;
            }
            
            $.ajax({
                url: s.ajaxUrl,
                data: params,
                success: function (data) {
                    
                    _this.removeLoadingIcon();

                    if ($.trim(data) && $(s.parentClass).length) {
                        
                        var ret = s.beforeSuccess(data);
                        
                        if(ret){

                            $(s.parentClass).append(data);

                            _this.attr(s.pageAttr, nextPage);
                        
                            if (!viewMore) {
                                _this.remove();
                            }
                            s.methodParams.push(data);
                            s.afterSuccess.apply(this, s.methodParams);
                            
                        }

                    } else {
                        _this.remove();
                    }
                }

            });
        };

        _this.addLoadingIcon = function () {

            var loadingImage = '<span class="load-add glyphicon glyphicon-refresh"></span>';

            _this.parent().find('.load-add').remove();
            _this.after(loadingImage);

        };

        _this.removeLoadingIcon = function () {

            _this.parent().find('.load-add').remove();

        };
        
        this.init();
        
        var outside = {
            
            setParams: function(key,value){
                s[key] = value;
            },
            
            getParams: function(key){
                return s && s[key] ? s[key] : false;
            }
        };
        
        return outside;

    };
}(jQuery));