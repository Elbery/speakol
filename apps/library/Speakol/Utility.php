<?php
namespace Speakol\Library;
class Utility extends \Phalcon\Mvc\User\Plugin {
    protected $_module;
    public function renderEmailContent($_this, array $renderFile, array $options) {
        $_this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $_this->assets->addCss('css/email.css?5750470504');
        $view = clone $_this->view;
        $view->start();
        foreach ($options as $key => $value) {
            $view->setVar($key, $value);
        }
        $view->render($renderFile[0], $renderFile[1]);
        $view->finish();
        return $view->getContent();
    }
    public function time_elapsed_string($ptime) {
        $etime = time() - $ptime;
        if ($etime < 1) {
            return '0 seconds';
        }
        $a = array(12 * 30 * 24 * 60 * 60 => 'year', 30 * 24 * 60 * 60 => 'month', 24 * 60 * 60 => 'day', 60 * 60 => 'hour', 60 => 'minute', 1 => 'second');
        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            }
        }
    }
    public function __construct($_module) {
        $this->_module = $_module;
        set_exception_handler(array($this, 'exceptionHandler'));
    }
    public function iCurl($_strQueryString, $_strMethod, $_arrHeaders, $_arrParams, $debug = false, $_boolThrowEx = false, $json = false) {
        $baseController = new \Speakol\Backend\Controllers\BaseController();
        $lang = $language = $baseController->getLang();
        $_arrParams['lang'] = $lang && in_array($lang, array('ar', 'en')) ? $lang : 'en';
        if (is_null($_arrHeaders)) {
            $_arrHeaders = array();
        }
        if ($json) {
            array_push($_arrHeaders, 'Content-Type: application/json');
        }
        array_push($_arrHeaders, 'Ip: ' . $_SERVER['REMOTE_ADDR']);
        $ch = curl_init();
        if (!empty($_arrHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $_arrHeaders);
        }
        $strURL = '';
        $_strMethod = strtoupper($_strMethod);
        switch ($_strMethod) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, TRUE);
                $params = ($json) ? json_encode($_arrParams) : $_arrParams;
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $strURL = $this->prepareQueryString($_strQueryString, NULL);
            break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_PUT, TRUE);
                $strURL = $this->prepareQueryString($_strQueryString, $_arrParams);
            break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                $params = ($json) ? json_encode($_arrParams) : $_arrParams;
                if ($json) {
                    $params = json_encode($_arrParams);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                    $strURL = $this->prepareQueryString($_strQueryString, NULL);
                } else {
                    $strURL = $this->prepareQueryString($_strQueryString, $_arrParams);
                }
            break;
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                $strURL = $this->prepareQueryString($_strQueryString, $_arrParams);
            default:
            break;
        }
        curl_setopt($ch, CURLOPT_URL, $strURL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!$result = curl_exec($ch)) {
            @trigger_error(curl_error($ch));
        }
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $objJSON = json_decode(trim($result), $_boolThrowEx);
        $debug = $debug ? $debug : array();
        if ($objJSON) {
            if (is_array($_boolThrowEx)) {
                $objJSON = $objJSON['code'] = $http_status;
                if ($objJSON['debug']) {
                    $debug[] = $objJSON['debug'];
                }
            } else if (is_object($objJSON)) {
                $objJSON->code = $http_status;
                if ($objJSON->debug) {
                    $debug[] = get_object_vars($objJSON->debug);
                }
            }
        }
        return $objJSON;
    }
    protected function prepareQueryString($_strQueryString, $_arrParams) {
        $_strURL = $this->config->application->webservice;
        if (!is_null($_strQueryString)) {
            $_strURL.= $_strQueryString;
        }
        if (!is_null($_arrParams)) {
            $strParams = '';
            $counter = count($_arrParams);
            foreach ($_arrParams as $key => $value) {
                if (!is_array($value)) {
                    $strParams.= $key;
                    $strParams.= "=";
                    $strParams.= urlencode($value);
                    if ($counter != 1) {
                        $strParams.= "&";
                    }
                } else {
                    foreach ($value as $i) {
                        $strParams.= $key . "[]";
                        $strParams.= "=";
                        $strParams.= urlencode($i);
                        if ($counter != 1) {
                            $strParams.= "&";
                        }
                    }
                }
                $counter--;
            }
            $_strURL.= '?' . $strParams;
        }
        return $_strURL;
    }
    public function getLoggedInUser() {
        return \Phalcon\DI\FactoryDefault::getDefault()->getShared('session')->get('user_data');
    }
    public function getRequestURL() {
        $crumbs = explode('&', $_SERVER['QUERY_STRING']);
        $cleanURL = '';
        $params = '';
        foreach ($crumbs as $key => $value) {
            if (strstr($value, '_url')) {
                $cleanURL = substr($value, strpos($value, '=') + 1);
                continue;
            }
            $params.= $value;
            $params.= '&';
        }
        return urlencode(((empty($_SERVER['HTTPS'])) ? 'http://' : 'https://') . $_SERVER['SERVER_NAME'] . '/' . $cleanURL . '?' . $params);
    }
    public function getAbsoluteURL($_controller, $_action) {
        return 'https://' . $_SERVER['HTTP_HOST'] . '/' . $_controller . '/' . $_action;
    }
    public function getAuthorizationHeaders() {
        $token = $this->session->get('user_data');
        $token = !empty($token->user->token) ? $token->user->token : false;
        return array('Authorization:' . (($token == NULL || $token == "") ? "" : $token),);
    }
    public function getAuthorizationHeadersForApp() {
        $token = $this->session->get('app_data');
        $token = !empty($token->user->token) ? $token->user->token : false;
        return array('Authorization:' . (($token == NULL || $token == "") ? "" : $token),);
    }
    public function prepareHeadersForJS($_arrHdrs) {
        $arrHdrs = array();
        foreach ($_arrHdrs as $string) {
            $key = substr($string, 0, strpos($string, ':'));
            $value = substr($string, strpos($string, ':') + 1);
            $arrHdrs["{$key}"] = $value;
        }
        return json_encode($arrHdrs);
    }
    public function getSignedInToken() {
        return $this->session->get('access_token');
    }
    public function throwCustomException($_messages) {
        $ex = '';
        if (isset($_messages)) {
            if (is_array($_messages)) {
                $ex = implode(' <br /> ', $_messages);
            } else {
                $ex = $_messages;
            }
        } else {
            $ex = 'ERROR: Unknown Exception';
        }
        throw new \Exception($ex, 0);
    }
    function exceptionHandler($exception) {
        $this->view->errorMessage = $exception->getMessage();
        $this->view->pick("argumentsbox/error");
    }
    public function getCurlValue($file, $audio = false) {
        $name = $audio ? date('Y-m-d-G-i-s') . '.ogg' : $file->getName();
        $type = $audio ? "audio/ogg" : $file->getType();
        if (function_exists('curl_file_create')) {
            return curl_file_create($file->getTempName(), $type, $name);
        }
        $value = "@" . $file->getTempName() . ";filename=" . $name;
        if ($type) {
            $value.= ';type=' . $type;
        }
        return $value;
    }
    public function insertHashtagHTML($text) {
        return preg_replace("/#([A-Za-z\p{Arabic}][A-Za-z0-9\p{Arabic}\_]*)/u", "<span class=\"light-blue\">#$1</span>", $text);
    }
    public function renderTemplate($mainFolder, $path, $params) {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->start();
        $this->view->setVars($params);
        $this->view->render($mainFolder, $path);
        $this->view->finish();
        $content = $this->view->getContent();
        return $content;
    }
    public function formatDate($date, $format = 'jS F, Y') {
        $date = date_create($date);
        return date_format($date, $format);
    }
    public function isFullURL($url) {
        $hasHTTP = strpos($url, 'http://') !== false;
        $hasHTTPS = strpos($url, 'https://') !== false;
        $hasFILE = strpos($url, 'file://') !== false;
        return $hasHTTP || $hasHTTPS || $hasFILE;
    }
    public function getDebateDuration($value = 0) {
        if (!$value) {
            return array('number' => 0, 'format' => 'days',);
        }
        if ($value % 365 === 0) {
            return array('number' => $value / 365, 'format' => 'years',);
        }
        if ($value % 30 === 0) {
            return array('number' => $value / 30, 'format' => 'months',);
        }
        return array('number' => $value, 'format' => 'days',);
    }
    public function getArticleContent($url = '') {
        return;
    }
    private function extractContent($dom, $length = 500) {
        $paragraphs = $dom->getElementsByTagName('p');
        $content = '';
        $contentArray = array();
        foreach ($paragraphs as $paragraph) {
            if (strlen($content) <= $length) {
                $content.= $paragraph->nodeValue;
                $contentArray[] = substr($paragraph->nodeValue, 0, $length) . '...';
            }
        }
        return $contentArray;
    }
    private function extractTitle($dom, $tag = 'h1') {
        if (!$tag) {
            return '';
        }
        $tags = $dom->getElementsByTagName($tag);
        if (!$tags->length) {
            return "";
        }
        return $tags->item(0)->nodeValue;
    }
}
