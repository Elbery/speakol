{% if arguments.args is defined %}
    {% for argument in arguments.args %}
        <li>
            {% include "partials/argumentsbox/argument" with ['argument': argument, 'argumentsBoxURL': argumentsBoxURL] %}
        </li>
    {% endfor %}
{% endif %}
