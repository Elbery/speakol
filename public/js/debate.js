$(document).ready(function () {
    //Add Cover popup (google search)
    //================================
    $(".Add_cover").click(function () {
        $(this).next(".dialog_container").fadeIn("fast");
    });
    $(".dialog_container").click(function () {
        $(this).fadeOut("fast");
    });
    $('.dialog_box').click(function (e) {
        e.stopPropagation();
    });

    $('#endDate').on('change', function() {
        var selectedValue = $('option:selected', this).val();
        var otherDate = $("#otherDate");
        if(selectedValue !== "other") {
            $("#max_days").val(selectedValue);
            if(!isContainerHidden()) {
                otherDate.addClass('hide');
            }
        } else {
            $("#max_days").val("");
            otherDate.removeClass('hide');
        }
    });

    $("#otherDate select").on('change', function() {
        if(isContainerHidden()) {
            return false;
        }
        var value = $("#otherDate input").val();
        var duration = $(this).val();
        calculateDays(value, duration);
    });

    $("#otherDate input").on('input paste', function() {
        if(isContainerHidden()) {
            return false;
        } 
        var value = $(this).val();
        var duration = $("#otherDate select").val();
        calculateDays(value, duration);
    });


    $("#max_days").on("jqv.field.result", function(e, element, errorFound, prompText) {
        if(errorFound) {
            $("#max_days").validationEngine('hide');
            if(isContainerHidden()) {
                $("#endDate").validationEngine('showPrompt', prompText, '');
            } else {
                $("#otherDate input").validationEngine('showPrompt', prompText, '');
            }
        } 
    });


    function calculateDays (value, duration) {

        switch(duration) {
            case 'months':
                value *= 30;
            break;
            case 'years':
                value *= 365;
            break;
        }

        $("#max_days").val(value);


    }

    function isContainerHidden() {
        return $("#otherDate").hasClass('hide') && $("#endDate select").val() !== "other";
    }

//    $("#debate_form").submit(function(e) {
//        e.preventDefault();
//        
//        alert(e);
//        return false;
//        var code, appId, debateSlug;
//        appId = $('#app_id').attr('data-app_id');
//        debateSlug = $('#debate_slug').attr('data-debate_slug');
//        code = "\
//            <div class=\"speakol-arguments-box\" \n\
//            data-app=\"" + appId + "\"   \n\
//            data-description=\"" + debateSlug + "\">\n\
//            </div>\n\
//            <script type=\"text/javascript\" src=\"http://qc.speakol.com/js/sdk.js\">\n\
//            </script>";
//
//        $('#gnrtd_code').text(code);
//        $('.cp-generated-code').text('Copy Generated Code');
//
//    });


});
