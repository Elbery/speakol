<?php
namespace Speakol\Routes;
class ModeratesRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'moderates',));
        $this->setPrefix('/moderates');
        $this->add('/index', array('action' => 'index'))->setName("moderate-list");
    }
}
