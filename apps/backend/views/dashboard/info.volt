<div id="wrapper">
    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content inset clearfix">
            <div class="col-lg-12 no_padding">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container mdrt info_container">
                    <div class="top_bar">
                        <div class="row">
                            <div class="img_circl_container">
                                <span class="img_circl">
                                    {% if appImage %}
                                        <a href="">{{ image(appImage) }}</a>
                                    {% else %}
                                        <a href="">{{ image("img/user_default.gif") }}</a>
                                    {% endif %}
                                </span>
                                <span><h1>{{ t._('info')  }}</h1></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 info">
                            <p class="h4">App ID: {{ app.id  }}</p>
                            <p class="h4">{{ t._('publisher-name')  }}:  {{ user.name  }}</p>
                            <p class="h4">{{ t._('publisher-website')  }}: {{ app.website  }}</p>
                            <p class="h4">{{ t._('publisher-category')  }}: {{ category }}</p>
                            <p class="h4">{{ t._('publisher-lang')  }}: {{ (user.locale === "ar") ? "العربية" : "English"  }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

