<div class="tab-pane fade" id="fltrs">
    <div class="blc hlf">
        <h2>{{ t._('restricted-words') }}</h2>
        <p>{{ t._('restricted-words-instr') }}.</p>
    </div>
    <div class="blc hlf">
        <textarea name="" id="" rows="5"></textarea>
    </div>
    <hr>
    <div class="blc hlf">
        <h2>{{ t._('blk-wite-lst') }}</h2>
        <p>{{ t._('blk-wite-lst-instr') }}.
        </p>
    </div>
    <div class="blc hlf">
        <ul class="nav nav-tabs" id="Tabs_fltr">
            <li class="active"><a href="#wt_lst" data-toggle="tab">
                    {{ t._('wite-lst') }}
                </a></li>
            <li><a href="#blk_lst" data-toggle="tab">
                    {{ t._('blk-lst') }}
                </a></li>
        </ul>
        <div class="tab-content Tabs_fltr">
            <div class="tab-pane fade in active" id="wt_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ t._('uname') }}</th>
                            <th>{{ t._('email') }}</th>
                            <th>{{ t._('IP-addr') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="tab-pane fade" id="blk_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ t._('uname') }}</th>
                            <th>{{ t._('email') }}</th>
                            <th>{{ t._('IP-addr') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="button" value="{{ t._('save') }}" class="btn blue_btn"/>
    </div>
</div>