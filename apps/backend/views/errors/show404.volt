<!-- Page content -->
<div id="page-content-wrapper">

    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset clearfix">
        <div class="col-lg-9 no_padding">
            <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
            <div class="story_container error_404">
                <p class="v_lrg">404
                    <span> {{ t._('nothn_here') }}! </span>
                </p>
                <p class="med">{{ t._('404-title') }}.</p>
                <p>{{ t._('404-bdy') }} {{ linkTo('static/contactus', t._('contact-us')) }}.</p>
                <p class="back">{{ t._('or') }} <a href="{{url('')}}" class="btn">{{ t._('go-spkl') }}</a></p>
            </div>
        </div>
    </div>
</div>
