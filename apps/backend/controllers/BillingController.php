<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Validations as Validations;
use Speakol\Backend\Controllers\BaseController;
class BillingController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('billing');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
    }
    public function indexAction() {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $app = $this->getAppData();
        $hdrs = array('Authorization:' . $this->getAppToken());
        $response = $this->utility->iCurl('/billing/history', 'GET', $hdrs, array(), true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('dashboard');
        }
        $this->view->cards = $response->data->card;
        if (empty($response->data->card)) {
            $this->view->noCards = true;
        }
        $this->view->defaultCard = $response->data->default_card;
        $expiryMonth = $this->view->card->expiry_month;
        $expiryMonth = $expiryMonth < 10 ? '0' . $expiryMonth : $expiryMonth;
        $plans = array(2 => array('name' => 'Premium', 'price' => '50$',), 3 => array('name' => 'Premium+', 'price' => '100$',), 4 => array('name' => 'Corporate', 'price' => '300$',),);
        $this->view->invoices = $response->data->invoices;
        $this->view->plan = $response->data->plan;
        $this->view->planID = intval($app->plan_id);
        $this->view->nextPlanID = $this->view->planID + 1;
        $this->view->nextPlan = $plans[$this->view->nextPlanID];
        $this->view->pendingPlan = $response->data->plan->pending_plan;
        $this->view->plan->formattedDate = date('F d, Y', strtotime($response->data->plan->billing_time));
        $this->view->currentYear = date('Y');
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.payment.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/billing.js?' . $this->config->application->cache_string);
    }
    public function updateAction($cardId) {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        if (!$cardId || !ctype_digit($cardId)) {
            return $this->response->redirect('billing');
        }
        if ($this->request->isPost()) {
            $app = $this->getAppData();
            $hdrs = array('Authorization:' . $this->getAppToken());
            $params = array();
            $fields = array('method', 'name', 'number', 'year', 'month', 'code');
            foreach ($fields as $field) {
                $post = $this->request->getPost($field);
                if (isset($post) && !empty($post)) {
                    $params[$field] = $post;
                }
            }
            $params['id'] = $cardId;
            $response = $this->utility->iCurl('/billing/card/update', 'POST', $hdrs, $params, false, false, true);
            if ($response->status !== 'OK') {
                if ($this->request->isAjax()) {
                    $this->response->setHeader("Content-Type", "application/json");
                    $this->response->setStatusCode(404, 'ERROR');
                    $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
                    return $this->response;
                } else {
                    $this->flash->error($response->message);
                    return $this->response->redirect('billing');
                }
            }
            if ($this->request->isAjax()) {
                $this->response->setHeader("Content-Type", "application/json");
                $this->response->setStatusCode(200, 'OK');
                $this->response->setJsonContent(array('status' => 'OK', 'message' => $response->message));
                return $this->response;
            } else {
                $this->flash->success($response->message);
                return $this->response->redirect('billing');
            }
        }
    }
    public function addAction() {
        if (!$this->request->isPost() && $this->request->isAjax()) {
            $this->response->setHeader("Content-Type", "application/json");
            $this->response->setStatusCode(400, 'Bad Request');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Invalid Action'));
            return $this->response;
        }
        if (!$this->request->isPost()) {
            $this->flash->error('Invalid Action');
            return $this->request->redirect('billing');
        }
        $app = $this->getAppData();
        if (!$app) {
            if ($this->request->isAjax()) {
                $this->response->setHeader("Content-Type", "application/json");
                $this->response->setStatusCode(401, 'Unauthorized');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
                return $this->response;
            } else {
                return $this->response->redirect('apps/login');
            }
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $params = array('plan' => $plan_id, 'name' => $this->request->getPost('name'), 'number' => $this->request->getPost('number'), 'month' => $this->request->getPost('month'), 'year' => $this->request->getPost('year'), 'code' => $this->request->getPost('code'), 'method' => $this->request->getPost('method'),);
        $response = $this->utility->iCurl('/billing/add/card', 'POST', $hdrs, $params, false, false, true);
        if ($response->status !== 'OK' && $this->request->isAjax()) {
            $this->response->setHeader("Content-Type", "application/json");
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
            return $this->response;
        }
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('billing');
        }
        if (!$this->request->isAjax()) {
            $this->flash->success('Credit card has added been successfully');
            return $this->response->redirect('billing');
        }
        $this->response->setHeader("Content-Type", "application/json");
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('card_id' => $response->data->card_id)));
        return $this->response;
    }
    public function showAction($cardId) {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        if (!$this->request->isAjax()) {
            return $this->request->redirect('billing');
        }
        $this->response->setHeader("Content-Type", "application/json");
        $app = $this->getAppData();
        if (!$cardId) {
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Card is not specified'));
            return $this->response;
        }
        if (!$app) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $response = $this->utility->iCurl('/billing/card/show/' . $cardId, 'GET', $hdrs, array());
        if ($response->status !== 'OK') {
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
            return $this->response;
        }
        $card = $response->data;
        $html = $this->utility->renderTemplate("partials", "billing/card", array('card' => $card, 'currentYear' => date('Y'),));
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'html' => $html, 'card' => $card));
        return $this->response;
    }
    public function deleteAction($cardId) {
        if (!$this->request->isAjax()) {
            return $this->request->redirect('billing');
        }
        $this->response->setHeader("Content-Type", "application/json");
        $app = $this->getAppData();
        if (!$cardId) {
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Card is not specified'));
            return $this->response;
        }
        if (!$app || !$this->request->isDelete()) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $response = $this->utility->iCurl('/billing/card/delete/' . $cardId, 'DELETE', $hdrs, array());
        if ($response->status !== 'OK') {
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
            return $this->response;
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'message' => $response->data));
        return $this->response;
    }
    public function sortinvoicesAction($sortMethod = 'asc') {
        if (!$this->request->isAjax()) {
            return $this->response->redirect('billing');
        }
        $this->response->setHeader("Content-Type", "application/json");
        $app = $this->getAppData();
        if (!$app) {
            $this->response->setStatusCode(401, 'Unauthorized');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => 'Unauthorized'));
            return $this->response;
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $response = $this->utility->iCurl('/billing/invoices/' . $sortMethod, 'GET', $hdrs, array());
        if ($response->status !== 'OK') {
            $this->response->setStatusCode(404, 'ERROR');
            $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $response->message));
            return $this->response;
        }
        $this->response->setStatusCode(200, 'OK');
        $this->response->setJsonContent(array('status' => 'OK', 'data' => array('invoices' => $response->data)));
        return $this->response;
    }
    public function viewreceiptAction($receiptId) {
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->view->disable();
        $this->response->setHeader("Content-Type", "application/pdf");
        if (!$receiptId) {
            $this->flash->error('Bad Request');
            return $this->response->redirect('billing');
        }
        $hdrs = array('Authorization:' . $this->getAppToken());
        $response = $this->utility->iCurl('/billing/receipt/show/' . $receiptId, 'GET', $hdrs, array());
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('billing');
        }
        $receipt = $response->data;
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetPrintHeader(false);
        $lang_file = dirname(__DIR__) . '/../library/tcpdf/examples/lang/eng.php';
        $pdf->SetFont('dejavusans', '', 11, '', true);
        $lang = $this->getLang();
        if ($lang == "ar") {
            $lang_file = dirname(__DIR__) . '/../library/tcpdf/examples/lang/ara.php';
            $pdf->SetFont('aealarabiya', '', 11);
        } else if ($lang == "en") {
            $lang_file = dirname(__DIR__) . '/../library/tcpdf/examples/lang/eng.php';
            $pdf->SetFont('dejavusans', '', 11, '', true);
        }
        if (@file_exists($lang_file)) {
            require_once ($lang_file);
            $pdf->setLanguageArray($l);
            $fontname = \TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/../library/tcpdf/fonts/inv2.ttf', 'TrueTypeUnicode', '', 96);
            $bold_font = \TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/../library/tcpdf/fonts/bold.ttf', 'TrueTypeUnicode', '', 96);
            $normal_font = \TCPDF_FONTS::addTTFfont(dirname(__DIR__) . '/../library/tcpdf/fonts/inv.ttf', 'TrueTypeUnicode', '', 96);
            $pdf->SetFont($fontname, '', 14, '', false);
        }
        $pdf->setFontSubsetting(true);
        $pdf->AddPage('P', 'A4', false, false);
        $logo = $this->tag->image(array('img/logo2.png'));
        $paid = $this->tag->image(array("img/paid.png", "height" => "50px", "width" => "120px"));
        $dueDate = date('F d, Y', strtotime($receipt->due_date));
        $payingDate = date('F d, Y', strtotime($receipt->paying_date));
        $html = <<<EOD
<table style="width:100%;">
    <tr>
        <td width="40%;">$logo</td>
        <td width="40%;"></td>
        <td width="20%;" style="font-family:$bold_font;" ><p><font size="14">Speakol, Inc.</font><br><font size="9">622 Albustan Street<br>Alhorrya Tower<br>Cairo, Egypt<br>info@speakol.com</font></p></td>
    </tr>
    <tr>
        <td style="font-family:$normal_font;font-size:11;"><font face="$bold_font" size="13">Bill To</font><br>$receipt->website<br/>Attention: $receipt->name<br/>$receipt->address
</td>
    </tr>
<tr>
<td>
</td>
<td>
$paid
</td>
</tr>
    </table>
<table style="width:35%;font-family:$bold_font;font-size:11;">
    <tr><td style="width:40%;">Invoice #:</td><td style="text-align:right;width:60%;">$receipt->invoice_no</td></tr>
    <tr><td style="width:40%;">Due Date</td><td style="text-align:right;width:60%;">$dueDate</td></tr>
    <tr><td style="width:40%;">Paid Date</td><td style="text-align:right;width:60%;">$payingDate</td></tr>
</table>
<br>
<br>
<br>
<table  style="width:100%;"  cellpadding="4">
    <tr style="font-family:$normal_font;">
        <td width="55%;">&nbsp;&nbsp;Description</td>
        <td width="15%" style="text-align:right;" >Unit Price</td>
        <td width="15%" style="text-align:right;">Quantity</td>
        <td width="15%" style="text-align:right;">Amount</td>
    </tr>
</table>
<table bgcolor="#e0e0e0" cellpadding="1">
<tr>
<td>
<table style="font-size:11;width:100%;" cellpadding="5">
    <tr bgcolor="#efefef">
        <td width="55%;">&nbsp;&nbsp;$receipt->plan Subscription</td>
        <td width="15%" style="text-align:right;">$$receipt->amount</td>
        <td width="15%" style="text-align:right;">1.0</td>
        <td width="15%" style="text-align:right;">$$receipt->amount</td>
    </tr>
</table>
</td>
</tr>
</table>
<br>
<table style="width:100%;" cellpadding="5">
<tr style="text-align:right;font-size:9;"><td width="75%"></td><td style="font-family:$bold_font;text-align:left;" width="15%">Subtotal:</td><td width="10%;">&nbsp; $$receipt->amount</td></tr>
<tr style="text-align:right;font-size:16;"><td width="75%"></td><td style="font-family:$bold_font;text-align:left;" width="15%">Total:</td><td width="10%;">$$receipt->amount</td></tr>
</table>
EOD;
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        return $pdf->Output('speakol-invoice-' . $receipt->invoice_no . '.pdf', 'I');
    }
}
