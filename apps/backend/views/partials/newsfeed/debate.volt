<div class="sp-block-group feed-item radius-5">
    {% include 'partials/newsfeed/plugin-header.volt' %}
    <div class="sp-block-group">
        <h2 class="font-20 light-blue"><i class="speakol-icon speakol-debate-green feed-item-icon"></i>&nbsp;{{ plugin.data.title }}</h2>
    </div>
    <div class="sp-block-group margin-top-lg">
        {% set sides = (lang === 'ar') ? [plugin.sides[1], plugin.sides[0]] : plugin.sides %}
        {% for side in sides %}
            {% set meta = side.side_data %}
            {% set right = false %}
            {% if loop.last %}
                {% set right = true %}
            {% endif %}
        <div class="sp-block six margin-bottom-sm {{ right ? 'padding-left-5' : 'padding-right-5' }}">
            <div class="media">
                <div class="{{ right ? 'pull-right text-right' : 'pull-left' }} debate-title-image">
                    <img class="inline-block radius-5 border-dedede debate-plugin-img" src="{{( meta.image)}}" alt="">
                </div>
                <div class="media-body {{ right ? 'text-right' : '' }}">
                    <h3 class="font-16 color-1362ad weight-bold no-margin media-heading margin-bottom-sm  {{ right ? 'text-right' : '' }}">{{ meta.title }}</h3>
                    <p class="color-676767 h6 debate-readmore margin-top-xs hide-xxs {{ right ? 'text-right' : '' }}">{{ meta.job_title }}</p>
                </div>
            </div>
        </div>
        {% endfor %}
    </div>
    <div class="sp-block-group">
        {% set sides = (lang === 'ar') ? [plugin.sides[1], plugin.sides[0]] : plugin.sides %}
        {% for side in sides %}
            {% set meta = side.side_data %}
            {% set right = false %}
            {% if loop.last %}
                {% set right = true %}
            {% endif %}
        <div class="sp-block six {{ right ? 'padding-left-5' : 'padding-right-5' }}">
            <div class="sp-block-group bg-f9f9f9 relative tip tip-f9f9f9 {{ right ? 'text-right right-tip' : 'left-tip' }}">
                <div class="margin-center width-90">
                    <p class="debate-readmore-lg h5">{{ meta.opinion }}</p>
                </div>
            </div>
        </div>
        {% endfor %}
    </div>
    <div class="sp-block-group margin-top-lg margin-bottom-lg padding-bottom-lg progress-container relative margin-center">
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? plugin.sides[1] : plugin.sides[0] %}
                {% set color = (lang === 'ar') ?  'color-d11b1f' : 'color-4dac54' %}
                {% set bg = (lang === 'ar') ?  'bg-d11b1f' : 'bg-4dac54' %}
                <div class="sp-block six text-left color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span> {{ t._('votes') }}
                </div>
                <div class="sp-block six text-right js-side-percentage side-percentage-left h4 {{ color }} padding-bottom-sm" data-side-id="{{ side.id }}">{{ side.perc ? side.perc : 0 }}%</div>
            </div>
            <div class="progress circular-radius flip vote-progress no-shadow no-border">
                <div class="progress-bar  circular-radius {{ bg }} js-progress-bar no-shadow no-border arg-progress-bar-left" style="width: {{ side.perc ? side.perc : 0 }}%" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : 0 }}%</span>
                </div>
            </div>
        </div>
        <div class="sp-block progress-circle text-center">
            <canvas class="naqeshny-canvas js-voting-circle" width="80" height="80" data-lang={{ lang }}>
                <div class="js-data data hide">
                    {
                    {% for side in plugin.sides %}
                        
                        {% if loop.first %}
                        "pro-votes": "{{ side.votes}}",
                        "pro-color": "#4dac54",
                        {% else %}
                        "con-votes": "{{ side.votes }}",
                        "con-color": "#d11b1f"
                        {% endif %}
                    {% endfor %}
                    }
                </div>
            </canvas>
        </div>
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? plugin.sides[0] : plugin.sides[1] %}
                {% set color = (lang === 'ar') ?  'color-4dac54' : 'color-d11b1f' %}
                {% set bg = (lang === 'ar') ?  'bg-4dac54' : 'bg-d11b1f' %}
                <div class="sp-block six text-left js-side-percentage side-percentage-right h4 {{ color }} padding-bottom-sm" data-side-id="{{ side.id }}">{{ side.perc ? side.perc : 0 }}%</div>
                <div class="sp-block six text-right color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span> {{ t._('votes') }}
                </div>
            </div>
            <div class="progress circular-radius vote-progress no-shadow no-border">
                <div class="progress-bar  circular-radius {{ bg }} no-shadow no-border js-progress-bar arg-progress-bar-right" style="width: {{side.perc ? side.perc : 0}}%" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : 0 }}%</span>
                </div>
            </div>
        </div>
    </div>
    <div class="sp-block-group text-center">
        <a href="/newsfeed/debate?slug={{ plugin.data.slug }}&app={{ plugin.app.id }}&lang={{ lang }}" class="{{ isLoggedIn ? '' : 'js-login-button' }} anchor-reset margin-center inline-block btn-default conversation-button bg-white radius-5 border-acb6bb">{{ t._('join-conversation') }}</a>
    </div>
</div>
