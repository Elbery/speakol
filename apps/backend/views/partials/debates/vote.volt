
<div class="support_buttons_container">
    <span>{{ t._('plz-choose-ur-side') }}:</span><br/>
    {% for side in debate.sides %}

        {% set meta = side.meta[0] %}
        {% if loop.first %}
            {% if side.voted is true %}
                {% set class = 'dimmed supported' %}
            {%else%}
                {% set class = '' %}
            {% endif %}
            <a class="Support Support_one vt_up {{class}}" href="#" data-debate-id="{{debate.id}}" data-side-id="{{side.id}}" data-debate-title="{{meta.title}}">{{ t._('support') }} <span>{{meta.title}}</span></a>
        {% else %}
            {% if side.voted is true %}
                {% set class = 'dimmed supported' %}
            {%else%}
                {% set class = '' %}
            {% endif %}
            <a class="Support Support_two orang vt_dwn {{class}}" href="#" data-debate-id="{{debate.id}}" data-side-id="{{side.id}}" data-debate-title="{{meta.title}}">{{ t._('support') }} <span>{{meta.title}}</span></a>
        {% endif %}
    {% endfor %}
    <div class="just-voted">
        <span>{{ t._('you-just-supported') }}  <strong class="debate-title">{{ t._('republicans') }}</strong>!
            <a class="undo_action" data-debate-id="{{debate.id}}" data-side-id="" data-thumb="" href="#">{{ t._('undo-that') }}?</a>
        </span>
    </div>
</div>