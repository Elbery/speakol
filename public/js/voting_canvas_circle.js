(function( $ ){
  // Settings and Constants

  var _settings = {

  };

  var _ctx = null;
  var _data = [];
  var _dominant_color = null;
  var _origin = 40;
  var _radius = 34;
  var _lang = "en";

  // public methods
  var _methods = {
    init: init
  }

  // --------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------------------------- CORE PUBLIC METHODS

  function init(options) {
    if(options){
      $.extend(_settings, options);
    }

    // calculate the data
    _ctx = $(this)[0].getContext('2d');
    _lang = $(this).attr('data-lang') || _lang;
    _data = $.parseJSON($(this).children(".data").html());

    var voting_data = [
      parseInt(_data["pro-votes"]),
      parseInt(_data["con-votes"])
    ];

    var total_votes = 0;
    $(voting_data).each(function(index, value){
      total_votes += value;
    });

    if(total_votes > 0){
      var colored_votes = [
        { color: _data["pro-color"], degrees: parseInt(_data["pro-votes"])/total_votes },
        { color: _data["con-color"], degrees: parseInt(_data["con-votes"])/total_votes }
      ];

      colored_votes = (_lang === "ar") ? colored_votes.reverse() : colored_votes;

      if(colored_votes[0].color == colored_votes[1].color)
          colored_votes[0].color = colored_votes[0].color.lighten_color("20%");
    } else {
      var colored_votes = [
        { color: "#e5e5e5", degrees: 1 }
      ];
    }

    // initialize the canvas
    _ctx.canvas.width = $(this).attr('width') ? +$(this).attr('width') : 100;
    _ctx.canvas.height = $(this).attr('height') ? +$(this).attr('height') : 100;
    // _ctx.canvas.width =  100;
    // _ctx.canvas.height =  100;
    _ctx.canvas.left = 40;

    _ctx.beginPath();
    _ctx.arc(_origin,_origin,_radius + 6,0,Math.PI*2,true);
    _ctx.clip();

    // draw the progress
    var start_angle = -(Math.PI * 3/2);

//    console.log(colored_votes)
    
    $(colored_votes).each(function(index, vote){
      
      if(vote.degrees != 0){
        var degrees = vote.degrees * 360.0;
        var radians = (degrees * (Math.PI / 180)) + start_angle;

        _ctx.strokeStyle = vote.color;
        _ctx.beginPath();
        _ctx.lineWidth = 10;
        _ctx.arc(_origin, _origin, _radius, start_angle+.03, radians, false);
        _ctx.stroke();
        start_angle = radians;

      }

    });

    // var grd = _ctx.createRadialGradient(_origin, _origin, 33, _origin, _origin, 36.5);
    // grd.addColorStop(0, "rgba(85, 85, 85, 0)");
    // grd.addColorStop(1, "rgba(85, 85, 85, 0.5)");
    // _ctx.fillStyle = grd;
    // _ctx.beginPath();
    // _ctx.arc(_origin,_origin,_radius,0.5,Math.PI*2,true);
    // _ctx.closePath();
    // _ctx.fill();

    // draw the overlay donut
    _ctx.fillStyle = "#3d3d3d";
    _ctx.beginPath();
    _ctx.arc(_origin,_origin,32,0,Math.PI*2,true);
    _ctx.closePath();
    _ctx.fill();

    normalized_votes = colored_votes.uniquePercentage();

    if(normalized_votes.length == 1){
      normalized_votes[0].color = "#333333";
      normalized_votes[0].flag_y = 0;
      if(total_votes == 0)
        normalized_votes[0].degrees = 0;
    } else {
      normalized_votes.sort(function(a,b){
         return b.degrees - a.degrees;
      });
    }

    _dominant_color = normalized_votes[0];

    var percentage = Math.round(_dominant_color.degrees * 100);

    _ctx.fillStyle = "#ffffff";
    _ctx.font = "13px 'HelveticaNeue' regular";
    _ctx.textAlign = "center";
    _ctx.textBaseline = "alphabetic";

    weight = ("" + total_votes).length;
    font_size = 4;

    if(weight < 8){
      font_size += (7 - (weight)) * 2;
    }


    _ctx.fillText("votes", _origin, _origin + font_size);

    _ctx.font = "20px 'HelveticaNeue' regular";
    _ctx.fillText(total_votes, _origin, _origin);
  }

  // Return new array with duplicate values removed
  Array.prototype.uniquePercentage = function() {
    var a = [];
    var l = this.length;
//    console.log(a);
    for(var i=0; i<l; i++) {
      for(var j=i+1; j<l; j++) {
        // If this[i] is found later in the array
        if (this[i].degrees === this[j].degrees)
          j = ++i;
      }
      a.push(this[i]);
    }
    return a;
  };

  // --------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------------------------- PLUGIN CORE

  $.fn.debateVersus = function( method ){

    // Method calling logic
    if ( _methods[method] ) {
      return _methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return _methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
    }
  };
})( jQuery );
