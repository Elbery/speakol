<?php
use Phalcon\Mvc\Application, Phalcon\DI\FactoryDefault;
$di = new FactoryDefault();
try {
    $loader = new Phalcon\Loader();
    $loader->registerNamespaces(array('Speakol\Routes' => '../apps/config/routes/',));
    $loader->register();
    $config = include '../apps/config/config.php';
    $di->set('config', $config);
    include '../apps/config/services.php';

   // var_dump( $di);
    //die;


$di->set('url', function(){
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri('/public/');
    return $url;
});


    $application = new Application($di);
    $application->registerModules(array('backend' => array('className' => 'Speakol\Backend\Module', 'path' => '../apps/backend/Module.php',)));
    echo $application->handle()->getContent();
}
catch(\Exception $e) {
    echo $e->getMessage();
}
