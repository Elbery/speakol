<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Validations as Validations;
use Speakol\Backend\Controllers\BaseController;
class StaticController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('static');
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        if ($this->getLang() != 'en') {
            $this->assets->addCss('css/main_ar.css?' . $this->config->application->cache_string);
        } else {
            $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        }
    }
    public function pressAction() {
        $this->tag->appendTitle(' Press');
        if ($this->getLang() != 'en') {
            $this->view->pick('static/ar/press');
        }
    }
    public function aboutAction() {
        $this->tag->appendTitle(' About');
        if ($this->getLang() != 'en') {
            $this->view->pick('static/ar/about');
        }
    }
    public function contactusAction() {
        $this->tag->appendTitle(' Contact Us');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/contactus.css?' . $this->config->application->cache_string);
        $dummy = $this->session->get('dummy');
        try {
            if ($this->request->isPost()) {
                $postData = array('contact_name' => $this->request->get('contact_name'), 'contact_email' => $this->request->get('contact_email'), 'contact_subject' => $this->request->get('contact_subject'), 'contact_message' => $this->request->get('contact_message'),);
                $staticValidator = new Validations\StaticValidation(array('contact_name', 'contact_email', 'contact_subject', 'contact_message'));
                $staticMsgs = $staticValidator->validate($postData);
                if (count($staticMsgs)) {
                    $this->showErrorMsgs($staticMsgs);
                } else {
                    $content = $this->utility->renderEmailContent($this, array('emails', 'contact-us'), $postData);
                    $emailPostFields = array('html_content' => $content, 'text' => '', 'subject' => 'Social plugins | ' . $postData['contact_subject'], 'from_email' => 'noreply@speakol.com', 'from_name' => 'Speakol', 'to_email' => 'ask@speakol.com');
                    $result = $this->utility->iCurl('/notifications/email/send', 'POST', array('Authorization:' . $this->getToken()), $emailPostFields, false, true);
                    if ($result['status'] == 'SUCCESS') {
                        $this->flash->success($this->t->_('msg-sent'));
                    } else {
                        $this->flash->error($this->t->_('msg-not-sent'));
                    }
                }
            }
        }
        catch(\Exception $ex) {
            $this->flash->error($ex->getMessage());
        }
        if ($this->getLang() != 'en') {
            $this->assets->addCss('css/login_ar.css?' . $this->config->application->cache_string);
        } else {
            $this->assets->addCss('css/login.css?' . $this->config->application->cache_string);
        }
    }
    public function termsAction() {
        $this->tag->appendTitle(' Terms');
        if ($this->getLang() != 'en') {
            $this->view->pick('static/ar/terms');
        }
    }
    public function privacyAction() {
        $this->tag->appendTitle(' Privacy');
        if ($this->getLang() != 'en') {
            $this->view->pick('static/ar/privacy');
        }
    }
    public function appsAction() {
        $this->tag->appendTitle(' Apps');
    }
    public function careersAction() {
        echo "string";/*
        $this->tag->appendTitle(' Careers');*/
    }
}
