<li class="cmprsn_item">
    <div class="cont_txt">
        <a href="#">{{ item['data']['title'] }}</a>
        <time>{{ item['data']['created_at'] }}</time>
    </div>
    <div class="cont_num">
        <span>{{ item['counts']['total_votes'] }}</span>
        <small>{{ t._('votes') }}</small>
    </div>
    <div class="cont_num">
        <span>{{ item['counts']['total_comments'] }}</span>
        <small>{{ t._('interactions') }}</small>
    </div>
</li>