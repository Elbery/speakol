<?php
namespace Speakol\Backend\Controllers;
use \Phalcon\Tag as Tag;
class AdsController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/moderate.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery-1.10.1.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap3-typeahead.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
    }
    public function indexAction() {
        if (!$this->view->admin_mail) {
            return $this->response->redirect('dashboard');
        }
        if ($this->request->isPost()) {
            $params = array('text' => $this->request->getPost('text'), 'owner_name' => $this->request->getPost('owner_name'), 'owner_email' => $this->request->getPost('owner_email'), 'owner_lang' => $this->request->getPost('owner_lang'), 'url' => $this->request->getPost('url'), 'plugin_type' => $this->request->getPost('plugin_type'), 'plugin' => $this->request->getPost('plugin'),);
            $files = $this->request->getUploadedFiles();
            if (!$files) {
                $this->flash->error('Owner photo is required');
                return $this->response->redirect('ads');
            }
            if (count($files) === 1) {
                $params['owner_photo'] = $this->utility->getCurlValue($files[0]);
            } else {
                $params['image'] = $this->utility->getCurlValue($files[0]);
                $params['owner_photo'] = $this->utility->getCurlValue($files[1]);
            }
            $hdrs = $this->utility->getAuthorizationHeadersForApp();
            $response = $this->utility->iCurl('/ads/create', 'POST', $hdrs, $params);
            if ($response->status !== "OK") {
                $this->flash->error($response->message);
            } else {
                $this->flash->success("ad has been added successfully");
            }
            $this->tag->appendTitle(' Ads');
        }
        $langs = array("en" => "English", "ar" => "العربية");
        $this->view->setVar('langs', $langs);
        Tag::setDefault("owner_lang", $this->session->get('lang'));
    }
    public function createAction() {
    }
}
