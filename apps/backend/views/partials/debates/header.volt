<div class="two_prts clearfix">
    {% for side in debate.sides %}
        {% set meta = side.meta[0] %}
        {% if loop.first %}
            <div class="left_prt_cont">
                <div class="prt_nm_img clearfix">
                    <a href="#">
                        <span class="img covered" style="background-image:url('{{( config.application.webservice ~ meta.image)}}')"></span>
                        <span class="nm user-title"> {{meta.title}} </span>
                        <span class="nm job-title"> {{meta.job_title}} </span>
                    </a>
                </div>
                <div class="left_prt">
                    <p class="readmore">{{meta.opinion}}</p>
                </div>
            </div>
        {% else %}
            <div class="right_prt_cont">
                <div class="prt_nm_img clearfix">
                    <a href="#">
                        <span class="img covered" style="background-image:url('{{( config.application.webservice ~ meta.image)}}')"></span>
                        <span class="nm user-title">{{meta.title}}</span>
                        <span class="nm job-title">{{meta.job_title}}</span>
                    </a>
                </div>
                <div class="right_prt">
                    <p class="readmore">{{meta.opinion}}</p>
                </div>
            </div>
        {% endif %}
    {% endfor %}
</div>
