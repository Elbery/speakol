<?php
namespace Speakol\Routes;
class StaticRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'static',));
        $this->setPrefix('/');
        $this->add("static/press", array('action' => 'press',))->setName("static-press");
        $this->add("static/about", array('action' => 'about',))->setName("static-about");
        $this->add("static/contactus", array('action' => 'contactus',))->setName("static-contactus");
        $this->add("static/terms", array('action' => 'terms',))->setName("static-terms");
        $this->add("static/privacy", array('action' => 'privacy',))->setName("static-privacy");
        $this->add("static/apps", array('action' => 'apps',))->setName("static-apps");
        $this->add("static/careers", array('action' => 'careers',))->setName("static-careers");
        return $this;
    }
}
