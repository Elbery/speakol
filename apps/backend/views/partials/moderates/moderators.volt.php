<div class="tab-pane fade in" id="mdrtrs">
    
    <?php if ($moderators) { ?>
        <?php foreach ($moderators as $user) { ?>
        <div class="form_row">
            <div class="img_circl_container">
                <span class="img_circl">
                    <?php if ($user['role_id'] == 4) { ?>
                        <?php if ($user['profile_picture']) { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('user/profile/' . $user['slug']); ?>">
                            <?php echo $this->tag->image(array($this->config->application->webservice . $user['profile_picture'], 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } else { ?>
                            <?php echo $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } ?>
                        </a>
                    <?php
        } else { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('app/profile/' . $app->slug); ?>">
                        <?php if ($app->image) { ?>
                            <?php echo $this->tag->image(array($this->config->application->webservice . $app->image, 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } else { ?>
                            <?php echo $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } ?>
                        </a>
                    <?php
        } ?>
                </span>
                <span class="nameAndDate">
                    <?php if ($user['role_id'] == 4) { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('user/profile/' . $user['slug']); ?>"> <?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?> </a>
                    <?php
        } else { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('app/profile/' . $app->slug); ?>"> <?php echo $app->name; ?> </a>
                    <?php
        } ?>
                </span>
                <?php if ($user['role_id'] == 4) { ?>
                <ul class="actions_buttons">
                    <li><a title="Delete" href="<?php echo $this->url->get('apps/delete/admin/' . $user['id']); ?>" class="admin-alert delete_button">Delete</a></li>
                </ul>
                <?php
        } ?>
            </div>
        </div>
        <?php
    } ?>
    <?php
} ?>
    
    <div class="form_row">
        <?php echo $this->tag->form(array('moderates/add')); ?>
        <div class="img_circl_container">
            <span id="add-user-cicrle" class="img_circl add"></span>
            <span class="nameAndDate">
                <input type="text" id="add_admin_user" autocomplete="off"/>
                <input type="hidden" id="add-user-hidden" name="new_admin_id"/>
                <div>
                    <input type="checkbox" id="admin" name="is_admin"/>
                    <label for="admin"> Administrator</label>
                </div>
            </span>
            <ul class="actions_buttons">
                <li><a title="Add" href="#" class="add_button">Add</a></li>
            </ul>
        </div>
        <?php echo $this->tag->endform(); ?>
    </div>
    
</div>