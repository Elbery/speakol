<div class="sp-block-group padding-bottom-sm relative sides-container width-95 margin-center">
    {% if lang === "ar" %}
        {% set sides = [debate.sides[1] , debate.sides[0] ]  %}
    {% else  %}
        {% set sides = debate.sides  %}
    {% endif  %}
    {% if debate.ads is defined %}
    {% if lang === "ar" %}
        {% set ads = [debate.ads[1] , debate.ads[0] ]  %}
    {% else  %}
        {% set ads = debate.ads  %}
    {% endif  %}
    {% endif %}
    {% for side in sides %}
            {% set right = (loop.first) ? false : true %}
            <div class="six sp-block side js-side {{ loop.first ? "vertical-separator-right" : "vertical-separator-left" }}"  data-side-id="{{ side.id }}" data-comments-page="1" {{ right ? 'data-align="right"' : '' }}>
                <ul>
                    {% if ads is defined %}
                        {%set ad = ads[loop.index0] %}
                        {% if ad is defined %}
                        <li>
                            {% include "partials/argumentsbox/ad" with ['ad': ad, 'argumentsBoxURL':argumentsbox.url, 'right': right] %}
                        </li>
                        {% endif %}
                    {% endif %}
                </ul>
                <ul class="js-arguments">
                    {% if side.args.args is defined %}
                        {% for argument in side.args.args %}
                            <li>
                                {% include "partials/argumentsbox/argument" with ['argument': argument, 'debate': debate, 'right': right] %}
                            </li>
                        {% endfor %}
                    {% endif %}
                </ul>
                 {%  if side.args.hasmore %}
                <div class="sp-block-group text-center {{ lang === 'ar' ? 'rtl' : '' }}">
                    <button class="js-show-more  bg-647a88 font-13 radius-5 inline-block width-78 padding-top-xs padding-bottom-xs white border-none" data-loading="{{ t._('loading') }}">{{ t._('show-more') }} </button>
                </div>
                {% endif %}
            </div>
    {% endfor %}
</div>
