<div id="wrapper">
    <!-- Sidebar -->
    
<?php $controller = $this->dispatcher->getControllerName(); ?>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar_brand"><?php $speakolLogo = $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?><?php echo $this->tag->linkto('index', $speakolLogo); ?></li>
        <?php if ($this->session->app_data) { ?>
        <li class="sidebar_dshbrd <?php echo ($controller == 'dashboard' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('dashboard', 'dashboard'); ?></li>
        <li class="sidebar_dbt <?php echo ($controller == 'debates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('debates/code', 'Create Debate'); ?></li>
        <li class="sidebar_cmprsn <?php echo ($controller == 'comparisons' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('comparisons/code', 'Create Comparison'); ?></li>
        <li class="sidebar_argmnt <?php echo ($controller == 'arguments' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('arguments/code', 'Create Argument'); ?></li>
        <li class="sidebar_mdrt <?php echo ($controller == 'moderates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('moderates/index', 'Moderate'); ?></li>
        <li class="sidebar_anlsys <?php echo ($controller == 'analysis' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('analysis', 'Analysis'); ?></li>
        <li class="sidebar_aignout"><?php echo $this->tag->linkto('apps/logout', 'Sign Out'); ?></li>
        <?php
} ?>
    </ul>
</nav>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content inset clearfix">
            <div class="col-lg-12 no_padding">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container mdrt">
                    <div class="top_bar">
                        <div class="img_circl_container">
                            <span class="img_circl">
                                <?php if ($appImage) { ?>
                                    <a href=""><?php echo $this->tag->image(array($appImage)); ?></a>
                                <?php
} else { ?>
                                    <a href=""><?php echo $this->tag->image(array('img/user_default.gif')); ?></a>
                                <?php
} ?>
                            </span>
                            <span><h1>Moderate</h1></span>
                        </div>
                    </div>
                    <div class="tabs_container">
                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active" ><a href="#rep" data-toggle="tab">
                                    Replies
                                </a></li>
                            <li><a href="#arg" data-toggle="tab">
                                    Arguments
                                </a></li>
                            <li><a href="#ur_cntnt" data-toggle="tab">
                                    Your Content
                                </a></li>
                            <li class=""><a href="#mdrtrs" data-toggle="tab">
                                    Moderators
                                </a></li>
                            <li><a href="#fltrs" data-toggle="tab">
                                    Filters
                                </a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="tab-pane fade" id="arg">
                                
                                    
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                
                                <div class="arg_rep_items_cont">
                                    <ul class="li_vert arg_items">
                                        <!-- Argument & replies tab content  -->
                                        
<?php if (!$arguments) { ?>
    <div class="blc_content clearfix">
        <ul class="ul_vert item_typs">
            <div class="cont_num">
                <small>No arguments created!</small>
            </div>
        </ul>
    </div>
<?php
} ?>

<?php foreach ($arguments as $element) { ?>
    <li>

        <div class="comment_unit clearfix">
            <ul class="actions_buttons">
                <li>
                    <a class="delete_button txt_ind arg_delete"  title="Delete" data-argument_id="<?php echo $element['id']; ?>" href="#">Delete</a>
                </li>
            </ul>
            <div class="img_circl_container">
                <span class="img_circl">
                    <a href="">
                        <?php if (isset($element['user']['profile_picture']) && $element['user']['profile_picture']) { ?>
                            <?php echo $this->tag->image(array($this->config->application->webservice . $element['user']['profile_picture'])); ?>
                        <?php
    } else { ?>
                            <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                        <?php
    } ?>
                    </a>
                </span>
                <span class="nameAndDate">
                    <a href="#"> <?php echo $element['user']['first_name']; ?> <?php echo $element['user']['last_name']; ?></a>
                    <time><span><?php echo $element['date']; ?></span></time>

                </span>
            </div>
            <div class="comment_area">
                <p><?php echo $element['context']; ?></p>
                <!--<img class="context_img" src="img/context_img.jpg" alt=""/>-->
            </div>
            <div class="replies_actions">
                <div class="vote_action">
                    <div class="votes_up_num">
                        <a href="#" class="votes_up" title="vote up"> <?php echo $element['thumbs_up']; ?></a>
                    </div>
                    <div class="votes_down_num">
                        <a href="#" class="votes_down" title="vote down"> <?php echo $element['thumbs_down']; ?></a>
                    </div>
                </div>
                <div class="reports">
                    <span> <?php echo $element['reports']; ?> Reports  </span>
                </div>
            </div>
        </div>
    </li>
<?php
} ?>

                                    </ul>
                                    <?php if ($hasMore['argument']) { ?>
                                        <a class="pag-arguments more_cont" data-argument="2" data-page="1" data-ajax-url="/moderates/index?type=argument" href="#">Show next 20 items</a>
                                    <?php
} ?>
                                </div>
                                
                                    
                                    
                                    
                                        
                                            
                                            
                                            
                                            
                                        
                                        
                                    
                                
                            </div>                    

                            <div class="tab-pane fade in active" id="rep">
                                

                                    
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                
                                <div class="arg_rep_items_cont">
                                    <ul class="li_vert arg_rep_items">
                                        <script>
    viewMore = '<?php echo $hasMore['reply']; ?>';
</script>

<?php if (!$replies) { ?>
    <div class="blc_content clearfix">
        <ul class="ul_vert item_typs">
            <div class="cont_num">
                <small>No replies created!</small>
            </div>
        </ul>
    </div>
<?php
} ?>

<?php foreach ($replies as $element) { ?>
    <li>
        <div class="chkbx_cont">
            <input type="checkbox" name="selected[args][]" value="<?php echo $element['id']; ?>"/>
        </div>
        <div class="comment_unit clearfix">
            <ul class="actions_buttons">
                <a class="delete_button txt_ind reply_delete"  title="Delete" data-reply_id="<?php echo $element['id']; ?>" href="#">Delete</a>
            </ul>
            <div class="img_circl_container">
                <span class="img_circl">
                    <a href="">
                        <?php if (isset($element['user']['profile_picture']) && $element['user']['profile_picture']) { ?>
                            <?php echo $this->tag->image(array($this->config->application->webservice . $element['user']['profile_picture'])); ?>
                        <?php
    } else { ?>
                            <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                        <?php
    } ?>
                    </a>
                </span>
                <span class="nameAndDate">
                    <a href="#"> <?php echo $element['user']['first_name']; ?> <?php echo $element['user']['last_name']; ?></a>
                    <time><span><?php echo $element['date']; ?></span></time>
                </span>
            </div>
            <div class="comment_area">
                <p><?php echo $element['content']; ?></p>
                <!--<img class="context_img" src="img/context_img.jpg" alt=""/>-->
            </div>
            <div class="replies_actions">
                <div class="vote_action">
                    <div class="votes_up_num">
                        <a href="#" class="votes_up" title="vote up"> <?php echo $element['thumbs_up']; ?></a>
                    </div>
                    <div class="votes_down_num">
                        <a href="#" class="votes_down" title="vote down"> <?php echo $element['thumbs_down']; ?></a>
                    </div>
                </div>
                <div class="reports">
                    <span> <?php echo $element['reports']; ?> Reports </span>
                </div>
            </div>
        </div>
    </li>
<?php
} ?>


                                    </ul>
                                    <?php if ($hasMore['reply']) { ?>
                                        <a class="pag-replies more_cont" data-reply="2" data-page="0" data-ajax-url="/moderates/index?type=reply" href="#">Show next 20 items</a>
                                    <?php
} ?>
                                </div>
                                
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                
                            </div>
                            
                            <div class="tab-pane fade" id="ur_cntnt">
                                <div class="shw_fltr">
                                    <ul class="nav nav-tabs nav-pills">
                                        <li><span>Show: </span></li>
                                        <li class="active"><a class="showAll paginate-me"href="#" data-type="all">All</a></li>
                                        <li><a class="paginate-me" data-type="debate" href="#debates">Debates</a></li>
                                        <li><a class="paginate-me" data-type="comparison" href="#comparisons">Comparisons</a></li>
                                        <li><a class="paginate-me" data-type="argumentbox" href="#argumenstbox">Argument Boxes</a></li>
                                    </ul>
                                    
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                        
                                    
                                </div>
                                
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                
                                <div class="arg_rep_items_cont">
                                    <ul class="moderate-show-all tab-content arg_rep_items feed_items">
                                        <!-- Your Content tab content  -->
                                        
<div class="tab-pane fade active in debates" id="debates">
    <?php if (!$debates) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No debates created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($debates as $item) { ?>
        <div class="debate">
            <li>
    <article>

        <div class="feed_item_content">

            <ul class="actions_buttons">
                <li>
                    <a class="delete_button remove_debate" href="#" data-debate_slug="<?php echo $item['data']['slug']; ?>" title="Delete">Delete</a>
                </li>
            </ul>
            
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>

            <h1 class="item_title">
                <a href="#"><?php echo $item['data']['title']; ?></a>
            </h1>
            <div class="users_debating">
                <span class="verses">Vs</span>
                <div class="user_one">
                    <div class="img_circl_container">
                        <span class="img_circl">
                            <a href="<?php echo $this->url->get('user/profile/' . $item['sides']['0']['slug']); ?>">
                                <?php if (isset($item['sides']['0']['image']) && $item['sides']['0']['image']) { ?>
                                    <?php echo $this->tag->image(array($this->config->application->webservice . $item['sides']['0']['image'])); ?>
                                <?php
    } else { ?>
                                    <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                                <?php
    } ?>
                            </a>
                        </span>
                    </div>

                    <div class="comment_area">
                        <span class="nameAndDate">
                            <b>
                                <?php echo $item['sides']['0']['title']; ?>
                            </b>
                        </span>
                        <p><?php echo $item['sides']['0']['opinion']; ?>
                            <a href="<?php echo $this->url->get('debates/view/' . $item['data']['slug']); ?>"> read more</a> </p>
                    </div>
                </div>
                <div class="user_two">
                    <div class="img_circl_container">
                        <span class="img_circl">
                            <a href="">
                                <?php if (isset($item['sides']['1']['image']) && $item['sides']['1']['image']) { ?>
                                    <?php echo $this->tag->image(array($this->config->application->webservice . $item['sides']['1']['image'])); ?>
                                <?php
    } else { ?>
                                    <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                                <?php
    } ?>
                            </a>
                        </span>
                    </div>
                    <div class="comment_area">
                        <span class="nameAndDate">
                            <b>
                                <?php echo $item['sides']['1']['title']; ?>
                            </b>
                        </span>
                        <p><?php echo $item['sides']['1']['opinion']; ?>
                            <a href="<?php echo $this->url->get('debates/view/' . $item['data']['slug']); ?>"> read more</a> </p>
                    </div>
                </div>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

            </div>
        </div>
    </article>
</li>

<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'debates/render?app=' . $this->session->get('app_data')->user->app->id . '&debate=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
        </div>
    <?php
} ?>
</div>

<div class="tab-pane fade active in comparisons" id="comparisons">
    <?php if (!$comparisons) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No comparisons created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($comparisons as $item) { ?>
        <div class="comparison">
            <li>
    <article>

        <div class="feed_item_content">

            <ul class="actions_buttons">
                <li>
                    <a class="delete_button remove_comparison"  data-comparison_slug="<?php echo $item['data']['slug']; ?>"  href="#" title="Delete">Delete</a>
                </li>
            </ul> 
            
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>



            <h1 class="item_title">
                <a href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $item['data']['title']; ?></a>
            </h1>

            <div class="items_compared">
                <ul class="comparison_items clearfix">
                    <?php foreach ($item['sides'] as $side) { ?>
                        <li>
                            <div class="item_info clearfix">
                                <div class="nameAndpercent clearfix">
                                    <div class="item_photo">
                                        <?php if (isset($side['image']) && $side['image']) { ?>
                                            <?php echo $this->tag->image(array($this->config->application->webservice . $side['image'])); ?>
                                        <?php
        } else { ?>
                                            <?php echo $this->tag->image(array('img/symbol.png')); ?>
                                        <?php
        } ?>
                                    </div>
                                </div>
                                <div class="item_name">
                                    <h2><?php echo $side['title']; ?></h2>
                                </div>
                                <div class="item_vote_btn"><a href="#" class="Support"> <?php echo $side['percentage']; ?>%</a></div>
                            </div>
                        </li>
                    <?php
    } ?>
                </ul>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

                <a class="interact" href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $t->_('join-this-comparison'); ?></a>
            </div>
        </div>
    </article>
</li>


<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'comparisons/render?app=' . $this->session->get('app_data')->user->app->id . '&comparison=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
        </div>
    <?php
} ?>
</div>

<div class="tab-pane fade active in argumenstbox" id="argumenstbox">
    <?php if (!$argboxes) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No argumentsbox created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($argboxes as $item) { ?>
        <div class="argumentsbox">
            <li class="feed_item_arg">
    <article>

        <div class="feed_item_content">
            <ul class="actions_buttons">
                <li><a class="delete_button" href="#" title="Delete">Delete</a></li>
            </ul>
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>

            <h1 class="item_title">
                <a target="blank" href="<?php echo $item['data']['url']; ?>"><?php echo $item['data']['recomended']['title']; ?></a>
            </h1>
            <div class="user_commented">
                <p>
                    <?php echo $item['data']['description']; ?>
                    <a target="blank" href="<?php echo $item['data']['url']; ?>">read more</a>
                </p>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

            </div>
        </div>
    </article>
</li>

<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'debates/render?app=' . $this->session->get('app_data')->user->app->id . '&debate=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>
    <?php
} ?>
</div>

<?php if ($hasMore['all']) { ?>
    <a  
        data-all="<?php echo $hasMore['all']; ?>" 
        data-type="all" 
        data-debate="<?php echo $hasMore['debate']; ?>" 
        data-comparison="<?php echo $hasMore['comparison']; ?>" 
        data-argumentbox="<?php echo $hasMore['argumentsbox']; ?>" 
        data-url="/moderates/index" 
        data-ajax-url="/moderates/index?type=all" 
        class="pag-types pagination more_cont" href="#" data-page="1">Show next</a>
<?php
} ?>
                                        <!--feed item end-->
                                    </ul>

                                </div>
                                
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                
                            </div>
                                        
                            <!-- Your Content tab content  -->
                            <div class="tab-pane fade in" id="mdrtrs">
    
    <?php if ($moderators) { ?>
        <?php foreach ($moderators as $user) { ?>
        <div class="form_row">
            <div class="img_circl_container">
                <span class="img_circl">
                    <?php if ($user['role_id'] == 4) { ?>
                        <?php if ($user['profile_picture']) { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('user/profile/' . $user['slug']); ?>">
                            <?php echo $this->tag->image(array($this->config->application->webservice . $user['profile_picture'], 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } else { ?>
                            <?php echo $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } ?>
                        </a>
                    <?php
        } else { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('app/profile/' . $app->slug); ?>">
                        <?php if ($app->image) { ?>
                            <?php echo $this->tag->image(array($this->config->application->webservice . $app->image, 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } else { ?>
                            <?php echo $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?>
                        <?php
            } ?>
                        </a>
                    <?php
        } ?>
                </span>
                <span class="nameAndDate">
                    <?php if ($user['role_id'] == 4) { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('user/profile/' . $user['slug']); ?>"> <?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?> </a>
                    <?php
        } else { ?>
                        <a href="<?php echo $this->config->application->main_website . $this->url->get('app/profile/' . $app->slug); ?>"> <?php echo $app->name; ?> </a>
                    <?php
        } ?>
                </span>
                <?php if ($user['role_id'] == 4) { ?>
                <ul class="actions_buttons">
                    <li><a title="Delete" href="<?php echo $this->url->get('apps/delete/admin/' . $user['id']); ?>" class="admin-alert delete_button">Delete</a></li>
                </ul>
                <?php
        } ?>
            </div>
        </div>
        <?php
    } ?>
    <?php
} ?>
    
    <div class="form_row">
        <?php echo $this->tag->form(array('moderates/add')); ?>
        <div class="img_circl_container">
            <span id="add-user-cicrle" class="img_circl add"></span>
            <span class="nameAndDate">
                <input type="text" id="add_admin_user" autocomplete="off"/>
                <input type="hidden" id="add-user-hidden" name="new_admin_id"/>
                <div>
                    <input type="checkbox" id="admin" name="is_admin"/>
                    <label for="admin"> Administrator</label>
                </div>
            </span>
            <ul class="actions_buttons">
                <li><a title="Add" href="#" class="add_button">Add</a></li>
            </ul>
        </div>
        <?php echo $this->tag->endform(); ?>
    </div>
    
</div>

                            <!-- Your Content tab content  -->
                            <div class="tab-pane fade" id="fltrs">
    <div class="blc hlf">
        <h2>Restricted Words</h2>
        <p>Please separate using a comma, Arguments and replies containing any of these words will not be published until approved.</p>
    </div>
    <div class="blc hlf">
        <textarea name="" id="" rows="5"></textarea>
    </div>
    <hr>
    <div class="blc hlf">
        <h2>Black and white lists</h2>
        <p>Control who is able to participate within your community. You can specify items such as users, emails, IP addresses. Blacklisted items prevent certain people from posting, while whitelisted items allow users to bypass certain moderation filters (such as spam).
        </p>
    </div>
    <div class="blc hlf">
        <ul class="nav nav-tabs" id="Tabs_fltr">
            <li class="active"><a href="#wt_lst" data-toggle="tab">
                    White List
                </a></li>
            <li><a href="#blk_lst" data-toggle="tab">
                    Black List
                </a></li>
        </ul>
        <div class="tab-content Tabs_fltr">
            <div class="tab-pane fade in active" id="wt_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>IP Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="tab-pane fade" id="blk_lst">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>IP Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                        <tr>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                            <td><input type="text"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="button" value="Save" class="btn blue_btn"/>
    </div>
</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>