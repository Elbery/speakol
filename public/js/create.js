(function($) {
    $('#checkEmailForm').on('submit', function(e) {
        $('.alert').remove();
        e.preventDefault();
        var formData = $(this).serializeArray();
        var data = {};
        formData.forEach(function(input) {
            if (data[input.name] !== undefined) {
                if (!data[input.name].push) {
                    data[input.name] = [data[input.name]];
                }
                data[input.name].push(input.value || '');
            } else {
                data[input.name] = input.value || '';
            }
        });

        $.ajax({
            url: '/apps/checkemail',
            type: 'POST',
            data: data,
            success: function(res) {
                if(res.data.html) {
                    $('.form_cont').html(res.data.html);
                    $('.js-register-form-container').html(res.data.html);
                    $('.cc-number').payment('formatCardNumber');
                    $('.cc-cvc').payment('formatCardCVC');
                    $('form').validationEngine({ 
                        validateNonVisibleFields: true,
                        onValidationComplete:function(form,status){
                            console.log(status);
                            return status;
                        },
                        scroll: false
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var response = JSON.parse(jqXHR.responseText);
                var html = '<div class="alert alert-danger">' + response.message  + '</div>';
                $(".form_cont").prepend(html);
                $(".js-register-form-container").prepend(html);
            },
            dataType: 'json'
        });
    });



    function validCardNumber(field, rules, i, options) {
        if(!$.payment.validateCardNumber(field.val())) {
            return "* Invalid card number";
        }
    }


    function validCardType(field, rules, i , options) {

        var cardTypes = {
            "Visa":"visa",
            "MasterCard":"mastercard",
            "American Express":"amex",
            "Discover":"discover",
            "Diners Club":"dinersclub",
            "JCB": "jcb"
        };

        var cardType = $.payment.cardType(field.val()),
            selectedCardType = cardTypes[$('#method').val()];
        if(cardType !== selectedCardType) {
            
            return "* Card type doesn't match card number";
        }
        
    }

    window.validCardNumber = validCardNumber;
    window.validCardType = validCardType;


    $(document).on('change' , '#plan', function(e) {
       var planId = +$(this).val();
       $('.js-payments-form-container input, .js-payments-form-container select').prop('disabled', planId === 1);
    });
})(jQuery);
