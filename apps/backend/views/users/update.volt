<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}
                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}

    {#     <!--[if lt IE 9]> #}
    {#     {{ assets.outputJs('header-ie9') }} #}
    {# <![endif]--> #}
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">
<div class="js-svg-spinner hide">
<div class="sp-block-group text-center">
    {% include 'partials/newsfeed/spinner.volt' %}
</div>
</div>
{% include 'partials/newsfeed/header.volt' %}


<div class="sp-block-group community-content-container bg-f5f5f6 js-community-content-container">
    <div class="sp-block four bg-f5f5f6 community-sidebar-container js-community-sidebar-container">
        <div class="sp-block seven-xxl five-offset-xxl color-777777 h5">
            {% include 'partials/newsfeed/sidebar.volt' %}
        </div>
    </div>
    <div class="sp-block eight four-offset bg-white height-all feed-container js-feed-container">
        <h3 class="margin-bottom-lg padding-bottom-xs">{{ t._('edit-profile') }}</h3>
        {% for type, messages in flash.getMessages() %}
            {% for message in messages %}
                {% set classes = ['notice': 'bg-4fc0e8', 'success' : 'bg-57b25f', 'error': 'bg-d11b1f']%}
                {% set icons = ['notice': 'sp-exclamation', 'success' : 'sp-check', 'error': 'sp-exclamation']%}
                <div class="{{ classes[type] }} font-13 white radius-5 media padding-top-sm padding-bottom-sm community-alert margin-bottom-lg">
                    <div class="pull-left">
                        <i class="sp-icon {{ icons[type] }} margin-left-5"></i>
                    </div>
                    <div class="media-body">
                        <p class="no-margin">
                            {{ message }}
                        </p>
                    </div>
                </div>
            {% endfor%}
        {% endfor %}
        {{ form('users/update', 'enctype': 'multipart/form-data', "class": "padding-top-xs","onsubmit":"return validateForm();") }}
        <div class="sp-block-group">
            <div class="sp-block six">
                {{ textField({"first_name", 
                    "placeholder": t._('first-name'), 
                    "class": "width-95 color-a4a9b2", 
                    "data-validation-engine" :"validate[required]", 
                    "data-errormessage-value-missing": t._('first-name-required'),
                    "value": user.first_name
                }) }}
            </div>
            <div class="sp-block six">
                {{ textField({"last_name", 
                    "placeholder": t._('last-name'), 
                    "class": "width-95 color-a4a9b2", 
                    "data-validation-engine" :"validate[required]", 
                    "data-errormessage-value-missing": t._('last-name-required'),
                    "value": user.last_name
                }) }}
            </div>
        </div>
        <div class="media js-upload-container">
            <div class="pull-left">
                <img src="{{ user.image }}" class="community-user-image js-preview" alt="">
            </div>
            <div class="media-body">
                <div class="media bg-f5f5f6 padding-top-xs padding-bottom-xs padding-5 radius-5 border-dfdfdf">
                    <div class="pull-right">
                        <div class="width-all fileUpload font-13 btn-reset radius-5 bg-555555 white browse-button">
                            <span>{{ t._('browse') }}</span>
                            {{ hiddenField({"image", 'class' :'js-picture-value'})  }}
                            <input class="js-upload-button" type="file" name="">
                        </div>
                    </div>
                    <div class="media-body h4 color-a4a9b2 padding-top-sm padding-left-10">
                        <p>
                            {{ t._('choose-photo') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="sp-block-group {{ pending ? 'color-a4a9b2' : 'color-555555' }} margin-top-lg padding-top-lg">
            <p class="no-margin margin-top-sm font-15">
                <strong class="decoration-underline">{{ t._('current-email') }}:</strong>&nbsp;{{ user.email }}
            </p>
            <p class="no-margin h5 wide-line-height">
                {{ t._('update-email-paragraph') }}<br>
                {{ t._('leave-blank') }}
            </p>
        </div>
        <div class="sp-block-group margin-top-sm padding-top-sm margin-bottom-sm">
            <div class="sp-block six">
              {{ textField( {
                    "email", 
                    'placeholder': t._('new-email'), 
                    'class': ' width-95', 
                    "data-validation-engine" :"validate[required, custom[email]]",
                    "data-errormessage-value-missing": t._('email-required'),
                    "data-errormessage-custom-error": t._('email-not-valid')
                } ) }}
            </div>
            <div class="sp-block six">
              {{ textField( {
                    "confirm_email", 
                    'placeholder': t._('confirm-new-email'), 
                    'class': 'width-95', 
                    "data-validation-engine" :"validate[required, custom[email]]",
                    "data-errormessage-value-missing": t._('email-required'),
                    "data-errormessage-custom-error": t._('email-not-valid')
                } ) }}
            </div>
        </div>
        <div class="sp-block-group margin-top-sm padding-top-sm {{  pending ? 'color-a4a9b2': 'color-555555' }}">
            <p class="no-margin h5 wide-line-height">
                {{ t._('update-password-paragraph') }}<br>
                {{ t._('leave-blank') }}
            </p>
        </div>
        <div class="sp-block-group margin-top-sm padding-top-sm">
            <div class="sp-block six">
                 {{ passwordField( {
                    "current_password",
                    'id': 'password',
                    'placeholder': t._('current-password'), 
                    'class': 'width-95', 
                    "autocomplete": "off", 
                    "data-validation-engine" :"validate[required]",
                    "data-errormessage-value-missing": t._('password-required'),
                    "data-prompt-position": "topRight:-130"
                    }) }}
            </div>
        </div>
        <div class="sp-block-group margin-top-sm">
            <div class="sp-block six">
                 {{ passwordField( {
                    "password",
                    'id': 'password',
                    'placeholder': t._('new-password'), 
                    'class': 'width-95', 
                    "autocomplete": "off", 
                    "data-validation-engine" :"validate[required]",
                    "data-errormessage-value-missing": t._('password-required')
                    }) }}
            </div>
            <div class="sp-block six">
                 {{ passwordField( {
                    "confirm_password",
                    'id': 'confirm_password',
                    'placeholder': t._('confirm-new-password'), 
                    'class': 'width-95', 
                    "autocomplete": "off", 
                    "data-validation-engine" :"validate[required]",
                    "data-errormessage-value-missing": t._('confirm-password-required')
                    }) }}
            </div>
        </div>

        <h4 class="font-22 color-555555 margin-top-lg padding-top-lg">{{ t._('communication-settings') }}</h4>
        <div class="sp-block-group {{ pending ? 'color-a4a9b2' : 'color-555555' }} padding-top-xs margin-bottom-sm">
            <div class="sp-block six">
                <label for="newsletter" class="weight-normal h4 padding-top-lg">{{ t._('newsletter') }}</label>
            </div>
            <div class="sp-block six text-right">
                <select id="newsletter" name="newsletter" class="width-95 select-reset community-select full-arrow border-dfdfdf radius-5 {{ pending ? 'color-a4a9b2': 'color-555555' }}">
                    {% set options = [ '2': t._('monthly'), '1': t._('weekly'), '0': t._('no-email') ] %}
                    {% for key, value in options %}
                        <option value="{{ key }}" {{ key === user.newsletter ? 'selected' : '' }}>{{ value }}</option>
                    {% endfor %}
                </select>
            </div>
        </div>

        <div class="media {{ pending ? 'color-a4a9b2': 'color-555555' }}">
            <div class="pull-right">
                <div class="community-checkbox {{ pending ? 'community-checkbox-pending' : ''  }}">
                    <input type="checkbox" name="subscription" id="subscription" {{ user.subscription === '1' ? 'checked' : '' }}>
                    <label for="subscription"></label>  
                </div>
            </div>
            <div class="media-body">
                <h4 class="no-margin"><label for="subscription" class="weight-normal">{{ t._('email-notifications') }}</label></h4>
                <p class="font-15 wide-line-height"><label for="subscription" class="weight-normal">{{ t._('email-notifications-paragraph') }}</label></p>
            </div>
        </div>
        <div class="sp-block-group padding-top-lg margin-top-lg h5">
            <input type="submit" value="{{ t._('save-changes') }}" class="btn-reset bg-57b25f white radius-5 submit-button pull-right">
        </div>
        {{ endForm() }}
    </div>
</div>

{{ assets.outputJs() }}


<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-56411923-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
// voting circle canvas
    if (($(".naqeshny-canvas")).length) {

        $(".naqeshny-canvas").each(function (index, obj) {
            $(obj).debateVersus();
        });
    }
</script>
</body>
</html>
