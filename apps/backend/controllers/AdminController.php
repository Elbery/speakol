<?php
namespace Speakol\Backend\Controllers;
class AdminController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
    }
    public function indexAction() {
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            return $this->response->redirect('apps/login?redirect_url=admin/index');
        }
        $this->currentUserIs(array('2', '3'), 'dashboard');
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $userSession = $this->session->get('app_data');
        if ($this->request->isPost()) {
            $newsfeedParagraph = $this->request->getPost('newsfeed_paragraph');
            if (!$newsfeedParagraph) {
                $this->flash->error($this->t->_('invalid-newsfeed-paragraph'));
                return $this->response->redirect('admin');
            }
            $params = array('description' => $newsfeedParagraph, 'speakol_lang' => $this->request->getPost('speakol_lang'),);
            $response = $this->utility->iCurl('/speakol/intro/update', 'POST', $hdrs, $params, false, false, true);
            if ($response->status !== 'OK') {
                $this->flash->error($response->message);
            } else {
                $this->flash->success($response->message);
            }
        }
        $langs = array("en" => "English", "ar" => "العربية");
        $response = $this->utility->iCurl('/speakol/intro/show', 'GET', $hdrs, array());
        $this->view->introParagraph = $response->data;
        $this->view->langs = $langs;
    }
}
