<table class="body-wrap" style="width: 100%; margin: 0; padding: 0;"><tr style="margin: 0; padding: 0;"><td style="margin: 0; padding: 0;"></td>
        <td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; border-radius: 10px; margin: 0 auto; padding: 0;">
            <div class="content" style="max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table style="width: 100%; margin: 0; padding: 0;"><tr style="display: table; width: 100%; margin: 0 0 10px; padding: 0;">
                        <td style="margin: 0; padding: 0;">
                            <p style="font-weight: normal; font-size: 14px; line-height: 1.6; color: #4c4c4c; margin: 0 0 10px; padding: 0;">
                                {{ t._('hi') }} <span style="margin: 0; padding: 0;">{{ t._('spkl-admin') }}</span>, </p>
                            <h5 style="line-height: 1.1; color: #4c4c4c; font-weight: 700; font-size: 20px; margin: 0 0 15px; padding: 0;">
                                {{ contact_name}} | {{ contact_subject }}
                            </h5>
                            <p>
                                Sender Email: {{ contact_email }}
                                <br>
                                {{ contact_message }}
                            </p>
                        </td>
                    </tr></table></div><!-- /content -->

        </td>
        <td style="margin: 0; padding: 0;"></td>
    </tr>
</table>
<!-- /BODY -->
