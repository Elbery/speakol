(function() {
    var Highlight = function() {
        this.lang = phpVars.lang;
        this.bindEvents();
    };

    Highlight.Events = {
        '.js-highlight': 'highlight',
    };

    Highlight.prototype.bindEvents = function() {
        var _this = this;
        $.each(Highlight.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    Highlight.prototype.highlight = function($el) {
        var type = $el.attr('data-type'),
            pluginId = $el.attr('data-plugin-id'),
            isHighlighted = $el.hasClass('js-highlighted'),
            url = '',
            method= '',
            text = '',
            data = {};
        if(isHighlighted) {
            url = '/newsfeed/unhighlight/' + pluginId + '?type=' + type;
            method = 'DELETE';
            text = 'Highlight';
        } else {
            url = '/newsfeed/highlight/' + pluginId;
            method = 'POST';
            text = 'Highlighted';
            data = { type: type };
        }
        $.ajax({
            url: url,
            type: method,
            data: data,
            dataType: 'json',
        }).done(function(data) {
            if(data.status === 'OK') {
                if(isHighlighted) {
                    $el.addClass('bg-white color-666c77 highlight-action border-666c77')
                    .removeClass('js-highlighted bg-666c77 white unhighlight-action');
                    
                } else {

                    $el.addClass('js-highlighted bg-666c77 white unhighlight-action')
                    .removeClass('bg-white color-666c77 highlight-action border-666c77');
                }
                $el.text(text);
            }
        }).fail(this.errorHandler.bind(this));
    };

    Highlight.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler();
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.message,
            403: response.messages,
            404: t.rnf,
            409: t.ec,
            500: response.messages
        };
        $('.js-normal-submit input,textarea').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(errorMsgs[status]);
        $('#errorbox').modal('show');
    };

    var highlight = new Highlight();
})();
