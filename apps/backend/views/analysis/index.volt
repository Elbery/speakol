{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': t._('analysis') ]  %}
        <div class="row margin-top-sm">
            <div class="col-sm-9">
                <ul class="nav nav-pills font-13">
                    <li><span>{{ t._('show')  }}: </span></li>
                    <li><a class="font-13" href="{{ linkfor }}">{{ t._('all')  }}</a></li>
                    <li class="active"><a class="font-13" href="{{linkforfilter}}debate">{{ t._('debates') }}</a></li>
                    <li class="active"><a class="font-13" href="{{linkforfilter}}comparison">{{ t._('comparisons') }}</a></li>
                    <li class="active"><a class="font-13" href="{{linkforfilter}}argumentbox">{{ t._('argumentboxes')  }}</a></li>
                </ul>
            </div>
            <div class="col-sm-3 font-13">
                <span> Sort By:</span>
                <div class="dropdown">
                    <button class="btn font-13 btn-default no-padding-height padding-bottom-xs dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                        {% set order = request.get('order')  %}
                        {{ order=== 'name' ? 'Name' : 'Date (Newest)' }}
                        <span class="caret font-13"></span>
                    </button>
                    <ul class="dropdown-menu font-13 dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{linkfororder}}created_at">Date (Newest)</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{linkfororder}}name">Name</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            
                {% if analysis['data'] is defined and analysis['data'] %}
                    <ul>
                        {% for item in analysis['data'] %}
                        <li>
                            <div class="row">
                                <div class="col-sm-1">
                                    {% if item['data']['module'] == "comparison" %}
                                        {% set class = "speakol-comparison"  %}
                                    {% elseif item['data']['module'] == "debate" %}
                                        {% set class = "speakol-debate"  %}
                                    {% elseif item['data']['module'] == "argumentsbox" %}
                                        {% set class = "speakol-argumentsbox"  %}
                                    {% endif %}
                                    <i class="speakol-icon  {{ class  }}"></i>
                                </div>
                                <div class="col-sm-6">
                                    <a class="h4 weight-bold" href="#">{{ item['data']['title'] ? item['data']['title'] : t._('no-title')}}</a>
                                    <p class="font-13 margin-top-xs">
                                        <time>{{ item['data']['created_at'] }}</time>
                                    </p>
                                </div>
                                <div class="col-sm-2">
                                    <p class="font-20 weight-bold">{{ item['counts']['total_votes'] }}</p>
                                    <p class="h6 uppercase">{{ t._('votes') }}</p>
                                </div>
                                <div class="col-sm-2">
                                    <p class="font-20 weight-bold">{{ item['counts']['total_comments'] }}</p>
                                    <p class="h6 uppercase">{{ t._('interactions') }}</p>
                                </div>
                            </div>
                        </li>
                        {% endfor %}
                    </ul>
                {% else %}
                    <div class="row text-center">
                        <h2>{{ t._('no-data') }}!</h2>
                    </div>
                {% endif %}
        </div>
        <div class="row">
            {% if analysis['extra']['hasmore'] == true %}
            <a class="h5 more_cont no-radius col-sm-6 col-sm-offset-3 padding-top-lg padding-bottom-lg weight-bold light-blue border-none bg-e8e8e8" href="{{ nextp }}">{{ t._('shw-nxt') }} {{ perpage }} {{ t._('items') }}</a>
            {% endif %}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
