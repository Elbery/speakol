<li>
    <article>

        <div class="feed_item_content">

            <ul class="actions_buttons">
                <li>
                    <a class="delete_button remove_comparison"  data-comparison_slug="<?php echo $item['data']['slug']; ?>"  href="#" title="Delete">Delete</a>
                </li>
            </ul> 
            
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>



            <h1 class="item_title">
                <a href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $item['data']['title']; ?></a>
            </h1>

            <div class="items_compared">
                <ul class="comparison_items clearfix">
                    <?php foreach ($item['sides'] as $side) { ?>
                        <li>
                            <div class="item_info clearfix">
                                <div class="nameAndpercent clearfix">
                                    <div class="item_photo">
                                        <?php if (isset($side['image']) && $side['image']) { ?>
                                            <?php echo $this->tag->image(array($this->config->application->webservice . $side['image'])); ?>
                                        <?php
    } else { ?>
                                            <?php echo $this->tag->image(array('img/symbol.png')); ?>
                                        <?php
    } ?>
                                    </div>
                                </div>
                                <div class="item_name">
                                    <h2><?php echo $side['title']; ?></h2>
                                </div>
                                <div class="item_vote_btn"><a href="#" class="Support"> <?php echo $side['percentage']; ?>%</a></div>
                            </div>
                        </li>
                    <?php
} ?>
                </ul>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

                <a class="interact" href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $t->_('join-this-comparison'); ?></a>
            </div>
        </div>
    </article>
</li>


<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'comparisons/render?app=' . $this->session->get('app_data')->user->app->id . '&comparison=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>