<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar_brand">{% set speakolLogo = image("img/symbol.png", "alt": "Speakol", "title": "Speakol") %}{{ linkTo('index', speakolLogo) }}</li>
    </ul>
</nav>
