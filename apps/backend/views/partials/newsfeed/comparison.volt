<div class="sp-block-group feed-item radius-5">
    {% include 'partials/newsfeed/plugin-header.volt' %}
    <div class="sp-block-group">
        <h2 class="font-20 light-blue"><i class="speakol-icon speakol-comparison-green feed-item-icon"></i>&nbsp;{{ plugin.data.title }}</h2>
    </div>
    <div class="sp-block-group margin-top-lg margin-bottom-lg">
        {% set classes = [ '2' : 'six-xl', '3' : 'four-xl', '4': 'three-xl']%}
        {% for side in plugin.sides %}
            <div class="sp-block margin-bottom-xs margin-top-xs {{ loop.length < 5 and plugin.data.align ? classes[loop.length] : '' }}">
                <div class="{{ loop.length < 5 and plugin.data.align ? 'width-90' : '' }} margin-center bg-f5f5f5 radius-5 community-comparison">
                    <div class="media">
                        {% if side.side_data.image %}
                            {% if loop.length < 5 and plugin.data.align %}
                            <div class="{{ lang === 'ar' ? 'comparison-image-right' : 'comparison-image-left' }}">
                            {% else %}
                            <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
                            {% endif %}
                                <img src="{{ side.side_data.image}}" alt="" class="profile-picture-xl radius-5"/>
                            </div>
                        {% endif %}
                        <div class="media-body">
                            {% if loop.length < 5 and plugin.data.align %}
                            <div class="sp-block-group margin-bottom-xs community-comparison-title">
                            {% else %}
                            <div class="sp-block-group margin-bottom-xs {{ lang === 'ar' ? 'text-right' : 'text-left' }}">
                            {% endif %}
                                    <h4 class="font-20 light-blue no-margin margin-bottom-sm">{{side.side_data.title}}</h4> 
                            </div>
                            <div class="{{ lang === 'ar' ? 'flip' : '' }}  {{ loop.length < 5 and plugin.data.align ? 'margin-center-xl' : '' }}">
                                <div class="comparison-progress-container">
                                    <div class="comparison-progress bg-green relative">
                                        <div 
                                            class="comparison-progress-indicator bg-ececec js-progress-bar"
                                            style="left: {{ side.perc }}%;"
                                        >
                                            <div 
                                                class="comparison-progress-text bg-green white radius-5 text-center {{ lang === 'ar' ? 'flip' : '' }}"
                                            >
                                                {{ side.perc }}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {% if loop.length < 5 and plugin.data.align %}
                            <div class="sp-block-group h6 padding-top-sm community-comparison-votes">
                            {% else %}
                            <div class="sp-block-group h6 padding-top-sm {{ lang === 'ar' ? 'text-right' : 'text-left' }}">
                            {% endif %}
                                <span class="" data-side-id="{{ side.id }}">{{ side.votes }}</span>&nbsp;{{  t._('votes') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        {% endfor %}
    </div>
    <div class="sp-block-group text-center">
        <a href="/newsfeed/comparison?slug={{ plugin.data.slug }}&app={{ plugin.app.id }}&lang={{ lang }}" class="{{ isLoggedIn ? '' : 'js-login-button' }} anchor-reset margin-center inline-block btn-default conversation-button bg-white radius-5 border-acb6bb">{{ t._('join-conversation') }}</a>
    </div>
</div>
