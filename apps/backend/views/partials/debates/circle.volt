<div class="voting_circle">
    <canvas class="naqeshny-canvas" data-lang="{{ lang }}" width="80" height="80">
        <div class="data" style="display:none;">
            {
            "pro-color": "#166e16",
            "con-color": "#980a0b",
            {% for side in debate.sides %}
                {% if loop.first %}
                    "pro-votes": {{side.votes}},
                {% else %}
                    "con-votes": {{side.votes}}
                {% endif %}
            {% endfor %}
            }

        </div>
    </canvas>
</div>
<div class="pros_cons_percentbars clearfix">
    {% for side in debate.sides %}
        {% if loop.first %}
            <div class="pros_percentbar">
                <span class="percent">{{side.perc}}%</span>
                <span class="number">  <strong>{{side.votes}}</strong> {{ t._('') }}  </span>
                <div class="pros_percent" style="width: {{side.perc}}%;"></div>
            </div>
        {% else %}
            <div class="cons_percentbar">
                <span class="percent">{{side.perc}}%</span>
                <span class="number"> <strong>{{side.votes}}</strong> {{ t._('') }}</span>
                <div class="cons_percent" style="width: {{side.perc}}%;"> </div>
            </div>
        {% endif %}
    {% endfor %}
</div>
{#<div class="prtcpnts_cont">
    <div class="pros_prtcpnts">
        <div class="prtcpnts_imgs_cont">
            <span class="img_circl">
                <a href=""><img title="User Name" alt="" src="img/user.jpg"></a>
            </span>
            <span class="img_circl">
                <a href=""><img title="User Name" alt="" src="img/user.jpg"></a>
            </span>
            <span class="img_circl">
                <a href=""><img title="User Name" alt="" src="img/user.jpg"></a>
            </span>
        </div>
        <div class="prtcpnts_info">
            <a class="othr_frnds" href="#">and 3 friends agree</a>
            <a class="invt_mor" href="#">Invite more</a>
        </div>
    </div>
    <div class="cons_prtcpnts">
        <div class="prtcpnts_info">
            <a class="othr_frnds" href="#">None of your friends disagrees yet</a>
            <a class="invt_mor" href="#">Invite them to participate</a>
        </div>
    </div>
</div>#}
