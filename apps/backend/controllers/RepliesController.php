<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Controllers\BaseController;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use \Phalcon\Tag as Tag;
class RepliesController extends BaseController {
    public function deleteAction($replyId) {
        $token = null;
        if ($this->getToken()) {
            $token = $this->getToken();
        } else {
            $token = $this->getAppToken();
        }
        $ad = $this->request->get('ad') === 'true' ? true : false;
        $url = "";
        if ($ad) {
            $url = "/ads/replies/$replyId/delete";
        } else {
            $url = "/replies/$replyId/delete";
        }
        $result = $this->utility->iCurl($url, 'DELETE', array('Authorization:' . $token), array(), true, true);
        if (strtoupper($result['status']) == "OK") {
            $this->response->setJsonContent(array('status' => 'success', 'data' => $result['data']));
        } else if (strtoupper($result['status']) == "ERROR") {
            $this->response->setJsonContent(array('status' => 'error'));
        }
        return $this->response;
    }
}
