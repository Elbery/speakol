(function() {
    var DebatePlugin = function() {
        this.url =  phpVars.weburl + 'apps/api/';
        this.lang = phpVars.lang;
        this.audio = $('#audioEnabled').length;
        this.recorder = speakolRecorder;
        this.timer = new Timer(45, undefined, function() {
            this.recorder.stop();
            this.timer.$display.parents('.js-record-bar').first().find('.js-recording-text').addClass('hide');
            var finishedText = this.timer.$display.attr('data-finished');
            this.timer.$display.html(finishedText);
        }.bind(this));
        this.bindEvents();
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                jqXHR.settings = settings;
            }
        });
    };


    DebatePlugin.Events = {
        '.js-vote': 'vote',
        '.js-unvote': 'unvote',
        '.js-submit-arg': 'submitArg',
        '.js-record-button': 'startRecording',
        '.js-cancel-record': 'cancelRecording',
        '.js-send-record': 'sendRecording'
    };


    function updateVotes(votes) {
        var pros = parseInt(votes[0]['votes']);
        var cons = parseInt(votes[1]['votes']);
        var canvasData = {
            "pro-votes": pros,
            "con-votes": cons,
            "pro-color": '#4dac54',
            "con-color": '#d11b1f'
        };
        $('.js-voting-circle .js-data').text(JSON.stringify(canvasData));
        $('.js-voting-circle').debateVersus();
        var total = votes.reduce(function(prev, next) {
            var prevVote = +prev.votes;
            var nextVote = +next.votes;
            return prevVote + nextVote;
        });
        votes.forEach(function(vote) {
            $('.js-side-votes[data-side-id="'+ vote.side_id  + '"]').text(vote.votes);
            if(total) {
                vote.votes = +vote.votes;
                var percentage = Math.round((vote.votes * 100) / total) + '%';
            } else {
                percentage = '0%';
            }
            $('.js-side-percentage[data-side-id="'+ vote.side_id  + '"]').text(percentage);
            var $progressBar = $('.js-progress-bar[data-side-id="' + vote.side_id + '"]');
            $progressBar.css('width', percentage)
            .find('.js-progress-sr')
            .text(percentage);
        });
    }


    DebatePlugin.prototype.submitArg = function($el, e, audio) {
        audio = audio || false;
        var $argumentForm = $el.parents('.js-argument-form'),
            debateId = $argumentForm.find('input[name="debate_id"]').val(),
            context = $argumentForm.find('textarea[name="comment"]').val(),
            file = $argumentForm.find('input[name="attached"]').prop('files')[0],
            lang = this.lang,
            url = this.url,
            sideId = '',
            audioEnabled = this.audio;
        $argumentForm.find('textarea, input, button').prop('disabled', true);
        var formData = new FormData();
        formData.append('context', context);
        var file;
        if(audio) {
            if(this.recorder.checkIfNoAudio()) {
                var message = lang === 'ar' ? 'لا يوجد صوت' : 'No audio detected';
                this.showErrorMsg(message);
                return;
            }
            formData.append('audio', 1);
            file = this.recorder.exportOgg(); 
        } else {
            file = $argumentForm.find('input[name="attached"]').prop('files')[0];
            if(!!file && file.size > 512 * 1024) {
                var message = lang === 'ar' ? 'يجب أن يكون حجم الصورة أقل من 512 كيلوبايت' : 'Image size must be smaller than 512 Kilobytes';
                this.showErrorMsg(message);
                return;
            }
        }
        formData.append('debate_id', debateId);
        formData.append('file', file);
        formData.append('action', '/arguments/debates/insert');
        formData.append('link', phpVars.url ? phpVars.url : '');
        formData.append('lang', lang);
        $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: formData
        }).then(function(data) {
            if(data.status === 'OK') {
                $argumentForm.find('textarea, input, button').prop('disabled', false);
                $argumentForm.find('textarea[name="comment"]').val('');
                var argumentId = data.data.argument_id;
                sideId = data.data.side_id;
                var right = $('.js-side[data-side-id="' + sideId + '"]').attr('data-align') === 'right';
                return $.ajax({
                    url: '/arguments/render/argument',
                    type: 'GET',
                    data: {
                        'argument_id': argumentId,
                        'lang': lang,
                        'right': right,
                        'audio': audioEnabled
                    }
                });

            }
        }).done(function(data) {
            data = '<li>' + data + '</li>';
            $('.js-side[data-side-id="' + sideId +'"]')
            .find('.js-arguments')
            .prepend(data);
            addReadMore();
        }).fail(this.errorHandler.bind(this));
    };

    DebatePlugin.prototype.startRecording = function($el) {
      var $argumentForm = $el.parents('.js-argument-form'),
          $recordBar = $argumentForm.find('.js-record-bar'),
          $normalSubmit = $argumentForm.find('.js-normal-submit'),
          $timer = $recordBar.find('.js-timer');
      $normalSubmit
      .addClass('hide')
      .find('input, button')
      .prop('disabled', true);
      $recordBar
      .removeClass('hide')
      .find('button')
      .prop('disabled', false);
      $recordBar.find('.js-recording-text').removeClass('hide');
      this.timer.setDisplay($timer);
      var start = function() {
          this.timer.set();
          this.recorder.record();
      }.bind(this);

      if(this.recorder.initialized) {
          if(this.recorder.recording) {
              this.recorder.stop();
          }
          start();
      } else {
          this.recorder.init(start);
      }

    };

    DebatePlugin.prototype.cancelRecording = function($el) {
        removeRecordBar($el);
        this.recorder.stop();
        this.timer.reset();
    };


    function removeRecordBar($el) {
        var $argumentForm = $el.parents('.js-argument-form'),
            $recordBar = $argumentForm.find('.js-record-bar'),
            $normalSubmit = $argumentForm.find('.js-normal-submit');
      $normalSubmit
      .removeClass('hide')
      .find('input, button')
      .prop('disabled', false);
      $recordBar
      .addClass('hide')
      .find('button')
      .prop('disabled', true);
    }


    DebatePlugin.prototype.sendRecording = function($el, e) {
        $el.text('Sending....');
        this.recorder.stop();
        this.timer.reset();
        setTimeout(function() {
            $el.text('Send');
            removeRecordBar($el);
            if(this.recorder.error) {
                var errorMsg = this.lang === 'ar' ? 'لم تقم بالسماح للميكروفون بالعمل' : 'You haven\'t allowed the microphone';
                this.showErrorMsg(errorMsg); 
            } else {
                this.submitArg($el, e, true);
            }
        }.bind(this), 200);
    };



    function getUrl() {

        var results = new RegExp('[\?&]url=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        } else{
            return decodeURIComponent(results[1]) || window.location.href;
        }
    }

    DebatePlugin.prototype.vote = function($el) {

        var debateId = $el.attr('data-debate-id'),
            sideId =$el.attr('data-side-id'),
            lang = this.lang,
            url = this.url,
            debateUrl = $("#url").val();
        if(!debateUrl) {
            debateUrl = getUrl();
            debateUrl = encodeURIComponent(debateUrl);
            $("#url").val(debateUrl);
        }
        $el.prop('disabled', true);
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                'action': '/vote',
                'type': 'debate',
                'id': debateId,
                'side_id': sideId,
                'lang': lang,
                'url': debateUrl,
                link: phpVars.url ? phpVars.url : ''
            }
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                var votes = data.data;
                updateVotes(votes);
                var $argumentForm = $('.js-argument-form[data-side-id="'+ sideId  +'"]');
                var $unvoteContainer = $('.js-unvote-container[data-side-id="'+ sideId  +'"]');
                $('.js-unvote-container').addClass('hide');
                $('.js-argument-form').addClass('hide');
                $('.js-argument-form').addClass('opacity-0');
                $('.js-vote').removeClass('hide');
                $unvoteContainer.removeClass('hide');
                ramjet.transform($el.get(0), $unvoteContainer.get(0), {
                    done: function() {
                        $unvoteContainer.removeClass('hide');
                        setTimeout(function() {
                            $argumentForm.removeClass('opacity-0');
                        }, 1);
                    },
                    easing: quadInOutEasing,
                    duration: 300

                });
                $el.addClass('hide');
                $unvoteContainer.addClass('hide');
                $argumentForm.removeClass('hide');
                


            }
        }).fail(this.errorHandler.bind(this));
    };

    DebatePlugin.prototype.unvote = function($el) {
        var debateId = $el.attr('data-debate-id'),
            sideId =$el.attr('data-side-id'),
            lang = this.lang,
            url = this.url;
        $el.prop('disabled', true);
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: '/unvote',
                method: 'DELETE',
                side_id: sideId,
                id: debateId,
                type: 'debate',
                lang: lang,
                link: phpVars.url ? phpVars.url : ''
            }
        }).done(function(data) {
            if(data.status === 'OK') {
                $el.prop('disabled', false);
                var votes = data.data;
                updateVotes(votes);
                var $argumentForm = $('.js-argument-form[data-side-id="'+ sideId  +'"]');
                var $unvoteContainer = $el.parents('.js-unvote-container');
                var $voteButton = $('.js-vote[data-side-id="' + sideId  + '"]');
                $('.js-unvote-container').addClass('hide');
                $('.js-argument-form').addClass('hide');
                $('.js-argument-form').addClass('opacity-0');
                $('.js-vote').removeClass('hide');
                $unvoteContainer.removeClass('hide');
                ramjet.transform($unvoteContainer.get(0), $voteButton.get(0), {
                    done: function() {
                        $voteButton.removeClass('hide');
                        setTimeout(function() {
                            $argumentForm.addClass('hide');
                        }, 1);
                    },
                    easing: quadInOutEasing,
                    duration: 300

                });
                $voteButton.addClass('hide');
                $unvoteContainer.addClass('hide');
                $argumentForm.removeClass('opacity-0');

            }
        }).fail(this.errorHandler.bind(this));
    };

    function addReadMore() {
        $(".readmore").readmore('destroy');
        $('.readmore').readmore({ 
            embedCSS: false, 
            collapsedHeight: 200,
            moreLink: '<a href="#" class="font-13">more</a>',
            lessLink: '<a href="#" class="font-13">Close</a>'
        });

    }
    DebatePlugin.prototype.bindEvents = function() {
        var _this = this;
        $.each(DebatePlugin.Events, function(key, value) {
            $(document).on('click', key,function(e) {
                e.preventDefault();
                var $el = $(this);
                _this[value]($el, e);
            });
        });
    };

    DebatePlugin.prototype.errorHandler = function(jqXHR, textStatus, errorThrown) {
        var status = jqXHR.status;
        if(status === 401) {
            this.unauthorizedHandler(jqXHR.settings);
            return;
        }
        var response = JSON.parse(jqXHR.responseText);
        var errorMsgs = {
            302: t.uhav,
            400: response.message,
            403: response.message,
            404: t.rnf,
            409: t.ec,
            500: response.message
        };
        
        this.showErrorMsg(errorMsgs[status]);
    };


    DebatePlugin.prototype.showErrorMsg = function(message) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        $('#errorbox').modal('hide');
        $('#errorbox #error-message').html(message);
        $('#errorbox').modal('show');
    }


    DebatePlugin.prototype.unauthorizedHandler = function(ajaxSettings) {
        $('.js-normal-submit input, .js-normal-submit button, textarea, .js-vote, .js-unvote').prop('disabled', false);
        if(ajaxSettings) {
            var lastAction = {
                url: ajaxSettings.url,
                type: ajaxSettings.type,
                dataType: ajaxSettings.dataType,
                data: ajaxSettings.data,
                processData: ajaxSettings.processData,
                contentType: ajaxSettings.contentType,
            };

            localStorage.setItem('lastAction', JSON.stringify(lastAction));
        }
        window.parent.postMessage(JSON.stringify({
            'id': 'iframeMessenger:dfjdkfjd',
            'type': 'popup'
        }), '*');
    };

    function quadInOutEasing (t) {
        t /= 0.5;
        if (t < 1) return 0.5*t*t;
        t--;
        return -0.5 * (t*(t-2) - 1);
    }
    var debatePlugin = new DebatePlugin();

})();
