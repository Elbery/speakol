
<div class="tab-pane fade active in debates" id="debates">
    <?php if (!$debates) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No debates created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($debates as $item) { ?>
        <div class="debate">
            <li>
    <article>

        <div class="feed_item_content">

            <ul class="actions_buttons">
                <li>
                    <a class="delete_button remove_debate" href="#" data-debate_slug="<?php echo $item['data']['slug']; ?>" title="Delete">Delete</a>
                </li>
            </ul>
            
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>

            <h1 class="item_title">
                <a href="#"><?php echo $item['data']['title']; ?></a>
            </h1>
            <div class="users_debating">
                <span class="verses">Vs</span>
                <div class="user_one">
                    <div class="img_circl_container">
                        <span class="img_circl">
                            <a href="<?php echo $this->url->get('user/profile/' . $item['sides']['0']['slug']); ?>">
                                <?php if (isset($item['sides']['0']['image']) && $item['sides']['0']['image']) { ?>
                                    <?php echo $this->tag->image(array($this->config->application->webservice . $item['sides']['0']['image'])); ?>
                                <?php
    } else { ?>
                                    <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                                <?php
    } ?>
                            </a>
                        </span>
                    </div>

                    <div class="comment_area">
                        <span class="nameAndDate">
                            <b>
                                <?php echo $item['sides']['0']['title']; ?>
                            </b>
                        </span>
                        <p><?php echo $item['sides']['0']['opinion']; ?>
                            <a href="<?php echo $this->url->get('debates/view/' . $item['data']['slug']); ?>"> read more</a> </p>
                    </div>
                </div>
                <div class="user_two">
                    <div class="img_circl_container">
                        <span class="img_circl">
                            <a href="">
                                <?php if (isset($item['sides']['1']['image']) && $item['sides']['1']['image']) { ?>
                                    <?php echo $this->tag->image(array($this->config->application->webservice . $item['sides']['1']['image'])); ?>
                                <?php
    } else { ?>
                                    <?php echo $this->tag->image(array('img/user_default.gif')); ?>
                                <?php
    } ?>
                            </a>
                        </span>
                    </div>
                    <div class="comment_area">
                        <span class="nameAndDate">
                            <b>
                                <?php echo $item['sides']['1']['title']; ?>
                            </b>
                        </span>
                        <p><?php echo $item['sides']['1']['opinion']; ?>
                            <a href="<?php echo $this->url->get('debates/view/' . $item['data']['slug']); ?>"> read more</a> </p>
                    </div>
                </div>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

            </div>
        </div>
    </article>
</li>

<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'debates/render?app=' . $this->session->get('app_data')->user->app->id . '&debate=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
        </div>
    <?php
} ?>
</div>

<div class="tab-pane fade active in comparisons" id="comparisons">
    <?php if (!$comparisons) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No comparisons created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($comparisons as $item) { ?>
        <div class="comparison">
            <li>
    <article>

        <div class="feed_item_content">

            <ul class="actions_buttons">
                <li>
                    <a class="delete_button remove_comparison"  data-comparison_slug="<?php echo $item['data']['slug']; ?>"  href="#" title="Delete">Delete</a>
                </li>
            </ul> 
            
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>



            <h1 class="item_title">
                <a href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $item['data']['title']; ?></a>
            </h1>

            <div class="items_compared">
                <ul class="comparison_items clearfix">
                    <?php foreach ($item['sides'] as $side) { ?>
                        <li>
                            <div class="item_info clearfix">
                                <div class="nameAndpercent clearfix">
                                    <div class="item_photo">
                                        <?php if (isset($side['image']) && $side['image']) { ?>
                                            <?php echo $this->tag->image(array($this->config->application->webservice . $side['image'])); ?>
                                        <?php
        } else { ?>
                                            <?php echo $this->tag->image(array('img/symbol.png')); ?>
                                        <?php
        } ?>
                                    </div>
                                </div>
                                <div class="item_name">
                                    <h2><?php echo $side['title']; ?></h2>
                                </div>
                                <div class="item_vote_btn"><a href="#" class="Support"> <?php echo $side['percentage']; ?>%</a></div>
                            </div>
                        </li>
                    <?php
    } ?>
                </ul>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

                <a class="interact" href="<?php echo $this->url->get('comparisons/view/' . $item['data']['slug']); ?>"><?php echo $t->_('join-this-comparison'); ?></a>
            </div>
        </div>
    </article>
</li>


<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'comparisons/render?app=' . $this->session->get('app_data')->user->app->id . '&comparison=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
        </div>
    <?php
} ?>
</div>

<div class="tab-pane fade active in argumenstbox" id="argumenstbox">
    <?php if (!$argboxes) { ?>
        <div class="blc_content clearfix">
            <ul class="ul_vert item_typs">
                <div class="cont_num">
                    <small>No argumentsbox created!</small>
                </div>
            </ul>
        </div>
    <?php
} ?>
    <?php foreach ($argboxes as $item) { ?>
        <div class="argumentsbox">
            <li class="feed_item_arg">
    <article>

        <div class="feed_item_content">
            <ul class="actions_buttons">
                <li><a class="delete_button" href="#" title="Delete">Delete</a></li>
            </ul>
            <a class="retrieve_code" href="#" data-target="#get_code_<?php echo $item['data']['id']; ?>" data-toggle="modal">Retrieve Code</a>

            <h1 class="item_title">
                <a target="blank" href="<?php echo $item['data']['url']; ?>"><?php echo $item['data']['recomended']['title']; ?></a>
            </h1>
            <div class="user_commented">
                <p>
                    <?php echo $item['data']['description']; ?>
                    <a target="blank" href="<?php echo $item['data']['url']; ?>">read more</a>
                </p>
            </div>
            <div class="feed_time clearfix">
                <time><?php echo $item['data']['created_at']; ?></time>

            </div>
        </div>
    </article>
</li>

<div>
    <div class="modal fade" id="get_code_<?php echo $item['data']['id']; ?>" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="raw_code_container">
                        <div><span>Please choose your content language:</span>
                            <select id="arg-box-lang" name="arg-box-lang">
                                <option value="en">English</option>
                                <option value="ar">اللغه العربيه</option>
                            </select>
                        </div>
                        <pre id="gnrtd_code">
<div class="speakol-debate-box">
&lt;iframe src="<?php echo $this->config->application->webhost . 'debates/render?app=' . $this->session->get('app_data')->user->app->id . '&debate=' . $item['data']['slug']; ?>" data-lang="en" width="680" height="680"&gt;&lt;/iframe&gt;
</div>
                        </pre>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form_row">
                        <!--<span class="alert-success"> Copied </span>-->
                        <a href="javascript:" class="btn blue_btn cp-generated-code">Copy Generated Code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>
    <?php
} ?>
</div>

<?php if ($hasMore['all']) { ?>
    <a  
        data-all="<?php echo $hasMore['all']; ?>" 
        data-type="all" 
        data-debate="<?php echo $hasMore['debate']; ?>" 
        data-comparison="<?php echo $hasMore['comparison']; ?>" 
        data-argumentbox="<?php echo $hasMore['argumentsbox']; ?>" 
        data-url="/moderates/index" 
        data-ajax-url="/moderates/index?type=all" 
        class="pag-types pagination more_cont" href="#" data-page="1">Show next</a>
<?php
} ?>