    {% include "partials/debates/header" with ['debate':debate] %}


    <!--content of argument box here-->
    <div class="argument_box_content">
        <section class="usr_action">
            {% include "partials/debates/vote" with ['debate':debate] %}
            {% set currentUserVoted = '' %}
            {% for side in debate.sides %}
                {% set currentUserVoted = 0 %}
                {% if side.voted %}
                    {% set currentUserVoted = 1 %}
                    {% break %}
                {% endif %}
            {% endfor %}

            <div class="comment_on_action clearfix{{ currentUserVoted ? '' : ' hide' }}">
                <div class="img_circl_container">
                    <span class="img_circl">

                        {% if LoggedInUserData.profile_picture %}
                            <a href="">{{ image(config.application.webservice~LoggedInUserData.profile_picture) }}</a>
                        {% else %}
                            <a href="">{{ image("img/user_default.gif") }}</a>
                        {% endif %}
                    </span>
                </div>
                <div class="comment_area">
                    <div class="tA_container">
                        <textarea name="comment" class="write_comment" placeholder="{{ t._('choose-side') }}"></textarea>
                        {{ hidden_field("debate_id", "value": debate.id) }}
                    </div>
                    <p class="vwrs_num" style="display:none; background: #53a960;margin: 6px 0 0 0;padding: 2px 17px;color: #fff;border-radius: 5px;"></p>
                    <input type="button" value="{{ t._('post-now') }}" class="submitbtn" />
                    <div class="arg_box_photo" style="width: 40px;height: 40px;float: right;margin-top: 8px;margin-right: 11px;display: none;">
                        <img src="#" alt="">
                    </div>
                    {{ file_field("attached", "class":"add_img",'accept':'.png,.jpeg,.jpg,.gif') }}
                </div>
            </div>
            <br/>
        </section>
        <section class="argumentBox_comments clearfix">
            {% include "partials/debates/circle" with ['debate':debate] %}
            {% include "partials/debates/sides" with ['debate':debate] %}
        </section>
    </div>


