{% if admin_mail %}

{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': t._('admin-settings') ]  %}
        <div class="row margin-top-lg">
            {{ form('admin', 'method': 'post', 'enctype': 'multipart/form-data', 'class': 'form-horizontal col-sm-11 color-686a6c') }}
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{ t._('intro-paragraph')  }}</label>
                    <div class="col-sm-3">
                        {{ textArea({"newsfeed_paragraph", "class": "form-control no-radius font-15 no-resize no-indent debate-textarea", "value": introParagraph.feeds }) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 text-right margin-top-xs font-15">{{  t._('publisher-lang') }}</label>
                    <div class="col-sm-3">
                        {{ select("speakol_lang", langs, 'using': ['id', 'name'],  'class': 'form-control no-radius font-15 no-padding-height') }}
                    </div>
                </div>
                <div class="form-group">
                    {{ submitButton({"submit", "value": t._('update'), "class": "font-15 col-sm-2 col-sm-offset-3 btn blue_btn no-radius bg-light-blue"}) }}
                </div>
            {{ endForm() }}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>

{% endif %}
