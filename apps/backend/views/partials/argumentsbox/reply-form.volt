<div class="js-reply-form sp-block-group padding-top-sm padding-bottom-sm">
    <div class="media">
          <div class="{{ right or (lang === 'ar' and comparison) ? 'pull-right' : 'pull-left' }} hide-xxs">
                {% if LoggedInUserData.profile_picture is defined and LoggedInUserData.profile_picture %}
                    <img class="media-object profile-picture-sm radius-5" src="{{LoggedInUserData.profile_picture}}" alt="" title="">
                {% else %}
                    {{ image("img/user_default.gif" , 'class': 'media-object profile-picture-sm radius-5') }}
                {% endif %}
            </div>
            <div class="media-body">
            {% if adID is defined and adID  %}
                {{ form('', 'method': 'post', 'enctype':'multipart/form-data', 'data-ad' : 'true') }}
            {% else %}
                {{ form('', 'method': 'post', 'enctype':'multipart/form-data') }}
            {% endif %}
                <div class="sp-block-group margin-bottom-sm">
                    {{ text_area("context", "rows": "4",  "class":"no-resize width-all h5 no-indent padding-5 margin-center " ~ (lang === 'ar' ? 'rtl' : ''), "placeholder": t._('commentbox-placeholder')) }}
                </div>
                    <div class="sp-block-group text-right js-normal-submit">
                        {% if debate.audio or comparison.audio or argumentsbox.audio or audio %}
                        <button class="js-reply-record-button reply-record-button  shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm four font-13 sp-block">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                <i class="fa fa-microphone fa-stack-1x white"></i>
                            </span>
                            <span class="hide">Record Audio</span>
                        </button>
                        {% else %}
                        <div class="four-xs four-sm four font-13 sp-block">
                        </div>
                        {% endif %}
                        <div class="js-image-form">
                            <div class="sp-block  one-offset-xs two-xs three-sm three js-image-upload-container text-center-xs">
                                <span class="fileUpload inline-block btn-reset margin-top-xs">
                                    <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                    {% if LoggedInUserData %}
                                        {% set classes = 'js-image-upload' %}
                                    {% else %}
                                        {% set classes = 'js-image-loggedout' %}
                                    {% endif %}
                                    {{ file_field(
                                        "attached",
                                        "class": classes,
                                        "accept":".png,.jpeg,.jpg,.gif"
                                    ) }}
                                </span>
                            </div>
                            <div class="sp-block three one-offset-xs two-xs three-sm js-image-container hide">
                                <button class="btn-reset js-delete-image delete-image absolute"><i class="fa fa-times"></i></button>
                                <img src="" alt="" class="js-image profile-picture-sm">
                            </div>
                        </div>
            {% if adID is defined and adID %}
                {{ hidden_field("ad_id", "value": adID) }}
            {% else %}
                {{ hidden_field("argument_id", "value": argumentID) }}
            {% endif %}
            {% if parent_reply is defined %}
                {{ hidden_field("parent_reply", "value": parent_reply) }}
            {% endif %}
            {{ hidden_field("link", "value": argumentsBoxURL) }}
                        <div class="sp-block four one-offset">
                            {{ submit_button("post", "value": t._('reply'), "class":"js-submit-reply reply-button btn-reset font-13 btn-default bg-4e4e4e width-all font-small-xs") }}
                        </div>
                    
                    </div>

                    {% if debate.audio or comparison.audio or argumentsbox.audio or audio %}
                    <div class="js-record-bar hide  sp-block-group h5">
                            <div class="sp-block-group timer white margin-top-xs radius-5 bg-ec2300 padding-bottom-xs padding-top-xs text-center">
                                <i class="fa fa-microphone weight-bold"></i>
                                <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
                            </div>
                            <div class="sp-block-group margin-top-xs">
                                <button class="js-reply-send-record  six white btn-reset radius-5 padding-top-sm padding-bottom-sm bg-4e4e4e">{{ t._('send') }}</button>
                                <button class="js-reply-cancel-record pull-right two color-ec2300 btn-reset radius-5 padding-top-sm padding-bottom-sm bg-f1f1f1">
                                        &#10005;
                                </button>
                            </div>
                    </div>
                    {% endif %}
                    </form>
            </div>
        </div>
</div>
