{{ form('users/login','class':'login-box-form') }}

{#<div class="login_form">

    <div class="input-group">
        <span class="input-group-addon email_ico"></span>
        {{ textField({"email", "placeholder": t._('email-address'), "class": "form-control"}) }}
    </div>
    <div class="input-group">
        <span class="input-group-addon password_ico"></span>
        {{ passwordField({"password", "placeholder": t._('password'), "class": "form-control"}) }}
    </div>
    <div class="form_row">
        <input type="checkbox" id="rememberMe"/>
        <label for="rememberMe">  {{ t._('remember-me') }} </label>
        <a target="_blank" class="forgot" href="{{ url('apps/forgetpassword') }}">{{ t._('forgot-password') }}</a>
    </div>

    <div class="form_row">

    </div>

    <div class="form_row">
        {{ submitButton({t._('login'), "class": "grey_btn"}) }}
    </div>
</div>#}
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
<div class="social_connect">
    <a target="_blank" class="fb_login" data-name="Facebook-Window" href="{{ url('users/connect/facebook') }}"> {{ t._('login-with-facebook') }}</a>
    <a target="_blank" class="ggl_login" data-name= "Google-Window" href="{{ url('users/connect/google') }}"> {{ t._('login-with-google') }}</a>
    <a target="_blank" class="sp_login" data-name="Speakol-Window" href="{{ url('users/login') }}"> {{ t._('login-with-speakol') }}</a>
</div>
{{ endForm() }}
