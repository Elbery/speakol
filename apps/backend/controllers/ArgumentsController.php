<?php
namespace Speakol\Backend\Controllers;
class ArgumentsController extends BaseController {
    public function deleteAction($argumentId) {
        $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
        $result = $this->utility->iCurl("/arguments/$argumentId/delete", 'DELETE', $hdrs, array(), false, true);
        if (strtoupper($result['status']) == "SUCCESS") {
            $this->response->setJsonContent(array('status' => 'success', 'data' => true));
        } else if (strtoupper($result['status']) == "ERROR") {
            $this->response->setJsonContent(array('status' => 'error'));
        }
        return $this->response;
    }
}
