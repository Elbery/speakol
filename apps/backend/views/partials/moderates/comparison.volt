<li class="js-comparison">

    <div class="row thin-underline margin-top-sm">
        <div class="col-xs-1">
            {% if appImage %}
                {{ image(appImage, "class": "img-circle media-object") }}
            {% else %}
                {{ image("img/user_default.gif", "class": "img-circle media-object") }}
            {% endif %}
        </div>
        <div class="col-xs-11">
            <div class="row">
                <div class="col-xs-11">
                    {% if planID and planID <= 1 %}
                        <h4 class="font-20 black weight-bold" target="_blank" ><i class="speakol-icon speakol-comparison"></i> {{ item['data']['title'] }}</h4>
                    {% else %}
                        <a href="{{url('comparisons/edit?slug=' ~ item['data']['slug']) }}" class="font-20 black weight-bold" target="_blank" ><i class="speakol-icon speakol-comparison"></i> {{ item['data']['title'] }}</a>
                    {% endif %}
                </div>
                <div class="col-xs-1 text-right">
                    <button class="btn btn-default bg-white remove_comparison border-none no-radius" data-comparison-slug="{{ item['data']['slug']  }}"><i class="fa fa-times red"></i></button>
                </div>
            </div>
            <div class="row color-7f8080 margin-bottom-sm">
                <ul>
                    {% for side in item['sides'] %}
                        {% if loop.length == 5  %}
                            {% set class = "col-xs-2"  %}
                        {% else %}
                            {% set class = "col-xs-" ~ 12/loop.length  %}
                        {% endif %}
                        <li>
                            <div class=" {{ class }} text-center">
                                <div class="row">
                                    {% if side['image'] is defined and side['image'] %}
                                        {{ image( side['image'], "class": "img-circle comparison-img") }}
                                    {% else %}
                                        {{ image("img/symbol.png", "class" : "img-circle comparison-img") }}
                                    {% endif %}
                                </div>
                                <div class="row">
                                    <h3 class="h5">{{ side['title']}}</h3>
                                </div>
                                <div class="row">
                                    <div class="btn font-13">
                                        <i class="fa fa-thumbs-o-up"></i>&nbsp;{{ side['percentage'] }}%
                                    </div>
                                </div>
                            </div>
                        </li>
                    {% endfor %}
                </ul>
            </div>
            <div class="row color-7f8080 font-13">
                <div class="col-xs-10 text-left">
                    <time>{{ item['data']['created_at'] }}</time> 
                </div>
                <div class="col-xs-2 text-right">
                    <p>
                        <i class="fa fa-comment"></i>&nbsp;{{ item['counts']['total_comments']}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</li>
