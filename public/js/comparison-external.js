var parent;

(function($) {
    if (typeof (ComparisonExternal) === "undefined") {
        /**
         * 
         * @type _L22@pro;S|_L22@pro;settings
         */
        var _this;
        /**
         * 
         * @type _L22@pro;S
         */
        var S;
        /**
         * 
         * @type type
         */
        var ComparisonExternal = {
            /**
             * 
             */
            settings: {
                comparisonContainer: '.comparison_container',
                submitBtn: '.comparison_container .comparison_items li .usr_action .comment_on_action .comment_area .sbmt-arg',
                replyForm: '.comment_unit .reply .reply_container .comment_area form',
                replyBtn: '.item_comments .comment_unit .replies_actions .reply .reply_container .usr_action .comment_on_action .comment_area form.tA_container input.subcomment_submit',
                socialContainer: '.social_shares'
            },
            /**
             * 
             * @returns {undefined}
             */
            init: function() {
                // constructor
                _this = this;
                S = _this.settings;
                _this.bindUIActions();
            },
            /**
             * 
             * @returns {undefined}
             */
            assginSocialUrls: function(){
                
                window.parent.postMessage(JSON.stringify({
                    'id': 'iframeMessenger:ldsklk',
                    'type' : 'parent-url'
                }),'*');

                window.addEventListener('message' , function(e) {
                    var message = JSON.parse(e.data);
                    if(message.type === 'parent-url') {
                        var parent_url = message.parent_url;
                        if ($(S.socialContainer).length && parent && parent_url) {
                            $(S.socialContainer).find('a.social-change').click(function (e) {
                                e.preventDefault();
                                var strUrl = $(this).attr('href');
                                var strWindowName = 'facebook-window';
                                var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,height=450,width=500,left=450,top=220";
                                var windowObjectReference = window.open(strUrl, strWindowName, strWindowFeatures);
                            });
                        }
                    }
                });
            },
            bindUIActions: function() {
                $(document).ready(function() {
                    
                    function inIframe () {
                        try {
                            return window.self !== window.top;
                        } catch (e) {
                            return true;
                        }
                    }
                    
                    if(inIframe()){
                        window.parent.postMessage(JSON.stringify({
                            'id': 'iframeMessenger:ldsklk',
                            'type' : 'init'
                        }),'*');
                        window.addEventListener('message', function(e) {
                            try {
                                var message = JSON.parse(e.data);
                                if(message.id && message.indexOf('iframeMessenger') > -1 && message.type === 'init') {
                                window.parent_data.title = message.title;
                                window.parent_data.image = message.image;
                                window.parent_data.parent_url = message.parent_url;
                                }
                            } catch(e) {

                            }
                        });
                        
                        $(S.comparisonContainer).voteComparison();
                        $(S.submitBtn).submitArgComparison(); 
                        $(document).MiscClass({element: $(this)}).ArgumentsClass({element: $(this)}).RepliesClass({element: $(this)}).NestedRepliesClass({element: $(this)});
                        $(S.replyForm).submitReplyComparison();
                        _this.assginSocialUrls();
                        // var comm = new CrossDomainFragment({
                        //     onReceive: function (message) {
                        //         parent = message;
                        //     }
                        // });
                    }else{
                        
                        parent = {
                            parent_url : '',
                            title: '',
                            image: '',
                        }
                        $(S.comparisonContainer).voteComparison();
                        $(S.submitBtn).submitArgComparison(); 
                        $(document).MiscClass().ArgumentsClass({element: $(this)}).RepliesClass({element: $(this)}).NestedRepliesClass({element: $(this)});
                        $(S.replyForm).submitReplyComparison();
                        parent.parent_url = document.URL;
                        _this.assginSocialUrls();
                    }
                    
                    
                });
            },
            Sides: {
            },
            Arguments: {
            },
            Replies: {
            },
            Misc: {
            }
        };
        ComparisonExternal.init();
    }
}(jQuery));
