<div class="sp-block-group js-argument argument thin-underline" data-arg-id="{{ argument.id }}" data-owner-id="{{ argument.owner_id  }}">
    <div class="sp-block-group no-margin">
        <div class="media">
          <div class="{{ right ? "pull-right" : "pull-left" }}">
                {% if argument.owner_photo is defined and argument.owner_photo != "" %}
                    <a href="javascript:"><img class="media-object profile-picture-sm radius-5" src="{{config.application.webservice~ argument.owner_photo}}" alt="" title=""></a>
                {% else %}
                    <a href="javascript:">{{ image("img/user_default.gif" , 'class': 'media-object profile-picture-sm radius-5') }}</a>
                {% endif %}
          <div class="media-body">
                {% set statusColor = (argument.owner_status) ? "color-53a960" : "color-bebebe" %}
                {% if right %}
                    {% set headingContent = '<i class="fa fa-circle-o font-11 hide-xxs ' ~ statusColor ~  '"></i>&nbsp;' ~ argument.owner_name %} 
                {% else %}
                    {% set headingContent = argument.owner_name ~  '&nbsp;<i class="fa fa-circle-o font-11 hide-xxs ' ~ statusColor ~  '"></i>' %} 
                {% endif %}
                {% if argument.url %}
                <a href="{{ utility.isFullURL(argument.url) ?   argument.url : 'http://' ~ argument.url }}" target="_blank" class="media-heading h5 light-blue display-block {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                </a>
                {% else %}
                <h4 class="media-heading h5 light-blue {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                </h4>
                {% endif %}
                <p class="font-11 color-aebac1 {{ right ? "text-right" : "" }}">{{ argument.created_at }}</p>
          </div>
        </div>
    </div>
    <div class="sp-block-group no-margin">
        <p class="readmore font-13">{{ utility.insertHashtagHTML(argument.context) }}</p>
        {% for file in argument.attached_photos %}
            {{ image(file.url, "class":"img-responsive") }}
        {% endfor %}
    </div>

    <div class="sp-block-group text-center margin-bottom-sm h6 js-argument-actions">
        <div class="sp-block four-xs three-sm two vertical-separator-right">
            <button data-argument-id="{{ argument.id }}" class="js-thumbs-up btn-reset {{ (argument.currentUserhasVoted and argument.currentUserVote) ? "js-voted color-5bb767" : "color-c1cacf"  }}" title="{{ t._('vote_up') }}" data-thumbs="{{ argument.thumbs_up }}">
                <i class="fa fa-thumbs-up"></i>&nbsp;
                <span>{{ argument.thumbs_up}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm two">
            <button data-argument-id="{{ argument.id }}" class="js-thumbs-down btn-reset {{ (argument.currentUserhasVoted and !argument.currentUserVote) ? "js-voted color-d11b1f" : "color-c1cacf"  }}" title="{{ t._('vote_down') }}" data-thumbs="{{ argument.thumbs_down }}">
                <i class="fa fa-thumbs-down"></i>&nbsp;
                <span>{{ argument.thumbs_down}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm three-offset-sm two six-offset-lg">
            <a class="color-c1cacf js-show-replies" href="javascript:"><span>{{argument.replies_count}}</span>&nbsp;<i class="fa fa-comment"></i></a>
        </div>
    </div>
    {% set argumentsBoxURL = "" %}
    {% if argument.replies is defined %}
        {% include "partials/argumentsbox/replies-container" with ['argumentID': argument.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies': argument.replies, 'replies_count' : argument.replies_count, 'total_replies_pages': argument.total_replies_pages is defined?argument.total_replies_pages:0 ] %}
    {% else %}
        {% include "partials/argumentsbox/replies-container" with ['argumentID': argument.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies_count' : argument.replies_count, 'total_replies_pages': argument.total_replies_pages is defined?argument.total_replies_pages:0] %}
    {% endif %}
</div>
