{% if adID is defined and adID %}
<div class="js-replies-container transition-opacity opacity-0 {{ right or (lang === 'ar' and comparison) ? 'one-offset-right' : 'one-offset' }} hide {{ comparison ? 'bg-f7f7f7 padding-10' : '' }}" data-comments-page="1" data-total-replies-pages="{{total_replies_pages}}" data-ad-id="{{ adID }}">
{% else %}
<div class="js-replies-container transition-opacity opacity-0 {{ right or (lang === 'ar' and comparison) ? 'one-offset-right' : 'one-offset' }} hide {{ comparison ? 'bg-f7f7f7 padding-10' : '' }}" data-comments-page="1" data-total-replies-pages="{{total_replies_pages}}" data-argument-id="{{ argumentID }}">
{% endif %}
        <!-- <span class="tip" style="left: 316px;"></span> -->

    {% if adID is defined and adID %}
        {% include "partials/argumentsbox/reply-form" with ['adID': adID, 'argumentsBoxURL': argumentsBoxURL] %}
    {% else %}
        {% include "partials/argumentsbox/reply-form" with ['argumentID': argumentID, 'argumentsBoxURL': argumentsBoxURL] %}
    {% endif %}

    <div class="sp-block-group">
        <ul class="js-replies">
            {% if adID is defined and adID %}
                {% include "partials/argumentsbox/replies" with ['replies': replies, 'adID': adID, 'argumentsBoxURL': argumentsBoxURL] %}
            {% else %}
                {% include "partials/argumentsbox/replies" with ['replies': replies, 'argumentID': argumentID, 'argumentsBoxURL': argumentsBoxURL] %}
            {% endif %}
        </ul>
    </div>
    {% if replies_count and replies_count > 10 %}
        <div class="{{ lang ==='ar' ? 'rtl' :  ''}} sp-block-group text-center margin-bottom-sm padding-top-sm">
            <button class=" js-more-replies  bg-647a88 font-13 radius-5 inline-block width-78 padding-top-xs padding-bottom-xs white border-none" data-loading="{{ t._('loading') }}" {{ adID is defined and adID ? 'data-ad="true"' : '' }}>{{ t._('show-more') }} </button>
        </div>
    {% endif %}
</div>
