var t,hasMore;

/* avoid conflicts with other versions or libraries */
jQuery.noConflict();
/* reserve $ for JQuery */
(function ($) {
    /******************************************************************************************
     ******************************************************************************************
     * Class Misc
     * 
     * Multi purpose functions which is not falling under any category
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     ******************************************************************************************/
    if (typeof (MiscClass) === "undefined")
    {
        var _selfM,
                SM,
                SCM;

        /**
         * 
         * @returns {_L29.ArgumentsClass}
         */
        var MiscClass = function (_options) {
            _selfM = this;
            this.defaults = {
                // These are the defaults.
                selectors: {
                    voteActions: '.vote_action',
                    voteUp: '.votes_up',
                    voteDn: '.votes_down',
                    repThumbUpBtn: '.reply_votes_up',
                    repThumbDnBtn: '.reply_votes_down',
                    ClassArgVoted: 'voted',
                    thumbUpBtn: '.arg_votes_up',
                    thumbDnBtn: '.arg_votes_down',
                    fblogin: '.not_logged_tb .facebook_icon, .fb_login',
                    glogin: '.not_logged_tb .gPlus_icon, .ggl_login',
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             */
            this.init();
        };
        /**
         * 
         * @returns {undefined}
         */
        MiscClass.prototype.init = function ()
        {
            _selfM.settings = $.extend(true, {}, _selfM.defaults, _selfM.options);
            SM = _selfM.settings;
            SCM = _selfM.settings.selectors;
            _selfM.assignEvents();
        };

        /**
         *
         * @returns {undefined}
         */

        MiscClass.isDisabled = function (elem) {
            if($(elem).hasClass('element-disabled')){
                return true;
            }else{
                $(elem).addClass('element-disabled');
                return false;
            }
        },
        MiscClass.enableDisabled = function (elem) {
            if($(elem).hasClass('element-disabled')){
                $(elem).removeClass('element-disabled');
            }
        },
        MiscClass.errrorHandler = function (text) {
            $('#errorbox').modal('hide');
            $('#errorbox #error-message').html(text);
            $('#errorbox').modal('show');
        },
        MiscClass.unauthorizedHandler = function(elem) {
            MiscClass.enableDisabled(elem);
            window.parent.postMessage(JSON.stringify({
                'id': 'iframeMessenger:dfjdkfjd',
                'type': 'popup'
            }), '*');
        },

        MiscClass.addReadMore = function() {
            $('.readmore').readmore('destroy');
            $('.readmore').readmore({ embedCSS: false, collapsedHeight: 200 });
        },


        


        /**
         * 
         * @param {type} _argumentID
         * @returns {ArgumentsClass.dataObj|_L29.ArgumentsClass.dataObj|data}
         */
        MiscClass.prototype.assignEvents = function () {
                $('.social_connect a').click(function () {

                    if (_this.isDisabled(this)) {
                        return false;
                    }

                    var strUrl = $(this).attr('href');
                    var strWindowName = $(this).attr('data-name');
                    var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,height=500,width=500,left=450,top=220";
                    var windowObjectReference = window.open(strUrl, strWindowName, strWindowFeatures);
                    return false;
                });
        };
        /**
         * 
         */
        MiscClass.prototype.recalculateThumbs = function (objElement, boolIncr) {
            var numThumbs = parseInt(objElement.text());
            objElement.text(++numThumbs);
            if (boolIncr) {
                // If has voted to thumb down
                // decrement thumbs down
                var downObj = objElement.parents(SCM.voteActions).first().find(SCM.voteDn).first();
                if (downObj.hasClass(SCM.ClassArgVoted)) {
                    var downCnts = parseInt(downObj.text());
                    downObj.text(--downCnts);
                }
            } else {
                //if has voted to thumb up
                // decrease thumbs up
                var upObj = objElement.parents(SCM.voteActions).first().find(SCM.voteUp).first();
                if (upObj.hasClass(SCM.ClassArgVoted)) {
                    var upCnts = parseInt(upObj.text());
                    upObj.text(--upCnts);
                }
            }
        };
        /**
         * 
         * @type String|String|String|String
         */
        MiscClass.prototype.switchArgThumbsClasses = function (objElment) {
            objElment.parents(SCM.voteActions).first().find(SCM.thumbUpBtn).first().removeClass(SCM.ClassArgVoted);
            objElment.parents(SCM.voteActions).first().find(SCM.thumbDnBtn).first().removeClass(SCM.ClassArgVoted);
            objElment.addClass(SCM.ClassArgVoted);
        };
        /**
         * 
         * @type String|String|String|String
         */
        MiscClass.prototype.switchRepThumbsClasses = function (objElement) {
            $(objElement.parents(SCM.voteActions).first().find(SCM.repThumbDnBtn).first()).removeClass(SCM.ClassArgVoted);
            $(objElement.parents(SCM.voteActions).first().find(SCM.repThumbUpBtn).first()).removeClass(SCM.ClassArgVoted);
            objElement.addClass(SCM.ClassArgVoted);
        };
        /**
         *
         * @type String|String
         */
        MiscClass.appendToCommentsContainerCallBack = function (_data, _oEle)
        {
            $(_oEle).prepend(_data);
            if($(_data).find('.context_img').length) {
                $('.vwrs_num').text(t.ids);
            } else {
                $('.vwrs_num').text(t.ads);
            }
            $(".vwrs_num").hide();
            $(".vwrs_num").fadeIn();

            setTimeout(function () {
                $(".vwrs_num").fadeOut();
            }, 3000);

        };
        /**
         * 
         */
        $.fn["MiscClass"] = function (options) {
            var pluginName = "MiscClass";
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(
                            this,
                            'plugin_' + pluginName,
                            new MiscClass(options)
                            );
                }
            });
        };
    }
    /******************************************************************************************
     ******************************************************************************************
     * Class argumentsClass
     * 
     * The 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     ******************************************************************************************/
    if (typeof (ArgumentsClass) === "undefined")
    {
        var _selfAC,
                SAC,
                SCAC;
        /**
         * 
         * @returns {_L29.ArgumentsClass}
         */
        var ArgumentsClass = function (_options) {
            if(typeof _options === "undefined") {
                _options = {
                    element: $(this)
                };
            }
            _selfAC = this;
            this.dataObj;
            this.misc = new MiscClass(); // composition
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                    type: 'POST',
                    data: '',
                    success: function (data, textStatus, jqXHR) {
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    statusCode: {
                        200: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            if (data.status == 'OK') {
                            }
                        },
                        302: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        403: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        404: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        401: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.unauthorizedHandler(_options.element);
                        },
                        409: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(t.emt);
                        }
                    },
                    dataType: 'JSON'
                },
                selectors: {
                    divComments: '.comment_unit',
                    reportBtn: '.report-arg-btn',
                    muteBtn: '.mute-arg-btn',
                    thumbUpBtn: '.arg_votes_up',
                    thumbDnBtn: '.arg_votes_down',
                    argumentUnit: 'ul .comment_unit',
                    repliesContainerArg: '.replies-container',
                    btnRepliesParent: '.reply_container',
                    btnReplies: '.reply_button',
                    argIDAttr: 'data-arg-id',
                    reply: '.reply',
                    moreArgsComments: '.showArgsComments',
                    voteSideIDAttr: 'data-side-id',
                    pageAttr: 'data-comments-page',
                    perPage: 4,
                    appendableContainer: '.appndbl_comnts_cont',
                    totalPagesAttr: 'data-total-pages'
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             */
            this.init();
        };
        /**
         * 
         * @returns {undefined}
         */
        ArgumentsClass.prototype.init = function ()
        {
            _selfAC.settings = $.extend(true, {}, _selfAC.defaults, _selfAC.options);
            SAC = _selfAC.settings;
            SCAC = _selfAC.settings.selectors;
            _selfAC.assignEvents();
        };
        /**
         * 
         * @param {type} _argumentID
         * @returns {ArgumentsClass.dataObj|_L29.ArgumentsClass.dataObj|data}
         */
        ArgumentsClass.assginSocialUrls = function () {
            
            if($(SCAC.socialContainer).length && parent && parent.parent_url){
    
                $(SCAC.socialContainer).hide();
                
                $(S.socialContainer).find('a.social-change').click(function(e){
                    e.preventDefault();
                    var strUrl = $(this).attr('href');
                    var strWindowName = 'facebook-window';
                    var strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,height=450,width=500,left=450,top=220";
                    var windowObjectReference = window.open(strUrl, strWindowName, strWindowFeatures);
                });
                
                $(SCAC.socialContainer).find('a.social-change').each(function(){
                    var sUrl = $(this).attr('href').replace('[url]',parent.parent_url);
                    $(this).attr('href',sUrl);
                });
                
                $(SCAC.socialContainer).promise().done(function(){
                    $(SCAC.socialContainer).show();
                });
            }
        }
        ArgumentsClass.prototype.assignEvents = function () {

            $('[data-toggle="popover"]').popover();
            $('[data-toggle="tooltip"]').tooltip();

            $(SCAC.appendableContainer).delegate(SCAC.reportBtn, 'click', function () {
                
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                
                if (!$(this).hasClass('reported')) {
                    return _selfAC.report($(this).attr('href'), $(this));
                } else {
                    return _selfAC.unreport($(this).attr('href'), $(this));
                }
            });
            
            $(SCAC.appendableContainer).delegate(SCAC.muteBtn, 'click', function () {
                
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                
                if (!$(this).hasClass('is-muted')) {
                    return _selfAC.mute($(this).attr('href'), $(this));
                } else {
                    return _selfAC.unmute($(this).attr('href'), $(this));
                }
            });
            $(SCAC.appendableContainer).delegate(SCAC.thumbUpBtn, 'click', function () {
                return MiscClass.isDisabled(this) ?  false : _selfAC.thumbUp($(this), $(this).attr('href'));
            });
            $(SCAC.appendableContainer).delegate(SCAC.thumbDnBtn, 'click', function () {
                return MiscClass.isDisabled(this) ?  false : _selfAC.thumbDn($(this), $(this).attr('href'));
            });

            $(SCAC.appendableContainer).delegate(SCAC.btnReplies, 'click', function (e) {
                
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                
                e.preventDefault();
                var $this = $(this);
                var argumentID = $this.parents('[' + SCAC.argIDAttr + ']').first().attr(SCAC.argIDAttr);
                _selfAC.styleReplies($this);
                _selfAC.loadReplies(argumentID, $(this));

                return false;
            });

            $('.arg_box_photo img').attr('src', '');
            $('.arg_box_photo').hide();

            $('body').delegate(SCSAD.imgfield, 'change', function (e) {

                var preview = '.arg_box_photo img';
                var element = this;

                $('.arg_box_photo').attr('src', '');
                $('.arg_box_photo').hide();

                if (this.files && this.files[0]) {

                    if (!this.files[0].type.match('image.*')) {
                        MiscClass.errrorHandler(t.ufmbio);
                        $(element).val('');
                        return false;
                    }

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(element).parents('.comment_area').find(preview).attr('src', e.target.result);
                        $(element).parents('.comment_area').find('.arg_box_photo').show();
                    }

                    reader.readAsDataURL(this.files[0]);
                }
                return false;
            });

            $(document).on('click',SCAC.moreArgsComments, function () {
                
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                
                var pagBar = {};
                var elem = $(this);
                
                //$(this).hide();
                $(this).attr('data-old',$.trim($(this).text()));
                $(this).text('Loading..');
                $(this).parents('[' + SCAC.voteSideIDAttr + ']').each(function () {
                    if ($(this).attr(SCAC.pageAttr)) {
                        var params = new Object();
                        params.sideId = parseInt($(this).attr(SCAC.voteSideIDAttr));
                        params.page = parseInt($(this).attr(SCAC.pageAttr)) + 1;
                        params.limit = SCAC.perPage;
                        params.element = $(this);
                        params.showMore = elem;
                        _selfAC.paginateArguments(params);
                    }
                });

                $(this).parents('li').trigger('click');

                return false;
            });
            // ArgumentsClass.updateOnlineUsers();
            ArgumentsClass.addSortDropdownEvents();
        };
        /**
         * @access  static
         * @param {type} _argumentID
         * @returns {data}
         */

        ArgumentsClass.getAllSides = function() {
            var sideIDs = $('.comparison_items li[data-side-id]').map(function() {
                return $(this).attr("data-side-id");
            }).toArray();
            return sideIDs;
        };
        ArgumentsClass.updateOnlineUsers = function() {
            var params = {
                url: '/users/checkonlineusers',
                type: 'GET',
                dataType: 'json',
                data: {
                    sides: _this.getAllSides()
                },
                success: function(data, textStatus, jqXHR) {
                    if(data.data) {
                        $('.owner-status.border-green').removeClass('border-green').addClass('border-light-grey');
                        $('.owner-status-text.green').text('Offline');
                        $('.owner-status-text.green').removeClass('green').addClass('light-grey');
                        $.each(data.data, function(i, id) {
                            var ownerStatus = $('[data-owner-id=' + id  + '] .owner-status');
                            var ownerStatusText = $('[data-owner-id=' + id  + '] .owner-status-text'); 
                            ownerStatus.removeClass("border-light-grey").addClass("border-green");
                            ownerStatusText.removeClass("light-grey").addClass("green");
                            ownerStatusText.text("Online");
                        });

                    }
                },
            };
            setInterval(function() {
                $.ajax(params);
            }, 10000);
        };
        ArgumentsClass.addSortDropdownEvents =  function() {
            $(".sortby-dropdown .dropdown-menu a").on('click', function() {
                var $this = $(this);
                if($('.sort-order').attr('data-value') === $this.attr('data-value')) {
                    return;
                }

                var params = {};
                params.side_ids = $("li[data-side-id]").map(function() {
                    return $(this).attr('data-side-id');
                }).get();
                params.order_by = $this.attr('data-value');
                params.limit = 5;
                ArgumentsClass.changeSortOrder(params);

                $('.sort-order')
                .text($this.text())
                .attr('data-value', $this.attr('data-value'));
            });
        };
        ArgumentsClass.fetchArgument = function (_argumentID, callback, _oEle) {
            var returnable;

            var object = {
                url: '/arguments/render/argument',
                type: 'GET',
                data: {
                    'argument_id': _argumentID
                },
                success: function (data, textStatus, jqXHR) {
                    MiscClass.enableDisabled(_oEle);
                    callback(data, _oEle);
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.enableDisabled(_oEle);
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            
            $.ajax(object);
            return false;
        };
        /**
         * @access  static
         * @param {type} _argumentID
         * @returns {data}
         */
        ArgumentsClass.fetchArguments = function (_data, _callback, _ele) {
            var object = {
                url: '/arguments/render/arguments/',
                type: 'GET',
                dataType: 'json',
                data: {
                    'side_id': _data.sideId,
                    'page': _data.page,
                    'limit': _data.limit,
                    'order_by': $('.sort-order').attr('data-value')
                },
                success: function (data, textStatus, jqXHR) {
                    MiscClass.enableDisabled(_data.showMore);
                    _callback(_data, data, _ele);
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.enableDisabled(_data.showMore);
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            
            $.ajax(object);
            return false;
        };
        /**
         * 
         */
        ArgumentsClass.prototype.report = function (_argumentID, _ele) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/arguments/' + _argumentID + '/report'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.addClass('reported');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SAC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        ArgumentsClass.prototype.mute = function (_argumentID, _ele) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/arguments/' + _argumentID + '/mute'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.addClass('is-muted').addClass('follow_button').removeClass('mute_button');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SAC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        ArgumentsClass.prototype.unreport = function (_argumentID, _ele) {
            var object = {
                type: 'POST',
                url: phpVars.webhost + 'apps/api',
                data: {
                    'method': 'DELETE',
                    action: '/arguments/' + _argumentID + '/unreport'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.removeClass('reported');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SAC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        ArgumentsClass.prototype.unmute = function (_argumentID, _ele) {
            var object = {
                type: 'POST',
                url: phpVars.webhost + 'apps/api',
                data: {
                    'method': 'DELETE',
                    action: '/arguments/' + _argumentID + '/unmute'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.removeClass('is-muted').removeClass('follow_button').addClass('mute_button');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SAC.oAJAX, object));
            return false;
        };
        /**
         * 
         * @type String|String|String
         */
        ArgumentsClass.prototype.thumbUp = function (_objElement, _argumentID) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/arguments/' + _argumentID + '/thumb_up',
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_objElement);
                        if (data.status == 'OK') {
                            _selfAC.misc.recalculateThumbs(_objElement, true);
                            _selfAC.misc.switchArgThumbsClasses(_objElement);
                        }
                    },
                    302: function () {
                        MiscClass.enableDisabled(_objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                        _objElement.removeClass('voted');
                        var val = parseInt(_objElement.text());
                        val = val ? val - 1 : 0;
                        _objElement.text(val);
                    }
                }
            };

            $.ajax($.extend(true, {}, SAC.oAJAX, object));

            return false;
        };
        ArgumentsClass.prototype.thumbDn = function (_objElement, _argumentID) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/arguments/' + _argumentID + '/thumb_down'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_objElement);
                        if (data.status == 'OK') {
                            _selfAC.misc.recalculateThumbs(_objElement, false);
                            _selfAC.misc.switchArgThumbsClasses(_objElement);
                        }
                    },
                    302: function () {
                        MiscClass.enableDisabled(_objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                        _objElement.removeClass('voted');
                        var val = parseInt(_objElement.text());
                        val = val ? val - 1 : 0;
                        _objElement.text(val);
                    }
                }
            };
            $.ajax($.extend(true, {}, SAC.oAJAX, object));
            return false;
        };

        ArgumentsClass.prototype.loadRepliesCallBack = function (_data, data, _element) {

            $(SCAC.btnRepliesParent).each(function (i, e) {
                $(e).hide();
            });
            MiscClass.enableDisabled(_element);
            data = data.data;
            if(data) {
                $html = $("<div>" + data.html + "</div>");
                _element.find("div[class^='replies-parent_'],div[class*=' replies-parent_']").remove();
                _element.prepend($html.html()).show();
                _element.find('.comment_on_action').show();
            }
        };

        ArgumentsClass.prototype.loadReplies = function (_argumentID, _element) {
            MiscClass.enableDisabled(_element);
            var _ole = _element.siblings(SCAC.btnRepliesParent).first();
            params = {
                'argumentID': parseInt(_argumentID),
                'page': 1,
                'limit': 4
            };
            RepliesClass.fetchReplies(params, _selfAC.loadRepliesCallBack, _ole);
        };
        ArgumentsClass.prototype.loadNestedReplies = function (_replyParent, _argumentID, _element) {
            var object = {
                url: '/arguments/render/nested-replies/',
                type: 'GET',
                data: {
                    'parent_reply': _replyParent,
                    'argument_id': _argumentID
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_element);
                        if (data) {
                            _element.parents('.replies-parent_' + _replyParent).find(S.nestedReplyContiners).prepend(data);
                        }

                        _element.attr('data-old-text', _element.text());
                        _element.text('Hide ^');

                        _element.parents('.replies-parent_' + _replyParent).find('.reply-form-nested:first, .nested_comments').show();
                        MiscClass.addReadMore();
                    }
                }
            };
            $.ajax(object);
            return false;
        };
        /**
         * 
         * @type String|String|String|String
         */
        ArgumentsClass.prototype.styleReplies = function ($this) {
            var top = $this.offset().top - $this.parents('.comment_unit').offset().top + 30;
            var dims = _selfAC.dimensionsReplies($this);
            var popupContainer = $(this).parent().find('.reply_container');

            if ($this.parents('.left_pros_comments').length) {
                popupContainer.find('.tip').css({left: dims.posLeft - dims.offsetLeft});
            } else {
                popupContainer.find('.tip').css({left: dims.posLeft - dims.offsetLeft});
            }

            if (popupContainer.hasClass('active')) {
                $('.reply_container').removeClass('active');
            } else {
                $('.reply_container').removeClass('active');
                popupContainer.addClass('active');
            }
            popupContainer.css({top: top, width: dims.fullWidth - 30, right: dims.right, left: dims.left});
        };
        /**
         * 
         * @type String|String|String|String
         */
        ArgumentsClass.prototype.dimensionsReplies = function ($this) {
            var dims = {};
            if ($this.parents('.comparison_items').length) {
                dims = {
                    posLeft: $this.offset().left,
                    offsetLeft: $this.parents('.comparison_container > ul > li').first().offset().left,
                    right: '0',
                    left: '0',
                    fullWidth: $('.comparison_items').width()
                };
            } else if ($this.parents('.left_pros_comments').length) {
                dims = {
                    posLeft: $this.offset().left,
                    offsetLeft: $('.left_pros_comments').offset().left,
                    right: '0',
                    left: '0',
                    fullWidth: $('.argumentBox_comments').width()
                };
            } else {
                dims = {
                    posLeft: $this.offset().left,
                    offsetLeft: $('.argumentBox_comments').offset().left,
                    right: '0',
                    left: 'initial',
                    fullWidth: $('.argumentBox_comments').width()
                };
            }
            return dims;
        };

        /**
         *
         * @type String|String
         */
        ArgumentsClass.changeSortOrder = function(params) {
            var object = {
                url: '/arguments/changesort/',
                type: 'GET',
                dataType: 'json',
                data: params,
                success: function (data, textStatus, jqXHR) {
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.errrorHandler(jqXHR.responseText);
                },
                statusCode: {
                    200: function (data) {
                        // _this.enableDisabled(objElement);
                        data = data.data;
                        if (data) {
                            $('li[data-side-id] .item_comments').html("");
                            data.forEach(function(el, i) {
                                $html = $("<div>" + el.html + "</div>");
                                var side = $('li[data-side-id="' + el.side_id  + '"]');
                                if(!side.find('.showArgsComments').length && el.hasmore) {
                                    var showMore = '<a class="showArgsComments" href="javascript:">Show more</a>';
                                    side.find('.show_arg_container').append(showMore);
                                }
                                side.attr('data-comments-page', 1);
                                side.find('.item_comments').append($html.html());
                            });
                            // MiscClass.addReadMore();
                        }
                    },
                    302: function (data) {
                        MiscClass.enableDisabled(objElement);
                        MiscClass.errrorHandler(t.uhav);
                    },
                    400: function (data) {
                        MiscClass.enableDisabled(objElement);
                        MiscClass.errrorHandler(t.br);
                    },
                    403: function (data) {
                        MiscClass.enableDisabled(objElement);
                        MiscClass.errrorHandler(data.responseJSON.messages);
                    },
                    404: function (data) {
                        MiscClass.enableDisabled(objElement);
                        MiscClass.errrorHandler(t.rnf);
                    },
                    401: function (data) {
                        MiscClass.unauthorizedHandler(objElement);
                    },
                    409: function (data) {
                        MiscClass.enableDisabled(objElement);
                        MiscClass.errrorHandler(t.ec);
                    },
                    500: function (data) {
                        if (data.responseText.messages) {
                            MiscClass.enableDisabled(objElement);
                            MiscClass.errrorHandler(data.responseText.messages);
                        }
                    }
                }
            };

            $.ajax(object);
        };
        ArgumentsClass.paginateArgsCallBack = function (_data, data, _oEle)
        {
            
            MiscClass.enableDisabled(_oEle);
            data = data.data;
            if(data) {
                $html = $("<div>" + data.html + "</div>");
                $(_oEle).append($html.html());
                var side = $(_oEle).parents('[' + SCAC.voteSideIDAttr + ']').first();
                var curr_page = parseInt(side.attr(SCAC.pageAttr)) + 1;
                side.attr(SCAC.pageAttr, curr_page); // _data.page?? 
                if(!data.hasmore) {
                    _data.showMore.remove();
                } else {
                    _data.showMore.text(_data.showMore.attr('data-old'));
                }
            }
        };

        /**
         *
         * @param {type} _data {argument_id, page, limit, element}
         * @returns {Boolean}
         */
        ArgumentsClass.prototype.paginateArguments = function (_data) {
            MiscClass.enableDisabled(_data.element);
            var ele = _data.element.find(SCAC.appendableContainer).first();
            ArgumentsClass.fetchArguments(_data, ArgumentsClass.paginateArgsCallBack, ele);
            return false;
        };

        /**
         * 
         * 
         */
        $.fn["ArgumentsClass"] = function (options) {
            var pluginName = "ArgumentsClass";
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(
                            this,
                            'plugin_' + pluginName,
                            new ArgumentsClass(options)
                            );
                }
            });
        };
    }

    /******************************************************************************************
     ******************************************************************************************
     * Class repliesClass
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     ******************************************************************************************/
    if (typeof (RepliesClass) === "undefined")
    {
        var _selfRC,
                SRC,
                SCRC;
        /**
         * 
         * @returns {_L29.ArgumentsClass}
         */
        var RepliesClass = function (_options) {
            if(typeof _options === "undefined") {
                _options = {
                    element: $(this)
                };
            }
            _selfRC = this;
            this.dataObj;
            this.misc = new MiscClass(); // composition
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                    type: 'POST',
                    data: '',
                    success: function (data, textStatus, jqXHR) {
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    statusCode: {
                        200: function (data) {
                            MiscClass.enableDisabled(data.element);
                            if (data.status == 'OK') {
                            }
                        },
                        302: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        403: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        404: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        401: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.unauthorizedHandler(_options.element);
                        },
                        409: function (data) {
                            MiscClass.enableDisabled(_options.element);
                            MiscClass.errrorHandler(t.ec);
                        }
                    },
                    dataType: 'JSON'
                },
                selectors: {
                    divComments: '.comment_unit',
                    reportBtn: '.report-reply-btn',
                    muteBtn: '.mute-reply-btn',
                    thumbUpBtn: '.reply_votes_up',
                    thumbDnBtn: '.reply_votes_down',
                    argumentUnit: 'ul .comment_unit',
                    repliesContainerArg: '.replies-container',
                    btnRepliesParent: '.reply_container',
                    btnReplies: '.reply_button',
                    argIDAttr: 'data-arg-id',
                    reply: '.reply',
                    nestedComments: '.nested_reply_button',
                    parentReply: 'data-parent-reply',
                    replies_actions: 'replies_actions',
                    moreComments: '.comment_unit .replies_actions .reply .reply_container .showMore',
                    pageAttr: 'data-comments-page',
                    perPage: 4,
                    totalPagesAttr: 'data-total-replies-pages'
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             */
            this.init();
        };
        /**
         * 
         * @returns {undefined}
         */
        RepliesClass.prototype.init = function ()
        {
            _selfRC.settings = $.extend(true, {}, _selfRC.defaults, _selfRC.options);
            SRC = _selfRC.settings;
            SCRC = _selfRC.settings.selectors;
            _selfRC.assignEvents();
        };
        /**
         * 
         * @param {type} _argumentID
         * @returns {ArgumentsClass.dataObj|_L29.ArgumentsClass.dataObj|data}
         */
        RepliesClass.prototype.assignEvents = function () {
            $(SCRC.divComments).delegate(SCRC.reportBtn, 'click', function (event) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                event.preventDefault();
                if (!$(this).hasClass('reported-reply')) {
                    return _selfRC.report($(this).attr('href'), $(this));
                } else {
                    return _selfRC.unreport($(this).attr('href'), $(this));
                }
            });
            $(SCRC.divComments).delegate(SCRC.muteBtn, 'click', function (event) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                event.preventDefault();
                if (!$(this).hasClass('muted-reply')) {
                    return _selfRC.mute($(this).attr('href'), $(this));
                } else {
                    return _selfRC.unmute($(this).attr('href'), $(this));
                }
            });

            $(SCRC.divComments).delegate(SCRC.thumbUpBtn, 'click', function (event) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                event.preventDefault();
                event.stopPropagation();
                return _selfRC.thumbUp($(this), $(this).attr('href'));
            });
            $(SCRC.divComments).delegate(SCRC.thumbDnBtn, 'click', function (event) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                event.preventDefault();
                event.stopPropagation();
                return _selfRC.thumbDn($(this), $(this).attr('href'));
            });

            $(document).delegate(SCRC.moreComments, 'click', function () {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                var params = new Object();
                params.argumentID = $(this).parents('[' + SCRC.argIDAttr + ']').first().attr(SCRC.argIDAttr);
                params.page = parseInt($(this).parents('[' + SCRC.pageAttr + ']').first().attr(SCRC.pageAttr)) + 1;
                params.limit = SCRC.perPage;
                params.element = $(this);
                $(this).hide();
                if ($(this).hasClass('nested_show_more')) {
                    params.replyParent = $(this).attr(SCRC.parentReply);
                    params.page = parseInt($(this).attr(SCRC.pageAttr)) + 1;
                    NestedRepliesClass.paginateReplies(params);
                } else {
                    params.page = parseInt($(this).parents('[' + SCRC.pageAttr + ']').first().attr(SCRC.pageAttr)) + 1;
                    _selfRC.paginateReplies(params);
                }
                return false;
            });

//            $(window.parent.document).find('iframe.speakol').load(function () {
//                var height = parseInt(this.contentWindow.document.body.offsetHeight) + 35;
//                this.style.height = height + 'px';
//                $(this).css({overflow: 'hidden !important'})
//            });

            /* Nested */
            $(document).delegate(SCRC.nestedComments, 'click', function (e) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                e.preventDefault();

                var argumentID = $(this).parents('[' + SCRC.argIDAttr + ']').first().attr(SCRC.argIDAttr);
                var parentReply = $(this).attr(SCRC.parentReply);

                if (!$(this).attr('data-old-text')) {
                    $('.comment_unit_parent_' + parentReply).show();
                    var params = {
                        'replyParent': parentReply,
                        'argument_id': argumentID,
                        'page': 1,
                        'limit': 2
                    };
                    NestedRepliesClass.fetchReplies(params, NestedRepliesClass.fetchRepliesCallBack, $(this));
                } else {

                    $('.comment_unit_parent_' + parentReply).hide();
                    var $txt = $(this).attr('data-old-text');
                    $(this).text($txt);
                    $(this).removeAttr('data-old-text');
                }

                return false;
            });
        };
        /**
         * @access  static
         * @param {type} _argumentID
         * @returns {data}
         */
        RepliesClass.fetchReply = function (_replyID) {
            var object = {
                url: '/arguments/render/reply',
                type: 'GET',
                data: {
                    'reply_id': _replyID
                },
                success: function (data, textStatus, jqXHR) {
                    RepliesClass.dataObj = data;
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            $.ajax(object);
            return RepliesClass.dataObj;
        };
        /**
         * 
         */
        RepliesClass.prototype.report = function (_replyID, _ele) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/replies/' + _replyID + '/report'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.addClass('reported-reply');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        RepliesClass.prototype.mute = function (_replyID, _ele) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/replies/' + _replyID + '/mute'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.addClass('muted-reply').addClass('follow_button').removeClass('mute_button');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        RepliesClass.prototype.unreport = function (_replyID, _ele) {
            var object = {
                type: 'POST',
                url: phpVars.webhost + 'apps/api',
                data: {
                    'method': 'DELETE',
                    action: '/replies/' + _replyID + '/unreport'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.removeClass('reported-reply');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };
        /**
         * 
         */
        RepliesClass.prototype.unmute = function (_replyID, _ele) {
            var object = {
                type: 'POST',
                url: phpVars.webhost + 'apps/api',
                data: {
                    'method': 'DELETE',
                    action: '/replies/' + _replyID + '/unmute'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_ele);
                        if (data.status == 'OK') {
                            _ele.removeClass('muted-reply').removeClass('follow_button').addClass('mute_button');
                        }
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };
        /**
         * 
         * @type String|String|String
         */
        RepliesClass.prototype.thumbUp = function (_objElement, _replyID) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/replies/' + _replyID + '/thumb_up'
                },
                statusCode: {
                    200: function (data) {
                        if (data.status == 'OK') {
                            MiscClass.enableDisabled(_objElement);
                            _selfRC.misc.recalculateThumbs(_objElement, true);
                            _selfRC.misc.switchRepThumbsClasses(_objElement);
                        }
                    },
                    302: function () {
//                                DebateExternal.errrorHandler(t.uhav);
                        _objElement.removeClass('voted');
                        var val = parseInt(_objElement.text());
                        val = val ? val - 1 : 0;
                        _objElement.text(val);
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };
        RepliesClass.prototype.thumbDn = function (_objElement, _replyID) {
            var object = {
                url: phpVars.webhost + 'apps/api',
                data: {
                    action: '/replies/' + _replyID + '/thumb_down'
                },
                statusCode: {
                    200: function (data) {
                        MiscClass.enableDisabled(_objElement);
                        if (data.status == 'OK') {
                            _selfRC.misc.recalculateThumbs(_objElement, false);
                            _selfRC.misc.switchRepThumbsClasses(_objElement);
                        }
                    },
                    302: function () {
                        MiscClass.enableDisabled(_objElement);
//                                DebateExternal.errrorHandler(t.uhav);
                        _objElement.removeClass('voted');
                        var val = parseInt(_objElement.text());
                        val = val ? val - 1 : 0;
                        _objElement.text(val);
                    }
                }
            };
            $.ajax($.extend(true, {}, SRC.oAJAX, object));
            return false;
        };

        /**
         * @access  static
         * @param {type} _argumentID
         * @returns {data}
         */
        RepliesClass.fetchReply = function (_argumentID, _replyID, _callback, _ele) {
            var object = {
                url: '/arguments/render/reply',
                type: 'GET',
                data: {
                    'reply_id': _replyID,
                    'argument_id': _argumentID
                },
                success: function (data, textStatus, jqXHR) {
                    MiscClass.enableDisabled(_ele);
                    _callback(data, _ele);
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.enableDisabled(_ele);
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            $.ajax(object);
            return false;
        };

        /**
         * @access  static
         * @param {type} _argumentID
         * @returns {data}
         */
        RepliesClass.fetchReplies = function (_data, _callback, _ele) {
            var object = {
                url: '/arguments/render/replies/',
                type: 'GET',
                dataType: 'json',
                data: {
                    'argument_id': _data.argumentID,
                    'page': _data.page,
                    'limit': _data.limit
                },
                success: function (data, textStatus, jqXHR) {
                    MiscClass.enableDisabled(_ele);
                    _callback(_data, data, _ele);
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            $.ajax(object);
            return false;
        };

        /**
         *
         * @type String|String
         */ 
        RepliesClass.paginateRepliesCallBack = function (_data, data , _oEle)
        {
            MiscClass.enableDisabled(_oEle);
            data = data.data;
            if(data) {
                $html = $("<div>" + data.html + "</div>");
                $(_oEle).find(".usr_action").before($html.html());
            }
            var curr_page = parseInt($(_oEle).attr(SCRC.pageAttr)) + 1;
            $(_oEle).attr(SCRC.pageAttr, curr_page);
            var total_pages = $(_oEle).attr(SCRC.totalPagesAttr);
            if (data.hasmore) {
                $(SCRC.moreComments).show();
            }
        };

        /**
         *
         * @param {type} _data {argument_id, page, limit, element}
         * @returns {Boolean}
         */
        RepliesClass.prototype.paginateReplies = function (_data) {
            MiscClass.enableDisabled(_data.element);
            var ele = _data.element.parents(SCRC.btnRepliesParent).first();
            RepliesClass.fetchReplies(_data, RepliesClass.paginateRepliesCallBack, ele);
            return false;
        };

        /**
         * 
         * 
         */
        $.fn["RepliesClass"] = function (options) {
            var pluginName = "RepliesClass";
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(
                            this,
                            'plugin_' + pluginName,
                            new RepliesClass(options)
                            );
                }
            });
        };
    }

    /******************************************************************************************
     ******************************************************************************************
     * Class voteDecorator
     * 
     * This class encapsulate the common functionality of the voting action
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     ******************************************************************************************/
    if (typeof (voteDecorator) === "undefined") {
        var _self, // holds reference to this
                S, // settings
                SC; // settings selectors
        var voteDecorator = function (_pluginName, _element, _options) {
            _self = this;
            /**
             * 
             */
            this.name = _pluginName;
            /**
             * 
             */
            this.element = _element;
            /**
             * 
             * @type type
             */
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                    url: phpVars.webhost + 'apps/api',
                    data: {
                        action: '/vote',
                    },
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    beforeSend: function () {
                    },
                    statusCode: {
                        200: function (data) {

                        },
                        302: function (data) {
                            MiscClass.enableDisabled(_element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        403: function (data) {
                            MiscClass.enableDisabled(_element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        404: function (data) {
                            MiscClass.enableDisabled(_element);
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        401: function (data) {
                            MiscClass.enableDisabled(_element);
                            MiscClass.unauthorizedHandler(_element);
                        },
                        409: function (data) {
                            MiscClass.enableDisabled(_element);
                            MiscClass.errrorHandler(t.ec);
                        }
                    },
                    dataType: 'JSON'
                },
                selectors: {
                    voteContainerIDAttr: 'data-container-id',
                    voteSideIDAttr: 'data-side-id',
                    voteContainerTypeAttr: 'data-container-type',
                    classDimmed: 'dimmed',
                    sideSupportedClass: 'supported'
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             * @type Arguments
             */
            this.init();
        };
        /**
         * 
         * @returns {_L26.voteDecorator@pro;element@call;each}
         */
        voteDecorator.prototype.assignEvents = function () {
            return _self.element.each(function (i, e) {
                // Using return allows for chainability
                $(e).on('click', '.item_vote_btn .Support', {'inheritance': _self}, function (event) {
                    if(MiscClass.isDisabled($(this))){
                        return false;
                    }
                    event.preventDefault();
                    return _self.vote(event, _self);
                });
            });
        };
        /**
         * 
         * @returns {undefined}
         */
        voteDecorator.prototype.init = function () {
            _self.settings = $.extend(true, {}, _self.defaults, _self.options);
            S = _self.settings;
            SC = _self.settings.selectors;
            _self.assignEvents();
        };
        /**
         * 
         * @returns {Boolean}
         */
        voteDecorator.prototype.vote = function (event, _self) {
            
            S.target = event.target;
            S.oAJAX.data = 'type=' + $(event.target).attr(SC.voteContainerTypeAttr);
            S.oAJAX.data += '&id=' + $(event.target).attr(SC.voteContainerIDAttr)
            S.oAJAX.data += '&side_id=' + $(event.target).attr(SC.voteSideIDAttr);
            S.oAJAX.data += '&action=/vote';

            if (!$(event.target).hasClass(SC.classDimmed)) {
                $.ajax(_self.settings.oAJAX);
            }
            return false;
        };
        /**
         * 
         * @returns {undefined}
         */
        voteDecorator.prototype.switchClasses = function () {
            aler('xxxyyyzzz');
        };
        /**
         * 
         * @returns {undefined}
         */
        voteDecorator.prototype.recalculateVotes = function () {
            aler('xxxyyyzzz');
        };
        /**
         * 
         * @returns {undefined}
         */
        voteDecorator.prototype.updateJustVoted = function () {
            aler('xxxyyyzzz');
        };
    }

    /******************************************************************************************
     ******************************************************************************************
     * Class voteComparison
     * 
     * This is a decorator design pattern
     * It extends the base class "voteDecorator"
     * Then decorates it with functionality needed in comparisons
     * 
     * 
     * 
     * 
     * 
     * 
     *****************************************************************************************/
    $.fn["voteComparison"] = function (options) {
        var pluginName = "voteComparison";
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                var voteComparison = function () {
                };
                var voteComparisonSettings = $.extend(true, {}, options, {
                    oAJAX: {
                        statusCode: {
                            200: function (data) {
                                if (data.status == 'OK') {
                                    _self.recalculateVotes();
                                    _self.toggleArgForm();
                                }
                            }
                        }
                    },
                    selectors: {
                        voteSide: '.item_vote_btn .Support',
                        voteCnt: '.comparison_percentbar .number strong',
                        compItm: '.item_info',
                        sumAttr: 'data-sides-sum',
                        votePerc: '.comparison_percentbar .pros_percentbar .percent',
                        percMeter: '.pros_percent',
                        formCntnr: '.comparison_container .comparison_items li',
                        argForm: '.usr_action',
                        active: 'active'
                    }
                });
                voteComparison.prototype = new voteDecorator(pluginName, $(this), voteComparisonSettings);
                /**
                 * 
                 * @returns {undefined}
                 */
                voteComparison.prototype.switchClasses = function () {
                    $(SC.voteSide).each(function (i, e) {
                        $(e).removeClass(SC.classDimmed).removeClass(SC.sideSupportedClass).text(t.vote_this);
                        MiscClass.enableDisabled($(e));
                    });
                    $(S.target).first().addClass(SC.classDimmed).addClass(SC.sideSupportedClass).text(t.voted_this);
                };
                /**
                 * 
                 * @returns {undefined}
                 */
                voteComparison.prototype.recalculateVotes = function () {
                    var newSum = 0;
                    $(SC.voteCnt).each(function (i, e) {
                        var votes = parseInt($(e).text());
                        if ($(e).parents(SC.compItm).first().find(SC.voteSide).first().hasClass(SC.sideSupportedClass)) {
                            if (votes > 0)
                            {
                                $(e).text(--votes);
                            }
                        }
                        newSum += votes;
                    });
                    var obj = $(S.target).parents(SC.compItm).first().find(SC.voteCnt).first();
                    var vot = parseInt(obj.text());
                    obj.text(++vot);
                    newSum += 1;

                    $(SC.votePerc).each(function (ii, ee) {
                        $(ee).attr(SC.sumAttr, newSum);
                    });
                    _self.recalculatePerc();
                    _self.switchClasses();
                };
                /**
                 * 
                 * @returns {undefined}
                 */
                voteComparison.prototype.toggleArgForm = function () {
                    $(SC.argForm).each(function (i, e) {
                        $(e).removeClass(SC.active);
                    });
                    $(S.target).parents(SC.formCntnr).first().find(SC.argForm).first().show().addClass(SC.active);
                };
                voteComparison.prototype.recalculatePerc = function () {
                    var sum = parseInt($(SC.votePerc).first().attr(SC.sumAttr));
                    if (sum != 0)
                    {
                        $(SC.votePerc).each(function (i, e) {
                            var votes = parseInt($(e).parents(SC.compItm).first().find(SC.voteCnt).first().text());
                            var perc = Math.round(votes / sum * 100);
                            $(e).text(perc + "%");
                            $(e).siblings(SC.percMeter).first().css('width', perc + '%');
                        });
                    }
                };
                $.data(
                        this,
                        'plugin_' + pluginName,
                        voteComparison
                        );
            }
        });
    };
    /******************************************************************************************
     ******************************************************************************************
     * Class submitArgumentDecorator
     * 
     * A base class to submit an Argument
     * It encapsulates the common functionality of submitting an Argument
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     *****************************************************************************************/
    if (typeof (submitArgDecorator) === "undefined") {
        var _selfSAD, // holds reference to this
                SSAD, // settings
                SCSAD; // settings selectors
        var submitArgDecorator = function (_pluginName, _element, _options) {
            _selfSAD = this;
            /**
             * 
             */
            this.name = _pluginName;
            /**
             * 
             */
            this.element = _element;
            /**
             * 
             * @type type
             */
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                    url: phpVars.webhost + 'apps/api',
                    type: 'POST',
                    data: {
                        action: '/arguments/factory/make',
                    },
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    beforeSend: function () {
                        $(S.submitArg).attr('disabled', 'disabled');
                        $(S.submitArg).data('origin-text', $(S.submitArg).val());
                        $(S.submitArg).val(t.submt);
                    },
                    statusCode: {
                        200: function (data) {
                            MiscClass.enableDisabled(_element);
                            if (data.status == 'OK') {
                                $(S.submitArg).removeAttr('disabled');
                                $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                $(S.textAreaArg).val('');
                               
                            }
                        },
                        302: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.errrorHandler(t.uhav);
                        },
                        400: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.errrorHandler(data.responseJSON.messages['0']);
                        },
                        403: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        404: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.errrorHandler(t.rnf);
                        },
                        401: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.unauthorizedHandler(_element);
                        },
                        409: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            MiscClass.errrorHandler(t.ec);
                        },
                        500: function (data) {
                            MiscClass.enableDisabled(_element);
                            $(S.submitArg).removeAttr('disabled');
                            $(S.submitArg).val($(S.submitArg).data('origin-text'));
                            if (data.responseText.messages) {
                                MiscClass.errrorHandler(data.responseText.messages);
                            }
                        }
                    },
                    dataType: 'JSON'
                },
                selectors: {
                    container_id: '.usr_action #container_id',
                    container_type: '.usr_action #container_type',
                    contextAreaContainer: '.usr_action .tA_container ',
                    contextArea: 'textarea#comment',
                    imgfield: '.usr_action .add_img',
                    side: '.argumentBox_comments div.col-lg-6',
                    sideIDAttr: 'data-side-id',
                    argsContainer: '.appndbl_comnts_cont'
                }
            };
            /**
             *
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             *
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             *
             * @type Arguments
             */
            this.init();
        };
        /**
         *
         * @returns {_L26.voteDecorator@pro;element@call;each}
         */
        submitArgDecorator.prototype.assignEvents = function () {
            return _selfSAD.element.each(function (i, e) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                // Using return allows for chainability
                $(e).click(function (event) {
                    event.preventDefault();
                    return _selfSAD.submit(event, _selfSAD);
                });
            });
        };
        /**
         *
         * @returns {undefined}
         */
        submitArgDecorator.prototype.init = function () {
            _selfSAD.settings = $.extend(true, {}, _selfSAD.defaults, _selfSAD.options);
            SSAD = _selfSAD.settings;
            SCSAD = _selfSAD.settings.selectors;
            _selfSAD.assignEvents();
        };
        /**
         *
         * @returns {Boolean}
         */
        submitArgDecorator.prototype.submit = function (event, _selfSAD) {
            var objFormData = new FormData();
            objFormData.append('id', $(event.target).siblings(SCSAD.container_id).first().val());
            objFormData.append('type', $(event.target).siblings(SCSAD.container_type).first().val());
            objFormData.append('context', $(event.target).siblings(SCSAD.contextAreaContainer).first().find(SCSAD.contextArea).first().val());
            // objFormData.append('link', $(S.linkArg).val());

            objFormData.append('file', $(event.target).siblings(SCSAD.imgfield).first().prop('files')[0]);
            objFormData.append('action', '/arguments/factory/make');

            $(SCSAD.contextArea).val('');
            $(SCSAD.imgfield).val('');

            $(event.target).siblings(SCSAD.imgfield)
            SSAD.target = event.target;
            SSAD.oAJAX.data = objFormData;
            $.ajax(_selfSAD.settings.oAJAX);
            return false;
        };

        /**
         *
         * @type String|String
         */
        submitArgDecorator.prototype.appendArgToSide = function (_sideID, _argID)
        {
            $(SCSAD.side).each(function (index, element) {
                if ($(element).attr(SCSAD.sideIDAttr) == _sideID) {
                    var oEle = $(element).children(SCSAD.argsContainer).first();
                    ArgumentsClass.fetchArgument(_argID, MiscClass.appendToCommentsContainerCallBack, oEle);
                }
            });
        };
    }

    /******************************************************************************************
     ******************************************************************************************
     * Class submitArgComparison
     *
     * It extends base class SubmitArgDecorator
     * It follows the decorator design pattern
     * It then decorates the functionality of its
     * base class with new functionality relevant only to Comparisons
     *
     *
     *
     *
     *
     *****************************************************************************************/
    $.fn["submitArgComparison"] = function (options) {
        var pluginName = "submitArgComparison";
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                var submitArgComparison = function () {
                };
                var submitArgComparisonSettings = $.extend(true, {}, options, {
                    oAJAX: {
                        statusCode: {
                            200: function (data) {
                                if (data.status == 'OK') {

                                    $(S.submitArg).removeAttr('disabled');
                                    $(S.submitArg).val($(S.submitArg).data('origin-text'));
                                    $(S.textAreaArg).val('');
                                    _selfSAD.appendArgToSide(data.data.side_id, data.data.argument_id.id);
                                }
                            }
                        }
                    },
                    selectors: {
                        side: '.comparison_container .comparison_items li',
                        argsContainer: '.item_comments'
                    }
                });
                submitArgComparison.prototype = new submitArgDecorator(pluginName, $(this), submitArgComparisonSettings);
                $.data(
                        this,
                        'plugin_' + pluginName,
                        submitArgComparison
                        );
            }
        });
    };
    /******************************************************************************************
     ******************************************************************************************
     * Class submitReplyDecorator
     *
     * Base Class
     * Encapsulates all common functionality of submitting a reply
     *
     *
     *
     *
     *
     *
     *
     *****************************************************************************************/

    if (typeof (submitReplyDecorator) === "undefined") {
        var _selfSRD, // holds reference to this
                SSRD, // settings
                SCSRD; // settings selectors
        var submitReplyDecorator = function (_pluginName, _element, _options) {
            _selfSRD = this;
            /**
             *
             */
            this.name = _pluginName;
            /**
             *
             */
            this.element = _element;
            /**
             *
             * @type type
             */
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                    url: phpVars.webhost + 'apps/api',
                    type: 'POST',
                    data: {
                        action: '/replies/create'
                    },
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    beforeSend: function () {
                        $(SCSRD.submitReply).attr('disabled', 'disabled');
                        $(SCSRD.submitReply).data('origin-text', $(SCSRD.submitReply).val());
                        $(SCSRD.textAreaArg).val(t.submt);
                    },
                    statusCode: {
                        200: function (data) {
                            MiscClass.enableDisabled(_element);
                            if (data.status == 'OK') {
                                _selfSRD.resetForm();

                                if (_selfSRD.parentReply == null) {
                                    _selfSRD.Replies.appendReplyToArg(argumentID, data.data);
                                } else {
                                    _selfSRD.appendNestedReply(argumentID, _selfSRD.parentReply, data.data);
                                }

                                _selfSRD.updateCounter(_selfSRD.target);
                            }
                        },
                        302: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            MiscClass.errrorHandler(t.uhav);
                        },
                        400: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            if(data.responseJSON.message) {
                                MiscClass.errrorHandler(data.responseJSON.message);
                            } else {
                                MiscClass.errrorHandler(data.responseJSON.messages['0']);
                            }
                        },
                        403: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            MiscClass.errrorHandler(data.responseJSON.messages);
                        },
                        404: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            MiscClass.errrorHandler(t.rnf);
                        },
                        401: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            MiscClass.unauthorizedHandler(_element);
                        },
                        409: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            MiscClass.errrorHandler(t.emt);
                        },
                        500: function (data) {
                            MiscClass.enableDisabled(_element);
                            _selfSRD.resetForm();
                            if (data.responseText.messages) {
                                MiscClass.errrorHandler(data.responseText.messages);
                            }
                        }
                    },
                    dataType: 'JSON'
                },
                selectors: {
                    submitReply: '.subcomment_submit',
                    textAreaArg: '.write_comment',
                    btnRepliesParent: '.reply',
                    repliesCounter: '.reply_button span',
                    argIDAttr: 'data-arg-id',
                    argumentUnit: '.comment_unit',
                    repliesContainerArg: '.replies-container',
                    divComments: '.argumentBox_comments',
                    parent: '.replies_actions .reply .reply_container'
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             * @type Arguments
             */
            this.init();
        };
        /**
         * 
         * @returns {_L26.voteDecorator@pro;element@call;each}
         */
        submitReplyDecorator.prototype.assignEvents = function () {
            $(document).delegate(_selfSRD.element, 'submit', function (event) {
                if(MiscClass.isDisabled(this)){
                    return false;
                }
                event.preventDefault();
                _selfSRD.target = event.target;
                return _selfSRD.submit(event, _selfSRD);
            });
            return _selfSRD.element;
        };
        /**
         * 
         * @returns {undefined}
         */
        submitReplyDecorator.prototype.init = function () {
            _selfSRD.settings = $.extend(true, {}, _selfSRD.defaults, _selfSRD.options);
            SSRD = _selfSRD.settings;
            SCSRD = _selfSRD.settings.selectors;
            _selfSRD.assignEvents();
        };
        /**
         * 
         * @returns {Boolean}
         */
        submitReplyDecorator.prototype.submit = function (event, _selfSRD) {
            _selfSRD.argumentID = null;
            _selfSRD.parentReply = null;
            var objFormData = new FormData();
            $(_selfSRD.target).children('input').each(function (index, element) {
                objFormData.append($(element).attr('name'), $(element).val());
                if ($(element).attr('type') == 'file') {
                    objFormData.append('file', $(element).prop('files')[0]);
                }
                if ($(element).attr('name') == 'argument_id') {
                    _selfSRD.argumentID = $(element).val();
                }
                if ($(element).attr('name') == 'parent_reply') {
                    _selfSRD.parentReply = $(element).val();
                }

            });
            $(_selfSRD.target).find('textarea').each(function (index, element) {
                objFormData.append($(element).attr('name'), $(element).val());
            });

            objFormData.append('action', '/replies/create');

            _selfSRD.target = event.target;
            SSRD.oAJAX.data = objFormData;
            $.ajax(_selfSRD.settings.oAJAX);
            return false;
        };
        submitReplyDecorator.prototype.appendReplyToArg = function (_argumentID, _replyID) {
            $(SCSRD.argumentUnit).each(function (index, element) {
                if ($(element).attr(SCSRD.argIDAttr) == _argumentID) {
                    var ele = $(element).find(SCSRD.parent).first();
                    RepliesClass.fetchReply(_argumentID, _replyID, MiscClass.appendToCommentsContainerCallBack, ele);
                }
            });
        };

        /**
         * 
         */
        submitReplyDecorator.prototype.resetForm = function ()
        {
            $(SCSRD.submitReply).removeAttr('disabled');
            $(SCSRD.submitReply).val($(SCSRD.submitReply).data('origin-text'));
            $(SCSRD.textAreaArg).val('');
        };

        /**
         * 
         */
        submitReplyDecorator.prototype.updateCounter = function (objFormElement)
        {
            var objRepliesCounter = objFormElement.parents(SCSRD.btnRepliesParent).find(SCSRD.repliesCounter).first();
            var numReplies = parseInt(objRepliesCounter.html());
            objRepliesCounter.html(++numReplies);
        };
    }

    /******************************************************************************************
     ******************************************************************************************
     * Class submitReplyComparison
     * 
     * It extends Base Class submitReplyDecorator
     * Follows a Decorator Design Pattern
     * Then decorates its functionality with ones relevant only 
     * to Comparisons
     * 
     * 
     * 
     * 
     * 
     *****************************************************************************************/
    $.fn["submitReplyComparison"] = function (options) {
        var pluginName = "submitReplyComparison";

        if (!$.data(this, 'plugin_' + pluginName)) {
            var submitReplyComparison = function () {
            };
            var submitReplyComparisonSettings = $.extend(true, {}, options, {
                oAJAX: {
                    statusCode: {
                        200: function (data) {
                            if (data.status == 'OK') {
                                _selfSRD.resetForm();
                                _selfSRD.appendReplyToArg(_selfSRD.argumentID, data.data);
                                return false;
                            }
                        }
                    }
                },
                selectors: {
                    side: '.comparison_container .comparison_items li',
                    argsContainer: '.item_comments'
                }
            });
            submitReplyComparison.prototype = new submitReplyDecorator(pluginName, $(this), submitReplyComparisonSettings);
            $.data(
                    this,
                    'plugin_' + pluginName,
                    submitReplyComparison
                    );
        }
        return this;

    };

    if (typeof (NestedRepliesClass) === "undefined")
    {
        var _selfNRC,
                SNRC,
                SCNRC;
        /**
         * 
         * @returns {_L29.NestedRepliesClass}
         */
        var NestedRepliesClass = function (_options) {
            _selfNRC = this;
            this.dataObj;
            this.misc = new MiscClass(); // composition
            this.defaults = {
                // These are the defaults.
                oAJAX: {
                },
                selectors: {
                    nestedReplyBtn: '.nested_reply_button',
                    divComments: '.comment_unit',
                    argIDAttr: 'data-arg-id',
                    nestedReplyContiners: '.nested_comments',
                    argumentUnit: '.comment_unit'
                }
            };
            /**
             * 
             * @type type
             */
            this.options = (this.options) ? $.extend(true, {}, this.options, _options) : _options;
            /**
             * 
             * @type type
             */
            this.settings = $.extend(true, {}, this.defaults, this.options);
            /**
             * 
             */
            this.init();
        };

        NestedRepliesClass.prototype.init = function ()
        {
            _selfNRC.settings = $.extend(true, {}, _selfNRC.defaults, _selfNRC.options);
            SNRC = _selfNRC.settings;
            SCNRC = _selfNRC.settings.selectors;
            _selfNRC.assignEvents();
        };

        NestedRepliesClass.prototype.assignEvents = function () {
        };

        /**
         * @access  static
         * @param {type} _data
         * @param {type} _element
         * @param {type} _replyParent
         * @returns {undefined}
         */
        NestedRepliesClass.fetchRepliesCallBack = function (_data, _element, _replyParent) {

            if (_data) {
                _element.parents('.replies-parent_' + _replyParent).find(SCNRC.nestedReplyContiners).html(_data);
            }

            _element.attr('data-old-text', _element.text());
            _element.text('Hide ^');

            _element.parents('.replies-parent_' + _replyParent).find('.reply-form-nested:first, .nested_comments').show();

        };

        /**
         * @access  static
         * @param {type} _data
         * @param {type} _callback
         * @param {type} _ele
         * @returns {Boolean}
         */
        NestedRepliesClass.fetchReplies = function (_data, _callback, _ele) {
            var object = {
                url: '/arguments/render/nested-replies/',
                type: 'GET',
                data: {
                    'parent_reply': _data.replyParent,
                    'argument_id': _data.argumentID,
                    'page': _data.page,
                    'limit': _data.limit
                },
                success: function (data, textStatus, jqXHR) {
                    MiscClass.enableDisabled(_ele);
                    _callback(data, _ele, _data.replyParent);
                    MiscClass.addReadMore();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    MiscClass.enableDisabled(_ele);
                    MiscClass.errrorHandler(jqXHR.responseText);
                }
            };
            $.ajax(object);
            return false;
        };

        /**
         *
         * @type String|String
         */
        NestedRepliesClass.paginateRepliesCallBack = function (_data, _oEle)
        {
            MiscClass.enableDisabled(_oEle);
            $(_oEle).before(_data);
            /**
             var curr_page = parseInt($(_oEle).attr(SCRC.pageAttr)) + 1;
             $(_oEle).attr(SCRC.pageAttr, curr_page);
             **/
            /**
             var total_pages = $(_oEle).attr(SCRC.totalPagesAttr);
             if (curr_page <= total_pages) {
             $(SCRC.moreComments).show();
             }
             **/
        };

        /**
         *
         * @param {type} _data {argument_id, page, limit, element}
         * @returns {Boolean}
         */
        NestedRepliesClass.paginateReplies = function (_data) {
            MiscClass.enableDisabled(_data.element);
            var ele = _data.element.parents(SCRC.argumentUnit).first().find('.nested_comments div:last').first();
            NestedRepliesClass.fetchReplies(_data, NestedRepliesClass.paginateRepliesCallBack, ele);
            return false;
        };

        /**
         * 
         * 
         */
        $.fn["NestedRepliesClass"] = function (options) {
            var pluginName = "NestedRepliesClass";
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(
                            this,
                            'plugin_' + pluginName,
                            new NestedRepliesClass(options)
                            );
                }
            });
        };
    }
}(jQuery));
