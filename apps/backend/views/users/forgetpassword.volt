<div id="wrapper">

    <!-- Sidebar -->

    <!-- Page content -->

        <!-- Keep all page content within the page-content inset div! -->
        <div class="row bg-green white login-header margin-bottom {{ (lang ==="ar") ? "rtl" : ""}}">
            <div class="col-xs-10">
                <h3>{{ t._('forget-password') }}</h3>
            </div>
            <div class="col-xs-2">
                <button class="no-border bg-transparent white login-close js-login-close inline-block width-all"><i class="fa fa-close fa-2x"></i></button>
            </div>
        </div>
<div class="row negative-margin-top">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
        <div class="row margin-bottom">
            {{ form('users/forgetpassword', 'class': 'login-form') }}
                <div class="form_row">
                  {{ textField( {
                        "email", 
                        'placeholder': t._('email-addr'), 
                        'class': 'form-control input-lg width-all', 
                        "data-validation-engine" :"validate[required, custom[email]]",
                        "data-errormessage-value-missing": t._('email-required'),
                        "data-errormessage-custom-error": t._('email-not-valid'),
                        "data-prompt-position": "topRight:-130"
                    } ) }}
                </div>
                <div class="form_row">
                    <input type="submit" value="{{ t._('send-verifi-code') }}" class="btn-lg {{ (lang === 'ar') ? "pull-left" : "pull-right" }}  bg-dark-blue" style="width: 45%;" >
                </div>
            {{ endForm() }}
        </div>
</div>
