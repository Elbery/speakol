<div class="rcmndd_cont">
    <section class="rcmndd_sec">
        <h2 class="sec_titl_sml"><a href="">{{ t._('recommended') }}</a></h2>
        <ul class="nws_itms_lst">
            {% for recomend in recomended %}
            <li>
                <a href="{{ recomend.url }}" target="_blank">
                    <span class="itm_img"><img src="{{ recomend.image }}" alt=""/></span>
                    <span class="itm_title">{{ recomend.title }}</span>
                </a>
                <span class="itm_src">{{ recomend.domain }}</span>
            </li>
            {% endfor %}
        </ul>
    </section>
</div>