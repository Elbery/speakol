<div class="vt_btns">
{% if argumentsbox.id %}
    {% set title = (argumentsbox.title === "") ? t._('what-do-you-think') : argumentsbox.title  %}
{% else %}
    {% set title = request.get('no-title') === "true" ? "" : request.get('title')  %}
    {% if request.get('no-title')  %}
        <input type="hidden" id="noTitle" value="true">
    {% endif %}
{% endif  %}
    <span class="title-text"> {{ title  }}</span>
    {% if argumentsbox.sides is defined %}
        {% for key, side in argumentsbox.sides %}
            {% if loop.first %}
                <a class="vt_up{{ side.voted ? ' dimmed vted vted_up' : '' }} " href="#" data-argumentsbox-id="{{argumentsbox.id}}" data-side-id="{{ side.id }}"> {{ t._('vote_up') }} </a>
            {% else %}
                <a class="vt_dwn{{ side.voted ? ' dimmed vted vted_down' : '' }}" href="#" data-argumentsbox-id="{{argumentsbox.id}}" data-side-id="{{ side.id }}"> {{ t._('vote_down') }} </a>
            {% endif %}
        {% endfor %}
    {% else %}
        <a class="vt_up" href="#" data-argumentsbox-id="" data-side-id=""> {{ t._('vote_up') }} </a>
        <a class="vt_dwn" href="#" data-argumentsbox-id="" data-side-id=""> {{ t._('vote_down') }} </a>
    {% endif %}
</div>

<div class="comment_on_action clearfix" {{ argumentsbox.currentUserVoted is defined ? ' style="display:block;"' : 'style="display:none;"' }}>
    <div class="img_circl_container">
        <span class="img_circl">
            {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                <a href=""> {{ image( config.application.webservice~LoggedInUserData.profile_picture) }}</a>
            {% else %}
                <a href=""> {{ image("img/user_default.gif") }}</a>
            {% endif %}
        </span>
    </div>
    <div class="comment_area">
        <div class="tA_container">
            {{ text_area("comment", "placeholder": t._('commentbox-placeholder'), "class":"write_comment", "cols": "6", "rows": 20) }}
        </div>
        <p class="vwrs_num" style="display:none; background: #53a960;margin: 6px 0 0 0;padding: 2px 17px;color: #fff;border-radius: 5px;"></p>
        {{ submit_button("post", "value": t._('sbmt-arg'), "class":"sbmt-arg") }}
        <div class="arg_box_photo" style="width: 40px;height: 40px;float: right;margin-top: 8px;margin-right: 11px;display: none;">
            <img src="#" alt="">
        </div>

        {{ file_field("attached", "class":"add_img",'accept':'.png,.jpeg,.jpg,.gif') }}
        {% if argumentsbox.id is defined %}
            {{ hidden_field("argumentbox_id", "value": argumentsbox.id) }}
            {{ hidden_field("link", "value": argumentsbox.url) }}
        {% else %}
            {{ hidden_field("argumentbox_id", "value": '') }}
            {{ hidden_field("link", "value": url) }}
        {% endif %}
    </div>
</div>
