<?php
namespace Speakol\Routes;
class IndexRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'index',));
        $this->setPrefix('/');
        $this->add('index', array('action' => 'index'))->setName("home");
    }
}
