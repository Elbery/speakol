<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">
        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>

                <div class="login_container">
                    <h2>{{ t._('set-password') }}</h2>
                    <div class="form_cont">
                        {{ form('users/setpassword', 'method': 'post', 'enctype': 'multipart/form-data') }}

                            <div class="input-group">
                                <span class="input-group-addon password_ico"></span>
                                {{ passwordField( {
                                    "password", 
                                    'placeholder': t._('password'), 
                                    'class': 'form-control', 
                                    "autocomplete": "off", 
                                    "data-validation-engine" :"validate[required , minSize[8]]",
                                    "data-errormessage-range-underflow": t._('password-min-length'),
                                    "data-errormessage-value-missing": t._('password-required')
                                } ) }}
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon password_ico"></span>
                                {{ passwordField( {
                                    "confirm_password", 
                                    'placeholder': t._('confirm-password'), 
                                    'class': 'form-control', 
                                    "autocomplete": "off", 
                                    "data-validation-engine" :"validate[equals[password]]",
                                    "data-errormessage-pattern-mismatch": t._('password-not-match')
                                } ) }}
                            </div>
                        <div class="form_row">
                            {{ submit_button(t._('set-password')) }}
                        </div>
                        {{ endForm() }}    
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
