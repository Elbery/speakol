{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        <div class="row margin-top-sm">
            <div class="bg-e6e6e6 color-7f8287 plan-notice col-sm-6 {{ planID == 4 ? "expanded" : "col-sm-6" }}">
                <h3 class="h6">{{ t._('current-plan') }}</h3>
                <h3 class="weight-bold font-20">
                    {{ plan.name }}
                </h3>
                {% if pendingPlan %}
                    <p class="h6">
                        {{ t._('account-downgraded') }} <span class="weight-bold">{{ pendingPlan }}</span> {{ t._('on') }} {{ plan.formattedDate }}.
                    </p>
                {% elseif planID  !== 1 %}
                <p class="h6 {{ lang === 'ar' ? 'rtl text-left' : '' }}">
                        {{ t._('account-billed') }} {{ plan.amount  }}$ {{ t._('on') }} {{ plan.formattedDate  }}
                    </p>
                    <div>
                        <div class="pull-left margin-top-sm">
                            <a class="anchor-reset pull-right margin-top-lg h6 decoration-underline" href="/pricing/downgrade/1">{{ t._('disable-renewal') }}</a>
                        </div>
                        <div class="pull-right">
                            <a class="btn pull-right pricing-button margin-top-sm" href="/pricing">{{ t._('more-details') }}</a>
                        </div>
                    </div>
                {% endif %}
            </div>
            <div class="offset-sm bg-medium-green white plan-notice {{ planID == 4 ? "hide" : "col-sm-6"}}">
                <h3 class="h6">{{ t._('upgrade-to') }}</h3>
                <h3 class="weight-bold font-20">
                    {{ nextPlan['name'] }}
                </h3>
                {% if nextPlan['name'] === 'Corporate' %}
                    <p class="h6">{{ t._('unlimited-features') }}</p>
                {% endif %}
                <a class="btn pull-right pricing-button margin-top-xs" href="/pricing/upgrade/{{ nextPlanID }}">{{ t._('upgrade') }}</a>
            </div>
        </div>
        <div class="row bg-white margin-top-lg update-notice {{ lang === 'ar' ? 'rtl' : ''}}">
            <p class="h5 padding-top-sm padding-bottom-xs">
                <span class="js-payment-text">
                    {% if defaultCard %}
                        {{ t._('current-payment') }} {{ defaultCard.payment_method }} {{ t._('ending-with') }} {{ substr(defaultCard.number, -4)}}, 
                    {% else %}
                        {{ t._('payment-not-specified') }}
                    {% endif %}
                </span>
                <button class="btn-link color-5bb767 js-show-payments">
                    {% if defaultCard %}
                        {{ t._('update-payment-method') }}
                    {% else %}
                        {{ t._('add-payment-method') }}
                    {% endif %}
                </button>
            </p>
        </div>
        <div class="row bg-white update-notice js-update-notice hide">
            <h3 class="h4 weight-bold no-margin">{{ t._('payment-methods') }}</h3>
            <table class="table margin-top-sm">
                {% for card in cards %}
                    {% include 'partials/billing/card.volt' %}
                {% endfor %}
                <tr class="js-add-form-container {{ noCards ? '' : 'hide'  }}">
                    <td colspan="6">
                               {{ form( 'billing/add', 'class': 'form-horizontal card-form js-add-card-form js-card-form','enctype': 'multipart/form-data') }}
                <div class="form-group">
                    <label for="name" class="col-sm-2 margin-top-xs font-15">{{ t._('payment-method') }}</label>
                    <div class="col-sm-3">
                        <select id="method" name="method" class="form-control no-radius no-padding-height arial-font" data-validation-engine="validate[required]">
                            <option value="">{{ t._('credit-card') }}</option>
                            <option value="Visa">Visa</option>
                            <option value="MasterCard">MasterCard</option>
                            <option value="American Express">American Express</option>
                            <option value="Discover">Discover</option>
                            <option value="Diners Club">Diners Club</option>
                            <option value="JCB">JCB</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 margin-top-xs font-15">{{ t._('name-card') }}</label>
                    <div class="col-sm-3">
                        {{ textField({"name", "class": "no-padding form-control no-radius font-15 arial-font", "data-validation-engine" :"validate[required]" }) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc-number" class="col-sm-2 margin-top-xs font-15">{{ t._('card-number') }}</label>
                    <div class="col-sm-3">
                        <input id="cc-number" name="number" class="no-padding form-control no-radius font-15 cc-number arial-font" type="tel" placeholder="•••• •••• •••• ••••" data-validation-engine="validate[required, funcCall[validCardNumber], funcCall[validCardType]]">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc-number" class="font-15 col-sm-2">{{ t._('expire-date') }}</label>
                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-sm-7">
                                <select id="month" name="month" class="form-control no-padding-height padding-5 no-radius arial-font">
                                    <option value="">{{ t._('month') }}</option>
                                    <option value="1">{{ t._('january') }}</option>
                                    <option value="2">{{ t._('february') }}</option>
                                    <option value="3">{{ t._('march') }}</option>
                                    <option value="4">{{ t._('april') }}</option>
                                    <option value="5">{{ t._('may') }}</option>
                                    <option value="6">{{ t._('june') }}</option>
                                    <option value="7">{{ t._('july') }}</option>
                                    <option value="8">{{ t._('august') }}</option>
                                    <option value="9">{{ t._('september') }}</option>
                                    <option value="10">{{ t._('october') }}</option>
                                    <option value="11">{{ t._('november') }}</option>
                                    <option value="12">{{ t._('december') }}</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <select id="year" name="year" class="form-control no-padding-height padding-5 no-radius arial-font">
                                    <option value="">{{ t._('year') }}</option>
                                    {% for year in currentYear..currentYear+10 %}
                                        <option value="{{ year }}">{{ year }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc-cvc" class="col-sm-2 font-15">{{ t._('security-code') }}</label>
                    <div class="col-sm-3">
                        <input id="cc-cvc" name="code" class="no-padding form-control no-radius font-15 cc-cvc arial-font" type="tel" placeholder="••••" autocomplete="off" data-validation-engine="validate[required]">
                    </div>
                </div>
                <div class="form-group">
                    {{ submitButton({
                        t._('add'),
                        'class': 'btn btn-primary col-sm-1 col-sm-offset-2 no-radius bg-light-blue',
                        'disabled': true
                    })}}
                    <button class="btn btn-primary no-radius bg-white col-sm-1 js-main-cancel-btn color-ed1d25 border-ed1d25 cancel-btn">{{ t._('cancel') }}</button>
                </div>
            {{ endform() }}
                     </td>
                </tr>
            </table>
        </div>
        <div class="row">
            <h3 class="weight-bold font-20">{{ t._('billing-history') }}</h3>
        </div>
        <div class="row margin-top-sm">
            <div class="col-sm-11 bg-white">
                <div class="row margin-top-xs margin-bottom-xs">
                    <div class="col-sm-3 col-sm-offset-9 font-13">
                        <div class="select-dropdown margin-top-sm">
                            <label for="" class="width-all font-13 color-c8c8c8 weight-normal {{ lang === 'ar' ? 'rtl text-left' : '' }}">
                                {{ t._('sort-by') }}
                            <select id="" name="" class="inline-block bg-transparent border-none margin-top-xs js-sort-select color-6e6f6f">
                                <option value="desc" selected>{{ t._('date-newest') }}</option>
                                <option value="asc">{{ t._('date-oldest') }}</option>
                            </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table class="table h5 table-bordered margin-top-xs js-invoices-table">
                        <tr>
                            <th class="text-center weight-normal">{{ t._('invoice-no') }}</th>
                            <th class="text-center weight-normal">{{ t._('payment-method-small') }}</th>
                            <th class="text-center weight-normal">{{ t._('payment-status') }}</th>
                            <th class="text-center weight-normal">{{ t._('date') }}</th>
                            <th class="text-center weight-normal">{{ t._('receipt') }}</th>
                        </tr>
                        {% for invoice in invoices %}
                        <tr class="js-invoice">
                            <td class="text-center weight-normal">{{ invoice.id }}</td>
                            <td class="text-center weight-normal">{{ invoice.payment_method ~ " " ~invoice.card_no }}</td>
                            <td class="text-center weight-normal">{{ invoice.status | capitalize }}</td>
                            <td class="text-center weight-normal">{{ date('d/m/Y', strtotime(invoice.date)) }}</td>
                            <td class="text-center weight-normal"><a href="/billing/viewreceipt/{{ invoice.receipt_id  }}">{{ t._('view-receipt') }}</a></td>
                        </tr>
                        {% endfor %}
                    </table>
                </div>
            </div>
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
