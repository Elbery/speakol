/* avoid conflicts with other versions or libraries */
jQuery.noConflict();

/* reserve $ for JQuery */
(function ($) {
    $(document).ready(function () {
        /* ------- my scripts ------- */
        // tabs
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // activated tab
            e.relatedTarget // previous tab
        });

        // remove highlight from notification when opened
        $('.ntfc').mouseenter(function () {
            $(this).removeClass('highlight');
        });

        // toggle replies container when click on reply button
        //=====================================================
        var replyButton = $(".reply_button");
        replyButton.click(function (e) {
            /**
             * todo: @mohamed_abdul_latif - fix or don't uncomment
             */
//            e.preventDefault();
//            e.stopPropagation();
            $(this).parent().toggleClass("active");
        });

        // Comparison details columns
        //======================================================
        var listItems = $(".comparison_details .comparison_items>li");
        //hover over an item for the first time
        function hoverItemFirstTime() {
            // var $this = $(this);
            // var timer = setInterval(function(){
            //     listItems.removeClass("i_expand").addClass("i_collapse");
            //     $this.removeClass("i_collapse").addClass("i_expand");
            //     clearInterval(timer)
            // },0);
        }
        // click on an item
        function clickListItem(e) {

            listItems.removeClass().addClass("i_collapse");
            $(this).removeClass().addClass("i_expand");
        }

        listItems.on('mouseenter', hoverItemFirstTime);

        $('html').click(function (e) {
            if (!$(e.target).hasClass('comparison_items') && !$(e.target).parents('.comparison_items').length) {
                listItems.removeClass();
                replyButton.parent().removeClass("active");
                listItems.on('mouseenter', hoverItemFirstTime);
            }
        });
        listItems.on('mouseenter', function () {
            listItems.on('click', clickListItem);
            listItems.off('mouseenter', hoverItemFirstTime);
        });

        // comparison details columns equal height
        //=======================================
        function thisHeight() {
            return $(this).height();
        }

        listItems.each(function () {
            var thisLIMax = Math.max.apply(Math, listItems.map(thisHeight));
            $(this).css({minHeight: thisLIMax});
        });

        /* var $container = $('.content_wrapper');
         $(window).scroll(function(){
         if($(document).scrollTop()> 40){
         $container.addClass('fixed');
         } else {
         $container.removeClass('fixed');
         }
         });
         $(window).scroll(function(){
         if($(document).scrollTop()> 300){
         $container.addClass('comparison-fixed');
         } else {
         $container.removeClass('comparison-fixed');
         }
         }); */
    });
}(jQuery));