<tr class="js-card" data-card-id="{{ card.id }}" data-default="{{ card.default_card ? "true" : "false" }}">
    <td>{{ card.payment_method | upper }}</td>
    <td>{{ card.number }}</td>
    <td>{{ card.name }}</td>
    <td>{{ card.expiry_month ~ "-" ~ card.expiry_year }}</td>
    <td>
        <button class="btn-link h6 color-5bb767 js-update-card" data-card-id="{{ card.id }}">{{ t._('update') }}</button>
        <button class="btn-link h6 color-dadada js-delete-card" data-card-id="{{ card.id }}">{{ t._('delete') }}</button>
    </td>
</tr>
<tr class="js-card-form-container hide" data-card-id="{{ card.id }}">
    <td colspan="6" class="update-card-form-container">
        {{ form( 'billing/update/' ~ card.id, 'class': 'form-horizontal js-update-card-form js-card-form card-form','enctype': 'multipart/form-data', 'data-card-id': card.id) }}
            <div class="form-group">
                <label for="name" class="col-sm-2 margin-top-xs font-15">{{ t._('payment-method') }}</label>
                <div class="col-sm-3">
                    <select id="method" name="method" class="form-control no-radius no-padding-height arial-font" data-validation-engine="validate[required]">
                        <option value="">{{ t._('credit-card') }}</option>
                        {% set options = ['Visa', 'MasterCard', 'American Express', 'Discover', 'Diners Club', 'JCB'] %}
                        {% for option in options %}
                            <option {{ card.payment_method === option ? 'selected' : '' }} value="{{ option }}">{{ option }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 margin-top-xs font-15">{{ t._('name-card') }}</label>
                <div class="col-sm-3">
                    {{ textField({"name", "class": "no-padding form-control no-radius font-15 arial-font", "data-validation-engine" :"validate[required]" , "value" : card.name }) }}
                </div>
            </div>
            <div class="form-group">
                <label for="cc-number" class="col-sm-2 margin-top-xs font-15">{{ t._('card-number') }}</label>
                <div class="col-sm-3">
                    <input id="cc-number" name="number" class="no-padding form-control no-radius font-15 cc-number arial-font" type="tel" placeholder="•••• •••• •••• ••••" data-validation-engine="validate[funcCall[validCardNumber], funcCall[validCardType]]">
                </div>
            </div>
            <div class="form-group">
                <label for="cc-number" class="font-15 col-sm-2">{{ t._('expire-date') }}</label>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-7">
                            <select id="month" name="month" class="form-control no-padding-height padding-5 no-radius arial-font">
                                <option value="">{{ t._('month') }}</option>
                                {% set months = [
                                    'january',
                                    'february',
                                    'march',
                                    'april',
                                    'may',
                                    'june',
                                    'july',
                                    'august',
                                    'september',
                                    'october',
                                    'november',
                                    'december' 
                                ]%}
                                {% for month in months %}
                                    <option {{ card.expiry_month == loop.index  ? 'selected' : '' }} value="{{ loop.index }}">{{ t._(month) }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select id="year" name="year" class="form-control no-padding-height padding-5 no-radius arial-font">
                                <option value="">{{ t._('year') }}</option>
                                {% for year in currentYear..currentYear+10 %}
                                    <option {{ card.expiry_year == year ? 'selected' : '' }} value="{{ year }}">{{ year }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="cc-cvc" class="col-sm-2 font-15">{{ t._('security-code') }}</label>
                <div class="col-sm-3">
                    <input id="cc-cvc" name="code" class="no-padding form-control no-radius font-15 cc-cvc arial-font" type="tel" placeholder="••••" autocomplete="off" data-validation-engine="validate[required]">
                </div>
            </div>
            <div class="form-group">
                {{ submitButton({
                    t._('update'),
                    'class': 'btn btn-primary col-sm-1 col-sm-offset-2 no-radius bg-light-blue',
                    'disabled': true
                })}}
                <button class="btn btn-primary no-radius bg-white col-sm-1 js-cancel-btn color-ed1d25 border-ed1d25 cancel-btn">{{ t._('cancel') }}</button>
            </div>
        {{ endform() }}
    </td>
</tr>
