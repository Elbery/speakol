<section class="tp_br_cnt clearfix">
    <div class="lft_usr drpdwn">
        <span class="usr_img">
            {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                {{ image(config.application.webservice ~ LoggedInUserData.profile_picture) }}
            {% else %}
                {{ image("img/user_default.gif") }}
            {% endif %}
        </span>
        <span class="usr_nm">

            {{ t._('hello') }}, {{ LoggedInUserData.name }}
        </span>
        <span class="glyphicon glyphicon-chevron-down"></span>
        <div class="drpdwn_cont">
            <a class="logout follower" href="{{ logout }}">{{ t._('logout') }}</a>
        </div>
    </div>
    {% include "partials/argumentsbox/notifications.volt"  %}
</section>
