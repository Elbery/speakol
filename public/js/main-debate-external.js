$(document).ready(function () {
    /* ------- my scripts ------- */
    // tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // activated tab
        e.relatedTarget // previous tab
    })

    // remove highlight from notification when opened
    $('.ntfc').mouseenter(function () {
        $(this).removeClass('highlight');
    });
    /* var $container = $('.content_wrapper');
     $(window).scroll(function(){
     if($(document).scrollTop()> 40){
     $container.addClass('fixed');
     } else {
     $container.removeClass('fixed');
     }
     }); */
});