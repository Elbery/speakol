<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="story_container static_content">

            {{ form( 'method': 'post', 'enctype': 'multipart/form-data') }}
            <div class="story_container contact">
                <div class="form_cont">
                    <h2>{{ t._('contact-speakol') }}</h2>
                    <div class="login_form">
                        <div class="form_row">
                            <div class="input-group">
                                <span class="input-group-addon name_ico"></span>
                                {{ textField({"contact_name", "placeholder": t._('your-name'), "class":"form-control"}) }}
                            </div>
                        </div>
                        <div class="form_row">
                            <div class="input-group">
                                <span class="input-group-addon email_ico"></span>
                                {{ textField({"contact_email", "placeholder": t._('your-email'), "class":"form-control" }) }}
                            </div>
                        </div>
                        <div class="form_row">
                            <div class="input-group">
                                <span class="input-group-addon subject_ico"></span> 
                                {{ textField({"contact_subject", "placeholder": t._('subject'), "class":"form-control" }) }}
                            </div>
                        </div>
                        <div class="form_row half">
                            {{ textArea({"contact_message", "placeholder": t._('type-your-message'), "class":"message_text form-control", "rows":"5"  }) }}
                        </div>
                        <div class="form_row">
                            {{ submit_button( t._('send')) }}
                        </div>
                    </div>
                </div>
            </div>
            {{ endForm() }}
        </div>
            <section class="tbd_br_cnt clearfix">
                    <div class="shr_dsc">
                        <div class="social_shares" style="display: inline-block;">
                            <a href="http://www.facebook.com/Speakol" target="_blank">{{ image('img/fb.png') }}</a>
                            <a href="http://www.twitter.com/Speakol_" target="_blank">{{ image('img/tw.png') }}</a>
                            <a href="https://plus.google.com/115379961634886743482" target="_blank">{{ image('img/gp.png') }}</a></div>
                            <a href="http://blog.speakol.com/" target="_blank">{{ image('img/sp.png') }}</a></div>
                        </div>
                    </div>
            </section>
    </div>
</div>
