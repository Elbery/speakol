<!-- Modal -->
<div class="modal fade" id="loginbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ t._('login') }}</span></button>
                <h2>{{ t._('login-to-continue') }}</h2>
            </div>
            <div class="modal-body">
                <div class="login_container">
                    <div class="input-group">
                        <label class="error-box alert hide"></label>
                    </div>
                    {% include 'partials/loginbox/login.volt' %}
                </div>
            </div>
            <div class="modal-footer" style="display:none">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ t._('login') }}</button>
            </div>
        </div>
    </div>
</div>

