<div class="speakol-container container-fluid no-padding {{ lang === 'ar' ? 'rtl' : '' }}">
    <div class="row margin-top-sm padding-top-xs padding-bottom-xs bg-f9c6c6 color-9b3535 {{ lang === 'ar' ? 'padding-right-10' : 'padding-left-10' }}">
        <div class="{{ lang === 'ar' ? 'pull-right' : 'pull-left' }}">
            <i class="speakol-icon speakol-icon-md speakol-times-circle color-dc5959"></i>
        </div>
        <div class="media-body font-15 margin-top-xs">
            <p class="{{ lang === 'ar' ? 'padding-right-5' : 'padding-left-5' }} no-margin">
                {{ t._('sorry-unauthorized') }}
            </p>
        </div>
    </div>
</div>
