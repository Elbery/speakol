<?php
namespace Speakol\Backend\Controllers;
use Speakol\Backend\Validations as Validations;
use Speakol\Backend\Controllers\BaseController;
class PricingController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('pricing');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
      $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/home.js?' . $this->config->application->cache_string);
        $actionName = $this->dispatcher->getActionName();
        $actions = array('upgrade', 'downgrade');
        if (in_array($actionName, $actions)) {
            $params = $this->dispatcher->getParams();
            $plan_id = $params[0];
            $app = $this->getAppData();
            if (!$app || !$plan_id) {
                return $this->response->redirect('pricing');
            }
            $hdrs = array('Authorization:' . $this->getAppToken());
            $response = $this->utility->iCurl('/billing/user/card/exist', 'GET', $hdrs, array());
            if ($response->status !== 'OK') {
                $this->flash->error($response->message);
                return $this->response->redirect('pricing');
            }
            $cardExists = $response->card_exists;
            $this->dispatcher->setParam('cardExists', $cardExists);
            $this->dispatcher->setParam('hdrs', $hdrs);
        }
    }
    public function indexAction() {
        $app = $this->getAppData();
        if (!$app) {
            if ($this->getLang() == 'en') {
                $this->assets->addCss('css/home.css?' . $this->config->application->cache_string);
            } else {
                $this->assets->addCss('css/home_ar.css?' . $this->config->application->cache_string);
            }
            $this->view->loggedin = false;
            $this->view->pick('pricing/loggedout');
        } else {
            $this->view->currentPlan = $app->plan_id;
            $this->view->loggedin = true;
            $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        }
    }
    public function addcardAction() {
        $this->loadCustomTrans('billing');
        $this->view->noAlerts = true;
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $plans = array(2 => array('name' => 'Premium', 'price' => '$50',), 3 => array('name' => 'Premium+', 'price' => '$100',), 4 => array('name' => 'Corporate', 'price' => '$300',),);
        if ($this->request->isPost()) {
            $app = $this->getAppData();
            $hdrs = array('Authorization:' . $this->getAppToken());
            $params = array('name' => $this->request->getPost('name'), 'number' => $this->request->getPost('number'), 'month' => $this->request->getPost('month'), 'year' => $this->request->getPost('year'), 'code' => $this->request->getPost('code'), 'method' => $this->request->getPost('method'),);
            $cardValidator = new Validations\CardsValidation(array('name', 'number', 'month', 'year', 'code', 'method',));
            $cardMsgs = $cardValidator->validate($params);
            if (count($cardMsgs) && !$this->request->isAjax()) {
                $this->showErrorMsgs($cardMsgs);
            }
            if (count($cardMsgs) && $this->request->isAjax()) {
                $this->response->setHeader("Content-Type", "application/json");
                $this->response->setStatusCode(500, 'ERROR');
                $this->response->setJsonContent(array('status' => 'ERROR', 'message' => $cardMsgs[0]->__toString()));
                return $this->response;
            }
            if (!count($cardMsgs)) {
                $response = $this->utility->iCurl('/billing/add/card', 'POST', $hdrs, $params, false, false, true);
                if ($response->status !== 'OK') {
                    $ajaxResponse = $this->sendResponse(array('code' => '500', 'status' => 'ERROR', 'message' => $response->message));
                    if ($this->request->isAjax()) {
                        return $ajaxResponse;
                    }
                } else {
                    $upgradePlan = $this->request->getPost('upgrade_plan');
                    if ($upgradePlan && intval($upgradePlan) > 1) {
                        $params = array('plan_id' => intval($upgradePlan),);
                        $response = $this->utility->iCurl('/billing/upgrade', 'POST', $hdrs, $params, true, false, true);
                        if ($response->status !== 'OK') {
                            $ajaxResponse = $this->sendResponse(array('code' => '500', 'status' => 'ERROR', 'message' => $response->message), 'pricing');
                            if ($this->request->isAjax()) {
                                return $ajaxResponse;
                            }
                        }
                        $session = $this->session->get('app_data');
                        $session->user->app->plan_id = $response->data->plan_id;
                        $this->session->set('app_data', $session);
                        $amount = $response->data->amount;
                        $message = '';
                        $message.= 'Thank you for signing up to Speakol ';
                        $message.= '<span class="weight-bold">' . $plans[$upgradePlan]['name'] . '</span>, ';
                        $message.= 'your payment was successful and amount of ';
                        $message.= '<span class="font-weight">' . $amount . '</span> ';
                        $message.= 'has been deducted from your account.';
                        $ajaxResponse = $this->sendResponse(array('code' => '200', 'status' => 'OK', 'message' => $message, 'modal' => 1, 'amount' => $amount));
                        if ($this->request->isAjax()) {
                            return $ajaxResponse;
                        }
                    } else {
                        $response = $this->sendResponse(array('code' => '200', 'status' => 'OK', 'message' => 'Credit card has added been successfully'));
                        if ($this->request->isAjax()) {
                            return $ajaxResponse;
                        }
                    }
                }
            }
        }
        $this->view->upgradePlan = intval($this->request->get('upgrade_plan'));
        $this->view->upgradePlanDetails = $plans[$this->view->upgradePlan];
        $this->assets->addCss('vendor/css/validationEngine.jquery.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.payment.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine-en.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery.validationEngine.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/billing.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/pricing.js?' . $this->config->application->cache_string);
        $this->view->planID = $plan_id;
        $this->view->currentYear = date('Y');
    }
    public function upgradeAction($plan_id) {
        $cardExists = $this->dispatcher->getParam('cardExists');
        $hdrs = $this->dispatcher->getParam('hdrs');
        if (!$cardExists) {
            return $this->response->redirect('pricing/addcard?upgrade_plan=' . $plan_id);
        }
        $params = array('plan_id' => $plan_id);
        $response = $this->utility->iCurl('/billing/upgrade', 'POST', $hdrs, $params, true, false, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('pricing');
        }
        $session = $this->session->get('app_data');
        $session->user->app->plan_id = $response->data->plan_id;
        $this->session->set('app_data', $session);
        $this->flash->success('Your plan has been upgraded successfully');
        return $this->response->redirect('dashboard');
    }
    public function downgradeAction($plan_id) {
        $hdrs = $this->dispatcher->getParam('hdrs');
        $params = array('plan_id' => $plan_id,);
        $response = $this->utility->iCurl('/billing/downgrade', 'POST', $hdrs, $params, false, false, true);
        if ($response->status !== 'OK') {
            $this->flash->error($response->message);
            return $this->response->redirect('pricing');
        }
        $session = $this->session->get('app_data');
        $session->user->app->plan_id = $response->data->plan_id;
        $this->session->set('app_data', $session);
        $this->flash->success('Your plan has been downgraded successfully, you\'ll enjoy the new plan starting from the next month.');
        return $this->response->redirect('dashboard');
    }
}
