{% set meta = side.meta[0] %}
<div class="item_info clearfix">
    <div class="nameAndpercent clearfix">
        {% if meta.image %}
            <div class="tooltip_container over-image">
                <a href="#" class="bg-black white" data-toggle="popover" data-trigger="hover" data-animation="true" data-placement="top" data-content="{{ meta.description  }}">i</a>
            </div>
        <div class="item_photo">
            <img src="{{meta.image}}" alt=""/>
        </div>
        {% endif %}
    </div>
    <div class="item_name">
        <h2>{{meta.title}}</h2>
    </div>
    <div class="comparison_percentbar">
        <div class="pros_percentbar">
            <span class="percent" data-sides-sum="{{side.sum}}">{{side.perc}}%</span>
            <span class="number">   <strong>{{side.votes}}</strong> {{ t._('votes') }}  </span>
            <div style="width: {{side.perc}}%;" class="pros_percent"></div>
        </div>
    </div>
    {% if side.voted == true %}
        {% set class = "supported dimmed" %}
        {% set text = t._('voted_this') %}
    {% else %}
        {% set class = "" %}
        {% set text = t._('vote_this') %}
    {% endif %}
    <div class="item_vote_btn">
        <a class="Support {{class}}" href="#" data-container-type="{{comparison.type}}" data-container-id="{{comparison.id}}" data-side-id="{{side.id}}" data-comparison-title="{{meta.title}}">{{ text }}</a>
    </div>
</div>
{% include "partials/comparison/argument-form" with ['comparison':comparison] %}
<div class="appndbl_comnts_cont">
    {% if ad %}
        {% include "partials/argumentsbox/ad" with ['ad' : ad]%}
    {% endif %}
</div>
<div class="item_comments appndbl_comnts_cont">
    {% if side.args.args %}
        {% for argument in side.args.args %}
            {% include "partials/argumentsbox/argument" with ['comparison':comparison, 'argument':argument] %}
        {% endfor %}
    {% endif %}
</div>
{%  if side.args.hasmore %}
    <div class="show_arg_container">
        <a class="showArgsComments" href="#">{{ t._('show-more') }} </a>
    </div>
{% endif %}
