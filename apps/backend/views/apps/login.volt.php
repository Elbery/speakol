<div id="wrapper">

    <!-- Sidebar -->
    
<?php $controller = $this->dispatcher->getControllerName(); ?>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar_brand"><?php $speakolLogo = $this->tag->image(array('img/symbol.png', 'alt' => 'Speakol', 'title' => 'Speakol')); ?><?php echo $this->tag->linkto('index', $speakolLogo); ?></li>
        <?php if ($this->session->app_data) { ?>
        <li class="sidebar_dshbrd <?php echo ($controller == 'dashboard' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('dashboard', 'dashboard'); ?></li>
        <li class="sidebar_dbt <?php echo ($controller == 'debates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('debates/code', 'Create Debate'); ?></li>
        <li class="sidebar_cmprsn <?php echo ($controller == 'comparisons' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('comparisons/code', 'Create Comparison'); ?></li>
        <li class="sidebar_argmnt <?php echo ($controller == 'arguments' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('arguments/code', 'Create Argument'); ?></li>
        <li class="sidebar_mdrt <?php echo ($controller == 'moderates' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('moderates/index', 'Moderate'); ?></li>
        <li class="sidebar_anlsys <?php echo ($controller == 'analysis' ? 'current' : ''); ?>"><?php echo $this->tag->linkto('analysis', 'Analysis'); ?></li>
        <li class="sidebar_aignout"><?php echo $this->tag->linkto('apps/logout', 'Sign Out'); ?></li>
        <?php
} ?>
    </ul>
</nav>

    <!-- Page content -->
    <div id="page-content-wrapper">

        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <a class="navicon-button x" href="#" id="menu-toggle"><div class="navicon"></div></a>
                <div class="story_container login">
                    <div class="top_bar">
                        <span>First time at Speakol ?</span>
                        <?php echo $this->tag->linkto(array('apps/create', 'Create an account now', 'class' => 'btn blue_btn')); ?>
                    </div>
                    <div class="login_container">
                        <h2>login</h2>
                        <?php echo $this->tag->form(array('apps/login')); ?>
                        <div class="form_cont">
                            <div class="input-group">
                                <span class="input-group-addon email_ico"></span>
                                <?php echo $this->tag->textfield(array('email', 'placeholder' => 'Email address', 'class' => 'form-control', 'data-validation-engine' => 'validate[required]')); ?>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon password_ico"></span>
                                <?php echo $this->tag->passwordfield(array('password', 'placeholder' => 'Password', 'class' => 'form-control', 'data-validation-engine' => 'validate[required]')); ?>
                            </div>
                            <div class="form_row">
                                <input type="checkbox" id="rememberMe"/>
                                <label for="rememberMe"> Remember me</label>
                                <?php echo $this->tag->submitbutton(array('Login', 'class' => 'btn blue_btn')); ?>
                            </div>
                            <div class="form_row">
                                <a class="forgot" href="<?php echo $this->url->get('apps/forgetpassword'); ?>"> Forgot password? </a>
                            </div>
                        </div>
                        <?php echo $this->tag->endform(); ?>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>