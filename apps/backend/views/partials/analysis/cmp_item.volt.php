<li class="cmprsn_item">
    <div class="cont_txt">
        <a href="#"><?php echo $item['data']['title']; ?></a>
        <time><?php echo $item['data']['created_at']; ?></time>
    </div>
    <div class="cont_num">
        <span><?php echo $item['counts']['total_votes']; ?></span>
        <small>Votes</small>
    </div>
    <div class="cont_num">
        <span><?php echo $item['counts']['total_comments']; ?></span>
        <small>Interactions</small>
    </div>
</li>