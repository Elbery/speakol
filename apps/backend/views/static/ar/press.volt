<style>

    li {
        list-style-type: decimal;
    }
</style>
<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">


        <div class="story_container static_content">


            <h2>البيان الصحفي لSpeakol – 5 نوفمبر 2014. قمة الموقع 14.  </h2>   
            {{ linkTo({'downloads/speakol_logo.zip', 'Download Speakol Logo','class':'grnbtn', 'title': 'Download Speakol Logo'}) }}

            <p class="large">أطلقت Speakol ملحقا جديدا يقوم بتوفير الإصدارات الإعلامية، ومقدمي المحتوى، والعلامات التجارية الشهيرة وبدء تفاعل محسن مع المستخدمين مع محتوى إعلامي بحسب المواضيع وإطلاق منتجات جديدة.</p>

            <p class="large">بدأ إطلاق Speakol عام 2012 تحت اسم (ناقشني)في أثناء الثورة المصرية. صُمم الموقع ليكون منصة مجانية ديمقراطية مفتوحة تسهّل المناظرات والنقاشات السياسية الحرجة حول الظروف الاجتماعية والسياسية داخل البلاد.</p>
            <p class="large">
مع وجود قاعدة مستخدمين أقوى بحجم يتجاوز 400 K فإن تغيير الاسم وإعادة الإطلاق يتوافق مع رؤية Speakol للذهاب للعالمية.
</p>
            <p class="large">
أولوياتهم للمستقبل القريب سوف تكون تقديم خدمة مميزة لناشري المحتوى الإعلامي والعلامات التجارية على هيئة إضافات ثلاث: مناظرات وصناديق المناظرة والمقارنات (حيث سوف يكون بإمكان المستخدمين المناظرة حول جانبين مقدمين لقصة ما، ومن ثم التصويت إما لصالح أو ضد أحد الجانبين والاختيار بين ما يصل إلى أربعة اختيارات في أي موضوع – على التوالي)
</p>

            <p class="large">
لديهم بالفعل أكثر من ثمانين ناشرا يستخدمون الخدمة في مصر.
</p>

            <p class="large">
            <h2>
فوائد استخدام Speakol
</h2>

            <ol>
                <li>أنشئ ملفا شخصيا للمستخدم بسهولة عن طريق صفحة التسجيل الخاصة بنا سهلة الاستخدام</li> 

                <li>إدماج فوري لأي من صيغ CMS من خلال رموز HTML مضمنة.</li>

                <li>اعقد مناظرات ومقارنات فورية وثيقة الصلة بالمحتوى المنشور الخاص بك عبر واجهة المستخدم جيدة التنظيم سهلة الاستعمال الخاصة بهم.</li>

                <li>عزز وغير وشجع تفاعل المستخدمين مع محتواك واعرف معلومات لا تقدر بثمن وقابلة للقياس لمؤشرات اتجاهات المستخدمين حول أي موضوع تقوم بنشره. </li>

                <li>شارك المناظرات بسهولة حول شبكات التواصل الاجتماعي التقليدية مثل فيس بوك وتويتر وجوجل بلس.</li>

                <li>توصل إلى البيانات التحليلية فورا  عبر لوحة القيادة بسيطة الاستعمال.</li>

                <li>حافظ على مستخدمي موقعك عبر أنظمة الإشعارات الملائمة الخاصة بالمستخدمين.</li>

                <li>استخدم Speakol إلى جانب أنظمة التعليق الخاصة بك (لا لتحل محلها) </li>

                <li>احذف بسهولة دون أي ضرر على أداء موقعك.</li>

                <li>مجاني الاستخدام (الاشتراكات بنظام التقسيط ذات الخدمات المتطورة سوف يتم إطلاقها في فترة من 6 إلى 12 شهرا)</li>

            </ol>
            </p>
            <p class="large">
أطلقت Speakol مؤخرا فرعا في المملكة المتحدة بهدف توفير مزودي محتوى محلي وعلامات تجارية لخدمتهم ويهدفون لخلق شراكات عالمية مشابهة وتعاون مع الأطراف المعنية.
            </p>
            <p class="large">  كما سوف يقومون بإعادة إطلاق شبكتهم الاجتماعية بحيث يكون بإمكان أي مستخدم أن يسجل دخوله ويقوم بعمل ملف شخصي والمضي قدما بعمل مناظرات ومناقشات مع الجمهور الأوسع على عدد غير محدود من المواضيع محل الاهتمام.
            </p>
            <p class="large"> Bootstrapped to date with USD300K and with a current 15 strong staff, Speakol are building on their success in Egypt and aim to change how users interact with online content in a unique, structured, civilized and different manner. 

            </p>
        </div>

    </div>
</div>
