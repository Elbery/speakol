<div class="sp-block-group js-argument argument" data-arg-id="{{ argument.id }}" data-owner-id="{{ argument.owner_id  }}">
    <div class="sp-block-group no-margin">
        <div class="media sp-block eleven {{ right ? 'one-push' : '' }}">
          <div class="{{ right ? "pull-right" : "pull-left" }}">
                {% if argument.owner_photo is defined and argument.owner_photo != "" %}
                    <a class="js-profile-link" href="{{ argument.admin_at ? config.application.main_website ~ 'apps/profile/' ~ argument.admin_at : '/users/profile/' ~ argument.owner_id }}" target="_blank"><img class="media-object profile-picture-sm radius-5" src="{{argument.owner_photo}}" alt="" title=""></a>
                {% else %}
                    <a class="js-profile-link" href="{{ argument.admin_at ? config.application.main_website ~ 'apps/profile/' ~ argument.admin_at : config.application.main_website ~ 'users/profile/' ~ argument.owner_id }}" target="_blank">{{ image("img/user_default.gif" , 'class': 'media-object profile-picture-sm radius-5') }}</a>
                {% endif %}
          </div>
          <div class="media-body {{ right ? 'text-right' : '' }}">
                {% set statusColor = (argument.owner_status) ? "color-53a960" : "color-bebebe" %}
                {% if right %}
                    {% set headingContent = '<i class="fa fa-circle-o font-11 hide-xxs js-owner-status ' ~ statusColor ~  '" data-owner-id="' ~ argument.owner_id~'"></i>&nbsp;' ~ argument.owner_name %} 
                {% else %}
                    {% set headingContent = argument.owner_name ~  '&nbsp;<i class="fa fa-circle-o font-11 hide-xxs ' ~ statusColor ~  '" data-owner-id="' ~ argument.owner_id~'"></i>' %} 
                {% endif %}
                
                <a class="js-profile-link media-heading" href="{{ argument.admin_at ? config.application.main_website ~ 'apps/profile/' ~ argument.admin_at : config.application.main_website ~ 'users/profile/' ~ argument.owner_id }}" target="_blank" class="media-heading h5 light-blue display-block {{ right ? "text-right" : "" }}">
                    {{ headingContent }}
                </a>
                <p class="font-11 color-aebac1 {{ right ? "text-right" : "" }} {{ lang === 'ar' ? 'rtl' : '' }}">{{ t._('on') }} {{ argument.created_at }}</p>
          </div>
        </div>
        {% if LoggedInUserData.id == argument.owner_id %}
        <div class="one sp-block {{ right ? 'eleven-pull' : '' }}">
            <button class="btn-reset arg_delete transition-opacity opacity-0 opacity-hover color-c1cacf" data-argument-id="{{ argument.id }}">
                <i class="fa fa-times"></i>
            </button>
        </div>
        {% endif %}
    </div>
    <div class="sp-block-group no-margin">
        <p class="readmore font-13 break-word no-margin {{ lang ==='ar' and comparison ? 'text-right' : '' }}">{{ utility.insertHashtagHTML(argument.context) }}</p>
        {% if argument.attached_photos %}
        {% for file in argument.attached_photos %}
            {% set extension = substr(file.url, -3)  %}
            {% if extension === "ogg"  %}
                <div class="js-player player">
                    <input type="hidden" value="{{ file.url  }}">
                    <button class="js-play-button play-button clearfix">
                        <span class="play-icon fa fa-play pull-left"></span>
                        <span class="play-text font-13 pull-left" data-text="{{ t._('click-play')  }}" data-text-small="{{ t._('click-play-small') }}"></span>
                    </button>
                </div>
            {% else %}
             {{ image(file.url, "class":"context_img margin-top-sm") }}
            {% endif %}
        {% endfor %}
        {% endif %}
    </div>

    <div class="sp-block-group text-center margin-bottom-sm h6 js-argument-actions margin-top-sm {{ lang === 'ar' and comparison ? 'flip' : '' }}">
        <div class="sp-block four-xs three-sm  vertical-separator-right two {{comparison ? 'comparison-thumbs-button' : ''}} text-left text-center-xs text-center-sm">
            <button data-argument-id="{{ argument.id }}" class="js-thumbs-up btn-reset no-padding {{ (argument.currentUserhasVoted and argument.currentUserVote) ? "js-voted color-5bb767" : "color-c1cacf"  }}" title="{{ t._('vote_up') }}" data-thumbs="{{ argument.thumbs_up }}">
                <i class="sp-icon sp-thumbs-up {{ lang === 'ar' and comparison ? 'flip ' : '' }}"></i>&nbsp;
                <span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{ argument.thumbs_up}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm two {{ comparison ? 'comparison-thumbs-button' : ''  }} text-right text-center-xs text-center-sm">
            <button data-argument-id="{{ argument.id }}" class="js-thumbs-down btn-reset no-padding {{ (argument.currentUserhasVoted and !argument.currentUserVote) ? "js-voted color-d11b1f" : "color-c1cacf"  }}" title="{{ t._('vote_down') }}" data-thumbs="{{ argument.thumbs_down }}">
                <i class="sp-icon sp-thumbs-down {{ lang === 'ar' and comparison ? 'flip' : '' }}"></i>&nbsp;
                <span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{ argument.thumbs_down}}</span>
            </button>
        </div>
        <div class="sp-block four-xs three-sm three-offset-sm {{ comparison ? 'two comparison-reply-button' : 'two six-offset-lg' }} text-right">
            <a class="color-c1cacf js-show-replies show-replies hover-c1cacf" href="javascript:"><span class="{{ lang === 'ar' and comparison ? 'flip inline-block' : '' }}">{{argument.replies_count}}</span>&nbsp;<i class="sp-icon sp-comment {{ lang === 'ar' and comparison ? 'flip' : '' }}"></i></a>
        </div>
    </div>
    {% set argumentsBoxURL = "" %}
    {% if argument.replies is defined %}
        {% include "partials/argumentsbox/replies-container" with ['argumentID': argument.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies': argument.replies, 'replies_count' : argument.replies_count, 'total_replies_pages': argument.total_replies_pages is defined?argument.total_replies_pages:0 ] %}
    {% else %}
        {% include "partials/argumentsbox/replies-container" with ['argumentID': argument.id, 'argumentsBoxURL': argumentsBoxURL?argumentsBoxURL:'', 'replies_count' : argument.replies_count, 'total_replies_pages': argument.total_replies_pages is defined?argument.total_replies_pages:0] %}
    {% endif %}
</div>
