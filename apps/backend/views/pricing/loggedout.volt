<div class="header-container">
    <header>
        <div class="logo"><a href="/"> {{ image("img/logo.png", "alt": "Speakol for websites", "title": "Speakol for websites") }}</a></div>
        <div class="lang_menu dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="lang-dropdown" data-toggle="dropdown" aria-expanded="true">
                {{ lang == "en" ? "English" : "العربية"  }}
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="lang-dropdown">
                {% if lang != "en" %}
                <li role="presentation"><a role="menuitem" tabindex="-1" href="/public/?lang=en">English</a></li>
                {% else %}
                <li role="presentation"><a role="menuitem" tabindex="-1" href="/public/?lang=ar">العربية</a></li>
                {% endif %}
            </ul>
        </div>
        <div class="r_lgn">
            {% if (userSession | length) == 0 %}
            {{ linkTo({'apps/create', 'Create a Publisher Account', "class": "lnk publisher-account"}) }}
            <span> or </span>
            {{ linkTo({'apps/login', 'Sign In', "class": "lnk"}) }}
            {% else %}
            {% set user = userSession %}
            {% set welcomeUser = user['name'] %} 
            <div style="width:30px; display:inline-block">
                {% if user and user['image'] %}
                {{ image(user['image']) }}
                {% else %}
                {{ image("img/user_default.gif") }}
                {% endif %}
            </div> 
            {{ linkTo({'dashboard', welcomeUser, "class": "lnk"}) }} | {{ linkTo({'apps/logout', 'Sign Out', "class": "lnk"}) }}
            {% endif  %}
        </div>
    </header>
</div>
<div class="main-container bg-f7f7f7 padding-bottom-lg">
<div class="main c-wrapper margin-top-sm">
    
    <div class="row margin-top-lg padding-top-lg margin-bottom-lg">
        <h3 class="font-35 text-center margin-top-lg margin-bottom-lg padding-bottom-lg color-434e59">{{ t._('plan-start') }}</h3>
    </div>
    <div class="helvetica-font margin-top-lg padding-top-lg">
        {% include 'partials/pricing/table.volt' %}
    </div>
</div>
</div> <!-- #main-container -->
<div class="footer-container">
    <footer>
        <div class="f-block">
            <a class="footer_logo" href="#">{{ image("img/logo_white.png", "alt": "Speakol", "title": "Speakol") }}</a>
        </div>
        <div class="f-block">
            <!-- Begin MailChimp Signup Form -->

            <form action="//speakol.us9.list-manage.com/subscribe/post?u=4867705d0bad713092d2d1928&amp;id=ab84493550" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <label for="mail_list">Join our mailing list</label>
                <input type="text" id="mail_list" name="EMAIL" placeholder="your email address"/>
                <input type="submit" value="JOIN" class="btn"/>
            </form>
        </div>
        <div class="f-block">
            <span> Get connected </span>
            <a target="_blank" class="soc_cnnct facebook" href="https://facebook.com/Speakol" title="Facebook"> Facebook</a>
            <a target="_blank" class="soc_cnnct twitter" href="http://www.twitter.com/speakol_" title="Twitter"> Twitter</a>
            <a target="_blank" class="soc_cnnct gPlus" href="https://plus.google.com/115379961634886743482/" title="Google plus"> google pluse</a>
            <a target="_blank" class="soc_cnnct instagram" href="http://instagram.com/speakol_" title="Instagram"> Instagram</a>
        </div>
        <div class="f-links">
            {{ linkTo('about', 'About Us') }}
            {{ linkTo('contactus', 'Contact Us') }}
            {{ linkTo('terms', 'Terms') }}
            {{ linkTo('privacy', 'Privacy') }}
            {{ linkTo('apps', 'Apps') }}
            {{ linkTo('careers', 'Careers') }}
            {{ linkTo('press', 'Press') }}
        </div>
        <div class="copyRights">Copyright © 2014 - All rights reserved, Speakol | part of Naqeshny for Information Technology | 36 Mussadak Street Dokki, Giza, Egypt. <br/>
            Speakol UK Ltd | Company Number 9234670 | Registered in England and Wales</div>
    </footer>
</div>
