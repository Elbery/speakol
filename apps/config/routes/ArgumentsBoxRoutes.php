<?php
namespace Speakol\Routes;
class ArgumentsBoxRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'argumentsbox',));
        $this->setPrefix('/arguments');
        $this->add('/render', array('action' => 'render'))->setName("render-arguments-box");
        $this->add('/render/argument', array('action' => 'renderArgument',))->setName("render-argument");
        $this->add('/changesort', array('action' => 'changeSort',))->setName("change-sort");
        $this->add('/render/reply', array('action' => 'renderReply',))->setName("render-reply");
        $this->add('/render/replies', array('action' => 'renderReplies',))->setName("render-replies");
        $this->add('/render/nested-replies', array('action' => 'renderNestedReplies',))->setName("nested-replies");
        $this->add('/render/arguments', array('action' => 'renderArguments',))->setName("render-arguments");
        $this->add('', array('action' => 'index'))->setName("list-arguments-box");
        $this->add('/create', array('action' => 'create'))->setName("create-arguments-box");
        $this->add('/{argument_id}/thumbup', array('action' => 'thumbup'))->setName("thumbup-arguments-box");
        $this->add('/{argument_id}/thumbdown', array('action' => 'thumbdown'))->setName("thumbup-arguments-box");
        $this->add('/code', array('action' => 'code'))->setName("arguments-box-code");
    }
}
