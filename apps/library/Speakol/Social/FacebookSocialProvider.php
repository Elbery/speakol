<?php
namespace Speakol\Social;
class FacebookSocialProvider extends \Phalcon\Mvc\User\Plugin implements SocialProvider {
    public function getAccessToken($code, $state_code_a, $redirectURI) {
        $redirectURI.= '?state_code_a=' . $state_code_a;
        return $this->facebook->getAccessToken($redirectURI);
    }
    public function getLoginURL($redirectURI) {
        return $this->facebook->getLoginUrl(array('redirect_uri' => $redirectURI, 'scope' => 'public_profile, email, user_birthday, user_work_history, user_hometown', 'display' => 'popup',));
    }
    public function getSocialId() {
        $user = $this->facebook->getUser();
        return ($user) ? $user : false;
    }
}
