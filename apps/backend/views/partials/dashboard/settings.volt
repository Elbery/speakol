<div class="settings_cont popup">
    <a href="javascript:"> {{ t._('settings') }} </a>
    <div class="popup_container">
        <div class="form_cont clearfix">
            {{ form('apps/update', 'method': 'post', 'enctype': 'multipart/form-data') }}
            <h3>{{ t._('app-dtls') }}</h3>
            <div class="form_row fr mt_mi ">
                <div class="input-group">

                    <span class="logo_thumb">
                        {% if appImage %}
                            {{ image( appImage, "alt": "Speakol", "title": "Speakol") }}
                        {% else %}
                            {{ image("img/symbol.png", "alt": "Speakol", "title": "Speakol") }}
                        {% endif %}
                    </span>

                    <div class="fileUpload btn blue_btn">
                        <span>{{ t._('upload-logo') }}</span>
                        {{ fileField({"app_logo", "class": "upload", "id":"uploadBtn"}) }}
                    </div>
                    <!--<input id="uploadFile" placeholder="Choose File" disabled="disabled" />-->
                </div>
            </div>
            <div class="form_row full">
                <div class="input-group">
                    <span class="input-group-addon name_ico"></span>
                    {{ textField({"app_name", "placeholder": t._('app-name'), "class": "form-control"}) }}
                </div>
            </div>
            <div class="form_row full">
                <div class="input-group">
                    <span class="input-group-addon url_ico"></span> 
                    {{ textField({"app_url", "placeholder": t._('website-URL'), "class": "form-control"}) }}
                </div>
            </div>
            <div class="form_row full">
                <div class="input-group">
                    <span class="input-group-addon category_ico"></span>
                    {{ select("category", categories, 'using': ['id', 'name'],  'class': 'form-control') }}
                </div>
            </div>
            <div class="form_row full">
                <div class="input-group">
                    <span class="input-group-addon country_ico"></span>
                    {{ select("lang", langs, 'using': ['id', 'name'],  'class': 'form-control') }}
                </div>
            </div>
            <a href="#" class="password-toggle">{{ t._('change-password')  }}</a>
            <hr/>            
            <div class="password-toggle-container hide">
                <h3>{{ t._('chng-passwd') }}</h3>
                <div class="form_row">
                    <div class="input-group">
                        <span class="input-group-addon password_ico"></span>
                        {{ passwordField({"old_password", "placeholder": t._('old-passwd'), "class": "form-control"}) }}
                    </div>
                </div>
                <div class="form_row">
                    <div class="input-group">
                        <span class="input-group-addon password_ico"></span>
                        {{ passwordField({"password", "placeholder": t._('nw-passwd'), "class": "form-control"}) }}
                    </div>
                </div>
                <div class="form_row">
                    <div class="input-group">
                        <span class="input-group-addon password_ico"></span>
                        {{ passwordField({"confirm_password", "placeholder": t._('cnfrm-passwd'), "class": "form-control"}) }}
                    </div>
                </div>
                <hr/>
            </div>
            <div class="form_row">
                {{ submitButton({"submit", "value": t._('updt-accnt'), "class": "btn blue_btn"}) }}

            </div>

            {{ endForm() }}
        </div>
    </div>
</div>
