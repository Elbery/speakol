{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title':  t._('stats') ]  %}
        <div class="row margin-top-sm">
            <div class="col-lg-9">
                <ul class="nav nav-pills" id="">
                    <li class="active" >
                        <a href="#users" data-toggle="pill">
                            {{ t._('users') }}
                        </a>
                    </li>
                    <li>
                        <a href="#apps" data-toggle="pill">
                            {{ t._('apps') }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3 ">
                <p><strong>Number of Apps Registered: </strong>{{ apps.number_of_apps }}</p>
                <p><strong>Number of Users Registered: </strong>{{ users.number_of_users }}</p>
            </div>
        </div>
        <div class="row">
            <div class="tab-content">
                <div class="tab-pane fade in active js-table-container" id="users" data-page="{{ page  }}" data-pane-type="users" data-sort="" data-sort-key="">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="js-sort-item cursor-pointer col-sm-2" data-sort-key="name" data-sort="none">
                                    Name
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-2" data-sort-key="email" data-sort="none">
                                    Email
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-2" data-sort-key="country" data-sort="none">
                                    Country
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-2" data-sort-key="created_at" data-sort="none">
                                    Registration Date
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-1" data-sort-key="job_title" data-sort="none">
                                    Occupation
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-2" data-sort-key="dob" data-sort="none">
                                    Dob
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-3" data-sort-key="votes" data-sort="none">
                                    No. of Votes
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer col-sm-3" data-sort-key="arguments" data-sort="none">
                                    No. of Comments
                                    <i class="fa fa-sort"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
            
                            {% for user in users.users_data  %}
                            <tr>
                                <td>
                                    {{ user.name  }}
                                </td>
                                <td>
                                    {{ user.email  }}
                                </td>
                                <td>
                                    {{ user.country  }}
                                </td>
                                <td>
                                    {{ user.created_at  }}
                                </td>
                                <td>
                                    {{ user.job_title  }}
                                </td>
                                <td>
                                    {{ user.dob  }}
                                </td>
                                <td>
                                    {{ user.votes  }}
                                </td>
                                <td>
                                    {{ user.arguments  }}
                                </td>
                            </tr>
                            {% endfor  %}
                        </tbody>
                    </table>
                    {% if users.has_more %}
                    <a class="pag-replies more_cont col-sm-6 col-sm-offset-3 bg-e8e8e8 border-none no-radius light-blue padding-top-lg padding-bottom-lg weight-bold" href="#">Show next 10 users</a>
                    {% endif %}
            
                </div>                    
            
                <div class="tab-pane fade js-table-container" id="apps" data-page="{{ page }}" data-pane-type="apps" data-sort="" data-sort-key="">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="js-sort-item cursor-pointer" data-sort-key="name" data-sort="none">
                                    App Name
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="website" data-sort="none">
                                    App Website
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="email" data-sort="none">
                                    Email
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="arguments" data-sort="none">
                                    No. of Comments
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="country" data-sort="none">
                                    Country
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="created_at" data-sort="none">
                                    Registration Date
                                    <i class="fa fa-sort"></i>
                                </th>
                                <th class="js-sort-item cursor-pointer" data-sort-key="plugins" data-sort="none">
                                    No. of Plugins
                                    <i class="fa fa-sort"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for app in apps.apps_data  %}
                            <tr>
                                <td>
                                    {{ app.name  }}
                                </td>
                                <td>
                                    {{ app.website  }}
                                </td>
                                <td>
                                    {{ app.email  }}
                                </td>
                                <td>
                                    {{ app.arguments  }}
                                </td>
                                <td>
                                    {{ app.country  }}
                                </td>
                                <td>
                                    {{ app.created_at  }}
                                </td>
                                <td>
                                    {{ app.plugins_count  }}
                                </td>
                            </tr>
                            {% endfor  %}
                        </tbody>
                    </table>
                    {% if apps.has_more %}
                    <a class="pag-replies more_cont col-sm-6 col-sm-offset-3 bg-e8e8e8 border-none no-radius light-blue padding-top-lg padding-bottom-lg weight-bold" href="#">Show next 10 apps</a>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
