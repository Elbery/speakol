<div class="sp-block-group margin-bottom-lg">
{% if isLoggedIn %}
    {% if LoggedInUserData.role_id === 4 %}
        {% set picture = LoggedInUserData.profile_picture %}
        {% set name = LoggedInUserData.name %}
        {% set logout = '/users/logout' %}
        {% set edit = '/users/update' %}
    {% else %}
        {% set picture = LoggedInUserData.app.image %}
        {% set name = LoggedInUserData.app.name %}
        {% set logout = '/apps/logout' %}
        {% set edit = 'https://plugins.speakol.com/apps/settings' %}
    {% endif %}
    <div class="dropdown profile-dropdown text-center margin-bottom-lg margin-top-lg">
        <button class="btn-reset dropdown-toggle sp-block-group text-left  width-95" type="button" id="profileDropdown" data-toggle="dropdown" aria-expanded="true">
            <img src="{{ picture  }}" alt="" class="publisher-image sp-block radius-5">
            <span class="color-333333 sp-block seven margin-top-xs inline-block padding-left-10 weight-bold break-word">
                {{ name }}
            </span>
            <i class="sp-icon sp-big-caret sp-block pull-right margin-top-sm"></i>
        </button>
        <ul class="dropdown-menu width-95 text-left" role="menu" aria-labelledby="profileDropdown">
            <li role="prensentation" class="">
                <a class="" href="{{ edit }}?lang={{ lang }}" title="" ><i class="sp-icon sp-alt-edit"></i>&nbsp;{{ t._('edit-profile') }}</a>
            </li>
            <li role="prensentation" class="">
                <a class="" href="{{ logout }}" title="" ><i class="sp-icon sp-alt-logout"></i>&nbsp;{{ t._('logout') }}</a>
            </li>
        </ul>
    </div>
{% else %}
    <h4 class="personalize-title h5 color-555555 {{ lang === 'ar' ? 'text-center ar' : 'text-left' }} margin-top-lg padding-top-xs">{{ t._('personalize-experience') }}</h4>
    <button class="js-register-button btn-reset display-block bg-57b25f width-90 white h5 padding-top-sm padding-bottom-sm radius-2 community-login-button">{{ t._('register') }}</button>
    <button class="js-login-button btn-reset display-block width-90 h5 margin-top-xs padding-top-sm padding-bottom-sm color-57b25f community-login-button radius-2 border-57b25f">{{ t._('login') }}</button>
{% endif %}
</div>
<ul class="js-categories categories">
    <li class="padding-top-sm padding-bottom-sm filter-link js-newsfeed">
        <a href="/newsfeed?lang={{ lang }}" class="anchor-reset {{ homePage  ? 'weight-bold' : ''}}"><i class="sp-icon sp-home margin-left-5 margin-right-10 margin-bottom-xs"></i>&nbsp;{{ t._('newsfeed') }}</a>
    </li>
    
    {% for category in favoriteCategories %}
    <li class="padding-bottom-sm padding-top-sm filter-link js-category js-category-highlighted" data-category-id="{{ category.id }}" data-category-name="{{ category.name }}">
        <button class="btn-reset  padding-left-5 padding-right-5 js-category-star js-category-starred">
            <i class="sp-icon sp-star-highlight margin-bottom-xs js-category-icon"></i>
        </button>&nbsp;
        <a href="/categories/show/{{ category.id }}?lang={{ lang }}" class="anchor-reset {{ currentCategory.id === category.id ? 'weight-bold' : '' }}">
            {{ t._(category.name) }}
        </a>
        {%if category.unseen %}
        <span class="inline-block pull-right bg-eaeaeb color-777777 radius-5 padding-10 padding-top-xs padding-bottom-xs margin-right-10 margin-left-5 unseen-category">
            {{ category.unseen }}
        </span>
        {% endif %}
    </li>
    {% endfor %}
    {% for category in normalCategories %}
    <li class="padding-bottom-sm padding-top-sm filter-link js-category" data-category-id="{{ category.id }}" data-category-name="{{ category.name }}">
        <button class="btn-reset  padding-left-5 padding-right-5 js-category-star">
            <i class="sp-icon sp-star margin-bottom-xs js-category-icon"></i>
        </button>&nbsp;
        <a href="/categories/show/{{ category.id }}?lang={{ lang }}" class="anchor-reset {{ currentCategory.id === category.id ? 'weight-bold' : '' }}">
            {{ t._(category.name) }}
        </a>
        {%if category.unseen %}
        <span class="inline-block pull-right bg-eaeaeb color-777777 radius-5 padding-10 padding-top-xs padding-bottom-xs margin-right-10 margin-left-5 unseen-category">
            {{ category.unseen }}
        </span>
        {% endif %}
    </li>
    {% endfor %}
</ul>

