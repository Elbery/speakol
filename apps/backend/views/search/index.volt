<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}
                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}

    {#     <!--[if lt IE 9]> #}
    {#     {{ assets.outputJs('header-ie9') }} #}
    {# <![endif]--> #}
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : '' }}">
<div class="js-svg-spinner hide">
<div class="sp-block-group text-center">
    {% include 'partials/newsfeed/spinner.volt' %}
</div>
</div>
{% include 'partials/newsfeed/header.volt' %}


<div class="sp-block-group community-content-container bg-f5f5f6 js-community-content-container">
    <div class="sp-block four bg-f5f5f6 community-sidebar-container js-community-sidebar-container">
        <div class="sp-block seven-xxl five-offset-xxl color-777777 h5">
            {% include 'partials/newsfeed/sidebar.volt' %}
        </div>
    </div>
    <div class="sp-block four-offset eight bg-white height-all feed-container js-feed-container">
        {% for flashType, messages in flash.getMessages() %}
            {% for message in messages %}
                {% set classes = ['notice': 'bg-4fc0e8', 'success' : 'bg-57b25f', 'error': 'bg-d11b1f']%}
                {% set icons = ['notice': 'sp-exclamation', 'success' : 'sp-check', 'error': 'sp-exclamation']%}
                <div class="{{ classes[flashType] }} font-13 white radius-5 media padding-top-sm padding-bottom-sm margin-top-xl margin-bottom-sm">
                    <div class="pull-left">
                        <i class="sp-icon {{ icons[flashType] }} margin-left-5"></i>
                    </div>
                    <div class="media-body">
                        <p class="no-margin">
                            {{ message }}
                        </p>
                    </div>
                </div>
            {% endfor%}
        {% endfor %}
        <form action="/search" method="GET" class="sp-block-group margin-top-lg margin-bottom-lg padding-bottom-lg">
            <input type="hidden" name="lang" value="{{ lang }}" >
            <div class="sp-block-group">
                <div class="sp-block ten margin-top-lg padding-top-sm">
                    <input id="" type="search" name="q" class="community-big-search text-indent-10 width-all radius-5 border-e9e9e9 padding-top-xs padding-bottom-xs h4" value="{{ query }}">
                </div>
                <div class="sp-block two padding-left-5 margin-top-lg padding-top-sm">
                    <input type="submit" value="{{ t._('search') }}" class="width-all btn-reset margin-left-5 no-padding padding-top-lg padding-bottom-lg h5 bg-57b25f">
                </div>
            </div>
            <div class="sp-block-group margin-top-sm color-909090">
                <label for="type" class="pull-left margin-top-xs padding-right-5 margin-right-5 weight-normal">
                    {{ t._('display') }}
                </label>
                <div class="pull-left width-20 border-e9e9e9 radius-5">
                    <select id="" name="type" class="select-reset color-909090 width-all community-select h5">
                        {% set values = ["all" :'All Content', "debates":'Debates', "comparisons":'Comparisons', "argumentboxes":'Argument boxes'] %}
                        {% for key, value in values %}
                            <option value="{{ key }}" {{ type === key ? 'selected' : '' }}>{{ t._(key) }}</option>
                        {% endfor %}
                    </select>
                </div>
                <label for="type" class="pull-left margin-top-xs padding-right-5 margin-right-5 margin-left-5 weight-normal">
                    {{ t._('categories') }}
                </label>
                <div class="pull-left width-20 border-e9e9e9 color-909090 radius-5">
                    <select id="" name="category" class="select-reset width-all color-909090 community-select h5">
                        <option value="">All Categories</option>
                        {% for category in categories %}
                            <option value="{{ category.id }}" {{ category.id === searchCategory ? 'selected' : '' }}>{{ t._(category.name) }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div>
        </form>
        {% include 'partials/newsfeed/feed.volt' %}
    </div>
</div>

{{ assets.outputJs() }}


<script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-56411923-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
// voting circle canvas
    if (($(".naqeshny-canvas")).length) {

        $(".naqeshny-canvas").each(function (index, obj) {
            $(obj).debateVersus();
        });
    }
</script>
</body>
</html>
