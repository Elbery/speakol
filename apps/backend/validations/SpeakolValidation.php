<?php
namespace Speakol\Backend\Validations;
use \Phalcon\Validation;
use Phalcon\Translate\Adapter\NativeArray;
class SpeakolValidation extends Validation {
    protected $_rules;
    protected $validations = array();
    protected $_messages;
    public function __construct($rules = false, $exclude = false) {
        $this->_rules = $rules;
        if (!$this->_rules) {
            $this->_rules = array_keys($this->validations);
        }
        if (is_array($exclude)) {
            $this->_rules = array_diff($this->_rules, $exclude);
        }
        $chosenValidations = array_intersect_key($this->validations, array_flip($this->_rules));
        foreach ($chosenValidations as $item => $validators) {
            foreach ($validators as $validator) {
                $this->add($item, $validator);
            }
        }
    }
    public function initialize() {
    }
    protected function _getTransPath() {
        $translationPath = '../apps/backend/messages/';
        $language = $this->session->get("lang");
        if (!$language) {
            $language = 'en';
        }
        if ($language === 'sp' || $language === 'en' || $language === 'ar') {
            return $translationPath . $language;
        } else {
            return $translationPath . 'en';
        }
    }
    public function loadMainTrans() {
        $translationPath = $this->_getTransPath();
        require $translationPath . "/main.php";
        $mainTranslate = new NativeArray(array("content" => $messages));
        $this->_messages = !empty($this->_messages) ? array_merge($this->_messages, $messages) : $messages;
        $this->t = $mainTranslate;
    }
    public function loadCustomTrans($transFile) {
        $this->loadMainTrans();
        $translationPath = $this->_getTransPath();
        require $translationPath . '/' . $transFile . '.php';
        $this->_messages = !empty($this->_messages) ? array_merge($this->_messages, $messages) : $messages;
        $controllerTranslate = new NativeArray(array("content" => $this->_messages));
        $this->t = $controllerTranslate;
    }
}
