<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <style>
        .story_container li {
            list-style-type: alpha !important;
        }
    </style>
    <div id="page-content-wrapper">

        <div class="story_container static_content">
            <h1 id="terms-and-conditions-of-use">Terms and conditions of use</h1>
            <ol>
                <li><h2 id="introduction">Introduction</h2>
                    <ol>
                        <li><p>These terms and conditions govern your use of our website.</p>
                        </li>
                        <li><p>By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these t erms and conditions, you must not use our website.</p>
                        </li>
                        <li><p>If you register with our website, submit any material to our website or use any of our website services, we will ask you to expressly agree to these terms and conditions.</p>
                        </li>
                        <li><p>You must be at least 13 years of age to use our website; and by using our website or agreeing to these terms and conditions, you warrant and represent to us that you are at least 13 years of age.</p>
                        </li>
                        <li><p>Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our privacy and cookies policy.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="copyright-notice">Copyright notice</h2>
                    <ol>
                        <li><p>Copyright (c) 2014 Naqeshny for Information Technology Limited.</p>
                        </li>
                        <li><p>Subject to the express provisions of these terms and conditions:</p>
                            <ol>
                                <li><p>we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and</p>
                                </li>
                                <li><p>all the copyright and other intellectual property rights in our website and the material on our website are reserved.</p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </li>
                <li><h2 id="licence-to-use-website">Licence to use website</h2>
                    <ol>
                        <li><p>You may:</p>
                            <ol>
                                <li><p>view pages from our website in a web browser;</p>
                                </li>
                                <li><p>download pages from our website for caching in a web browser;</p>
                                </li>
                                <li><p>print pages from our website;</p>
                                </li>
                                <li><p>stream audio and video files from our website; and</p>
                                </li>
                                <li><p>use our website services by means of a web browser,</p>
                                </li>
                            </ol>
                            <p>subject to the other provisions of these terms and conditions.</p>
                        </li>
                        <li><p>Except as expressly permitted by Section 3.1 or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.</p>
                        </li>
                        <li><p>You may only use our website for your own personal and business purposes, and you must not use our website for any other purposes.</p>
                        </li>
                        <li><p>Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.</p>
                        </li>
                        <li><p>Unless you own or control the relevant rights in the material, you must not:</p>
                            <ol>
                                <li><p>republish material from our website (including republication on another website);</p>
                                </li>
                                <li><p>sell, rent or sub-license material from our website;</p>
                                </li>
                                <li><p>show any material from our website in public;</p>
                                </li>
                                <li><p>exploit material from our website for a commercial purpose; or</p>
                                </li>
                                <li><p>redistribute material from our website.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>Notwithstanding Section 3.5, you may redistribute our newsletter in print and electronic form to any person.</p>
                        </li>
                        <li><p>We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="rss-feed">RSS feed</h2>
                    <ol>
                        <li><p>You may access our RSS feed using an RSS reader or aggregator.</p>
                        </li>
                        <li><p>By accessing our RSS feed, you accept these terms and conditions.</p>
                        </li>
                        <li><p>Subject to your acceptance of these terms and conditions, we grant to you a non-exclusive, non-transferable, non-sub-licensable licence to display content from our RSS feed in unmodified form on any non-commercial website owned and operated by you, providing that you must not aggregate our RSS feed with any other feed when displaying it in accordance with this Section 4.3.</p>
                        </li>
                        <li><p>It is a condition of this licence that you include a credit for us and hyperlink to our website on each web page where the RSS feed is published (in such form as we may specify from time to time, or if we do not specify any particular form, in a reasonable form).</p>
                        </li>
                        <li><p>We may revoke any licence set out in this Section 4 at any time, with or without notice or explanation.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="acceptable-use">Acceptable use</h2>
                    <ol>
                        <li><p>You must not:</p>
                            <ol>
                                <li><p>use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</p>
                                </li>
                                <li><p>use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</p>
                                </li>
                                <li><p>use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</p>
                                </li>
                                <li><p>conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;</p>
                                </li>
                                <li><p>access or otherwise interact with our website using any robot, spider or other automated means;</p>
                                </li>
                                <li><p>violate the directives set out in the robots.txt file for our website; or</p>
                                </li>
                                <li><p>use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing).</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>You must not use data collected from our website to contact individuals, companies or other persons or entities.</p>
                        </li>
                        <li><p>You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="registration-and-accounts">Registration and accounts</h2>
                    <ol>
                        <li><p>To be eligible for an individual account on our website under this Section 6, you must be at least 13 years of age.</p>
                        </li>
                        <li><p>You may register for an account with our website by completing and submitting the account registration form on our website, and clicking on the verification link in the email that the website will send to you.</p>
                        </li>
                        <li><p>You must notify us immediately if you become aware of any unauthorised use of your account.</p>
                        </li>
                        <li><p>You must not use any other person&#39;s account to access the website, unless you have that person&#39;s express permission to do so.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="user-ids-and-passwords">User IDs and passwords</h2>
                    <ol>
                        <li><p>If you register for an account with our website, you will be asked to choose a user ID and password.</p>
                        </li>
                        <li><p>Your user ID must not be liable to mislead and must comply with the content rules set out in Section 12; you must not use your account or user ID for or in connection with the impersonation of any person.</p>
                        </li>
                        <li><p>You must keep your password confidential.</p>
                        </li>
                        <li><p>You must notify us in writing immediately if you become aware of any disclosure of your password.</p>
                        </li>
                        <li><p>You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="cancellation-and-suspension-of-account">Cancellation and suspension of account</h2>
                    <ol>
                        <li><p>We may:</p>
                            <ol>
                                <li><p>suspend your account;</p>
                                </li>
                                <li><p>cancel your account; and/or</p>
                                </li>
                                <li><p>edit your account details,</p>
                                </li>
                            </ol>
                            <p>at any time in our sole discretion without notice or explanation.</p>
                        </li>
                        <li><p>You may cancel your account on our website using your account control panel on the website.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="social-networking">Social networking</h2>
                    <ol>
                        <li><p>Registered users will have access to such additional features on our website as we may from time to time determine, which may include:</p>
                            <ol>
                                <li><p>facilities to complete a detailed personal profile on the website, to publish that profile on the website, and to restrict the publication of that profile to particular groups or individuals registered on the website;</p>
                                </li>
                                <li><p>facilities to create groups, manage groups that you have created, join and leave groups, and share information amongst group members;</p>
                                </li>
                                <li><p>the facility to send private messages via the website to particular groups or individuals registered on the website; and</p>
                                </li>
                                <li><p>the facility to post and publish text and media on the website.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>You agree to the publication of posts relating to you, by others, on our website; you acknowledge that such posts may be critical or defamatory or otherwise unlawful; and, subject to Section 15.1, you agree that you will not hold us liable in respect of any such posts, irrespective of whether we are aware or ought to have been aware of such posts.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="personal-profiles">Personal profiles</h2>
                    <ol>
                        <li><p>All information that you supply as part of a personal profile on the website must be true, accurate, current, complete and non-misleading.</p>
                        </li>
                        <li><p>You must keep your personal profile on our website up to date.</p>
                        </li>
                        <li><p>Personal profile information must also comply with the provisions of Section 5 and Section 12.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="your-content-licence">Your content: licence</h2>
                    <ol>
                        <li><p>In these terms and conditions, &quot;your content&quot; means all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website.</p>
                        </li>
                        <li><p>You grant to us a worldwide, irrevocable, non-exclusive, royalty-free licence to use, reproduce, store, adapt, publish, translate and distribute your content in any existing or future media / reproduce, store and publish your content on and in relation to this website and any successor website / reproduce, store and, with your specific consent, publish your content on and in relation to this website.</p>
                        </li>
                        <li><p>You grant to us the right to sub-license the rights licensed under Section 11.2.</p>
                        </li>
                        <li><p>You grant to us the right to bring an action for infringement of the rights licensed under Section 11.2.</p>
                        </li>
                        <li><p>You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.</p>
                        </li>
                        <li><p>You may edit your content to the extent permitted using the editing functionality made available on our website.</p>
                        </li>
                        <li><p>Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="your-content-rules">Your content: rules</h2>
                    <ol>
                        <li><p>You warrant and represent that your content will comply with these terms and conditions.</p>
                        </li>
                        <li><p>Your content must not be illegal or unlawful, must not infringe any person&#39;s legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law).</p>
                        </li>
                        <li><p>Your content, and the use of your content by us in accordance with these terms and conditions, must not:</p>
                            <ol>
                                <li><p>be libellous or maliciously false;</p>
                                </li>
                                <li><p>be obscene or indecent;</p>
                                </li>
                                <li><p>infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;</p>
                                </li>
                                <li><p>infringe any right of confidence, right of privacy or right under data protection legislation;</p>
                                </li>
                                <li><p>constitute negligent advice or contain any negligent statement;</p>
                                </li>
                                <li><p>constitute an incitement to commit a crime, instructions for the commission of a crime or the promotion of criminal activity;</p>
                                </li>
                                <li><p>be in contempt of any court, or in breach of any court order;</p>
                                </li>
                                <li><p>be in breach of racial or religious hatred or discrimination legislation;</p>
                                </li>
                                <li><p>be blasphemous;</p>
                                </li>
                                <li><p>be in breach of official secrets legislation;</p>
                                </li>
                                <li><p>be in breach of any contractual obligation owed to any person;</p>
                                </li>
                                <li><p>depict violence, in an explicit, graphic or gratuitous manner;</p>
                                </li>
                                <li><p>be pornographic, lewd, suggestive or sexually explicit;</p>
                                </li>
                                <li><p>be untrue, false, inaccurate or misleading;</p>
                                </li>
                                <li><p>consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death, or any other loss or damage;</p>
                                </li>
                                <li><p>constitute spam;</p>
                                </li>
                                <li><p>be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory; or</p>
                                </li>
                                <li><p>cause annoyance, inconvenience or needless anxiety to any person.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>Your content must be appropriate, civil and tasteful, and accord with generally accepted standards of etiquette and behaviour on the internet.</p>
                        </li>
                        <li><p>You must not use our website to link to any website or web page consisting of or containing material that would, were it posted on our website, breach the provisions of these terms and conditions.</p>
                        </li>
                        <li><p>You must not submit to our website any material that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="report-abuse">Report abuse</h2>
                    <ol>
                        <li><p>If you learn of any unlawful material or activity on our website, or any material or activity that breaches these terms and conditions, please let us know.</p>
                        </li>
                        <li><p>You can let us know by email or by using our abuse reporting form.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="limited-warranties">Limited warranties</h2>
                    <ol>
                        <li><p>We do not warrant or represent:</p>
                            <ol>
                                <li><p>the completeness or accuracy of the information published on our website;</p>
                                </li>
                                <li><p>that the material on the website is up to date; or</p>
                                </li>
                                <li><p>that the website or any service on the website will remain available.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent that these terms and conditions expressly provide otherwise, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.</p>
                        </li>
                        <li><p>To the maximum extent permitted by applicable law and subject to Section 15.1, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="limitations-and-exclusions-of-liability">Limitations and exclusions of liability</h2>
                    <ol>
                        <li><p>Nothing in these terms and conditions will:</p>
                            <ol>
                                <li><p>limit or exclude any liability for death or personal injury resulting from negligence;</p>
                                </li>
                                <li><p>limit or exclude any liability for fraud or fraudulent misrepresentation;</p>
                                </li>
                                <li><p>limit any liabilities in any way that is not permitted under applicable law; or</p>
                                </li>
                                <li><p>exclude any liabilities that may not be excluded under applicable law.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>The limitations and exclusions of liability set out in this Section 15 and elsewhere in these terms and conditions:</p>
                            <ol>
                                <li><p>are subject to Section 15.1; and</p>
                                </li>
                                <li><p>govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
                        </li>
                        <li><p>We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</p>
                        </li>
                        <li><p>We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.</p>
                        </li>
                        <li><p>We will not be liable to you in respect of any loss or corruption of any data, database or software.</p>
                        </li>
                        <li><p>We will not be liable to you in respect of any special, indirect or consequential loss or damage.</p>
                        </li>
                        <li><p>You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="indemnity">Indemnity</h2>
                    <ol>
                        <li><p>You hereby indemnify us, and undertake to keep us indemnified, against any and all losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute) incurred or suffered by us and arising directly or indirectly out of:</p>
                            <ol>
                                <li><p>any breach by you of any provision of these terms and conditions; or</p>
                                </li>
                                <li><p>your use of our website.</p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </li>
                <li><h2 id="breaches-of-these-terms-and-conditions">Breaches of these terms and conditions</h2>
                    <ol>
                        <li><p>Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:</p>
                            <ol>
                                <li><p>send you one or more formal warnings;</p>
                                </li>
                                <li><p>temporarily suspend your access to our website;</p>
                                </li>
                                <li><p>permanently prohibit you from accessing our website;</p>
                                </li>
                                <li><p>block computers using your IP address from accessing our website;</p>
                                </li>
                                <li><p>contact any or all your internet service providers and request that they block your access to our website;</p>
                                </li>
                                <li><p>commence legal action against you, whether for breach of contract or otherwise; and/or</p>
                                </li>
                                <li><p>suspend or delete your account on our website.</p>
                                </li>
                            </ol>
                        </li>
                        <li><p>Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="third-party-websites">Third party websites</h2>
                    <ol>
                        <li><p>Our website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations.</p>
                        </li>
                        <li><p>We have no control over third party websites and their contents, and subject to Section 15.1 we accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="trade-marks">Trade marks</h2>
                    <ol>
                        <li><p>Naqeshny for Information Technology Limited, our logos and our other registered and unregistered trade marks are trade marks belonging to us; we give no permission for the use of these trade marks, and such use may constitute an infringement of our rights.</p>
                        </li>
                        <li><p>The third party registered and unregistered trade marks or service marks on our website are the property of their respective owners and, unless stated otherwise in these terms and conditions, we do not endorse and are not affiliated with any of the holders of any such rights and as such we cannot grant any licence to exercise such rights.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="competitions">Competitions</h2>
                    <ol>
                        <li><p>From time to time we may run competitions, free prize draws and/or other promotions on our website.</p>
                        </li>
                        <li><p>Competitions will be subject to separate terms and conditions (which we will make available to you as appropriate).</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="variation">Variation</h2>
                    <ol>
                        <li><p>We may revise these terms and conditions from time to time.</p>
                        </li>
                        <li><p>The revised terms and conditions will apply to the use of our website from the date of their publication on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of the terms and conditions. / We will give you written notice of any revision of these terms and conditions, and the revised terms and conditions will apply to the use of our website from the date that we give you such notice; if you do not agree to the revised terms and conditions, you must stop using our website.</p>
                        </li>
                        <li><p>If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="assignment">Assignment</h2>
                    <ol>
                        <li><p>You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions.</p>
                        </li>
                        <li><p>You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="severability">Severability</h2>
                    <ol>
                        <li><p>If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</p>
                        </li>
                        <li><p>If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="third-party-rights">Third party rights</h2>
                    <ol>
                        <li><p>These terms and conditions are for our benefit and your benefit, and are not intended to benefit or be enforceable by any third party.</p>
                        </li>
                        <li><p>The exercise of the parties&#39; rights under these terms and conditions is not subject to the consent of any third party.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="entire-agreement">Entire agreement</h2>
                    <ol>
                        <li>Subject to Section 15.1, these terms and conditions, together with our privacy and cookies policy, constitute the entire agreement between you and us in relation to your use of our website and supersede all previous agreements between you and us in relation to your use of our website.</li>
                    </ol>
                </li>
                <li><h2 id="law-and-jurisdiction">Law and jurisdiction</h2>
                    <ol>
                        <li><p>These terms and conditions shall be governed by and construed in accordance with Egyptian law.</p>
                        </li>
                        <li><p>Any disputes relating to these terms and conditions shall be subject to the exclusive (and non-exclusive users outside of Egypt) jurisdiction of the courts of Egypt.</p>
                        </li>
                    </ol>
                </li>
                <li><h2 id="statutory-and-regulatory-disclosures">Statutory and regulatory disclosures</h2>
                    <ol>
                        <li>We are registered in Egypt; and our registration number is Egypt Registration number: 57339 issued on 21/3/2012 </li>
                    </ol>
                </li>
                <li><h2 id="our-details">Our details</h2>
                    <ol>
                        <li><p>This website is owned and operated by Naqeshny for Information Technology Ltd.</p>
                        </li>
                        <li><p>We are registered in Egypt under registration number 57339 issued on 21/3/2012 , and our registered office is at 36 Mussadak Street Dokki, Giza 12311, Egypt.</p>
                        </li>
                        <li><p>Our principal place of business is at 36 Mussadak Street Dokki, Giza 12311, EGYPT.</p>
                        </li>
                        <li><p>You can contact us by writing to the business address given above, by using our website contact form, by email to maan[at]speakol[dot]com.</p>
                        </li>
                    </ol>
                </li>
            </ol>
        </div>

    </div>
</div>
