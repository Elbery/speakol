<li class="js-debate">
    <div class="row thin-underline margin-top-sm">
        <div class="col-xs-1">
            {% if appImage %}
                {{ image(appImage, "class": "media-object img-circle") }}
            {% else %}
                {{ image("img/user_default.gif", "class": "media-object img-circle") }}
            {% endif %}
        </div>
        <div class="col-xs-11">
            <div class="row margin-bottom-sm">
                <div class="col-xs-11">
                    {% if planID and planID <= 1 %}
                        <h4 class="font-20 black weight-bold" target="_blank" ><i class="speakol-icon speakol-debate"></i> {{ item['data']['title'] }}</h4>
                    {% else %}
                        <a href="{{url('debates/edit/?slug=' ~ item['data']['slug']) ~ '&app=' ~ app.id }}" class="font-20 black weight-bold" target="_blank" ><i class="speakol-icon speakol-debate"></i> {{ item['data']['title'] }}</a>
                    {% endif %}
                </div>
                <div class="col-xs-1 text-right">
                    <button class="btn btn-default bg-white remove_debate border-none no-radius" data-debate-slug="{{ item['data']['slug']  }}"><i class="fa fa-times red"></i></button>
                </div>
            </div>
            <div class="row color-7f8080 margin-bottom-sm">
                <div class="col-xs-5">
                    <div class="media">
                        <div class="pull-left">
                            {% if item['sides']['0']['image'] is defined and item['sides']['0']['image'] %}
                                {{ image( item['sides']['0']['image'], "class": "media-object img-circle") }}
                            {% else %}
                                {{ image("img/user_default.gif", "class": "media-object img-circle") }}
                            {% endif %}
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading light-blue weight-bold h5">{{ item['sides']['0']['title']}}</h4>
                            <p class="h5">{{ item['sides']['0']['opinion'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="circle versus font-20 padding-top-xs bg-cccdce text-center color-75777b">
                        {{ t._('vs') }}
                    </div>
                </div>
                <div class="col-xs-5">
                    <div class="media">
                        <div class="pull-right">
                            {% if item['sides']['1']['image'] is defined and item['sides']['1']['image'] %}
                                {{ image( item['sides']['1']['image'], "class": "media-object img-circle") }}
                            {% else %}
                                {{ image("img/user_default.gif", "class": "media-object img-circle") }}
                            {% endif %}
                        </div>
                        <div class="media-body text-right">
                            <h4 class="media-heading light-blue weight-bold h5">{{ item['sides']['1']['title']}}</h4>
                            <p class="h5">{{ item['sides']['1']['opinion'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row color-7f8080 font-13">
                <div class="col-xs-10 text-left">
                    <time>{{ item['data']['created_at'] }}</time> 
                </div>
                <div class="col-xs-2 text-right">
                    <p>
                        <i class="fa fa-comment"></i>&nbsp;{{ item['counts']['total_comments']}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</li>

