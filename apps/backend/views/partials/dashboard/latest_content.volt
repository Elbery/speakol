<div class="row thick-underline margin-left-sm">
    <h3 class="weight-bold font-20">{{ t._('ltst-contnt') }}</h3>
</div>
<div class="row margin-left-sm margin-top-sm">
    <ul>
        {% if data['debate'] is defined and data['debate'] %}
            {% for debate in data['debate'] %}
            <li class="margin-top-sm">
                <div class="media thin-underline">
                    <div class="pull-left no-margin">
                        <i class="speakol-icon speakol-debate-green"></i>
                    </div>
                    <div class="media-body">
                        {% if planID and planID <= 1%} 
                            <h4 class="media-heading font-15 weight-bold light-blue">{{ debate['title'] }}</h4>
                        {% else %}
                            <a href="{{ '/debates/edit?slug=' ~ debate['slug'] }}" class="media-heading font-15 weight-bold light-blue">{{ debate['title'] }}</a>
                        {% endif %}
                        <p class="font-11">
                            <time datetime="{{ debate['created_at']  }}" class="color-898b8e">{{ utility.formatDate(debate['created_at']) }}</time>
                            <a href="#" data-target="#get_code_{{ debate['id'] }}" data-toggle="modal">{{ t._('code') }}</a>
                        </p>
                    </div>
                </div>
            </li>
            <div class="modal fade js-code-modal" id="get_code_{{ debate['id'] }}" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="raw_code_container">
                                <div><span>{{ t._('choz-cntnt-lang') }}:</span>
                                    <select id="arg-box-lang" name="arg-box-lang">
                                        <option value="en">English</option>
                                        <option value="ar">اللغه العربيه</option>
                                    </select>
                                </div>
<div class="speakol-debate-box">
{% set slug  = this.config.application.webhost ~ "debates/render?app=" ~ appData.id ~ "&debate=" ~ debate['slug'] %}
                                            <pre id="gnrtd_code"><code>&lt;div class="speakol-debate"  data-lang="en" data-width="600" data-href="{{ slug }}" data-lang="en" &gt;&lt;/div&gt; &lt;script type="text/javascript" &gt; (function(doc, sc, id) { var js, sjs = document.getElementsByTagName(sc)[0]; if(doc.getElementById(id)) return; js = document.createElement(sc); js.src = "{{config.application.sdk_url}}js/sdk.js"; js.id=id; sjs.parentNode.insertBefore(js, sjs); })(document, 'script', 'speakol-sdk'); &lt;/script&gt;</code> </pre>
                                        </div>

                                <p>{{ t._('argbox-code') }}</p></div>
                        </div>
                        <div class="modal-footer">
                            <div class="form_row">
                                <!--<span class="alert-success"> Copied </span>-->
                                <a href="javascript:" class="btn blue_btn cp-generated-code">{{ t._('copy-code') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {% endfor %}
        {% endif %}
        {% if data['comparison'] is defined and data['comparison'] %}
            {% for comparison in data['comparison']  %}
            <li class="margin-top-sm">
                <div class="media thin-underline">
                    <div class="pull-left">
                        <i class="speakol-icon speakol-comparison-green"></i>
                    </div>
                    <div class="media-body">
                        {% if planID and planID <= 1 %}
                            <h4 class="media-heading font-15 weight-bold light-blue">{{ comparison['title'] }}</h4>
                        {% else %}
                            <a href="/comparisons/edit?slug={{ comparison['slug'] }}" class="media-heading font-15 weight-bold light-blue">{{ comparison['title'] }}</a>
                        {% endif %}
                        <p class="font-11">
                            <time datetime="{{ comparison['created_at']  }}" class="color-898b8e">{{ utility.formatDate(comparison['created_at']) }}</time>
                            <a href="#" data-target="#get_code_{{ comparison['id'] }}" data-toggle="modal">{{ t._('code') }}</a>
                        </p>
                    </div>
                </div>
            </li>
            <div class="modal fade js-code-modal" id="get_code_{{ comparison['id'] }}" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="raw_code_container">
                                <div><span>{{ t._('choz-cntnt-lang') }}:</span>
                                    <select id="arg-box-lang" name="arg-box-lang">
                                        <option value="en">English</option> 
                                        <option value="ar">اللغه العربيه</option>
                                    </select>
                                </div>
<div class="speakol-debate-box">
{% set slug  = this.config.application.webhost ~ "comparisons/render?app=" ~ appData.id ~ "&comparison=" ~  comparison['slug']%}
                                                <pre id="gnrtd_code"><code>&lt;div class="speakol-comparison" data-lang="en" data-width="600" data-href="{{ slug  }}" data-lang="en" &gt;&lt;/div&gt; &lt;script type="text/javascript" &gt; (function(doc, sc, id) { var js, sjs = document.getElementsByTagName(sc)[0]; if(doc.getElementById(id)) return; js = document.createElement(sc); js.src = "{{config.application.sdk_url}}js/sdk.js";js.id=id; sjs.parentNode.insertBefore(js, sjs); })(document, 'script', 'speakol-sdk'); &lt;/script&gt;</code></pre>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form_row">
                                    <!--<span class="alert-success"> Copied </span>-->
                                    <a href="javascript:" class="btn blue_btn cp-generated-code">{{ t._('copy-code') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {% endfor %}
        {% endif %}
        {% if data['argumentsbox'] and data['argumentsbox'] %}
            {% for argumentsbox in data['argumentsbox']  %}
            <li class="margin-top-sm">
                <div class="media thin-underline">
                    <div class="pull-left">
                        <i class="speakol-icon speakol-argumentsbox-green"></i>
                    </div>
                    <div class="media-body">
                        {% set title = argumentsbox['title'] | trim %}
                        {% if title === '' %}
                            {% set title = t._('no-title') %}
                        {% elseif not title %}
                            {% set title = t._('what-do-you-think') %}
                        {% endif %}
                        {% if planID and planID <= 1 %}
                            <h4 class="media-heading font-15 weight-bold light-blue">{{ title }}</h4>
                        {% else %}
                            {% set url =  '/argumentsbox/edit?url=' ~ argumentsbox['url'] | url_encode %} 
                            {% if argumentsbox['code'] is defined and argumentsbox['code'] %}
                                {% set url = url ~ '&uuid=' ~ argumentsbox['code'] %}
                            {% endif %}
                            <a 
                                href="{{ url }}"
                                class="media-heading font-15 weight-bold light-blue"
                            >
                                {{ title }}
                            </a>
                        {% endif %}

                        <p class="font-11">
                            <time datetime="{{ argumentsbox['created_at']  }}" class="color-898b8e">{{ utility.formatDate(argumentsbox['created_at']) }}</time>
                            <a href="#" data-target="#get_code_{{ argumentsbox['id'] }}" data-toggle="modal">{{ t._('code') }}</a>
                        </p>
                    </div>
                </div>
            </li>
            <div class="modal fade js-code-modal" id="get_code_{{ argumentsbox['id'] }}" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="raw_code_container">
                                <div><span>{{ t._('choz-cntnt-lang') }}:</span>
                                    <select id="arg-box-lang" name="arg-box-lang">
                                        <option value="en">English</option> 
                                        <option value="ar">اللغه العربيه</option>
                                    </select>
                                </div>
{% set code = '' %}
{% set code = code ~ '&lt;div class="speakol-arguments-box" ' %}
{% set code = code ~ 'data-app="' ~ appData.id ~ '" ' %}
{% set code = code ~ 'data-width="600" ' %}
{% set code = code ~ 'data-href="' ~ argumentsbox['url'] ~ '" ' %}
{% set code = code ~ 'data-option="' ~ argumentsbox['option_id'] ~ '" ' %}
{% if argumentsbox['code'] is defined and argumentsbox['code'] %}
    {% set code = code ~ 'data-uuid="' ~ argumentsbox['code'] ~ '" ' %}
{% endif %}
{% set code = code ~ 'data-lang="en" ' %}
{% set code = code ~ '&gt;&lt;/div&gt; ' %}
{% set code = code ~ '&lt;script&gt; ' %}
{% set code = code ~ '(function(doc, sc, id) { var js, sjs = document.getElementsByTagName(sc)[0]; if(doc.getElementById(id)) return; js = document.createElement(sc); ' %}
{% set code = code ~ 'js.src = "' ~ config.application.sdk_url  ~ 'js/sdk.js"; ' %}
{% set code = code ~ 'js.id=id; sjs.parentNode.insertBefore(js, sjs); })(document, "script", "speakol-sdk"); &lt;/script&gt;' %}
                                        <div class="speakol-debate-box">
                                            <pre id="gnrtd_code">{{ code }}<code></code></pre>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form_row">
                                        <!--<span class="alert-success"> Copied </span>-->
                                        <a href="javascript:" class="btn blue_btn cp-generated-code">{{ t._('copy-code') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {% endfor %}
        {% endif %}



    </ul>
</div>
