<?php
namespace Speakol\Routes;
class AppsRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'apps',));
        $this->setPrefix('/apps');
        $this->add('/create', array('action' => 'create'))->setName("create-app");
        $this->add('/login', array('action' => 'login'))->setName("login-app");
    }
}
