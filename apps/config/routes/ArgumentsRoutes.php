<?php
namespace Speakol\Routes;
class ArgumentsRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'arguments',));
        $this->setPrefix('/arguments');
        $this->add("/delete", array('action' => 'delete',))->setName("arguments-delete");
        return $this;
    }
}
