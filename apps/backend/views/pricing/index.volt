{% include "partials/main/sidebar.volt" %}
<div class="bg-f6f6f6 col-lg-9 col-md-8 col-sm-8 col-lg-offset-3 col-md-offset-4 col-sm-offset-4 height-all">
    {% include "partials/main/header.volt"  %}
    <div class="main pl30">
        {% include "partials/main/page-title" with [ 'title': t._('pricing') ]  %}
        <div class="row margin-top-sm">
            {% include 'partials/pricing/table.volt' %}
        </div>
    </div>
    {% include "partials/main/inner_footer.volt"  %}
</div>
