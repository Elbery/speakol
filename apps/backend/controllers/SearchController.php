<?php
namespace Speakol\Backend\Controllers;
class SearchController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->loadCustomTrans('main');
        $this->loadCustomTrans('debates');
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('arguments');
        $this->loadCustomTrans('replies');
        $this->loadCustomTrans('newsfeed');
    }
    public function indexAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $query = $this->request->get('q');
        if (!$query) {
            $this->flash->error($this->t->_('undefined-query'));
            return $this->response->redirect('newsfeed/index');
        }
        $this->view->communityPage = true;
        $type = $this->request->get('type');
        $page = $this->request->get('page');
        $lastCreatedDate = $this->request->get('last_date');
        $categoryId = $this->request->get('category');
        $type = $type ? $type : 'all';
        $page = $page ? $page : 1;
        $limit = 10;
        $params = array('type' => $type, 'page' => $page, 'search_key' => $query, 'limit' => $limit,);
        if ($page !== 1) {
            $params['last_date'] = $lastCreatedDate;
        }
        if (!$this->cookies->has(session_name()) || ($this->cookies->has(session_name()) && !$this->getToken() && !$this->getAppToken())) {
            $this->view->isLoggedIn = false;
        } else {
            $this->view->isLoggedIn = true;
        }
        if ($categoryId) {
            $params['category'] = $categoryId;
        }
        $response = $this->utility->iCurl('/feeds/search', 'GET', array('Authorization:xxxxxxxxxxx'), $params, true);
        $plugins = $response->data;
        $lastCreatedDate = $plugins[count($plugins) - 1]->created_at;
        $lastCreatedDate = str_replace(' ', '+', $lastCreatedDate);
        array_walk($plugins, array($this, 'formatSearchPlugins'), $query);
        $url = $this->request->get('_url');
        $nextUrl = $url;
        $url.= '?q=' . $query;
        $url.= '&type=' . $type;
        $url.= '&page=' . $page;
        $url.= '&last_date=' . $lastCreatedDate;
        $nextUrl.= '?q=' . $query;
        $nextUrl.= '&type=' . $type;
        $nextUrl.= '&page=' . $page + 1;
        $nextUrl.= '&last_date=' . $lastCreatedDate;
        if ($categoryId) {
            $url.= '&category=' . $categoryId;
            $nextUrl.= '&category=' . $categoryId;
        }
        $url.= '&lang=' . $this->view->lang;
        $nextUrl.= '&lang=' . $this->view->lang;
        $this->view->url = $url;
        $this->view->nextUrl = $nextUrl;
        $this->view->plugins = $plugins;
        $this->view->nextPage = $page + 1;
        $this->view->type = $type;
        $this->view->query = $query;
        $this->view->searchCategory = $categoryId;
        $this->view->paginate = false;
        if ($this->cookies->has(session_name())) {
            $hdrs = ($this->session->get('app_data')) ? $this->utility->getAuthorizationHeadersForApp() : $this->utility->getAuthorizationHeaders();
            $objUserProfile = $this->utility->iCurl('/user/current-user', 'get', $hdrs, null, false, false);
            if (is_null($objUserProfile) || $objUserProfile->status == 'ERROR') {
                $this->session->remove('user_data');
                $objUserProfile = false;
            } else {
                $this->view->isLoggedIn = true;
                $this->view->LoggedInUserData = $objUserProfile->data->user;
                if ($this->view->LoggedInUserData->pending && !$this->request->isAjax()) {
                    $this->flash->notice($this->t->_('confirm-email-2'));
                }
                $this->view->superAdmin = $this->view->LoggedInUserData->role_id === 2;
                $this->setLang($objUserProfile->data->user->locale);
                $favorites = $objUserProfile->data->favorites;
            }
        } else {
            $objUserProfile = false;
        }
        if ($this->request->isAjax()) {
            $this->view->highlightedPlugins = array();
            $this->view->paginate = true;
            $this->view->pick('partials/newsfeed/feed');
            return;
        }
        $favorties = array();
        $categoriesData = $this->formatCategories($favorites);
        $this->view->favoriteCategories = $categoriesData['favoriteCategories'];
        $this->view->normalCategories = $categoriesData['normalCategories'];
        $this->view->categories = array_merge($categoriesData['favoriteCategories'], $categoriesData['normalCategories']);
        $this->view->jsVars = array('headers' => '', 'weburl' => $this->config->application->webhost, 'lang' => $this->getLang(),);
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string)->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string)->addCss('css/fonts.css?' . $this->config->application->cache_string)->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string)->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/iframeMessenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery-ui.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/jquery.jscroll.min.js?' . $this->config->application->cache_string)->addJs('vendor/js/readmore.js?' . $this->config->application->cache_string)->addJs('js/messenger.js?' . $this->config->application->cache_string)->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string)->addJs('js/main.js?' . $this->config->application->cache_string)->addJs('js/voting_canvas_circle.js?' . $this->config->application->cache_string)->addJs('js/notifications.js?' . $this->config->application->cache_string)->addJs('js/community.js?' . $this->config->application->cache_string)->addJs('js/search.js?' . $this->config->application->cache_string);
        if ($this->view->superAdmin) {
            $this->assets->addJs('js/highlight.js?' . $this->config->application->cache_string);
        }
        $this->view->pick('search/index');
    }
    private function formatSearchPlugins(&$plugin, $key, $query) {
        if (!$query) {
            return;
        }
        if ($plugin->type === 'argumentboxes') {
            $plugin->articleContent = $this->utility->getArticleContent($plugin->data->url);
        }
        if ($plugin->type === 'debates') {
            foreach ($plugin->sides as $side) {
                $side->side_data->title = $this->highlightQuery($side->side_data->title, $query);
                $side->side_data->job_title = $this->highlightQuery($side->side_data->job_title, $query);
                $side->side_data->opinion = $this->highlightQuery($side->side_data->opinion, $query);
            }
        }
        if ($plugin->type === 'comparisons') {
            foreach ($plugin->sides as $side) {
                $side->side_data->title = $this->highlightQuery($side->side_data->title, $query);
                $side->side_data->description = $this->highlightQuery($side->side_data->description, $query);
            }
        }
        $plugin->data->title = $this->highlightQuery($plugin->data->title, $query);
    }
    private function highlightQuery($content, $query) {
        if (!$query) {
            return "";
        }
        return str_replace($query, '<em class="weight-bold">' . $query . '</em>', $content);
    }
}
