<div id="wrapper">

    <!-- Sidebar -->
    {% include "partials/main/sidebar.volt" %}

    <!-- Page content -->
    <div id="page-content-wrapper">

        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset row clearfix">
            <div class="col-lg-12">
                <div class="login_container">
                    <h3>You have unsubscribed from all notifications</h3>
                </div>
            </div>
        </div>
    </div>

</div>
