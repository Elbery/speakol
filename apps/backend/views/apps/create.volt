<div class="row helvetica-font">
    <div class="col-sm-5 bg-green white height-all fixed banner">
        <div class="row margin-top-lg margin-bottom-lg">
            <div class="col-sm-4 col-md-6 col-lg-7 margin-top-lg">
                <a href="/">
                    <img src="../../../public/img/speakol-icon-white.png" alt="Speakol" class="img-responsive">
                </a>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-5 margin-top-lg">
                <p class="font-13">Already registered? <a class="btn bg-green border-white ml5 font-13 white login-button" href="login">Login</a></p>
            </div>
        </div>
        <div class="row margin-top-lg margin-bottom-lg">
            <h3 class="font-20 pr8">
                Drive real-time reader engagement and involvement on your site through Speakol plugins:
            </h3>
        </div>
        <div class="row margin-top-lg text-center">
            <h4 class="col-sm-10 h2 margin-bottom-lg">Argument Box</h4>
            <img src="../../../public/img/sample-argbox.png" alt="Argument Box" class="img-responsive margin-top-lgi margin-bottom-sm">
            <p class="font-13 col-sm-10">
                Readers vote for or against a question or statement, and support their vote with an argument.
            </p>
        </div>

    </div>
    <div class="col-sm-7 col-sm-offset-5 height-all color-5bb767">
        <div class="row alerts-container">
        {% if cookies.has(sessionName())  %}
            {{ flash.output() }}
        {% endif %}
        </div>
        <h2 class="weight-bold pl30">{{ t._('app_new_accnt') }}</h2>
        <div class="js-register-form-container pl30">
            {{ form('action':'', 'enctype': 'multipart/form-data', 'id':'checkEmailForm', 'class': 'margin-top-lg col-sm-6') }}
                <div class="form-group">
                    <h4>{{ t._('what-email-address')  }}</h4>
                    {{ textField( {
                        "email", 
                        'size': 45, 
                        'placeholder': t._('email-addr'), 
                        'class': 'no-padding form-control no-shadow margin-top-lg green-focus', 
                        "data-validation-engine" :"validate[required, custom[email]]",
                        "data-errormessage-value-missing": t._('email-required'),
                        "data-errormessage-custom-error": t._('email-not-valid')
                    } ) }}
                </div>
                <div class="form-group">
                    <h4 {{ lang === "ar" ? 'class="rtl text-left"' : ''  }}>{{ t._('have-speakol-account')  }}</h4>
                    <div class="radio">
                        {{ radio_field('new_user', 'value': '1', 'id': 'new-user')  }}
                        <label for="new-user" class="medium-grey">{{ t._('new-user')  }}</label>
                    </div>
                    <div class="radio">
                        {{ radio_field('new_user', 'value': '0', 'id': 'old-user')  }}
                        <label for="old-user" class="medium-grey">{{ t._('old-user')  }}</label>
                    </div>
                </div>
                <div class="form-group">
                    {{ submitButton( {"Create APP", 'class': 'btn green-btn', 'value': t._('next')} ) }}
                </div>
            {{ endform() }}
        </div>
    </div>
</div>
