{#<!-- Button trigger modal -->
<button data-type="click" class="ajaxfiy-me btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>#}
<!-- Modal -->
<div class="modal fade" id="errorbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ t._('close') }}</span></button>
                <h4>{{ t._('error-title') }}</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger fade in" role="alert">
                    <p id="error-message"></p>
                </div>
                <div class="login_container">
                    <div class="input-group">
                        <label class="error-box alert hide"></label>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display:none">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ t._('close') }}</button>
            </div>
        </div>
    </div>
</div>

