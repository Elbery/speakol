<div class="row thick-underline">
    <h3 class="weight-bold font-20">{{ t._('latest-comments')  }}</h3>
</div>
{% if data['argument'] is defined and data['argument'] %}
<ul>
    {% for argument in data['argument'] %}
    <li class="js-argument {{ loop.last ? "" : "thin-underline"}}">
        <div class="row margin-top-lg">
            <div class="col-xs-11">
                <div class="media">
                    <div class="pull-left">
                        {% if argument['owner_photo']  %}
                        {{ image( argument['owner_photo'], "class": "media-object img-circle" ) }}
                        {% else %}
                        {{ image("img/user_default.gif", "class": "media-object img-circle" ) }}
                        {% endif %}
                    </div>
                    <div class="media-body">
                        {% if argument['url'] %}
                        <a href="{{ urldecode(argument['url']) }}" target="_blank">
                            <h4 class="font-15 media-heading light-blue weight-bold">{{ argument['owner_name'] }}</h4>
                        </a>
                        {% else %}
                            <h4 class="font-15 media-heading light-blue weight-bold">{{ argument['owner_name'] }}</h4>
                        {% endif %}
                        <p class="h6 {{ lang === 'ar' ? 'rtl text-left' : ''  }}">
                            {% set title = argument['plugin_title'] ? argument['plugin_title'] : argument['plugin_url']%}
                            <time><span>{{ argument['date'] }}</span></time>  
                            {% if argument['plugin_url'] %}
                            ,<a href="{{ urldecode(argument['plugin_url']) }}" target="_blank">
                                <span class="light-blue"> {{ t._('in')  }} {{ title }}</span>
                            </a>                        
                            {% else %}
                                ,<span class="light-blue"> {{ t._('in')  }} {{ title }}</span>
                            {% endif %}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-1 text-right">
                <button class="btn btn-default bg-white arg_delete no-radius border-none" data-argument-id="{{ argument['id'] }}"><i class="fa fa-lg fa-times red fa-lg padding-bottom-xs"></i></button>
            </div>
        </div>
        <div class="row margin-top-sm">
            <p class="h5">{{ argument['context']  }}</p>
                {% if argument['attached_photos'] %}
                    {% set extension = substr(argument['attached_photos'], -3)  %}
                    {% set file = argument['attached_photos'] %}
                    {% if extension === "ogg"  %}
                        <div class="js-player player">
                            <input type="hidden" value="{{ file  }}">
                            <button class="js-play-button play-button clearfix">
                                <span class="play-icon glyphicon glyphicon-play pull-left"></span>
                                <span class="play-text" data-text="{{ t._('click-play')  }}" data-text-small="{{ t._('click-play-small') }}"></span>
                            </button>
                        </div>
                    {% else %}
                        {{ image(file, "class":"") }}
                    {% endif %}
                {% endif %}
        </div>
    </li>
    {% endfor %}
</ul>
<div class="row">
    <a class="h5 btn btn-default bg-e8e8e8 width-all light-blue border-none no-radius weight-bold padding-top-lg padding-bottom-lg" href="{{ url('moderates/index') }}">{{ t._('show-more-comments') }}</a>
</div>
{% else %}
    <div class="row margin-top-lg">
        <h3>{{ t._('no-data') }}!</h3>
    </div>
{% endif %}
