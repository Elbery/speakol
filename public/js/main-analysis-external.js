/* avoid conflicts with other versions or libraries */
jQuery.noConflict();
/* reserve $ for JQuery */
(function($) {
    $(document).ready(function() {
        /* ------- my scripts ------- */

        // user notifications box Hover to open
        //=======================================
        $('.popup').hover(function() {
            $(this).addClass('active').find('.popup_container').show().closest('#page-content-wrapper').addClass('dimmed');
        }, function() {
            $(this).removeClass('active').find('.popup_container').hide().closest('#page-content-wrapper').removeClass('dimmed');
        });

        // Aside full page height
        var pageHeight = $(document).height();
        $('.outer_aside').height(pageHeight);
        $('.ht').height(pageHeight);
        $(window).resize(function() {
            $('.outer_aside').height(pageHeight);
            $('.ht').height(pageHeight);
        });
    });
}(jQuery));