<script>
    viewMore = '{{ hasMore['reply'] }}';
</script>

{% if not replies %}
    <div class="row">
        <h3>{{ t._('no-replies') }}!</h3>
    </div>
{% endif %}
<ul class="js-reply-items col-lg-11">
 {% include "partials/moderates/replies_items.volt"  %}
</ul>
