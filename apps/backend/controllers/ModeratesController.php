<?php
namespace Speakol\Backend\Controllers;
class ModeratesController extends BaseController {
    public function initialize() {
        parent::initialize();
        $this->currentUserIs(array('2', '3'), 'apps/login');
        $this->assets->addCss('vendor/css/bootstrap.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('vendor/css/font-awesome.min.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/fonts.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/main.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/mediaelementplayer.css?' . $this->config->application->cache_string);
        $this->assets->addCss('css/moderate.css?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/modernizr-2.6.2-respond-1.1.0.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/jquery-1.10.1.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/mediaelement-and-player.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/pagination.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/bootstrap3-typeahead.min.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/clipboard.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/audiorecorder.js?' . $this->config->application->cache_string);
        $this->assets->addJs('vendor/js/speexextensions.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/recorder.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/audioplayer.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/main.js?' . $this->config->application->cache_string);
        $this->assets->addJs('js/moderate.js?' . $this->config->application->cache_string);
        $this->loadCustomTrans('user');
        $this->loadCustomTrans('feed');
    }
    public function indexAction() {
        $this->tag->appendTitle(' Moderate');
        $hdrs = $this->utility->getAuthorizationHeadersForApp();
        $token = $this->getAppToken();
        $appId = $this->getAppData()->id;
        $postData = array('page' => json_encode($this->request->get('page')), 'per_page' => 10, 'type' => $this->request->get('type'));
        $appsAdmin = $this->utility->iCurl('/user/list-apps-admin/' . $appId, 'get', $hdrs, array(), true, true);
        $this->view->moderators = $appsAdmin['data'] ? $appsAdmin['data'] : array();
        $this->view->app = $this->getAppData();
        $moderateData = $this->utility->iCurl('/apps/moderate/' . $appId, 'get', $hdrs, $postData, false, true);
        try {
            if ($moderateData['status'] == 'OK') {
                $this->view->debates = $moderateData['data']['data']['debate'];
                $this->view->comparisons = $moderateData['data']['data']['comparison'];
                $this->view->argboxes = $moderateData['data']['data']['argumentsbox'];
                $this->view->arguments = $moderateData['data']['data']['argument'];
                $this->view->replies = $moderateData['data']['data']['reply'];
                $this->view->hasMore = $moderateData['data']['hasmore'];
                $moderateData['data']['hasmore']['all'] = false;
                foreach ($moderateData['data']['hasmore'] as $key => $row) {
                    if ($row) {
                        if (in_array($key, array('argument', 'reply', 'all'))) {
                            continue;
                        }
                        $moderateData['data']['hasmore']['all'] = true;
                        break;
                    }
                }
                $this->view->hasMore = $moderateData['data']['hasmore'];
                $this->view->ajax = false;
                if ($this->request->isAjax() == true) {
                    $this->view->ajax = true;
                    if ($postData['type'] == 'reply') {
                        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                        $this->view->pick('partials/moderates/replies_items');
                    } elseif ($postData['type'] == 'argument') {
                        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                        $this->view->pick('partials/moderates/arguments_items');
                    } else {
                        $this->response->setHeader("Content-Type", "application/json");
                        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                        $view = '';
                        $view = clone $this->view;
                        $view->start();
                        $view->render('partials/moderates', 'yourcontent_tabs');
                        $view->finish();
                        $content = $view->getContent();
                        if (!empty($postData['type'])) {
                            $postData['page'] = json_decode($postData['page'], 1);
                            if ($postData['type'] == 'all') {
                                foreach ($postData['page'] as $k => $a) {
                                    $postData['page'][$k]+= 1;
                                }
                            } else {
                                $postData['page'][$postData['type']]+= 1;
                            }
                        }
                        $this->response->setJsonContent(array('status' => 'OK', 'data' => $content, 'hasmore' => $this->view->hasMore, 'startVal' => $moderateData['data']['startValue'], 'type' => $postData['type'], 'page' => $postData['page'],));
                        $content = '';
                        return $this->response;
                    }
                }
            }
        }
        catch(Exception $ex) {
        }
    }
    public function addAction() {
        $app = $this->getAppData();
        if (!$app || !$this->request->isPost()) {
            return $this->response->redirect('apps/login');
        }
        $postData = array('email' => $this->request->get('email'), 'is_admin' => $this->request->get('is_admin', 'string', 0) ? 1 : 0);
        try {
            $hdrs = $this->utility->getAuthorizationHeadersForApp();
            $arrUser = $this->utility->iCurl('/user/link-admin', 'post', $hdrs, $postData, true, true);
            if ($arrUser['status'] == 'OK') {
                $this->flash->success($arrUser['data']);
            } else {
                throw new \Exception($arrUser['messages']);
            }
        }
        catch(\Exception $ex) {
            $this->flash->error($ex->getMessage());
        }
        $this->response->redirect('moderates/index');
        return $this->response;
    }
    public function removeAction($user_id) {
        $app = $this->getAppData();
        if (!$app || !$this->request->isDelete() || !$user_id) {
            return $this->response->redirect('apps/login');
        }
        try {
            $this->response->setHeader("Content-Type", "application/json");
            $hdrs = $this->utility->getAuthorizationHeadersForApp();
            $data = array('app_id' => $app->id);
            $response = $this->utility->iCurl('/user/remove-admin/' . $user_id, 'delete', $hdrs, $data, false);
            if (strtoupper($response->status) == 'OK') {
                $this->response->setStatusCode(200, "OK");
                $this->response->setJsonContent(array('status' => 'OK', 'data' => $response->data));
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->setJsonContent(array('status' => 'ERROR', 'messages' => $response->messages));
            }
        }
        catch(\Exception $ex) {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->setJsonContent(array('status' => 'ERROR', 'data' => $ex->getMessage()));
        }
        return $this->response;
    }
}
