<?php
use Phalcon\Mvc\Url as UrlResolver;
$di->set('router', function () {
    require __DIR__ . '/routes.php';
    return $router;
});
