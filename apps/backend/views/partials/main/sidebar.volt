{% set controller = this.dispatcher.getControllerName() %}
{% set action = this.dispatcher.getActionName() %}
{% if controller== 'static' or (controller=='apps' and action == 'index')  %}
{% include 'partials/main/sidebar_loggedout.volt'  %}
{% elseif not (cookies.has(sessionName()) and session.app_data) %}
{% include 'partials/main/sidebar_loggedout.volt'  %}
{% else  %}
{#  get current controller  #}
<div class="col-lg-3 col-md-4 col-sm-4  bg-medium-green fixed height-all">
    <div class="row bg-dark-green text-center padding-top-lg padding-bottom">
        <img src="/img/speakol-logo-white.png" alt="Speakol">
    </div>
    <div class="row">
        <nav class="col-sm-10 col-sm-offset-2 col-lg-9 col-lg-offset-3 no-padding margin-top-lg">
            <ul>
                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'dashboard' ? 'current' : '' }}" href="/dashboard">
                        <i class="speakol-icon speakol-dashboard {{ controller == 'dashboard' ? 'speakol-current' : '' }}"></i>&nbsp; {{ t._('dashboard') }}
                    </a>
                </li>
                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'debates' and action == 'code' ? 'current' : '' }} shepherd-debate" href="/debates/code">
                        <i class="speakol-icon speakol-add-debate  {{ controller == 'debates' and action == 'code' ? 'speakol-current' : '' }}"> </i> &nbsp; {{ t._('cret-debt') }}
                    </a>
                </li>

                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'comparisons' and action == 'code' ? 'current' : '' }} shepherd-comparison" href="/comparisons/code">
                        <i class="speakol-icon speakol-add-comparison  {{ controller == 'comparisons' ? 'speakol-current' : '' }}"> </i> &nbsp; {{ t._('cret-comprsn') }}
                    </a>
                </li>

                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'argumentsbox' and action == 'code' ? 'current' : '' }} shepherd-argument" href="/argumentsbox/code">
                        <i class="speakol-icon speakol-add-argumentsbox {{ controller == 'argumentsbox' and action == 'code' ? 'speakol-current' : '' }}"> </i> &nbsp; {{ t._('cret-arg') }}
                    </a>
                </li>

                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'moderates' or action == 'edit' ? 'current' : '' }}" href="/moderates">
                        <i class="speakol-icon speakol-moderates {{ controller == 'moderates' or action == 'edit' ? 'speakol-current' : '' }}"> </i> &nbsp;  {{ t._('moderate') }}
                    </a>
                </li>
                {# <li> #}
                {#     <a class="font-15 sidebar-link inline-block white white-hover {{ controller == 'analysis' ? 'current' : '' }}" href="/analysis"> #}
                {#         <i class="speakol-icon speakol-analysis {{ controller == 'analysis' ? 'speakol-current' : '' }}"> </i> &nbsp; {{ t._('analysis') }} #}
                {#     </a> #}
                {# </li> #}

                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ (controller == 'billing') ? 'current' : ''  }}" href="/billing">
                        <i class="speakol-icon speakol-billing {{ (controller == 'billing') ? 'speakol-current' : ''  }}"></i>&nbsp; {{ t._('billing-history') }}
                    </a>
                </li>
                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ (controller == 'pricing' and action == 'index') ? 'current' : ''  }}" href="/pricing">
                        <i class="speakol-icon speakol-pricing"></i>&nbsp; {{ t._('pricing-plan') }}
                    </a>
                </li>
                <li>
                    <a class="font-15 sidebar-link inline-block white white-hover {{ (controller == 'apps' and action == 'settings') ? 'current' : ''  }}" href="/apps/settings">
                        <i class="speakol-icon speakol-settings {{ (controller == 'apps' and action == 'settings') ? 'speakol-current' : ''  }}"></i>&nbsp; {{ t._('settings') }}
                    </a>
                </li>
                {% if admin_mail %}
                <li >
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller=='stats' ? 'current': '' }}" href="/stats">
                        <i class="speakol-icon speakol-settings {{ controller=='stats' ? 'speakol-current': '' }}"></i>&nbsp; {{ t._('stats') }}
                    </a>
                </li>
                <li >
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller=='ads' ? 'current': '' }}" href="/ads">
                        <i class="speakol-icon speakol-settings {{ controller=='ads' ? 'speakol-current': '' }}"></i>&nbsp; {{ t._('ads') }}
                    </a>
                </li>
                {% endif %}
                {% if session.get('app_data').user.role_id == 2%}
                <li >
                    <a class="font-15 sidebar-link inline-block white white-hover {{ controller=='admin' ? 'current': '' }}" href="/admin">
                        <i class="speakol-icon speakol-settings {{ controller=='admin' ? 'speakol-current': '' }}"></i>&nbsp; {{ t._('admin-settings') }}
                    </a>
                </li>
                {% endif %}
            </ul>
        </nav>
    </div>
</div>

{% endif  %}
