<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ title }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
        <link rel="icon" href="/img/favicon.ico">

        <!-- PASS VARIABLES FROM PHP TO JAVASCRIPT -->
        <script type="text/javascript">
            var phpVars = new Object();
            {% for key, value in jsVars %}
                phpVars.{{key}} = '{{value}}';
            {% endfor %}

                var t ={{ jsTranslation }}</script>

        {{ assets.outputCss() }}
        {{ assets.outputJs('header') }}
        <!--[if lt IE 9]>
        {{ assets.outputJs('header-ie9') }}
    <![endif]-->
    </head>
    <body class="{{ lang === 'ar' ? 'tahoma' : ''}}">
        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

{% if authorized %}
{% if debate.audio %}
    <input type="hidden" id="audioEnabled">
{% endif %}
        <input id="url" type="hidden" name="" value="{{ debate.url  }}">

        <div class="speakol-container container-fluid no-padding">
            <div class="sp-block-group menu-bar padding-top-xs padding-bottom-sm {{ newsfeed ? '' : 'dropdown-underline' }} width-90 margin-center">
        <div class="five-xs sp-block seven">
            {% if isLoggedIn and not newsfeed %}
            <div class="dropdown plugin-dropdown">
                <button class="padding-left-0 btn btn-default dropdown-toggle no-radius" type="button" id="userDropdown" data-toggle="dropdown" aria-expanded="true">
                    {% if LoggedInUserData and LoggedInUserData.profile_picture %}
                        {{ image(LoggedInUserData.profile_picture, 'class': 'radius-5 profile-picture-sm') }}
                    {% else %}
                        {{ image("img/user_default.gif", 'class': 'radius-5 profile-picture') }}
                    {% endif %}
                    <span class="{{ lang === 'ar' ? 'rtl inline-block' : '' }}">
                        <span class="hide-xxs">{{ t._('hello') }}, </span>{{ LoggedInUserData.name }}
                    </span>
                    <i class="fa fa-caret-down color-c1cacf"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius" role="menu" aria-labelledby="userDropdown">
                    {% if debate.app_id === LoggedInUserData.app.id and LoggedInUserData.app.plan_id > 1 %}
                        <li role="presentation" id=""><a role="menuitem" tabindex="-1" href="/debates/edit?slug={{ debate.slug }}" target="_blank">{{ t._('edit') }}</a></li>
                    {% endif %}
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ logout }}">{{ t._('logout') }}</a></li>
                </ul>
            </div>
            {% endif %}
            {% if newsfeed %}
                <div class="media">
                    <div class="pull-left">
                        <img src="{{ debate.app_image }}" class="publisher-image radius-5" alt="...">
                    </div>
                    <div class="media-body">
                        <h3 class="font-15 no-margin margin-bottom-xs">
                        <a href="/apps/profile/{{ debate.app_id }}" class="anchor-reset light-blue" target="_blank"><span class="weight-bold">{{ debate.app_name }}</span></a> {{ t._('posted-debate') }}</h3>
                        <p class="h6 color-777777">{{ utility.formatDate(debate.created_at, 'F jS, Y \a\t h:i A') }}</p>
                    </div>
                </div>
            {% endif %}
        </div>
        <div class="seven-xs sp-block five">
            {% if not newsfeed %}
            <div class="dropdown plugin-dropdown pull-right padding-top-sm">
                <button class="btn-reset margin-top-xs dropdown-toggle no-radius js-notifications-button" type="button" id="notificationDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-bell sp-icon-lg color-9ca7ad"></i>
                </button>
                {% if isLoggedIn %}
                <ul class="tip notification-plugin-tip border-none dropdown-menu right-0 left-auto no-shadow no-radius color-9ca7ad js-notifications notifications-dropdown bg-f0f0f0" role="menu" aria-labelledby="notificationDropdown">
                </ul>
                {% endif %}
            </div>
            {% if isLoggedIn %}
                <div class="hide js-notification-indicator notification-indicator pull-right radius-5 bg-bb1111 white padding-left-5 padding-right-5 weight-bold">0</div>
            {% endif %}
            {% endif %}
            <div class="dropdown plugin-dropdown pull-right {{  newsfeed ? '' : 'padding-top-sm'}}">
                <button class="btn btn-default dropdown-toggle no-radius" type="button" id="shareDropdown" data-toggle="dropdown" aria-expanded="true">
                    <i class="sp-icon sp-icon-lg sp-share"></i>
                </button>
                <ul class="dropdown-menu no-shadow right-0 left-auto no-radius color-9ca7ad" role="menu" aria-labelledby="shareDropdown">
                                <li role="prensentation"><a class="" href="https://www.facebook.com/sharer/sharer.php?u={{ url  }}" title="{{ t._('share-on-facebook') }}" target="_blank"><i class="fa fa-facebook"></i>&nbsp;{{ t._('share-on-facebook') }}</a></li>
                                <li role="prensentation"><a class="" href="https://twitter.com/home?status={{ url }}" title="{{ t._('tweet') }}" target="_blank"><i class="fa fa-twitter"></i>&nbsp;{{ t._('tweet') }}</a></li>
                                <li role="prensentation"><a class="" href="https://plus.google.com/share?url={{ url }}" title="{{ t._('share-on-google-plus') }}" target="_blank"><i class="fa fa-google-plus"></i>&nbsp;{{ t._('share-on-google-plus') }}</a></li>
                                <li role="prensentation"><a class="" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url }}" title="{{ t._('share-on-linkedin') }}" target="_blank"><i class="fa fa-linkedin"></i>&nbsp;{{ t._('share-on-linkedin') }}</a></li>
                </ul>
            </div>            
            <div class="dropdown plugin-dropdown pull-right {{  newsfeed ? '' : 'padding-top-sm'}}">
                <button class="btn btn-default dropdown-toggle no-radius js-current-sort" type="button" id="sortDropdown" data-toggle="dropdown" aria-expanded="true" data-value="most_voted">
                    <i class="sp-icon sp-sort sp-icon-lg color-9ca7ad"></i>
                </button>
                <ul class="dropdown-menu no-shadow no-radius right-0 left-auto" role="menu" aria-labelledby="sortDropdown">
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="most_voted"><i class="fa fa-2x fa-area-chart"></i>&nbsp;{{ t._('most-voted') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="desc"><i class="fa fa-2x fa-sort-amount-desc"></i>&nbsp;{{ t._('desc') }}</button>
                    </li>
                    <li role="presentation" class="margin-bottom-sm">
                        <button class="js-sort-button btn-reset color-9ca7ad" data-value="asc"><i class="fa fa-2x fa-sort-amount-asc"></i>&nbsp;{{ t._('asc') }}</button>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
    {%if newsfeed %}
        <div class="sp-block-group text-left width-90 margin-center">
            <h3 class="font-20 no-margin color-636363">
                <i class="speakol-icon speakol-debate-green margin-top-xs newsfeed-margin"></i>&nbsp;{{ debate.title }}
            </h3>
        </div>
    {% else %}
                <div class="sp-block-group margin-top-sm margin-bottom-lg padding-bottom-sm width-90 margin-center">
                    <h2 class="text-center weight-bold h3">{{ debate.title }}</h2>
                </div>
    {% endif %}
                <div class="sp-block-group margin-top-lg width-90 margin-center">
                    {% set sides = (lang === 'ar') ? [debate.sides[1], debate.sides[0]] : debate.sides %}
                    {% for side in sides %}
                        {% set meta = side.meta[0] %}
                        {% set right = false %}
                        {% if loop.last %}
                            {% set right = true %}
                        {% endif %}
                        <div class="sp-block six margin-bottom-sm {{ right ? 'padding-left-5' : 'padding-right-5' }}">
                            <div class="media">
                                <div class="{{ right ? 'pull-right text-right' : 'pull-left' }} debate-title-image">
                                    <img class="inline-block radius-5 border-dedede debate-plugin-img" src="{{( meta.image)}}" alt="">
                                </div>
                                <div class="media-body {{ right ? 'text-right' : '' }}">
                                    <h3 class="font-16 color-1362ad weight-bold no-margin media-heading margin-bottom-sm  {{ right ? 'text-right' : '' }}">{{ meta.title }}</h3>
                                    <p class="color-676767 h6 debate-readmore margin-top-xs hide-xxs {{ right ? 'text-right' : '' }}">{{ meta.job_title }}</p>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
                <div class="sp-block-group width-90 margin-center">
                    {% set sides = (lang === 'ar') ? [debate.sides[1], debate.sides[0]] : debate.sides %}
                    {% for side in sides %}
                        {% set meta = side.meta[0] %}
                        {% set right = false %}
                        {% if loop.last %}
                            {% set right = true %}
                        {% endif %}
                        <div class="sp-block six {{ right ? 'padding-left-5' : 'padding-right-5' }}">
                            <div class="sp-block-group bg-f9f9f9 relative tip tip-f9f9f9 {{ right ? 'text-right right-tip' : 'left-tip' }}">
                                <div class="margin-center width-90">
                                    <p class="debate-readmore-lg h5">{{ meta.opinion }}</p>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
                <div class="sp-block-group margin-top-lg margin-bottom-lg text-center">
                    <h3 class="weight-light font-20">{{ t._('support-text') }}</h3>
                </div>
                <div class="sp-block-group text-center margin-top-lg margin-bottom-lg padding-top-sm width-95 margin-center">
                    <div class="six sp-block {{ lang === 'ar' ? 'six-push' : '' }}">

            {% set side = debate.sides[0] %}
            {% if LoggedInUserData %}
            <div class="sp-block-group">
                <div class="{{  lang === 'ar' ? 'rtl text-right' : '' }} js-unvote-container sp-block-group width-90 bg-green white radius-5 margin-center margin-bottom-sm text-left {{ side.voted ? '' : 'hide' }}" data-side-id="{{ side.id }}">
                   <button class="js-unvote btn-reset white {{ lang === 'ar' ? 'green-vertical-separator-left' : 'green-vertical-separator-right'}} inline-block no-padding-height padding-bottom-xs margin-top-xs margin-bottom-xs" data-debate-id="{{ debate.id }}" data-side-id="{{ side.id }}">
                       <i class="fa fa-times"></i>
                   </button>
                    &nbsp;{{ t._('voted!') }}
                </div>
                <div class="sp-block-group js-argument-form transition-opacity {{ side.voted ? '' : 'hide opacity-0' }}" data-side-id="{{ side.id }}">
                    <div class="sp-block-group margin-bottom-sm">
                        {{ text_area("comment", "placeholder": t._('commentbox-placeholder'), "class":"no-resize width-90 h5 no-indent padding-5 " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "4") }}
                    </div>
                    <div class="sp-block-group text-right width-90 margin-center js-normal-submit">
                        {% if debate.audio %}
                        <button class="js-record-button {{ lang ==='ar' ? 'rtl' : '' }} no-padding padding-top-xs padding-bottom-xs shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm seven font-13 sp-block">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                <i class="fa fa-microphone fa-stack-1x white"></i>
                            </span>
                            <span class="hide-xxs hide-xs">{{ t._('audio-comment') }}</span>
                        </button>
                        {% else %}
                        <div class="{{ lang ==='ar' ? 'rtl' : '' }} four-xs four-sm seven font-13 sp-block">
                        </div>
                        {% endif %}
                        <div class="js-image-form">
                            <div class="sp-block  one-offset-xs one-offset two-xs three-sm two js-image-upload-container text-center">
                                <span class="fileUpload inline-block btn-reset margin-top-xs">
                                    <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                    {% if LoggedInUserData %}
                                        {% set classes = 'js-image-upload' %}
                                    {% else %}
                                        {% set classes = 'js-image-loggedout' %}
                                    {% endif %}
                                    {{ file_field(
                                        "attached",
                                        "class": classes,
                                        "accept":".png,.jpeg,.jpg,.gif"
                                    ) }}
                                </span>
                            </div>
                            <div class="sp-block two one-offset-xs two-xs three-sm js-image-container hide">
                                <button class="btn-reset js-delete-image delete-image absolute"><i class="fa fa-times"></i></button>
                                <img src="" alt="" class="js-image profile-picture-sm">
                            </div>
                        </div>
                        {{ hidden_field("debate_id", "value": debate.id) }}
                        <div class="sp-block two four-xs four-sm  {{ lang === 'ar' ? 'font-13' : '' }}">
                            {{ submit_button("post", "value": t._('sbmt-arg'), "class":"js-submit-arg btn-reset font-13 btn-default bg-4e4e4e width-all post-button") }}
                        </div>
                    </div>
                    {% if debate.audio %}
                    <div class="js-record-bar hide record-bar sp-block-group width-90 margin-center h5 padding-bottom-xs padding-top-xs">
                            <div class="sp-block seven twelve-xs timer white margin-top-xs text-left padding-left-5">
                                <i class="fa fa-microphone weight-bold"></i>
                                <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
                                <span class="hide-xxs hide-xs js-recording-text"> - {{ t._('recording') }} </span>
                            </div>
                            <div class="sp-block five twelve-xs record-buttons text-right padding-right-5">
                                <button class="js-send-record  six-xs send-record btn-reset radius-5 padding-top-xs padding-bottom-xs">{{ t._('send') }}</button>
                                <button class="js-cancel-record float-right-xs two-xs cancel-record btn-reset radius-5 padding-top-xs padding-bottom-xs">
                                        &#10005;
                                </button>
                            </div>
                    </div>
                    {% endif %}
                </div>
            </div>
            {% endif %}
                        <button class="btn btn-default width-90 vote-button bg-fafafa js-vote {{ side.voted ? 'hide' : '' }}" data-debate-id="{{ debate.id }}" data-side-id="{{ side.id }}">
                            <span class="{{ lang === 'ar' ? 'text-right' : 'text-left' }} sp-block margin-top-xs margin-bottom-xs color-4dac54 ">
                                {% if lang === 'ar' %}
                                <span class="text-right h5 sp-block nine-xs eight-sm eight two-offset-right-lg one-offset-right-xl">
                                        {% set side = debate.sides[0] %}
                                        {% set meta = side.meta[0] %}
                                        <span class="sp-block weight-light">
إدعم
                                        </span>
                                         <span class="sp-block weight-bold white-space-normal">
                                             {{ meta.title }}
                                         </span>
                                </span>
                                {% endif %}
                                <i class="speakol-icon speakol-agree speakol-icon-sm sp-block text-center three-xs sp-inline-xs margin-top-sm"></i>
                                <span class="sp-block four-sm two sp-hide-xs">
                                    <span class="fa-1-half-x fa-stack circle border-4dac54 bg-white">
                                        <i class="fa fa-circle white fa-stack-2x"></i>
                                        <i class="speakol-icon speakol-agree fa-stack-1x"></i>
                                    </span>
                                </span>
                                {% if lang !== 'ar' %}
                                <span class="h5 sp-block nine-xs eight-sm eight two-offset-lg one-offset-xl">
                                        {% set side = debate.sides[0] %}
                                        {% set meta = side.meta[0] %}
                                        <span class="sp-block weight-light">
                                            Support
                                        </span>
                                         <span class="sp-block support-title weight-bold white-space-normal">
                                             {{ meta.title }}
                                         </span>
                                </span>
                                {% endif %}
                            </span>
                        </button>
                    </div>
                    <div class="six sp-block {{ lang === 'ar' ? 'six-pull' : '' }}">

            {% set side = debate.sides[1] %}
            {% if LoggedInUserData %}
            <div class="sp-block-group">
                <div class="{{  lang === 'ar' ? 'rtl text-right' : '' }} js-unvote-container sp-block-group width-90 bg-d11b1f white radius-5 margin-center margin-bottom-sm text-left {{ side.voted ? '' : 'hide'}}" data-side-id="{{ side.id }}">
                   <button class="js-unvote btn-reset white {{ lang === 'ar' ? 'red-vertical-separator-left' : 'red-vertical-separator-right'}} inline-block no-padding-height padding-bottom-xs margin-top-xs margin-bottom-xs" data-debate-id="{{ debate.id }}" data-side-id="{{ side.id }}">
                       <i class="fa fa-times"></i>
                   </button>
                    &nbsp;{{ t._('voted!') }}
                </div>
                <div class="js-argument-form sp-block-group transition-opacity {{ side.voted ? '' : 'hide opacity-0' }}" data-side-id="{{ side.id }}">
                    <div class="sp-block-group margin-bottom-sm">
                        {{ text_area("comment", "placeholder": t._('commentbox-placeholder'), "class":"no-resize width-90 h5 no-indent padding-5 " ~ (lang === 'ar' ? 'rtl' : ''), "rows": "4") }}
                    </div>
                    <div class="sp-block-group text-right width-90 margin-center js-normal-submit">
                        {% if debate.audio %}
                        <button class="js-record-button {{ lang ==='ar' ? 'rtl' : '' }} no-padding padding-top-xs padding-bottom-xs shadow-none radius-5 bg-fdfdfd border-e7e7e7 four-xs four-sm seven font-13 sp-block">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x color-d11b1f"></i>
                                <i class="fa fa-microphone fa-stack-1x white"></i>
                            </span>
                            <span class="hide-xxs hide-xs">{{ t._('audio-comment') }}</span>
                        </button>
                        {% else %}
                        <div class="{{ lang ==='ar' ? 'rtl' : '' }} four-xs four-sm seven font-13 sp-block">
                        </div>
                        {% endif %}
                        <div class="js-image-form">
                            <div class="sp-block  one-offset-xs one-offset two-xs three-sm two js-image-upload-container text-center">
                                <span class="fileUpload inline-block btn-reset margin-top-xs">
                                    <i class="fa fa-picture-o fa-2x opacity-50 font-large-xs"></i>
                                    {% if LoggedInUserData %}
                                        {% set classes = 'js-image-upload' %}
                                    {% else %}
                                        {% set classes = 'js-image-loggedout' %}
                                    {% endif %}
                                    {{ file_field(
                                        "attached",
                                        "class": classes,
                                        "accept":".png,.jpeg,.jpg,.gif"
                                    ) }}
                                </span>
                            </div>
                            <div class="sp-block two one-offset-xs two-xs three-sm js-image-container hide">
                                <button class="btn-reset js-delete-image delete-image absolute"><i class="fa fa-times"></i></button>
                                <img src="" alt="" class="js-image profile-picture-sm">
                            </div>
                        </div>
                        {{ hidden_field("debate_id", "value": debate.id) }}
                        <div class="sp-block two four-xs four-sm  {{ lang === 'ar' ? 'font-13' : '' }}">
                            {{ submit_button("post", "value": t._('sbmt-arg'), "class":"js-submit-arg btn-reset font-13 btn-default bg-4e4e4e width-all post-button") }}
                        </div>
                    </div>
                    {% if debate.audio %}
                    <div class="js-record-bar hide record-bar sp-block-group width-90 margin-center h5 padding-bottom-xs padding-top-xs">
                            <div class="sp-block seven twelve-xs timer white margin-top-xs text-left padding-left-5">
                                <i class="fa fa-microphone weight-bold"></i>
                                <span class="js-timer weight-bold" data-finished="{{ t._('finished-recording') }}">00:00</span>
                                <span class="hide-xxs hide-xs js-recording-text"> - {{ t._('recording') }} </span>
                            </div>
                            <div class="sp-block five twelve-xs record-buttons text-right padding-right-5">
                                <button class="js-send-record  six-xs send-record btn-reset radius-5 padding-top-xs padding-bottom-xs">{{ t._('send') }}</button>
                                <button class="js-cancel-record float-right-xs two-xs cancel-record btn-reset radius-5 padding-top-xs padding-bottom-xs">
                                        &#10005;
                                </button>
                            </div>
                    </div>
                    {% endif %}
                </div>
            </div>
            {% endif %}
                        <button class="btn btn-default width-90 vote-button bg-fafafa js-vote {{ side.voted ? 'hide' : '' }}" data-debate-id="{{ debate.id }}" data-side-id="{{ side.id }}">
                            <span class="{{ lang === 'ar' ? 'text-right' : 'text-left' }} sp-block margin-top-xs margin-bottom-xs color-d11b1f">
                                {% if lang === 'ar' %}
                                <span class="h5 sp-block nine-xs eight-sm eight two-offset-right-lg one-offset-right-xl">
                                        {% set side = debate.sides[1] %}
                                        {% set meta = side.meta[0] %}
                                        <span class="sp-block weight-light">
إدعم
                                        </span>
                                         <span class="sp-block support-title weight-bold white-space-normal">
                                             {{ meta.title }}
                                         </span>

                                </span>
                                {% endif %}
                    <i class="speakol-icon speakol-disagree flip-z speakol-icon-sm sp-block text-center three-xs sp-inline-xs margin-top-sm"></i>
                    <span class="sp-block four-sm two sp-hide-xs">
                        <span class="fa-1-half-x fa-stack circle border-d11b1f bg-white">
                            <i class="fa fa-circle white fa-stack-2x"></i>
                            <i class="speakol-icon speakol-disagree flip-z fa-stack-1x"></i>
                        </span>
                    </span>
                                {% if lang !== 'ar' %}
                                <span class="h5 sp-block nine-xs eight-sm eight two-offset-lg one-offset-xl">
                                        {% set side = debate.sides[1] %}
                                        {% set meta = side.meta[0] %}
                                        <span class="sp-block weight-light">
                                            Support
                                        </span>
                                         <span class="sp-block support-title weight-bold white-space-normal">
                                             {{ meta.title }}
                                         </span>

                                </span>
                                {% endif %}
                            </span>
                        </button>
                    </div>
                </div>
    <div class="sp-block-group margin-top-lg margin-bottom-lg progress-container relative width-90 margin-center">
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? debate.sides[1] : debate.sides[0] %}
                {% set color = (lang === 'ar') ?  'color-d11b1f' : 'color-4dac54' %}
                {% set bg = (lang === 'ar') ?  'bg-d11b1f' : 'bg-4dac54' %}
                <div class="sp-block six text-left color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span> {{ t._('votes') }}
                </div>
                <div class="sp-block six text-right js-side-percentage side-percentage-left h4 {{ color }} padding-bottom-sm" data-side-id="{{ side.id }}">{{ side.perc ? side.perc : 0 }}%</div>
            </div>
            <div class="progress circular-radius flip vote-progress no-shadow no-border">
                <div class="progress-bar  circular-radius {{ bg }} js-progress-bar no-shadow no-border arg-progress-bar-left" style="width: {{ side.perc ? side.perc : 0 }}%" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : 0 }}%</span>
                </div>
            </div>
        </div>
        <div class="sp-block progress-circle text-center">
            <canvas class="naqeshny-canvas js-voting-circle" width="80" height="80" data-lang={{ lang }}>
                <div class="js-data data hide">
                    {
                    {% for side in debate.sides %}
                        
                        {% if loop.first %}
                        "pro-votes": "{{ side.votes}}",
                        "pro-color": "#4dac54",
                        {% else %}
                        "con-votes": "{{ side.votes }}",
                        "con-color": "#d11b1f"
                        {% endif %}
                    {% endfor %}
                    }
                </div>
            </canvas>
        </div>
        <div class="sp-block six arg-progress-bar">
            <div class="sp-block-group">
                {% set side = (lang === 'ar') ? debate.sides[0] : debate.sides[1] %}
                {% set color = (lang === 'ar') ?  'color-4dac54' : 'color-d11b1f' %}
                {% set bg = (lang === 'ar') ?  'bg-4dac54' : 'bg-d11b1f' %}
                <div class="sp-block six text-left js-side-percentage side-percentage-right h4 {{ color }} padding-bottom-sm" data-side-id="{{ side.id }}">{{ side.perc ? side.perc : 0 }}%</div>
                <div class="sp-block six text-right color-9fa9af h5 margin-top-xs hide-xxs {{ lang === 'ar' ? 'rtl' : '' }}">
                    <span class="js-side-votes" data-side-id="{{ side.id }}">{{ side.votes }}</span> {{ t._('votes') }}
                </div>
            </div>
            <div class="progress circular-radius vote-progress no-shadow no-border">
                <div class="progress-bar  circular-radius {{ bg }} no-shadow no-border js-progress-bar arg-progress-bar-right" style="width: {{side.perc ? side.perc : 0}}%" data-side-id="{{ side.id }}">
                    <span class="sr-only js-progress-sr">{{ side.perc ? side.perc : 0 }}%</span>
                </div>
            </div>
        </div>
    </div>
            {% include "partials/debates/sides" with ['debate':debate] %}
    {% include "partials/argumentsbox/byspeakol.volt" %}
                
       </div>

{% include "partials/main/errorbox.volt" %}
{% else %}
{% include "partials/argumentsbox/unauthorized.volt" %}
{% endif %}
        {{ assets.outputJs('footer') }}
        <script>window.jQuery || document.write('<script src="vendor/js/jquery-1.10.1.min.js"><\/script>')</script>

        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-56411923-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script>
            // voting circle canvas
            $(".naqeshny-canvas").each(function (index, obj) {
                $(obj).debateVersus();
            });
        </script>
    </body>
</html>
