<?php
namespace Speakol\Routes;
class DashboardRoutes extends \Phalcon\Mvc\Router\Group {
    public function initialize() {
        $this->setPaths(array('module' => 'backend', 'namespace' => 'Speakol\Backend\Controllers', 'controller' => 'dashboard',));
        $this->setPrefix('dashboard');
        $this->add('index', array('action' => 'index'))->setName("dashboard-home");
    }
}
