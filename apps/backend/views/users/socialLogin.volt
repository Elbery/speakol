<div class="row bg-green white login-header margin-bottom">
    <div class="col-xs-10">
    </div>
    <div class="col-xs-2">
        <button class="no-border bg-transparent white login-close js-login-close inline-block width-all"><i class="fa fa-close fa-2x"></i></button>
    </div>
</div>
{% if not refresh %}
<div class="row">
{% if cookies.has(sessionName())  %}
    {{ flash.output() }}
{% endif %}
</div>
{% endif %}
{% if refresh %}
<div class="row text-center">
<img src="/img/speakol-logo-slogan.png" alt="">
  </div>
{% endif %}
<script type="text/javascript" src="{{ this.config.application.static_js ~ '/vendor/js/jquery.min.js'}}"></script>
<script type="text/javascript">
{% if refresh %}
    var lastAction = localStorage.getItem('lastAction');
if(!!lastAction) {
    lastAction = JSON.parse(lastAction);
    console.log(lastAction['postDataObject']);
    if(lastAction['postDataObject']) {
        var postDataObject = lastAction['postDataObject'],
            formData = new FormData();
        for( key in postDataObject ) {
            if( postDataObject.hasOwnProperty(key) ) {
                if(key === 'file') {
                    var file = dataURItoFile(postDataObject['file']);
                    formData.append('file', file.data, file.name);
                } else {
                    formData.append(key, postDataObject[key]);
                }
            }
        }
        lastAction['data'] = formData;
    }
    console.log(lastAction);
    $.ajax(lastAction).then(function(res) {

        var createAction = !!lastAction.createAction;
        if(createAction) {
            return $.ajax({
                url: lastAction.url,
                type: 'GET',
                dataType: 'json',
                contentType: false,
                data: {
                    action: '/argument-box/show/url',
                    url: lastAction.argumentsboxURL,
                    lang: lastAction.lang,
                    "app_id": lastAction.postDataObject.app_id
                }
        }).then(function(res) {
            var data = res.data['0'];
            var lang = lastAction.lang;
            var sideId= 0;
            var argumentId = data.id;
            var direction = lastAction.direction;  
            if(direction === 'left') {
                sideId = data.sides['0'].id;
            } 

            if(direction === 'right') {
                sideId = data.sides['1'].id;
            }

            return $.ajax({
                url: lastAction.url,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: '/argumentsbox/vote/' + argumentId + '/' + sideId,
                    lang: lang
                }
            }); 
        });
        } else {
            return $.Deferred().resolve().promise();
        }
    }).done(function(res) {
        localStorage.removeItem('lastAction');
        sendReloadMessage();
    }).fail(function(jqXHR) {
        sendReloadMessage();
    });
} else {
    sendReloadMessage();
}
function dataURItoFile(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    //var ia = new Uint8Array(byteString.length);
    var ia = [];
    for (var i = 0; i < byteString.length; i++) {
        //ia[i] = byteString.charCodeAt(i);
        ia.push(byteString.charCodeAt(i));
    }

    var blob =  new Blob([new Uint8Array(ia)], {type:mimeString});
    var extension = mimeString.split('/')[1];
    return { data: blob, name: 'image.' + extension};
}
function sendReloadMessage() {
    if(window.opener && window.opener.location){
        window.opener.postMessage(JSON.stringify({
            'id': 'iframeMessenger:asdjas',
            'type': 'reload-plugins'
        }), '*');
        window.close();
    } else if(window.parent) {
            window.parent.postMessage(JSON.stringify({
                'id': 'iframeMessenger:dfjdkfjd',
                'type': 'close-popup',
                'reload': true
            }), "*");
    }
}

{% endif %}
</script>
