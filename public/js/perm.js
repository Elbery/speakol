   <script>
        (function() {
            window.addEventListener('message', function(e) {
                var speakolFrame = document.getElementById('speakol-iframe'); 
                if(e.source !== speakolFrame.contentWindow) {
                    return;
                }
                var message = JSON.parse(e.data);
                if(message.id.indexOf("iframeMessenger") > -1 && message.type === "set-height") {
                    speakolFrame.height = message.value;
                }
            });
        })();
    </script>
