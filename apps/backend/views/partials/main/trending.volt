<section class="trending ">
    <h2 class="Section_title">{{ t._('trending') }}</h2>
    <a class="side_link" href="#"> {{ t._('view-all') }}</a>

    <ul class="vertical_list clearfix">
        <li>
            <a href="#">

                {{ image("img/Subject1.jpg") }}
                <div class="black_gradient">
                    <span>Bill Gates</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="black_gradient">
                    <span>Mark Zuckerberg</span>
                </div>
                {{ image("img/Subject2.jpg") }}
            </a>
        </li>
        <li>
            <a href="#">
                <div class="black_gradient">
                    <span>Web Summit</span>
                </div>
                {{ image("img/Subject3.jpg") }}

            </a>
        </li>
        <li>
            <a href="#">
                {{ image("img/Subject1.jpg") }}
                <div class="black_gradient">
                    <span>Bill Gates</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">

                {{ image("img/Subject1.jpg") }}
                <div class="black_gradient">
                    <span>Bill Gates</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="black_gradient">
                    <span>Mark Zuckerberg</span>
                </div>
                {{ image("img/Subject2.jpg") }}
            </a>
        </li>
        <li>
            <a href="#">
                <div class="black_gradient">
                    <span>Web Summit</span>
                </div>
                {{ image("img/Subject3.jpg") }}

            </a>
        </li>
        <li>
            <a href="#">
                {{ image("img/Subject1.jpg") }}
                <div class="black_gradient">
                    <span>Bill Gates</span>
                </div>
            </a>
        </li>
    </ul>
</section>