<!DOCTYPE html>
<html lang="{{ lang  }}">
    <head>
        <meta charset="UTF-8">
        {{  tag.getTitle() }}
        {{ assets.outputCss() }}
        <meta property="og:image" content="http://plugins.speakol.com/img/logo.png"/>
        <meta property="og:title" content="Speakol"/>
        <meta property="og:site_name" content="Speakol"/>
		<meta property="og:description" content="{{ t._('og-desc')  }}"/>
        <link rel="icon" href="/img/favicon.ico">
    </head>
    {% if cookies.has(sessionName()) and session.get('app_data')  %}
    <body class="bg-f6f6f6 clearfix">
    {% else %}
    <body class="clearfix">
    {% endif  %}
        {{ this.getContent() }}
        <!--  footer JS -->
        {{ assets.outputJs() }}
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-56411923-1', 'auto');
            ga('send', 'pageview');
        </script>
{% set controller = this.dispatcher.getControllerName()  %}
{% if config.application.webhost === 'http://plugins.speakol.com/' and ( controller === 'static' or controller === 'index' or controller === 'users' ) %}
    <script src="/vendor/js/cookieControl-6.2.min.js?486931928475" type="text/javascript"></script>
  <script type="text/javascript">//<![CDATA[
      cookieControl({
          t: {
              title: '<p>This site uses cookies to store information on your computer.</p>',
              intro: '<p>Some of these cookies are essential to make our site work and others help us to improve by giving us some insight into how the site is being used.</p>',
              full:'<p>These cookies are set when you submit a form, login or interact with the site by doing something that goes beyond clicking some simple links.</p><p>We also use some non-essential cookies to anonymously track visitors or enhance your experience of this site. If you\'re not happy with this, we won\'t set these cookies but some nice features on the site may be unavailable.</p><p>To control third party cookies, you can also <a class="ccc-settings" href="browser-settings" target="_blank">adjust your browser settings.</a></p><p>By using our site you accept the terms of our <a href="http://plugins.speakol.com/privacy">Privacy Policy</a>.</p>'
          },
          position:CookieControl.POS_LEFT,
          style:CookieControl.STYLE_SQUARE,
          theme:CookieControl.THEME_DARK, // light or dark
          startOpen:true,
          autoHide:7000,
          subdomains:true,
          protectedCookies: [], // list the cookies you do not want deleted, for example ['analytics', 'twitter']
          apiKey: '9539a5503c87191f905bd37d0183ed93526607d7',
          product: CookieControl.PROD_FREE,
          consentModel: CookieControl.MODEL_IMPLICIT,
          onAccept:function(){ccAddAnalytics()},
          onReady:function(){},
          onCookiesAllowed:function(){ccAddAnalytics()},
          onCookiesNotAllowed:function(){}
          });

          function ccAddAnalytics() {
            jQuery.getScript("//www.google-analytics.com/ga.js", function() {
              var GATracker = _gat._createTracker('UA-56411923-1');
              GATracker._trackPageview();
            });
          }
       //]]>
    </script>
{% endif %}
    
    </body>

</html>
