{{ form('apps/create', 'enctype': 'multipart/form-data', 'class': 'form-horizontal margin-top-lg col-sm-6 color-666666 reset-align') }}
    <input type="hidden" name="email" value="{{ request.getPost("email")  }}">
    <div class="form-group margin-bottom-sm js-upload-container">
        <div class="col-sm-3">
            {{ image( "img/image-logo.png" , "alt": "Speakol", "title": "Speakol", "class": "profile-picture-big img-circle js-preview") }}
        </div>
        <div class="col-sm-6 margin-top-sm">
            <div class="width-all fileUpload btn btn-default bg-white padding-bottom-sm color-5bb767 font-15">
                <span>{{ t._('upload-logo') }}</span>
                {{ hiddenField({"logo", 'class' :'js-picture-value'})  }}
                <input class="js-upload-button" type="file" name="" size="45" placeholder="{{ t._('app-logo')  }}">
            </div>
        </div>
    </div>
    <div class="form-group margin-bottom-sm margin-top-sm">
        {{ textField( {
        "name", 
        'size': 45, 
        'placeholder': t._('app-name'), 
        'class': 'no-padding form-control no-shadow green-focus', 
        "data-validation-engine": "validate[required, minSize[2], maxSize[64]]",
        "data-errormessage-value-missing": t._('name-required'),
        "data-errormessage-range-underflow": t._('name-min-length'),
        "data-errormessage-range-overflow": t._('name-max-length')
        } ) }}
    </div>
    <div class="form-group margin-bottom-sm">
        {{ textField( {
        "website", 
        'size': 45, 
        'placeholder': t._('website-URL'), 
        'class': 'no-padding form-control no-shadow green-focus', 
        "data-validation-engine" :"validate[required, minSize[8], maxSize[255]]",
        "data-errormessage-value-missing": t._('website-required'),
        "data-errormessage-range-underflow": t._('website-min-length'),
        "data-errormessage-range-overflow": t._('website-max-length')
        } ) }}
    </div>
    {% if state == 3 or state == 4  %}
        <div class="form-group margin-bottom-sm">
            {{ passwordField( {
            "password", 
            'size': 45, 
            'placeholder': t._('passwd'), 
            'class': 'no-padding form-control no-shadow green-focus', 
            "autocomplete": "off", 
            "data-validation-engine" :"validate[required , minSize[8]]",
            "data-errormessage-value-missing": t._('password-required'),
            "data-errormessage-range-underflow": t._('password-min-length')
            } ) }}
        </div>
        <div class="form-group margin-bottom-sm">
            {{ passwordField( {
            "confirm_password", 
            'size': 45, 
            'placeholder': t._('confirm-passwd'), 
            'class': 'no-padding form-control no-shadow green-focus', 
            "autocomplete": "off", 
            "data-validation-engine" :"validate[equals[password]]",
            "data-errormessage-pattern-mismatch": t._('password-not-match')
            } ) }}
        </div>
    {% endif %}
    <div class="form-group margin-bottom-sm">
            {{ select("category", categories, 'using': ['id', 'name'], 'class': 'form-control no-shadow no-padding-height green-focus width-78', "data-validation-engine" :"validate[required]", "data-errormessage-value-missing": t._('category-required')) }}
    </div>
    <div class="form-group margin-bottom-sm">
            {{ select("country", countries, 'using': ['id', 'name'], 'class': 'form-control no-shadow no-padding-height green-focus width-78', "data-validation-engine" :"validate[required]", "data-errormessage-value-missing": t._('country-required'))  }}
    </div>
    <div class="form-group margin-bottom-sm">
       <select id="plan" class="form-control no-shadow no-padding-height green-focus width-78" name="plan">
            <option value="">Plan</option>
           {% set plans = ['Free', 'Premium ($50/mo)', 'Premium Plus ($100/mo)', 'Corporate ($300/mo)'] %}
           {% for plan in plans %}
                <option value="{{ loop.index }}">{{ plan }}</option>
           {% endfor %}
       </select>
    </div>
    <div class="js-payments-form-container margin-top-lg padding-top-sm">
        <h4 class="color-5bb767 h2">Payment method</h4>
        <div class="form-group margin-bottom-sm margin-top-lg">
                <select id="method" name="card_method" class="form-control no-padding-height no-shadow color-666666 green-focus width-78">
                    <option value="">Credit Card</option>
                    <option value="Visa">Visa</option>
                    <option value="MasterCard">MasterCard</option>
                    <option value="American Express">American Express</option>
                    <option value="Discover">Discover</option>
                    <option value="Diners Club">Diners Club</option>
                    <option value="JCB">JCB</option>
                </select>
        </div>
        <div class="form-group margin-bottom-sm">
            {{ textField({"card_name", "class": "no-padding form-control no-shadow green-focus", "data-validation-engine" :"validate[required]", "placeholder": t._('card-name') }) }}
        </div>
        <div class="form-group margin-bottom-sm">
            <input id="cc-number" name="card_number" class="no-padding form-control cc-number green-focus no-shadow" type="tel" placeholder="Card number" data-validation-engine="validate[required, funcCall[validCardNumber], funcCall[validCardType]]">
        </div>
        <div class="form-group margin-bottom-sm">
            <div class="col-sm-6">
                <label for="cc-number" class="h5">Expire date</label>
                <div class="row margin-top-sm">
                    <div class="col-sm-7">
                        <select id="month" name="card_month" class="form-control no-padding green-focus no-shadow">
                            <option value="">Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select id="year" name="card_year" class="form-control no-padding green-focus no-shadow">
                            <option value="">Year</option>
                            {% for year in currentYear..currentYear+10 %}
                            <option value="{{ year }}">{{ year }}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <label for="cc-cvc" class="h5">Security code</label>
                <div class="margin-top-sm">
                    <input id="cc-cvc" name="card_code" class="no-padding form-control  font-15 cc-cvc green-focus no-shadow width-82" type="tel" placeholder="Code" autocomplete="off" data-validation-engine="validate[required]">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group margin-bottom-sm margin-top-sm padding-top-sm">
        <div class="g-recaptcha" data-sitekey="{{ publicKey  }}"></div>
    </div>
    <div class="form-group margin-bottom-sm">
        {{ submitButton( {"Create APP", 'class': 'btn green-btn', 'value': t._('create-account')} ) }}
    </div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
