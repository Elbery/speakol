(function() {
    window.addEventListener('message', function(e) {
        var speakolFrame = document.getElementById('speakol-iframe');
        var message = JSON.parse(e.data);
        if (message.id.indexOf("iframeMessenger") > -1) {
            if (message.type === "close-popup") {
                speakolFrame.contentWindow.postMessage(e.data, '*');
            }
        }
    });
})();
