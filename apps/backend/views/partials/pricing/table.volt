    <table class="bg-white font-16 text-center pricing-table {{ lang === 'ar' ? 'rtl' : ''}}">
        <tr>
            <td class="bg-f7f7f7"></td>
            {% for index in 1..4 %}
                {% if loggedin and index == currentPlan %}
                    <td class="bg-d4d4d4 color-6e767e uppercase h5 current-plan">{{ t._('current-plan') }}</td>
                {% elseif index === 3%}
                    <td class="bg-677079 white uppercase h5 most-popular">{{ t._('most-popular') }}</td>
                {% else %}
                    <td class="bg-f7f7f7"></td>
                {% endif %}
            {% endfor %}
            {# <td class="bg-f7f7f7"></td> #}
            {# <td class="bg-f7f7f7"></td> #}
            {# <td class="bg-f7f7f7"></td> #}
            {# <td class="bg-677079 white uppercase h5 most-popular">{{ t._('most-popular') }}</td> #}
            {# <td class="bg-f7f7f7"></td> #}
        </tr>
        <tr class="white bg-78be79 font-20">
            <th class="{{ lang === 'ar' ? 'text-right': 'text-left'}} col-sm-4">
                {{ t._('features') }}
            </th>
            <th class="text-center {{ lang === 'ar' ? 'ltr' : '' }}">
                <span class="block">{{ t._('free-plan') }}</span>
                <span class="h5">{{ t._('small-publishers') }}</span>
            </th>
            <th class="text-center {{ lang === 'ar' ? 'ltr' : '' }}">
                <span class="block">{{ t._('premium') }}</span>
                <span class="h5">{{ t._('small-publishers') }}</span>
            </th>
            <th class="text-center {{ lang === 'ar' ? 'ltr' : '' }}">
                <span class="block">{{ t._('premium') }}<sup>+</sup></span>
                <span class="h5">{{ t._('medium-publishers') }}</span>
            </th>
            <th class="text-center {{ lang === 'ar' ? 'ltr' : '' }}">
                <span class="block">{{ t._('corporate') }}</span>
                <span class="h5">{{ t._('large-publishers') }}</span>
            </th>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('publisher-admin-account') }}</td>
            <td class="color-5bb767">1</td>
            <td class="color-5bb767">2</td>
            <td class="color-5bb767">5</td>
            <td class="color-5bb767">{{ t._('unlimited') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('plugins-creation') }}</td>
            <td class="color-5bb767">6</td>
            <td class="color-5bb767">30</td>
            <td class="color-5bb767">70</td>
            <td class="color-5bb767">{{ t._('unlimited') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">
                {{ t._('customize-color') }}
                {% if not loggedin %}
                <span class="feature-info circle inline-block text-center" data-toggle="tooltip" title="{{ t._('number-users') }}" data-placement="{{ lang === 'ar' ? 'left' : 'right' }}">?</span>
                {% endif %}
            </td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('audio-commenting') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('access-stats') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('customize-options') }}<p class="h6">{{ t._('agree-disagree') }}</p></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">
                {{ t._('edit-plugins') }}
                {% if not loggedin %}
                <span class="feature-info circle inline-block text-center" data-toggle="tooltip" title="{{ t._('change-all-settings') }}" data-placement="{{ lang === 'ar' ? 'left' : 'right' }}">?</span>
                {% endif %}
            </td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
            <td><i class="fa fa-check color-5bb767"></i></td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('moderate-review') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('remove-logo') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('customize-plugins') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('custom-url') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('customize-login') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('discount-speakol') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="{{ lang === 'ar' ? 'text-right': 'text-left'}}">{{ t._('monthly-report') }}</td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td><i class="fa fa-times color-fabbbd"></i></td>
            <td class="color-5bb767">{{ t._('coming-soon') }}</td>
        </tr>
        <tr>
            <td class="bg-f7f7f7"></td>
            <td><h3 class="font-35">{{ t._('free') }}</h3></td>
            <td><h3 class="font-35">$50 </h3> <p class="h6">{{ t._('billed-monthly') }}</p></td>
            <td><h3 class="font-35">$100</h3> <p class="h6">{{ t._('billed-monthly') }}</p></td>
            <td><h3 class="font-35">$300</h3> <p class="h6">{{ t._('billed-monthly') }}</p></td>
        </tr>
        <tr>
            <td class="bg-f7f7f7"></td>
            {% set plans = [1,2,3,4] %}
            {% for plan in plans %}
            {% if loggedin %}
                <td>
                    {% if plan < currentPlan %}
                    <a class="btn font-22 padding-bottom-sm" href="/pricing/downgrade/{{ plan }}">{{ t._('downgrade') }}</a>
                    {% elseif plan > currentPlan %}
                    <a class="btn font-22 padding-bottom-sm" href="/pricing/upgrade/{{ plan }}">{{ t._('upgrade') }}</a>
                    {% else %}
                        <span class="btn font-22 padding-bottom-sm current-plan disabled">{{ t._('current-plan') }}</span>
                    {% endif %}
                </td>
            {% else %}
                <td>
                    <a class="btn btn-default font-22 login-button bg-green white" href="apps/create">{{ t._('sign-up') }}</a>
                </td>
            {% endif %}
            {% endfor %}
        </tr>

</table>
